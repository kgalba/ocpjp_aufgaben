/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.files;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;
import kgalba.utils.Common;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author werkstatt
 */
public class FileCounterTest {
    private static File workingDir;
    
    public FileCounterTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        workingDir = new File(System.getProperty("java.io.tmpdir") + Common.getClassName() + new Date().getTime());
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
        workingDir.deleteOnExit();
    }

    @Test
    public void testCount() throws IOException {
        //GIVEN the extension to search for
        //AND files in workingDir
        String[] extensions = new String[] {"ext"};
        FileCreator.createFiles(workingDir, this.getClass().getSimpleName(), extensions[0], 13);
        
        //WHEN
        int count = new FileCounter(workingDir).count(extensions[0]);
        
        //THEN        
        assertEquals(FileUtils.listFiles(workingDir, extensions, false).size(), count);
    }   
    
    @Test
    public void testCountDeep() throws IOException {
        //GIVEN the user.dir for class files
        String[] extensions = new String[] {"class"};
        File testDir = new File(System.getProperty("user.dir"));

        //System.out.println("workingDir: " +workingDir.getAbsolutePath());
        //WHEN
        int count = new FileCounter(testDir).countDeep(extensions[0]);
        
        //THEN        
        assertEquals(FileUtils.listFiles(testDir, extensions, true).size(), count);
    } 
}
