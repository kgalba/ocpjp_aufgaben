package kgalba.files;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.Date;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;

/**
 *
 * @author kgalba
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class FileCreatorTest {

    static File workingDir;

    private static void createWorkingDir() {
        String tempDir = System.getProperty("java.io.tmpdir");
        String uniqueID = FileCreatorTest.class.getCanonicalName() + new Date().getTime();
        String pathName = tempDir + uniqueID;
        workingDir = new File(pathName);
        workingDir.mkdir();
    }

    private static void deleteWorkingDir() throws IOException {
        File[] files = workingDir.listFiles();
        for (File file : files) {
            file.deleteOnExit();
        }
        workingDir.deleteOnExit();
    }

    File[] files;
    String prefix;
    String[] extensions;
    int count;
    FileFilter fileFilter;

    public FileCreatorTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        createWorkingDir();
    }

    @AfterClass
    public static void tearDownClass() throws IOException {
        //deleteWorkingDir();
    }

    @Before
    public void setUp() {

        prefix = "prefix";
        extensions = new String[]{"ext"};
        count = 10;
        files = new File[count];
        fileFilter = (path) -> {
            if (path.isDirectory()) {
                return false;
            }
            return path.getName().toLowerCase().startsWith(prefix) && path.getName().toLowerCase().endsWith(extensions[0]);
        };
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of createFiles method, of class FileCreator.
     */
    @Test
    public void testCreateFiles() throws IOException {
        //GIVEN a empty workingDir
        assertEquals("workingDir must be empty !", 0, workingDir.list().length);

        //WHEN
        File[] createFiles = FileCreator.createFiles(workingDir, prefix, extensions[0], count);
        //THEN
        assertEquals(count, createFiles.length);
        for (File createFile : createFiles) {
            assertTrue(createFile.getCanonicalPath() + " has to exist", createFile.exists());
        }
    }

    /**
     * Test of deleteFiles method, of class FileCreator.
     */
    @Test
    public void testDeleteFiles() {
        //GIVEN workingDir with files
        assertTrue("Assume workingDir contains files from testCreateFiles()", workingDir.listFiles(fileFilter).length >0);

        //WHEN
        File[] deleted = FileCreator.deleteFiles(workingDir, prefix, extensions[0]);
        
        //THEN no files anymore
        assertEquals(
                "There must be no files like: '" + prefix + "*." + extensions[0] + "' in dir: " + workingDir,
                0, workingDir.listFiles(fileFilter).length
        );
    }
}
