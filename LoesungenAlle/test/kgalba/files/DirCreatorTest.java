package kgalba.files;

import java.io.File;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author werkstatt
 * @todo cleanup after, depends ?!
 */
public class DirCreatorTest {
    
    public DirCreatorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of createDirectories method, of class Directorys.
     */
    @Test
    public void testCreateDirectorys() {
        //GIVEN a path with dirs to create
        String path = "a/b/c";
        path.replaceAll("/", File.pathSeparator);
        
        //WHEN create
        assertTrue(Directorys.createDirectories(path));
        
        //THEN directorys from path exists
        File dirs = new File(path);
        assertTrue(dirs.isDirectory());
    }
    
        /**
     * Test of createDirectories method, of class Directorys.
     */
    @Test
    public void testDeleteDirectorys() {
        //GIVEN a path with dirs to delete
        String path = "a/b/c";
        path.replaceAll("/", File.pathSeparator);
        
        //WHEN delete
        assertTrue(Directorys.deleteDirectories(path));
        
        //THEN directorys from path is gone
        File dirs = new File(path);
        assertFalse(dirs.isDirectory());
    }
}
