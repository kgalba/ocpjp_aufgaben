package kgalba.files;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.startsWith;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.Assume.assumeTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author kgalba
 */
public class ListDrivesTest {    
    ListDrives ld;
    static String os = System.getProperty("os.name").toLowerCase();

    public ListDrivesTest() {

    }

    @BeforeClass
    public static void setUpClass() {
        assumeTrue("Test works only on Windows !", os.startsWith("windows", 0));
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        ld = new ListDrives();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testPrintForTableHeader() {
        //header
        assertThat(ListDrives.print(), startsWith("|"));
        assertThat(ListDrives.print(), containsString("LW"));
        assertThat(ListDrives.print(), containsString("Frei(MB)"));
        assertThat(ListDrives.print(), containsString("Belegt(MB)"));
        assertThat(ListDrives.print(), containsString("Gesamt(MB)"));
        assertThat(ListDrives.print(), containsString("|"));
    }

    @Test
    /**
     * test for correct row/col format
     */
    public void testPrintForFormat() throws IOException {
        //GIVEN a used space on drive and its string length
        int expectedColWidth = String.valueOf(getFirstDriveUsedSpaceFromOs(1024)).length();

        //WHEN print drive-table
        String table = ListDrives.print();
        String[] cols = table.split("\\|");

        //THEN every col has expectedColWidth as minimal size 
        //System.out.println("DEBUG cols:" + Arrays.asList(cols));
        assertTrue(cols[3].length() >= expectedColWidth);
    }

    @Test
    public void testPrintForTableBody() throws IOException {
        //drives
        assertThat(ListDrives.print(), containsString("C:\\ |"));
        assertThat(ListDrives.print(), containsString("D:\\ |"));

        //freeSpace
        String freeSpace = "0"; //CD-Drive D:
        assertThat(ListDrives.print(), containsString(freeSpace));

        //usedSpace
        String usedSpace = String.valueOf(format(getFirstDriveUsedSpaceFromOs(1024)));
        assertThat(ListDrives.print(), containsString(usedSpace));
    }

    /**
     *
     * @return ls output
     */
    private long getFirstDriveUsedSpaceFromOs(long divisor) throws IOException {
        String drives = "";

        String osCmd = (os.startsWith("windows", 0)) ? "cmd /C dir /-C" : "ls -al /";
        Runtime rt = java.lang.Runtime.getRuntime();
        Process exec = null;
        try {
            exec = rt.exec(osCmd);
            exec.getOutputStream();
            exec.waitFor();
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(ListDrivesTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        BufferedReader stdInput = new BufferedReader(new InputStreamReader(exec.getInputStream()));

        String s = null;
        ArrayList<String> lines = new ArrayList();
        while ((s = stdInput.readLine()) != null) {
            lines.add(s);
        }

        String[] parts = lines.get(lines.size() - 1).split("Bytes")[0].split(" ");
        long usedSize = Long.parseLong(parts[parts.length - 1]);

        return usedSize / divisor;
    }

    @Test
    public void testTableForSorting() {
        long[] freeSpace = new long[2];
        //GIVEN drives in system

        //WHEN printTable
        String table = ListDrives.print();
        String[] cols = table.split("\\|");
        freeSpace[0] = Long.parseLong(cols[7].trim().replace(".", ""));
        freeSpace[1] = Long.parseLong(cols[12].trim().replace(".", ""));

        //THEN sort ASC in col free-space
        assertTrue(String.format("%d is no lesser or equal to %d", freeSpace[0], freeSpace[1]), freeSpace[0] <= freeSpace[1]);
    }

    @Test
    public void testTableForHumanReadableSizes() throws IOException {

        //GIVEN drives in system
        //WHEN printTable
        String table = ListDrives.print();
        String[] rows = table.split("C");
        String firstDriveRow = rows[rows.length - 1];

        //THEN output should format the sizes in MB with seperator
        String readableSize = format(getFirstDriveUsedSpaceFromOs(1024));
        //System.out.println("readableSize:" + readableSize);
        assertThat(firstDriveRow, containsString(readableSize));
    }

    /**
     *
     * @param sizeMB
     * @return formated with Use locale-specific grouping separators
     */
    private String format(long sizeMB) {
        return String.format("%,d", sizeMB);
    }
}
