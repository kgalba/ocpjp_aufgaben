package kgalba;

import kgalba.collections.BusScheduleTest;
import kgalba.collections.FileTypesTest;
import kgalba.files.DirCreatorTest;
import kgalba.files.FileCounterTest;
import kgalba.files.FileCreatorTest;
import kgalba.files.ListDrivesTest;
import kgalba.lotto.LottoTest1;
import kgalba.tictactoe.model.BoardIT;
import kgalba.tictactoe.model.GameLogicTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * All tests from kgalba
 *
 * @author kgalba
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
    GameLogicTest.class,
    DirCreatorTest.class,
    FileCreatorTest.class,
    ListDrivesTest.class,
    FileCounterTest.class,
    LottoTest1.class,
    BoardIT.class,
    FileTypesTest.class,
    BusScheduleTest.class,
})
public class AllTestsSuite {
}
