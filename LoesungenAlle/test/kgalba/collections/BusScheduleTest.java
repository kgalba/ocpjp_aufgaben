package kgalba.collections;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author werkstatt
 */
public class BusScheduleTest {
    
    public BusScheduleTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of construction  of class BusSchedule.
     * 
     * - Die Abfahrtzeiten sollen als Strings in einem TreeSet gespeichert werden. Die 
            Strings sollen folgende Form haben:

            "06:12", "06:32", "06:52" ... "23:52"
     */
    @Test
    public void TestBusSchedule() {
        //GIVEN  //WHEN 
        BusSchedule busSchedule = new BusSchedule();

        //THEN schedule has all needed entrys in 20 minutes distance     
        assertEquals("06:12", busSchedule.schedule.first());
        assertEquals("23:52", busSchedule.schedule.last());
        
        //AND the correct order
        assertEquals("06:52", busSchedule.schedule.higher("06:50"));
        assertEquals("23:32", busSchedule.schedule.lower("23:52"));
    }
    
}
