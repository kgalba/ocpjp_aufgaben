/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.collections;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import kgalba.utils.Common;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author werkstatt
 */
public class FileTypesTest {
    private static File workingDir;

    
    @BeforeClass
    public static void setUpClass() {
        String pathName = System.getProperty("java.io.tmpdir") + Common.getClassName() + new Date().getTime();
        workingDir = new File(pathName);
        workingDir.mkdirs();
        //System.out.println("pathName: " + pathName);
    }
    
    @AfterClass
    public static void tearDownClass() throws IOException {
        FileUtils.forceDeleteOnExit(workingDir);
    }

    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * @see  FileTypes#getFileTypes()
     */
    @Test
    public void testGetFileTypes() throws IOException {
        //GIVEN dir with diffenrent expectedExtensions and amount of extensions
        Integer amount = 7;
        String[] expectedExtensions = createExtensionFiles(amount);
        
        //WHEN
        HashMap<String, Integer> extensionsRead = new FileTypes(workingDir.getAbsolutePath()).getFileTypes();
        
        //THEN all expectedExtensions are listed once
        Arrays.sort(expectedExtensions);
        Set<String> actualExtensionsSet = extensionsRead.keySet();
        String[] actualExtensionsArr = actualExtensionsSet.toArray(expectedExtensions);
        Arrays.sort(actualExtensionsArr);
        assertArrayEquals(expectedExtensions, actualExtensionsArr);
        
        //AND the extension have the correct count
        for (Integer value : extensionsRead.values()) {
            assertEquals(amount, value);
        }
    }

    private String[] createExtensionFiles(int amount) throws IOException {
        String[] extensions = new String[] {"txt", "ser", "java", "class", "eps"};
        for (String extension : extensions) {
            for (int i = 0; i < amount; i++) {
                FileUtils.touch(new File(workingDir + "/testfile" + i + "." + extension));
            }
        }
        Arrays.sort(extensions);
        return extensions;
    }
    
}
