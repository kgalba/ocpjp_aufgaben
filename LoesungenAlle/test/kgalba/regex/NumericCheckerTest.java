/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.regex;

import java.util.Random;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author kgalba
 */
public class NumericCheckerTest {    
    /**
     * Test of isDecimalLiteral method, of class NumericChecker.
     */
    @Test
    public void testIsDecimalLiteral() {
        assertTrue(NumericChecker.isDecimalLiteral("10"));
        assertTrue(NumericChecker.isDecimalLiteral("-10"));
        assertTrue(NumericChecker.isDecimalLiteral("+10"));
        assertTrue(NumericChecker.isDecimalLiteral("10.1"));
        
        assertFalse(NumericChecker.isDecimalLiteral("abc10"));
        assertFalse(NumericChecker.isDecimalLiteral("10abc"));
        assertFalse(NumericChecker.isDecimalLiteral("10..2"));
        assertFalse(NumericChecker.isDecimalLiteral("3.10..2"));
        assertFalse(NumericChecker.isDecimalLiteral("3,10"));

        Double random = new Random().nextDouble() * 10000 -5000;
        //System.out.println("random" + random);
        assertTrue(random + " Must match!" + random, NumericChecker.isDecimalLiteral(random.toString()));
    }    
    
}
