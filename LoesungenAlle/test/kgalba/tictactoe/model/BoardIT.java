package kgalba.tictactoe.model;

import kgalba.tictactoe.view.Cli;
import org.apache.commons.lang3.SerializationUtils;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author kgalba
 */
public class BoardIT {

    Board testBoard = new Board();

    public BoardIT() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSerialize() {

        //GIVEN a Board with some square set
        testBoard = new Board();
        testBoard.setSquare(1, 1, new Computer(Symbol.X, testBoard), new GameLogic(new Cli()));

        //WHEN serialize
        testBoard.serialize();

        //THEN deserialized board has equal values in tableau
        Board deserialized = SerializationUtils.roundtrip(testBoard);
        assertEquals(testBoard.getTableau()[1][1].getClass(), deserialized.getTableau()[1][1].getClass());
        assertEquals(testBoard.getTableau()[1][1].getSymbol(), deserialized.getTableau()[1][1].getSymbol());
    }

    @Test
    public void testUnSerialize() {
        //GIVEN a Board with some square set was serialized
        testBoard = new Board();
        testBoard.setSquare(1, 1, new Computer(Symbol.X, testBoard), new GameLogic(new Cli()));
        testBoard.serialize();

        //WHEN unserialize
        Board unserialized = testBoard.unserialize();

        //THEN deserialize board has equal values in tableau
        assertEquals(testBoard.getTableau()[1][1].getClass(), unserialized.getTableau()[1][1].getClass());
        assertEquals(testBoard.getTableau()[1][1].getSymbol(), unserialized.getTableau()[1][1].getSymbol());
    }
}
