package kgalba.tictactoe.model;

import java.util.Arrays;
import static kgalba.tictactoe.model.Board.DIMENSIONS;
import kgalba.tictactoe.model.Symbol;
import kgalba.tictactoe.view.Cli;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameter;
import org.junit.runners.Parameterized.Parameters;

/**
 *
 * @author kgalba
 */
@RunWith(Parameterized.class)
public class GameLogicTest {

    @Parameters(name = "{index}: winningType({0})")
    public static String[] data() {
        return new String[]{"row", "column", "diagonal1", "diagonal2"};
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }


    @Parameter // first data value (0) is default
    public /* NOT private */ String winningType;

    GameLogic game;
    Board board;
    Player player;
    private Human firstPlayer;
    private Computer secondPlayer;

    @Before
    public void setUp() {
        game = new GameLogic(new Cli());
        board = game.getBoard();
        player = new Computer(Symbol.O, board);
        firstPlayer = new Human(Symbol.X, board);
        secondPlayer = new Computer(Symbol.O, board);
        game.setPlayers(new Player[] {firstPlayer, secondPlayer});
    }

    @After
    public void tearDown() {
    }

    /**
     * Test GameLogic.isWinner() Scenario: NO winner
     */
    @Test
    public void testIsWinnerForFalse() {
        //GIVEN board with NO winning condition
        game.moveCounter = getParameterIndex();
        setTie(true);

        //WHEN check 
        //THEN player is NO winner
        assertFalse("There must be NO winner !", game.isWinner(player));
    }

    private int getParameterIndex() {
        return Arrays.asList(data()).indexOf(winningType);
    }

    /**
     * Test GameLogic.isWinner() Scenario: A winner
     */
    @Test
    public void testIsWinnerForTrue() {
        //GIVEN a board with winning condition
        setWinner(2, player, winningType);
        //WHEN check
        //THEN player is winner
        assertTrue("There has to be a winner !", game.isWinner(player));
    }
    
    @Test
    public void testSetBeginner1() {
        //GIVEN a GameLogic
        game = new GameLogic(new Cli());
        //WHEN check
        game.setBeginner();
        //THEN a player is beginner
        assertThat("A player has to begin !", game.getFirstPlayer(), isA(Player.class));
        assertNotEquals("Players must be different !", game.getPlayers()[0], game.getPlayers()[1]);
    }
    
    /**
     * helper to set a winnig condition, by setting winnig tuple on board and set the moveCounter
     *
     * @param position , position of winner tuple on board for row or column
     * @param player, winning player
     * @param winType, type of win {"row", "column, "diagonal1", "diagonal2"}
     *
     */
    private void setWinner(int position, Player player, String winType) {
        String[] legalInput = {"row", "column", "diagonal1", "diagonal2"};
        for (int i = 0; i < Board.DIMENSIONS; i++) {
            switch (winType) {
                case "row":
                    board.setSquare(position, i, player, game);
                    game.move[0] = position;
                    game.move[1] = i;
                    break;
                case "column":
                    board.setSquare(i, position, player, game);
                    game.move[0] = i;
                    game.move[1] = position;
                    break;
                case "diagonal1":
                    board.setSquare(i, i, player, game);
                    game.move[0] = i;
                    game.move[1] = i;
                    break;
                case "diagonal2":
                    board.setSquare(i, DIMENSIONS - 1 - i, player, game);
                    game.move[0] = i;
                    game.move[1] = i;
                    break;
                default:
                    throw new IllegalWinnigType(winType, legalInput);
            }
        }
        game.moveCounter = 3;
    }

    /**
     * Test of isTie method, of class GameLogic.
     */
    @Test
    public void testIsTie() {
        //GIVEN scenario: board is A tie
        board.setTableau(setTie(true));
        //WHEN //THEN
        assertTrue(game.isTie());

        //GIVEN scenario: board is NOT tie because not full
        board.setTableau(setTie(false));
        //WHEN //THEN
        assertFalse(game.isTie());

        //GIVEN scenario board is NOT tie because there is a winner 
        setWinner(0, player, this.winningType);
        //WHEN //THEN
        assertFalse(game.isTie());
    }

    /**
     *
     * helper to to set/unset a tie board
     *
     * @param tie yes or no
     */
    private Player[][] setTie(boolean tie) {
        Player[][] tableau = board.getTableau();
        Human human = new Human(Symbol.X, board);
        Computer computer = new Computer(Symbol.O, board);

        if (tie) {
            //@todo refine to dynamic set tie    
            tableau = new Player[][]{
                {human, human, computer},
                {computer, computer,human},
                {human, human, computer}
            };
            board.setTableau(tableau);
        } else {
            tableau = new Board().getTableau();
        }
        return tableau;
    }

    /**
     * Test GameLogic.gameLoop() Scenario: A winner
     */
    @Test
    public void testGameLoopForWinner() {
        //GIVEN a board with winning condition for Computer
        player = new Computer(Symbol.X, board);
        setLastChoiceForPlayer("win", player);

        //WHEN check
        //THEN player is winner
        Assert.assertEquals(player + " has to be winner !", player, game.gameLoop());
    }

    /**
     * Test GameLogic.gameLoop() Scenario: tie
     */
    @Test
    public void testGameLoopForTie() {
        //GIVEN a board with tie condition
        setLastChoiceForPlayer("tie", player);
        //WHEN check
        //THEN player is winner
        assertNull("There must be NO Winner !", game.gameLoop()); //"There has to be a winner !",
    }

    /**
     * helper to set board with only one last choice to win or tie for computer-player cross
     * @param outcome
     */
    private void setLastChoiceForPlayer(String outcome, Player player) throws IllegalWinnigType {
        Player opponent = game.getOpponent(player);
        String[] legalInput = {"win", "tie"};
        Player[][] tableau = board.getTableau();
        tableau = new Player[][]{
            {player, player, opponent},
            {opponent, opponent, player},
            {player, player, opponent}
        };

        switch (outcome) {
            case "win":
                tableau[0][2] = null;
                break;
            case "tie":
                tableau[0][1] = null;
                break;
            default:
                throw new IllegalWinnigType(outcome, legalInput);
        }

        board.setTableau(tableau);

        //nextPlayer has to be CROSS played by computer
        opponent = game.getOpponent(player);
        game.currentPlayer = opponent;
        game.setPlayers(new Player[] {opponent, player});

        //winner has to moved at leas 3 times
        game.moveCounter = 3;
    }
    
    /**
     * //SCENARIO move > 0
     */
    @Test
    public void testSetCurrentPlayerAfterFirstMove() {        
        //SCENARIO move > 0 
        //GIVEN
        game.moveCounter = 2;
        game.currentPlayer=secondPlayer;
        
        //WHEN 
        game.changePlayer();
        
        //THEN
        assertThat(game.currentPlayer, is(equalTo(firstPlayer)));
    }
    
    
    @Test
    public void testGetOpponent() {
        //GIVEN a game with palyers
        Player testPlayer = game.getFirstPlayer();
        Player expectedPlayer = game.getSecondPlayer();
        //WHEN
        Player opponent = game.getOpponent(testPlayer);
        //THEN player is the other
        assertThat(expectedPlayer, is(equalTo(opponent)));
    }
    
    class IllegalWinnigType extends IllegalArgumentException {

        IllegalWinnigType(String outcome, String[] legalInput) {
            super(String.format("@param outcome has to be in: %1$s ! Was: %2$s", Arrays.toString(legalInput), outcome));
        }
    }
}
