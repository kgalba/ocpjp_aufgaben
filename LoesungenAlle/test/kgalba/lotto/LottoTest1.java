package kgalba.lotto;

import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertFalse;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author kgalba
 */
public class LottoTest1 {

    //CuT
    Lotto lotto;
    
    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }



    public LottoTest1() {
    }

    @Before
    public void setUp() {
        lotto = new Lotto(7, 49);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of nextUniqueRandom method, of class Lotto.
     */
    @Test
    public void testNextUniqueRandom() {
        //repeat the test a lot just to be more certain
        for (int i = 0; i < 1000; i++) {
            //GIVEN a LottoGame with drawing        
            int[] usedRandoms = {1, 2, 3, 4, 5, 6, 7};
            lotto.drawing = usedRandoms;
//            System.out.println("usedRandoms:" + lotto.toString());
            
            //WHEN 
            int nextUniqueRandom = lotto.nextUniqueRandom();
//            System.out.println("nextUniqueRandom: " + nextUniqueRandom);

            //THEN nextUniqueRandom MUST NOT be in drawing
            assertFalse("usedRandoms:" + lotto + " nextUniqueRandom: "
                            + nextUniqueRandom,
                    Arrays.asList(lotto.drawing).contains(nextUniqueRandom)
            );
        }
    }
}
