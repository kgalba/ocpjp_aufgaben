/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.files;

import java.util.ArrayList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tverrbjelke
 */
public class DrivesTest {
    
    /** 
     * mock result for getDrives()
     */
    ArrayList<Drives.DriveStats> driveInstances;
    
    public DrivesTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        driveInstances = new ArrayList<>();
        driveInstances.add (new Drives.DriveStats( "/",
                                                   5 * Drives.SizeUnit.MB.value , 
                                                   100* Drives.SizeUnit.MB.value ,
                                                   105* Drives.SizeUnit.MB.value));
        driveInstances.add (new Drives.DriveStats( "C:\\", 
                                                   4 * Drives.SizeUnit.GB.value,
                                                   10 * Drives.SizeUnit.GB.value,
                                                   14 * Drives.SizeUnit.GB.value));
        driveInstances.add (new Drives.DriveStats( "F:\\", 
                                                   40 * Drives.SizeUnit.MB.value,
                                                   1 * Drives.SizeUnit.GB.value,
                                                   1 * Drives.SizeUnit.GB.value + 40 * Drives.SizeUnit.MB.value));
    }
    
    @After
    public void tearDown() {
        driveInstances = null;
    }

    /**
     * Test of print method, of class Drives.
     */
    @Test
    public void testPrint() {
    }

    /**
     * Test of createTableFromDrives method, of class Drives.
     */
    @Test
    public void testCreateTableFromEmptyDrivesList() {
        ArrayList<Drives.DriveStats> drives = new ArrayList<Drives.DriveStats>();
        ArrayList<StringBuilder[]> result  = Drives.createTableFromDrives(drives, Drives.SizeUnit.GB);
        Assert.assertEquals(1, result.size());
    }

    /**
     * Test of createTableFromDrives method, of class Drives.
     */
    @Test
    public void testCreateTablebaseGB() {
        Drives.SizeUnit unit = Drives.SizeUnit.GB;
        ArrayList<StringBuilder[]> result = Drives.createTableFromDrives(this.driveInstances, unit);
        Assert.assertEquals(this.driveInstances.size() +1  , result.size()); 
        
        StringBuilder[] row = result.get(0);
        assertEquals( "Name" , row[0].toString());
        assertEquals( "Frei(GB)" , row[1].toString());
        assertEquals( "Belegt(GB)" , row[2].toString());
        assertEquals( "Gesamt(GB)" , row[3].toString());

        // 1st element only is in MB range -> 0 GB 
        row = result.get(1);
        assertEquals( "/" , row[0].toString());
        assertEquals( "0" , row[1].toString());
        assertEquals( "0" , row[2].toString());
        assertEquals( "0" , row[3].toString());
        
        row = result.get(2);
        assertEquals( "C:\\" , row[0].toString());
        assertEquals( "4" , row[1].toString());
        assertEquals( "10" , row[2].toString());
        assertEquals( "14" , row[3].toString());

        row = result.get(3);
        assertEquals( "F:\\" , row[0].toString());
        assertEquals( "0" , row[1].toString());
        assertEquals( "1" , row[2].toString());
        assertEquals( "1" , row[3].toString());
    }

    
    /**
     * Test of drive2RowOfStringBuilders method, of class Drives.
     */
    @Test
    public void testDrive2RowOfStringBuildersWithNullDrive() {

        try{
        StringBuilder[] instance = Drives.drive2RowOfStringBuilders(null, Drives.SizeUnit.GB.value);
        Assert.fail("Should have thrown exception");
        }
        catch ( NullPointerException e)
        {
            System.out.println("testDrive2RowOfStringBuildersWithNullDrive passed.");
        }
    }

    /**
     * Test of drive2RowOfStringBuilders method, of class Drives.
     */
    @Test
    public void testDrive2RowOfStringBuildersWithBaseMB() {
        final long base = Drives.SizeUnit.MB.value;
        for ( Drives.DriveStats d :  this.driveInstances){
            StringBuilder[] instance = Drives.drive2RowOfStringBuilders(d , base);
            assertEquals(Drives.nColumns, instance.length);
            assertEquals(d.name, instance[0].toString());
            assertEquals(String.valueOf(d.free/base), instance[1].toString());
            assertEquals(String.valueOf(d.occupied/base), instance[2].toString());
            assertEquals(String.valueOf(d.total/base), instance[3].toString());
        }
    }

    
        /**
     * Test of drive2RowOfStringBuilders method, of class Drives.
     */
    @Test
    public void testDrive2RowOfStringBuildersWithBaseGB() {
        final long base = Drives.SizeUnit.GB.value;
        for ( Drives.DriveStats d :  this.driveInstances){
            StringBuilder[] instance = Drives.drive2RowOfStringBuilders(d , base);
            assertEquals(Drives.nColumns, instance.length);
            assertEquals(d.name, instance[0].toString());
            assertEquals(String.valueOf(d.free/base), instance[1].toString());
            assertEquals(String.valueOf(d.occupied/base), instance[2].toString());
            assertEquals(String.valueOf(d.total/base), instance[3].toString());
        }
    }

    /**
     * Test of getDrives method, of class Drives.
     */
    @Test
    public void testGetDrives() {
    }

    /**
     * Test of parseFileIntoDrive method, of class Drives.
     */
    @Test
    public void testParseFileIntoDrive() {
        
    }
    
}
