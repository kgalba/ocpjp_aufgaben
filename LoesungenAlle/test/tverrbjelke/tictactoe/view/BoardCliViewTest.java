/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.tictactoe.view;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tverrbjelke
 */
public class BoardCliViewTest {
    
    public BoardCliViewTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of toString method, of class BoardCliView.
     */
    @Test
    public void testToStringOnEmptyBoard() {
        BoardCliView instance = new BoardCliView(1);
        
        String expResult = "---\n| |\n---\n";
        String result = instance.toString();
        System.out.println(result);
        assertEquals(expResult, result);
    }
    
}
