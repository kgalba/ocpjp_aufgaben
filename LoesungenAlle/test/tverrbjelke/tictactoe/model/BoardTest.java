/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.tictactoe.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tverrbjelke
 */
public class BoardTest {
    Board instance;
    
    public BoardTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        instance = new Board(3);
    }
    
    @After
    public void tearDown() {
        instance = null;
    }

    /**
     * Test of setField method, of class Board.
     */
    @Test
    public void testSetAndGetFieldWithValidValue() {
        System.out.println("setField");
        int x = 0;
        int y = 0;

        Board.PlayFieldStatus status = Board.PlayFieldStatus.EMPTY;
        instance.setField(x, y, status);
        assertEquals(status, instance.getField(x, y));
        
        status = Board.PlayFieldStatus.PLAYER1;
        instance.setField(x, y, status);
        assertEquals(status, instance.getField(x, y));
        
    }

    /**
     * Test of clearBoard, of class Board.
     */
    @Test
    public void testSweepBoard() {
        System.out.println("clearBoard");
        Board.PlayFieldStatus expResult = Board.PlayFieldStatus.EMPTY;
        
        // after ctor all should be EMPTY.
        for (int x=0; x < instance.BOARDSIZE_X; x++){
            for (int y=0; y < instance.BOARDSIZE_Y; y++){
                Board.PlayFieldStatus result = instance.getField(x, y);
                assertEquals(expResult, result);
            }
        }

        expResult = Board.PlayFieldStatus.PLAYER1;
        instance.sweepBoard(expResult);
        // all values properly set?
        for (int x=0; x < instance.BOARDSIZE_X; x++){
            for (int y=0; y < instance.BOARDSIZE_Y; y++){
                Board.PlayFieldStatus result = instance.getField(x, y);
                assertEquals(expResult, result);
            }
        }
        
    }
    
    /**
     * Test of ctor, of class Board.
     */
    @Test
    public void testCtor() {
        System.out.println("constructor");
        Board.PlayFieldStatus expResult = Board.PlayFieldStatus.EMPTY;
        
        // after ctor all should be EMPTY.
        for (int x=0; x < instance.BOARDSIZE_X; x++){
            for (int y=0; y < instance.BOARDSIZE_Y; y++){
                Board.PlayFieldStatus result = instance.getField(x, y);
                assertEquals(expResult, result);
            }
        }
    }

}
