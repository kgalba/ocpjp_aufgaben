/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.tictactoe.model;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tverrbjelke.tictactoe.model.Game.Move;

/**
 *
 * @author tverrbjelke
 */
public class GameTest {
    
    Game instance = null;
    GameLogic logic = null;
    
    public GameTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        this.instance = new Game(3);
        this.logic = new GameLogic(instance);
    }
    
    @After
    public void tearDown() {
        this.instance = null;
    }

    /**
     * Test of setCurrentPlayer method, of class Game.
     */
    @Test
    public void testSetAndGetCurrentPlayer() {
        System.out.println("testSetAndGetCurrentPlayer");

        // test for default player
        Player currentPlayer = instance.getCurrentplayer();
        assertEquals("Player 1", currentPlayer.name );
        
        String newPlayerName = "value set by test P2";
        Player newPlayer = new Player(Player.PlayRole.PLAYER2, newPlayerName);
        this.instance.setCurrentPlayer(newPlayer);
        assertEquals(newPlayerName, instance.getCurrentplayer().name);
        assertEquals(Player.PlayRole.PLAYER2 , instance.getCurrentplayer().role);
                
    }

    /**
     * Test of getPlayers method, of class Game.
     */
    @Test
    public void testGetPlayers() {
        System.out.println("getPlayers");
        
        Player[] result = instance.getPlayers();
        assertEquals(result.length, 2);
        assertEquals(result[0].role, Player.PlayRole.PLAYER1);
        assertEquals(result[1].role, Player.PlayRole.PLAYER2);
    }

    /**
     * Test of isValidMove method, of class Game.
     */
    @Test
    public void testIsValidMoveCannotMoveToAlreadyOccupiedField() {
        System.out.println("isValidMoveCannotMoveToAlreadyOccupiedField");

        Player p =  new Player( Player.PlayRole.PLAYER1);
        Game.Move d1 = new Game.Move(p, 1, 1);
        instance.getBoard().sweepBoard(Board.PlayFieldStatus.EMPTY);
        assertEquals(true, this.logic.isValidMove(d1) );
        logic.doMove(d1);
        assertEquals(false, this.logic.isValidMove(d1) );
    }

    /**
     * Test of doMove method, of class Game.
     */
    @Test
    public void testDoMove() {
        System.out.println("doDraw");

        int testX = 0;
        int testY = 1;
        
        assertEquals(instance.getBoard().getField(testY, testY) , Board.PlayFieldStatus.EMPTY);
        
        Game.Move d = new Game.Move(new Player (Player.PlayRole.PLAYER2),testX, testY );
        assertEquals(true, this.logic.doMove(d)); // expect no problems with this move        
        assertEquals(Board.PlayFieldStatus.PLAYER2, instance.getBoard().getField(testX, testY) );
   
    }
    
}
