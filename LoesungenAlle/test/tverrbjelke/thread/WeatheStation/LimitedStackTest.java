/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.thread.WeatheStation;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tverrbjelke
 */
public class LimitedStackTest {
    
    public LimitedStackTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of add method, of class LimitedStack.
     */
    @Test
    public void testAdd() {
        LimitedStack buffer = new LimitedStack(3);
        assertEquals(buffer.size(), 0);
        buffer.add(1.0);
        assertEquals(buffer.size(), 1);
        assertEquals(buffer.get(0), 1.0, 0.001);
        buffer.add(2.0);
        assertEquals(buffer.size(), 2);
        assertEquals(buffer.get(0), 2.0, 0.001);
        assertEquals(buffer.get(1), 1.0, 0.001);
        buffer.add(3.0);
        assertEquals(buffer.size(), 3);
        assertEquals(buffer.get(0), 3.0, 0.001);
        assertEquals(buffer.get(1), 2.0, 0.001);
        assertEquals(buffer.get(2), 1.0, 0.001);
        buffer.add(4.0);
        assertEquals(buffer.size(), 3);
        assertEquals(buffer.get(0), 4.0, 0.001);
        assertEquals(buffer.get(1), 3.0, 0.001);
        assertEquals(buffer.get(2), 2.0, 0.001);

    }

    /**
     * Test of get method, of class LimitedStack.
     */
    @Test
    public void testGet() {
    }

    /**
     * Test of size method, of class LimitedStack.
     */
    @Test
    public void testSize() {
    }

    /**
     * Test of toString method, of class LimitedStack.
     */
    @Test
    public void testToString() {
    }

    /**
     * Test of subFromFirst method, of class LimitedStack.
     */
    @Test
    public void testSubFromFirst() {
    }
    
}
