/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.thread.WeatheStation;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tverrbjelke
 */
public class TrendObserverTest {
    
    public TrendObserverTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of run method, of class TrendObserver.
     */
    @Test
    public void testRun() {
    }

    /**
     * Test of isTrendUp method, of class TrendObserver.
     */
    @Test
    public void testIsTrendUp() {
        LimitedStack buffer = new LimitedStack(3);
        buffer.add(1.0);
        buffer.add(2.0);
        TrendObserver instance = new TrendObserver(buffer, 3);
        assertEquals(false, instance.isTrendUp());
        buffer.add(3.0);
        assertEquals(true, instance.isTrendUp());
        buffer.add(3.0);
        assertEquals(true, instance.isTrendUp());
        buffer.add(2.0);
        assertEquals(false, instance.isTrendUp());
    }

    /**
     * Test of isTrendDown method, of class TrendObserver.
     */
    @Test
    public void testIsTrendDown() {
        LimitedStack buffer = new LimitedStack(3);
        buffer.add(3.0);
        buffer.add(2.0);
        TrendObserver instance = new TrendObserver(buffer, 3);
        assertEquals(false, instance.isTrendDown());
        buffer.add(1.0);
        assertEquals(true, instance.isTrendDown());
    }
    
}
