/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.enums;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author tverrbjelke
 */
public class DogRaceTest {
    
    public DogRaceTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of valueOf method, of class DogRace.
     */
    @Test
    public void testMaxSizeDACKEL() {
        System.out.println("maxSize");
        DogRace instance  = DogRace.DACKEL;
        double result = instance.maxSize;
        double expResult = 0.5;
        assertEquals(expResult, result, 0.0001);
    }

    /**
     * Test of maxSize property, of class DogRace.
     */
    @Test
    public void testMaxSizeCOLLIE() {
        System.out.println("maxSize");
        DogRace instance  = DogRace.COLLIE;
        double result = instance.maxSize;
        assertEquals(result, 1, 0.0001);
    }

    /**
     * Test of maxSize property, of class DogRace.
     */
    @Test
    public void testMaxSizeDOGGE() {
        System.out.println("maxSize");
        DogRace instance  = DogRace.DOGGE;
        double result = instance.maxSize;
        assertEquals(result, 1.5, 0.0001);
    }

    /**
     * Test of toString method, of class DogRace.
     */
    @Test
    public void testToStringCOLLIE() {
        System.out.println("toStringCollie");
        DogRace instance = DogRace.COLLIE;
        String expResult = "COLLIE, max. Größe: 1.0";
        String result = instance.toString();
        assertEquals(expResult, result);
    }

    /**
     * Test of toString method, of class DogRace.
     */
    @Test
    public void testToString() {
        System.out.println("toString");
        DogRace[] instances = { DogRace.COLLIE, DogRace.DACKEL, DogRace.DOGGE };
        String[] expResults = {
            "COLLIE, max. Größe: 1.0", 
            "DACKEL, max. Größe: 0.5", 
            "DOGGE, max. Größe: 1.5", 
        };
        
        for ( int i=0; i<instances.length; i++){
            String result = instances[i].toString();
            assertEquals(expResults[i], result);
        }
    }

}
