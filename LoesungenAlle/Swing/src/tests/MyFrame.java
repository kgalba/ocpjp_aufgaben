package tests;

import javax.swing.JFrame;

public class MyFrame extends JFrame {
    
    public MyFrame() {
        setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        setLocationRelativeTo(null);
    }
}
