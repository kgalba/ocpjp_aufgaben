/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obenin.Klassen;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author obenin
 */


public class LottoSpiel {    
   int anzahlKugel, anzahlKugelGesamt;
   int[] Array; 
    
   
  public LottoSpiel (int anzahlKugel, int anzahlKugelGesamt ){
    this.anzahlKugel= anzahlKugel;
    this.anzahlKugelGesamt=anzahlKugelGesamt;      
  }
  
  
   void ziehen (){
       Random r = new Random();
       Array = new int[anzahlKugel];
   for (int i=0; i<Array.length;i++){
          int  z=r.nextInt(anzahlKugelGesamt)+1;
          Array[i]=z;
          for (int j=0; j<i;j++){             
              if(Array[j]==z)
                  i=0;              
          }
       
       
     }
   } 
    
        public String toString() {   
         Arrays.sort(Array);  
         String s=String.format("Spiel %s aus  %s. ", anzahlKugel,anzahlKugelGesamt);
         s=s.concat(Arrays.toString(Array));
         return s;
        }   
        
        public int vergleichen ( LottoTipp tipp){
            int richrig=0;
            int gewinn=0;
           for (int i=0; i<Array.length;i++){
             if (this.Array[i]==tipp.Array[i])
                  richrig++;               
           }
           
           
             switch (richrig) {
            case 0:
                gewinn=0;break;
            case 1:
                gewinn=1;break;
            case 2:
                gewinn=10;break;
            case 3:
                gewinn=100;break;
            case 4:
                gewinn=1000;              
                
        }
           
           
          return gewinn;
        }
        
}



 class LottoSpielSimulation{
    
public static void main(String[] args) {
    int anzahlKugel = 7;
    int anzahlKugelGesamt = 49;
    LottoSpiel lotto = new LottoSpiel(anzahlKugel, anzahlKugelGesamt);
    lotto.ziehen(); 
//    System.out.println( lotto );
    System.out.println( lotto );
    
    
    LottoTipp tipp = new LottoTipp (anzahlKugel, anzahlKugelGesamt);
    tipp.abgeben();
    System.out.println( tipp );
    
    int gewinn = lotto.vergleichen(tipp);
     System.out.println("Der Gewinn beträgt: " + gewinn + " Euro" );
    
    
 }
}