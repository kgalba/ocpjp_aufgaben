/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obenin.Enums;

/**
 *
 * @author obenin
 */
public enum Hunderasse {
    DACKEL(0.5),COLLIE(1.0),DOGGE(1.5);
    
    private double maxGroesse;
    
    Hunderasse(double maxGroesse){
    this.maxGroesse=maxGroesse;    
    }
    
    public double getMax(){return maxGroesse;}
    
    public static void main(String[] args) {
        
        for(Hunderasse h: Hunderasse.values()){
            System.out.println(h + ", max. Grösse: " + h.getMax()); 
            
            
        }
    }
}
