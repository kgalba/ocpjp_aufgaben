/*
 Aufgabe "Collections/HashSet - FileTypes"

 - Erstellen Sie eine Klasse 'FileTypes'

 - Definieren Sie einen Konstruktor für die Klasse 'FileTypes', an den man ein 
 String übergeben kann:

 FileTypes ft = new FileTypes("C:\\Windows");

 - Definieren Sie eine Methode 'getFileTypes' in der Klasse 'FileTypes', die 
 eine Collection von Strings liefert, in der die alle auftretenden 
 Datei-Erweiterungen eines Verzeichnisses (s. Konstruktor) aufgelistet sind 
 und einmalig auftauchen:

 Collection<String> extColl = ft.getFileTypes();

 - Geben Sie die gefundenen Datei-Erweiterungen aus
 */
package kgalba.collections;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashSet;

/**
 * jse_066260 / Aufgaben / aufgabe_coll_hashset_filetypes.txt
 *
 * @author kgalba
 */
public class FileTypes {

    File dir;

    public FileTypes(String dir) {
        this.dir = new File(dir);
    }

    public static void main(String[] args) {
        String searchDir = System.getProperty("user.dir");
        searchDir = "C:\\Windows";
        FileTypes ft = new FileTypes(searchDir);
        HashMap<String, Integer> extColl = ft.getFileTypes();

        System.out.printf("Extensions in '%s' %s",ft.dir , extColl.toString());
        System.out.println("");
    }

    HashMap<String, Integer> getFileTypes() {
        File[] files = dir.listFiles();
        HashMap<String, Integer> types = new HashMap<String, Integer>();
        for (File file : files) {
            String[] nameParts = file.getName().split("\\.");
            String extensionKey = nameParts[nameParts.length - 1];
            
            if (!file.isDirectory() && nameParts.length > 1 && nameParts[0].length() > 0) {
                //System.out.println("nameParts: " + Arrays.asList(nameParts));
                Integer extensionValue = types.get(extensionKey);
                if (extensionValue== null) {
                    types.put(extensionKey, 1);
                } else
                    types.put(extensionKey, ++extensionValue);
                
            }
        }
        return types;
    }
}
