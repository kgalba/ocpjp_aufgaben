package kgalba.collections;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.Random;
import kgalba.utils.Common;

/**
 *
 * @author werkstatt
 */
public class Benchmark {

    public static void main(String[] args) throws InterruptedException {
        Benchmark benchmark = new Benchmark(new ArrayList<Integer>());
        System.out.println("ArrayList<Integer> benchmark:" + benchmark.run());

        //wait 5 sec for GarbageCollector 
        Thread.sleep(5000);
        Runtime.getRuntime().gc();
        
        benchmark = new Benchmark(new LinkedList<Integer>());
        System.out.println("LinkedList<Integer> benchmark:" + benchmark.run());
    }

    AbstractList<Integer> listClass;
    AbstractList<Integer>[] lists;

    Benchmark(AbstractList<Integer> listClass) {
        this.lists = new AbstractList[1000];
        this.listClass = listClass;
        createLists();
    }

    long run() {
        Date start = new Date();

        //benchmark
        Date intermediate = new Date();
        for (int i = 0; i < 1000; i++) {
            insertFirstElement();
        }

        return new Date().getTime() - start.getTime();
    }

    private void createLists() {
        for (int i = 0; i < lists.length; i++) {
            if (listClass instanceof ArrayList) {
                lists[i] = new ArrayList<Integer>();

            }
            if (listClass instanceof LinkedList) {
                lists[i] = new LinkedList<Integer>();
            }
            initList(lists[i]);
        }
    }

    private void initList(AbstractList<Integer> list) {
        for (int i = 0; i < 1000; i++) {
            list.add(new Random().nextInt(Integer.MAX_VALUE));
        }
    }

    private void insertFirstElement() {
        for (int i = 0; i < lists.length; i++) {
            lists[i].add(0, 1);
        }
    }

}
