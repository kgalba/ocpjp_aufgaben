/*
 Aufgabe "Collections/Treeset - Abfahrtzeiten"

 - Generieren Sie bitte die Abfahrtzeiten für eine Buslinie von einer Haltestelle: 
 - Der erste Bus fährt um 06:12 ab.
 - Der letzte Bus fährt um 23:52 ab.
 - Die Busse fahren in 20-Minuten-Takt

 - Die Abfahrtzeiten sollen als Strings in einem TreeSet gespeichert werden. Die 
 Strings sollen folgende Form haben:

 "06:12", "06:32", "06:52" ... "23:52"

 - Untersuchen Sie die Abfahrtzeiten:
 - suchen Sie die erste Abfahrtzeit nach 12:03
 - suchen Sie nach der ersten Abfahrtzeit vor 12:03
 - suchen Sie nach der ersten Abfahrtzeit nach 17:12 inklusive
 - suchen Sie nach der ersten Abfahrtzeit nach 17:12 exklusive
 - suchen Sie nach allen Abfahrtzeiten zwischen 12:00 bis 13:00
 - suchen Sie nach allen Abfahrtzeiten zwischen 11:52 exklusive bis 13:12 inklusive
 - suchen Sie nach der erstmöglichen Abfahrtzeit
 - suchen Sie nach der letztmöglichen Abfahrtzeit
 */
package kgalba.collections;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.TreeSet;

/**
 *
 * @author kgalba
 */
public class BusSchedule {

    //time constans in millisecs

    final static long MINUTE = 60 * 1000;
    final static long HOUR = 60 * MINUTE;

    final static long START = 6 * HOUR + 12 * MINUTE;
    final static long END = 23 * HOUR + 52 * MINUTE;
    final static long INTERVALL = 20 * MINUTE;
    final static String TIME_FORMAT = "HH:mm";

    public static void main(String[] args) {
        BusSchedule busSchedule = new BusSchedule();

        System.out.println("- Untersuchen Sie die Abfahrtzeiten:");
        System.out.println("- suchen Sie die erste Abfahrtzeit nach 12:03 : " +busSchedule.schedule.higher("12:03"));
        System.out.println("- suchen Sie die erste Abfahrtzeit vor 12:03 : " +busSchedule.schedule.lower("12:03"));
        System.out.println("- suchen Sie nach der ersten Abfahrtzeit nach 17:12 inklusive : " +busSchedule.schedule.ceiling("17:12"));
        System.out.println("- suchen Sie nach der ersten Abfahrtzeit vor 17:12 exklusive : " +busSchedule.schedule.floor("17:12"));
        System.out.println("- suchen Sie nach allen Abfahrtzeiten zwischen 12:00 bis 13:00 : " +busSchedule.schedule.subSet("12:00", "13:00"));
        System.out.println("- suchen Sie nach allen Abfahrtzeiten zwischen 11:52 exklusive bis 13:12 inklusive : " +busSchedule.schedule.subSet("11:52", false, "13:12", true));
        System.out.println("- suchen Sie nach der erstmöglichen Abfahrtzeit : " +busSchedule.schedule.first());
        System.out.println("- suchen Sie nach der letztmöglichen Abfahrtzeit : " +busSchedule.schedule.last());
    }
    SimpleDateFormat df = new SimpleDateFormat(TIME_FORMAT);


    Date departure;
    TreeSet<String> schedule = new TreeSet<>();

    public BusSchedule() {
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        initSchedule();
    }
    
    final void initSchedule() {
        for (long i = START; i <= END; i = i + INTERVALL) {
            departure = new Date(i);
            //System.out.println("format " + df.format(departure));
            schedule.add(df.format(departure));
        }
    }
}
