package kgalba.collections;

import kgalba.comparablekg.*;
import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author kgalba
 */
public class Person implements Comparable<Object>{

    public String foreName;
    public String surName;
    public int age;


    public Person(String vor, String nach, int birth) {
        this.foreName = vor;
        this.surName = nach;
        this.age = birth;
    }


    
    public String toString() {
        return "Person .foreName " + foreName +" .surName " + surName + " .age "+age;
    }

    @Override
    public int compareTo(Object o) {
        return 1;
    }
}
