package kgalba.collections;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import kgalba.utils.Common;

/**
 *
 * @author kgalba
 */
public class PersonCollection {

    public static void main(String[] args) {

        PersonCollection pCollection = new PersonCollection();

        //WITHOUT overriding
        pCollection.initPersonCollections();
        System.out.println("WITHOUT overriding equals() and hashcode");
        System.out.println("Number of Elements in Array:" + pCollection.personArr.length);
        System.out.println("Number of Elements in HashSet:" + pCollection.personSet.size());
        System.out.println("Number of Elements in LinkedList:" + pCollection.personList.size());
        System.out.println("Number of Elements in TreeSet:" + pCollection.personTree.size());

        //WITH overriding
        System.out.println("");

        pCollection.initPersonHashers();
        System.out.println("WITH overriding equals() and hashcode");
        System.out.println("Number of Elements in Array:" + pCollection.personArr.length);
        System.out.println("Number of Elements in HashSet:" + pCollection.personSet.size());
        System.out.println("Number of Elements in LinkedList:" + pCollection.personList.size());
        System.out.println("Number of Elements in TreeSet:" + pCollection.personTree.size());

    }

    Person person1 = new Person("Vorname", "Nachname", 1970);
    Person person2 = new Person("Vorname", "Nachname", 1970);
    Person person3 = new Person("Vorname3", "Nachname3", 1973);
    Person person4 = new Person("Vorname4", "Nachname4", 1974);

    Person[] personArr;
    HashSet<? extends Person> personSet;
    LinkedList<? extends Person> personList;
    TreeSet<? extends Person> personTree;

    public PersonCollection() {

    }

    private void initPersonCollections() {
        personArr = new Person[]{person1, person2, person3, person4};
        personSet = new HashSet<Person>(Arrays.asList(personArr));
        personList = new LinkedList<Person>(Arrays.asList(personArr));
        personTree = new TreeSet<Person>(Arrays.asList(personArr));
    }

    private void initPersonHashers() {
        personArr = new PersonHasher[]{new PersonHasher(person1), new PersonHasher(person2), new PersonHasher(person3), new PersonHasher(person4)};
        personSet = new HashSet<>(Arrays.asList(personArr));
        personList = new LinkedList<>(Arrays.asList(personArr));
        personTree = new TreeSet<Person>(Arrays.asList(personArr));
    }

    class PersonHasher extends Person implements Comparable<Object> {

        public PersonHasher(String vor, String nach, int birth) {
            super(vor, nach, birth);
        }

        public PersonHasher(Person person) {
            super(person.foreName, person.surName, person.age);
        }

        @Override
        public int hashCode() {
            return this.foreName.hashCode() + this.surName.hashCode() + this.age;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            if (this.hashCode() == ((PersonHasher) obj).hashCode()) {
                Common.printMethodName(obj.toString());
                return true;
            }
//            final PersonHasher other = (PersonHasher) obj;
//
            return true;
        }

        @Override
        public int compareTo(Object o) {
            PersonHasher p = (PersonHasher) o;
            String o1Str = foreName + surName + String.valueOf(age);
            String o2Str = p.foreName + p.surName + String.valueOf(p.age);

            return o1Str.compareTo(o2Str);
        }
    }
}
