/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.strings;

/**
 *
 * @author kgalba
 */
public class Aufgabe_String {
    static final String str ="Java ist toll";
    
    public static void main(String[] args) {
        System.out.println("-- " + Thread.currentThread().getStackTrace()[1].getClassName() + "--");
        
        System.out.println("I. Reverse:  " + reverse(Aufgabe_String.str));
        
        System.out.println("II. isTextFile:  " + isTextFile("AAAQAQ"));
        System.out.println("II. isTextFile:  " + isTextFile("AAAQAQ.txt"));
        System.out.println("II. isTextFile:  " + isTextFile("AAAQAQ.tXt"));

    }
            
    static String reverse(String str) {
        String reverse = "";
        for (int i = str.length()-1; i >= 0 ; i--) {
            reverse += str.charAt(i);
        }
        return reverse;
    } 
    
    public static boolean isTextFile(String haystack) {
        String needle = "txt";
        return haystack.toLowerCase().contains(needle);
    }
}
