/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.strings;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author kgalba
 */
public class String_Api {
    static String strA = "AAAbbb";
    static char[] chars = {'c', 'd'};
    static String strB = new String(chars);
    
    public static void main(String[] args) {
        System.out.println("-- " + Thread.currentThread().getStackTrace()[1].getClassName() + "--");

        System.out.println(" I. - concat(String):" + strA.concat(strB));
        System.out.println(" I. - +(String):" + strA + strB);
        
        System.out.println(" I. - Kann man concat durch den Konkatenationsoperator (+) ersetzen?:");
        String strC = null + strA;
        System.out.println("String strC = null + strA; WORKS ! :" + strC); //WORKS !
        //strC = strA.concat(null); //NullPointerException
        System.out.println("strA.concat(null) != null + strA; throws NullPointerException ! :" + strC);
        
        System.out.println(" - charAt(3)):" + strA.charAt(3));

        System.out.println(" - length):" + strA.length());
        

        System.out.println(" - isEmpty()  von : \"" + strA + "\" is " + strA.isEmpty());
        strA = "";
        System.out.println(" - isEmpty()  von : \"" + strA + "\" is " + strA.isEmpty());
    
        System.out.println(" - toUpperCase() von aaaa):" + "aaaa".toUpperCase());
        System.out.println(" - toLowerCase()) von AAAA:" + "AAAA".toLowerCase());
        
        System.out.println(" - \"abcd\".endsWith(ab)()): " + "abcd".endsWith("ab"));
        System.out.println(" - \"abcd\".endsWith(cd)()): " + "abcd".endsWith("cd"));

        System.out.println(" - \"abcd\".startsWith(ab)()): " + "abcd".startsWith("ab"));
        System.out.println(" - \"abcd\".startsWith(cd)()): " + "abcd".startsWith("cd"));
        
        System.out.println(" - \"abcd\".startsWith(ab), 0()): " + "abcd".startsWith("ab", 0));
        System.out.println(" - \"abcd\".startsWith(cd), 1()): " + "abcd".startsWith("ab", 1));
        
        /** 
         *  {@link java.lang.String#equals(Object)}
         */
        System.out.println("equals(Object) - Kann man die equals durch den Vergleichsoperator (==) ersetzen?:");
        System.out.println(" \"aab\".equals(\"aab\") :" + "aab".equals("aab")); //WORKS !
        System.out.println(" \"aab\".equals(\"aa\") :" + "aab".equals("aa")); //WORKS !
        strC = null;
        strB = null;
        System.out.println(" (null == null :" + (null == null)); //WORKS !
        System.out.println(" (strC == strB) :" + (strC == strB)); //WORKS !
        //boolean test = strC.equals(null); //NOT WORKING !

        System.out.println(" strC.equals(null) : nullPointerException !" ); //NOT WORKING !
        System.out.println(" \"AA\".equalsIgnoreCase(\"aa\") :" + "AA".equalsIgnoreCase("aa")); 

        System.out.println(" \"Abcdef\".indexOf('b') :" + "Abcdef".indexOf('b'));
        System.out.println(" \"Abcdef\".indexOf('b', 2):" + "Abcdef".indexOf('b', 2));
        System.out.println(" \"Abcdef\".indexOf('b', 1):" + "Abcdef".indexOf('b', 1)); 
        System.out.println(" \"Abcdef\".indexOf(\"bc\"):" + "Abcdef".indexOf("bc", 0));

        System.out.println(" \"Abcdef\".- lastIndexOf('e'):" + "Abcdef".lastIndexOf('e'));
        System.out.println(" \"Abcdef\".- lastIndexOf('e',4 ):" + "Abcdef".lastIndexOf('e', 4));
        System.out.println(" \"Abcdef\".- lastIndexOf('e',3):" + "Abcdef".lastIndexOf('e', 3));

        System.out.println(" \"Abcdef\".- lastIndexOf(\"ef\"):" + "Abcdef".lastIndexOf("ef"));
        System.out.println(" \"Abcdef\".- lastIndexOf(\"ef\"):" + "Abcdef".lastIndexOf("ef", 4));
        System.out.println(" \"Abcdef\".- lastIndexOf(\"ef\"):" + "Abcdef".lastIndexOf("ef", 3));

        System.out.println(" \"Abcdef\".replace('f', 'F'):" + "Abcdef".replace('f', 'F'));

        System.out.println(" \"Abcdef\".substring(1):" + "Abcdef".substring(1));
        System.out.println(" \"Abcdef\".substring(1, 2):" + "Abcdef".substring(1, 2));
        
        System.out.println(" String.valueOf(true):" + String.valueOf(true));
        System.out.println(" String.valueOf('b'):" + String.valueOf('b'));
        System.out.println(" String.valueOf({'a','b'}):" + String.valueOf(new char[] {'a','b'}));
        System.out.println(" String.valueOf(}):" + String.valueOf(new char[] {'a','b'}));
        System.out.println(" String.valueOf(Object}):" + String.valueOf(new Date()));
    }
}
