/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.strings;

import java.util.Arrays;

/**
 *
 * @author kgalba
 */
public class Aufgabe_String_Namen {

    public static void main(String[] args) {
        System.out.println("-- " + Thread.currentThread().getStackTrace()[1].getClassName() + "--");
        
        System.out.println(" I: Silben:" + Arrays.asList(generateSyllables()));
        
        System.out.println(" II. Name:" + generateName(generateSyllables(), 10));
        
        System.out.println(" III. 50 Namen:");
        for (int i = 0; i < 50; i++) {
            System.out.println("Name" +i + ": " +generateName(generateSyllables(), 10));
        }
    }

    static String[] generateSyllables() {
        String[] syllables = {
            "bereit-",
            "bloß-",
            "breit-",
            "fehl-",
            "fern-",
            "fest-",
            "frei-",
            "für-",
            "gleich-",
            "gut-",
            "heim-",
            "hoch-",
            "irre-",
            "kalt-",
            "kaputt-",
            "klar-",
            "krank-",
            "kund-",
            "kurz-",
            "lang-",
            "lieb-",
            "miss-",
            "rück-",
            "scharf-",
            "schön-",
            "schräg-",
            "schwarz-",
            "statt-",
            "still-",
            "teil-",
            "tief-",
            "tot-",
            "wahr-",
            "weich-",
            "weis-",
            "wett-",
            "wohl-",
            "zwangs-"};

        return syllables;
    }

    static String generateName(String[] syllables, int length) {
        String name = "";
        java.util.Random random = new java.util.Random();
        for (int i = 0; i < length; i++) {
            name += syllables[random.nextInt(length)];
        }
        return name;
    }
}
