/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.methoden;

/**
 *
 * @author kgalba
 */
public class Aufgaben {

    public static void main(String[] args) {
        System.out.println("1. printFromTo");
        printFromTo(20, 30);

        //2.
        System.out.println("");
        System.out.println("2. sum");
        System.out.println(sum(20, 30));

        System.out.println("");
        System.out.println("3. printFromTo");
        System.out.println(sumFromTo(10, 13));

        System.out.println("4. zeichneRechteck");
        zeichneRechteck(5, 4);

        System.out.println("");
        System.out.println("5. zeichneRechteck fill yes");
        zeichneRechteck(5, 4, true);

        System.out.println("");
        System.out.println("5. zeichneRechteck fill no");
        zeichneRechteck(5, 4, false);
    }

    private static void printFromTo(int from, int to) {
        for (int i = from; i <= to; i++) {
            System.out.print(" " + i);
        }
    }

    private static int sum(int from, int to) {
        return from + to;
    }

    private static int sumFromTo(int from, int to) {
        int result = 0;
        for (int i = from; i <= to; i++) {
            //System.out.println("result:" + result);
            result += i;
        }
        return result;
    }

    private static void zeichneRechteck(int breite, int hoehe) {

        for (int i = 0; i < breite; i++) {
            System.out.println("");
            for (int j = 0; j < hoehe; j++) {
                System.out.print("*");
            }
        }
    }

    private static void zeichneRechteck(int breite, int hoehe, boolean fill) {

        for (int h = 0; h < hoehe; h++) {
            System.out.print("+");
            for (int b = 0; b < breite; b++) {

                if (h == 0 && !fill) {
                    System.out.print("*");
                }

                if (fill) {
                    System.out.print("*");
                    continue;
                } else {
                    System.out.print(" ");
                };

                if (h == hoehe-1 && !fill) {
                    System.out.print("*");
                }
            }
            System.out.println("+");
        }
    }
}
