package kgalba.iterable;

import kgalba.comparablekg.Helper;

/**
 *
 * @author kgalba
 */
public class PersonBean {

    private String forName;
    private String surName;

    public PersonBean(String forName, String surName) {
        this.forName = forName;
        this.surName = surName;
    }

    public String getForName() {
        return forName;
    }

    public void setForName(String forName) {
        this.forName = forName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String toString() {
        return "I'm the " + this.getClass().getSimpleName()
                + " with ID:" + this.hashCode() + "\nMy properties are: "
                + getProperties()
                + "\n";
    }

    private String getProperties() {
        return "forName: " + getForName() + " surName: " + getSurName();
    }

}
