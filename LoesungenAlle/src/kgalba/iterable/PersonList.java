/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.iterable;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author kgalba
 */
public class PersonList implements Iterable<PersonBean>{

    public PersonList() {
        list = new ArrayList<PersonBean>();
    }
    ArrayList<PersonBean> list; //10 not needed, because it's default

    
    @Override
    public Iterator<PersonBean> iterator() {
        Iterator<PersonBean> iprof = list.iterator();
        return iprof; 
    }

    void add(PersonBean p) {
        this.list.add(p);
    }
 
}
