package kgalba.iterable;

/**
 *
 * @author kgalba
 */
public class TestPersonenList {

    public static void main(String[] args) {
        PersonList list = new PersonList();
        list.add(new PersonBean("Peter", "Braun"));
        list.add(new PersonBean("Michael", "Roth"));

        for (PersonBean p : list) {
            System.out.println(p);
        }
    }
}
