package kgalba.serialize;

/**
 *
 * @author kgalba
 */
    class Defragmentor extends Service {

    public Defragmentor(int timedelta, String drive) {
        super(MemoryManager.class.getName());
        this.timedelta = timedelta;
        this.drive = drive;
    }

    int timedelta;
    String drive;

    public Defragmentor(String name) {
        super(name);
    }
    
    
    public String toString() {
        return "Defrag-Dienst Zeitabstand(" + this.timedelta + ") LW(" + this.drive +")";
    }
}