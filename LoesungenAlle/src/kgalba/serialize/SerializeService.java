package kgalba.serialize;

/**
 *
 * @author kgalba
 */
public class SerializeService {

    public static void main(String[] args) {

        MemoryManager mm = new MemoryManager(2000, new Defragmentor(3000, "C:\\"));

        mm.serialize();
//
        MemoryManager sManager2 = mm.unserialize();
        System.out.println(sManager2.toString());
    }    
}

