package kgalba.serialize;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kgalba
 */
class MemoryManager extends Service implements Serializable {

    public static final String RUNTIME_DIR = "runtime/serialize/";

    public static final String SER_FILE_NAME = RUNTIME_DIR + "sManager.ser";

    transient String name;
    int size;
    transient Defragmentor defrag;

    public MemoryManager(int size, Defragmentor defrag) {
        super(MemoryManager.class.getName());
        this.size = size;
        this.defrag = defrag;
    }

    public MemoryManager() {
        super(MemoryManager.class.getName());
    }

    public String toString() {
        return "Manager. Size:" + this.size + " " + defrag;
    }

    void serialize() {
        try {
            FileOutputStream fs = new FileOutputStream(SER_FILE_NAME);
            ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(this);
            os.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MemoryManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MemoryManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    MemoryManager unserialize() {
        FileInputStream fis;
        MemoryManager unserialize = null;
        try {
            fis = new FileInputStream(SER_FILE_NAME);
            ObjectInputStream ois = new ObjectInputStream(fis);
            unserialize = (MemoryManager) ois.readObject();
            ois.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(MemoryManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(MemoryManager.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(MemoryManager.class.getName()).log(Level.SEVERE, null, ex);
        }

        return unserialize;
    }

    private void writeObject(ObjectOutputStream oos) throws IOException {
        //System.out.println("MemoryManager / writeObject" + defrag.drive);
        oos.defaultWriteObject();
        oos.writeInt(defrag.timedelta);
        oos.writeUTF(defrag.drive);
    }

    private void readObject(ObjectInputStream ois) throws IOException, ClassNotFoundException {
       
        ois.defaultReadObject();
        int defragTimedelta = ois.readInt();
        String defragDrive = ois.readUTF();
        //System.out.println("MemoryManager / readObject defragDrive" +defragDrive);

        defrag = new Defragmentor(defragTimedelta, defragDrive);
    }
}
