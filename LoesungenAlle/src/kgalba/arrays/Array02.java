/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.arrays;

import java.util.Random;
import static kgalba.arrays.Array01.createArray;

/**
 *
 * @author kgalba
 */
public class Array02 {

    public static void main(String[] args) {
        //a)
        System.out.println("Part c:");

        int a[][] = createArray(4, 5);
        printArray(a);

        //b
        System.out.println("Part b:");
        int b[][] = setValue(a, 1, 1, 2);
        printArray(b);

        //c
        System.out.println("Part c:");
        int c[][] = setAllValues(b, 2);
        printArray(c);
    }

    static int[][] createArray(int width, int height) {
        int arr[][] = new int[width][height];

        return arr;
    }

    static int[][] setValue(int[][] arr, int x, int y, int value) {
        arr[x][y] = value;
        return arr;
    }

    static int[][] setAllValues(int[][] arr, int value) {

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                arr[i][j] = value;

            }
        }

        return arr;

    }

    static void printArray(int[][] arr) {
        //breite
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j]);
                if (j < arr.length) {
                    System.out.print(",");
                }
            }
            System.out.println("");
        }
    }
}
