/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.arrays;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author kgalba
 */
public class IntMatrix {

    int heigth, width, value;
    int[][] matrix;

    public IntMatrix(int width, int heigth) {
        this.heigth = heigth;
        this.width = width;
        this.matrix = new int[width][heigth];
    }

    public IntMatrix(int width, int heigth, int value) {
        this(width, heigth);
        this.value = value;
        setAllValuesSame(value);
    }

    public IntMatrix(int width, int heigth, int value, boolean random) {
        this(width, heigth, value);
        if (random) {
            System.out.println("random");
            setAllValuesRandom();
        } else {
            setAllValuesSame(value);
        };
    }

    public void setValue(int x, int y, int value) {
        this.matrix[x][y] = value;
    }

    public void setAllValuesSame(int value) {
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < heigth; j++) {
                this.setValue(i, j, value);
            }
        }
    }

    public int get(int x, int y) {
        return this.matrix[x][y];
    }

    public String toString() {
        String matStr = "";

        for (int w = 0; w < matrix.length; w++) {
            for (int h = 0; h < matrix[w].length; h++) {
                matStr += matrix[w][h];
                if (h < matrix.length) {
                    matStr += ",";
                }
            }
            matStr += "\n";
        }
        return matStr;
    }

    private void setAllValuesRandom() {
        Random r = new Random();
        System.out.println("DEBUG:" + this.value);
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < heigth; j++) {
                this.setValue(i, j, r.nextInt(this.value));
            }
        }
    }

    public boolean equals(IntMatrix mat) {
        boolean equals = false;
        
        if (this.heigth != mat.heigth || this.width != mat.width) {
            return false;
        }
        
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < heigth; j++) {
                equals = this.matrix[i][j] == mat.matrix[i][j];
                if (!equals) return false;
            }
        }
        return equals;
    }
}
