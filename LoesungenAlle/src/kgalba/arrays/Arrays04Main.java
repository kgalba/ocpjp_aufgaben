/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.arrays;

/**
 *
 * @author kgalba
 */
public class Arrays04Main {
    public static void main(String[] args) {
            
        System.out.println("AufgabeArrays04_IntMatrix: ");
        
        System.out.println("I. ");
        IntMatrix iMatrix = new IntMatrix(2, 3);
        System.out.println(iMatrix);
        
        System.out.println("II. ");
        iMatrix = new IntMatrix(5, 3, 100);
        System.out.println(iMatrix);
        
        System.out.println("III. ");
        iMatrix = new IntMatrix(5, 3, 666);
        int i = iMatrix.get(0, 1);
        System.out.println("Wert an 1,2:" +i);
        
        System.out.println("IV. ");
        IntMatrix m3 = new IntMatrix(4, 6, 200, true);
        System.out.println(m3);
        
        System.out.println("V. ");
        IntMatrix m4 = new IntMatrix(4, 6, 200);
        IntMatrix m5 = new IntMatrix(4, 6, 200);
        IntMatrix m6 = new IntMatrix(4, 6, 2);
        IntMatrix m7 = new IntMatrix(4, 5, 2);

        System.out.println(m4.equals(m5));
        System.out.println(m5.equals(m6));
        System.out.println(m6.equals(m7));

        System.out.println("VII. Overflow");
        IntMatrix m8 = new IntMatrix(Integer.MAX_VALUE, Integer.MAX_VALUE, 200);
    }
}
