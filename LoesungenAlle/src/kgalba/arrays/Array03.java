/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.arrays;

import java.util.Random;
import static kgalba.arrays.Array01.createArray;

/**
 *
 * @author kgalba
 */
public class Array03 {

    public static void main(String[] args) {
        //a)
        System.out.println("Part a:");

        char a[][] = createBorderedArray(4, 'X');
        printArray(a);

    }

    static char[][] createBorderedArray(int dim, char c) {
        char arr[][] = new char[dim][dim];
        setBorder(arr, c);
        return arr;
    }

    static char[][] setValue(char[][] arr, int x, int y, char value) {
        arr[x][y] = value;
        return arr;
    }

    static char[][] setBorder(char[][] arr, char value) {

        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                    arr[i][j] = (isBorder(arr, i, j)) ? 'X' :'_';
            }
        }   
        return arr;

    }
    
    static boolean isBorder(char[][] arr, int x, int y) {
        boolean b = false;
        if ((x == 0 || x == arr.length-1) || (y == 0 || y == arr[0].length-1 )) {
//            System.out.println("isBorder: x:" + x + " y: " +y );
            return true;
        }
        return b;
    }
    
    static void printArray(char[][] arr) {
        //breite
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                System.out.print(arr[i][j]);
                if (j < arr.length) {
                    System.out.print(",");
                }
            }
            System.out.println("");
        }
    }
}
