/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.arrays;

import java.util.Random;

/**
 *
 * @author kgalba
 */
public class Array01 {

    public static void main(String[] args) {

        int a[] = createArray(2, 15, 30);

        printArray(a);

    }

    static int[] createArray(int start, int end, int l) {
        Random r = new Random();
        int arr[] = new int[l];

        for (int i = 0; i < l; i++) {
            arr[i] = r.nextInt(end - start + 1) + start;
        }

        return arr;
    }

    static void printArray(int[] arr) {
        System.out.println("array: " + java.util.Arrays.toString(arr));

    }
}
