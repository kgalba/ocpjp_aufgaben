package kgalba.filter;

/**
 *
 * @author kgalba
 */
public interface Filter {
    boolean accept(Person p);
}
