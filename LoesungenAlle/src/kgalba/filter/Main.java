package kgalba.filter;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author kgalba
 */
public class Main {

    static Person[] filter(Person[] persons, Filter filter) {

        ArrayList<Person> filtered = new ArrayList<>();

        for (Person person : persons) {
            if (filter.accept(person)) {
                filtered.add(person);
            }
        }
        return filtered.toArray(new Person[0]);
    }

    public static void main(String[] args) {
        System.out.println("-------------------------------------------------");
        Person[] persons = Person.generatePersons(20);

        System.out.println("All persons: \n" + Arrays.deepToString(persons));

        System.out.println("--- accept nothing -------------------------------");

        //Filter instance f, with anonymous class
        Filter f = new Filter() {
            public boolean accept(Person p) {
                return false;
            }
        };
        Person[] filtered = filter(persons, f);
        System.out.println("Filtered persons: \n" + Arrays.deepToString(filtered));

        System.out.println("--- accept all -------------------------------");

        //anonymous class as Filter param
        filtered = filter(persons, new Filter() {
            public boolean accept(Person p) {
                return true;
            }
        });
        System.out.println("Filtered persons: \n" + Arrays.deepToString(filtered));

        System.out.println("--- accept younger 10 ------------------------------");
        

        //anonymous class as Filter param
        filtered = filter(persons, new Filter() {
            public boolean accept(Person p) {
                if (p.getAge() < 10) {
                    return true;
                } else {
                    return false;
                }

            }
        });
        System.out.println("Filtered persons: \n" + Arrays.deepToString(filtered));
    }
}
