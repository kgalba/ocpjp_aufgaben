package kgalba.filter;

import java.util.Random;
import kgalba.comparablekg.Helper;

/**
 *
 * @author kgalba
 */
public class Person extends Helper implements Comparable<Object> {

    public String foreName;

    public String getForeName() {
        return foreName;
    }

    public void setForeName(String foreName) {
        this.foreName = foreName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    public String surName;
    public int age;

    public static Person[] generatePersons(int count) {
        Random random = new Random();

        Person[] persons = new Person[count];
        String pre = "Vorname-";
        String sur = "Nachname-";
        NameGenerator names = new NameGenerator();
        for (int i = 0; i < persons.length; i++) {
            pre = NameGenerator.generateName(2);
            sur = NameGenerator.generateName(3);
            int year = random.nextInt(100);

            persons[i] = new Person(pre, sur,
                    year);
        }
        return persons;
    }

    public Person(String vor, String nach, int birth) {
        this.foreName = vor;
        this.surName = nach;
        this.age = birth;
    }

    @Override
    /**
     * meaningless compareTo just to implement Comparable
     */
    public int compareTo(Object o) {
        return Integer.compare(this.hashCode(), o.hashCode());
    }
}
