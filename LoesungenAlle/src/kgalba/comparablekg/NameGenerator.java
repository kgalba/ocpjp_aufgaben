/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.comparablekg;

import java.util.Arrays;

/**
 *
 * @author kgalba
 */
public class NameGenerator {

    public static final String[] SYLLABLES = {
            "bereit-",
            "bloß-",
            "breit-",
            "fehl-",
            "fern-",
            "fest-",
            "frei-",
            "für-",
            "gleich-",
            "gut-",
            "heim-",
            "hoch-",
            "irre-",
            "kalt-",
            "kaputt-",
            "klar-",
            "krank-",
            "kund-",
            "kurz-",
            "lang-",
            "lieb-",
            "miss-",
            "rück-",
            "scharf-",
            "schön-",
            "schräg-",
            "schwarz-",
            "statt-",
            "still-",
            "teil-",
            "tief-",
            "tot-",
            "wahr-",
            "weich-",
            "weis-",
            "wett-",
            "wohl-",
            "zwangs-"};
;
    
    public NameGenerator() {
    }

    public static void main(String[] args) {
        System.out.println("-- " + Thread.currentThread().getStackTrace()[1].getClassName() + "--");
        
        System.out.println(" I: Silben:" + Arrays.asList(SYLLABLES));
        
        System.out.println(" II. Name:" + generateName(10));
        
        System.out.println(" III. 50 Namen:");
        for (int i = 0; i < 50; i++) {
            System.out.println("Name" +i + ": " +generateName(10));
        }
    }


    static String generateName(int length) {
        String name = "";
        java.util.Random random = new java.util.Random();
        for (int i = 0; i < length; i++) {
            name += SYLLABLES[random.nextInt(length)];
        }
        return name;
    }
}
