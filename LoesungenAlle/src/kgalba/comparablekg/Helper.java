
package kgalba.comparablekg;

import java.lang.reflect.Field;

/**
 * Collection of helper methods
 * @author kgalba
 */
public abstract class Helper {

    public String toString() {
        return "I'm the " + this.getClass().getSimpleName()
                + " with ID:" + this.hashCode() + "\nMy properties are: "
                + getProperties()
                +"\n";
    }

    public String getProperties() {
        
        Field[] fields = getClass().getDeclaredFields();
        String propertyMap = "";
        for (Field field : fields) {
            try {
//                System.out.println("field" + field);
                propertyMap += field.getName() + ": " + field.get(this) +" ";
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            } 
        }
        return propertyMap;
    }
}
