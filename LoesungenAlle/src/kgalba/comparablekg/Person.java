package kgalba.comparablekg;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author kgalba
 */
public class Person extends Helper implements Comparable<Object> {

    public String foreName;
    public String surName;
    public int age;

    public static void main(String[] args) {

        Person[] persons = generatePersons(10);

        System.out.println("-------------------------------------------------");
        System.out.println("Sort via  Comparable<Person>");
        System.out.println("Unsorted: \n" + Arrays.asList(persons));
        Arrays.sort(persons);
        System.out.println("Sorted:" + Arrays.asList(persons));

        System.out.println("-------------------------------------------------");
        System.out.println("Via  BirthComparator<Person>");
        System.out.println("Unsorted:" + Arrays.asList(persons));
        Arrays.sort(persons, new BirthComparator());
        System.out.println("Sorted:" + Arrays.asList(persons));

        System.out.println("-------------------------------------------------");
        System.out.println("Via  AllFieldsComparator<Person>");
        System.out.println("Unsorted:" + Arrays.asList(persons));
        Arrays.sort(persons, new AllFieldsComparator());
        System.out.println("Sorted:" + Arrays.asList(persons));
    }

    public static Person[] generatePersons(int count) {
        Random random = new Random();

        Person[] persons = new Person[count];
        String pre = "Vorname-";
        String sur = "Nachname-";
        NameGenerator names = new NameGenerator();
        for (int i = 0; i < persons.length; i++) {
            pre = NameGenerator.generateName(2);
            sur = NameGenerator.generateName(3);
            int year = random.nextInt(100);

            persons[i] = new Person(pre, sur,
                    year);
        }
        return persons;
    }

    public Person(String vor, String nach, int birth) {
        this.foreName = vor;
        this.surName = nach;
        this.age = birth;
    }

    @Override
    /**
     * meaningless compareTo just to implement Comparable
     */
    public int compareTo(Object o) {
        return Integer.compare(this.hashCode(), o.hashCode());
    }
    
    public String toString() {
        return "Person .foreName " + foreName +" .surName " + surName + " .age "+age;
    }
}
