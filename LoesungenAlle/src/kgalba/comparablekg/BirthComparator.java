package kgalba.comparablekg;

import java.util.Comparator;

/**
 * Comparator for order by age
 *
 * @author kgalba
 */
public class BirthComparator implements Comparator<Person> {

    public int compare(Person o1, Person o2) {
        return Integer.compare(o1.age, o2.age);
    }
}