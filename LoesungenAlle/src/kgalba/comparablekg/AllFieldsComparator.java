package kgalba.comparablekg;

import java.util.Comparator;

/**
 * Comparator for order by age
 *
 * @author kgalba
 */
public class AllFieldsComparator implements Comparator<Person> {

    public int compare(Person o1, Person o2) {
        String o1Str = o1.foreName + o1.surName + String.valueOf(o1.age);
        String o2Str = o2.foreName + o2.surName + String.valueOf(o2.age);

        return o1Str.compareTo(o2Str);
    }
}