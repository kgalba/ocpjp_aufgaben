package kgalba.gui;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JToggleButton;

/**
 *
 * @author kgalba
 */
public class FirstJFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Mein Fenster");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout( new BorderLayout(3, 3) );

        JToggleButton button = new JToggleButton("0,0");
        button.invalidate();
        
        frame.add( button, BorderLayout.PAGE_START );
                frame.add( button, BorderLayout.PAGE_START );


        frame.setSize(300, 200);
        frame.setLocationRelativeTo(null);

        frame.setVisible(true);
        
        new JButton();

        System.out.println("end of main");
    }
}
