package kgalba.tictactoe.model;

import java.io.Serializable;

/**
 *
 * @author werkstatt
 */
public abstract class Player implements Serializable {

    protected Symbol symbol;
    protected String type;
    protected Board board;

    public Player(Symbol symbol, Board board) {
        this.symbol = symbol;
        this.board = board;
    }
    
    abstract int[] move();
    
    /**
     * @param symbol the symbol to set
     */
    public void setSymbol(Symbol symbol) {
        this.symbol = symbol;
    }

    public Symbol getSymbol() {
        return symbol;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }
    
    public String toString() {
    
        return this.getClass().getSimpleName()+ "("+getSymbol().toString() +")";
    }
    

}
