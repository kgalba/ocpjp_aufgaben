package kgalba.tictactoe.model;

import kgalba.tictactoe.view.Cli;

/**
 *
 * @author werkstatt
 */
public class Human extends Player {

    public Human(Symbol symbol, Board board) {
        super(symbol, board);
    }


    int[] move() {
        int[] move = new int[2];
        do {
            move = new Cli().requestHumanMove();
            //System.out.println("DEBUG: move " + Arrays.toString(move));

        } while (move == null || !board.isValid(move));

        return move;
    }
}
