package kgalba.tictactoe.model;

import java.util.Random;
import java.util.ArrayList;

/**
 *
 * @author werkstatt
 */
public class Computer extends Player {

    public Computer(Symbol symbol, Board board) {
        super(symbol, board);
    }
    
    
    int[] move() {
        ArrayList<int[]> emptySquares = board.getEmptySquares();
        //@todo smarter nextMove
        int nextMove = new Random().nextInt(emptySquares.size());

        return emptySquares.get(nextMove);
    }
    
}
