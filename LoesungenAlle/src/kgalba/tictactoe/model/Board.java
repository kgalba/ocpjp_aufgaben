package kgalba.tictactoe.model;

import java.awt.Point;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;
import kgalba.tictactoe.model.Symbol;
import kgalba.tictactoe.view.Cli;

/**
 * container for the game - tableau
 *
 * @author kgalba
 */
public class Board implements Serializable {

    
    public static final int DIMENSIONS = 3;
    public static final int[] TOP_LEFT = new int[]{0, 0};
    public static final int[] BOTTOM_RIGHT = new int[]{DIMENSIONS, DIMENSIONS};
    public static final String RUNTIME_DIR = "runtime/serialize/";

    public static final String SER_FILE_NAME = RUNTIME_DIR + Board.class.getSimpleName() + ".ser";

    public static boolean isInBounds(int x, int y) {
        return (0 <= x && x <= Board.DIMENSIONS) && (0 <= y && y <= Board.DIMENSIONS);
    }
    private Player[][] tableau = new Player[DIMENSIONS][DIMENSIONS];

    public ArrayList<Player[]> getDiagonals(GameLogic gameLogic) {
        ArrayList<Player[]> diagonals = new ArrayList(2);
        diagonals.add(getDiagonal(TOP_LEFT));
        diagonals.add(getDiagonal(BOTTOM_RIGHT));
        return diagonals;
    }

    private Player[] getDiagonal(int[] start) {
        Player[] diagonal = new Player[Board.DIMENSIONS];

        if (!Arrays.equals(start, TOP_LEFT) && !Arrays.equals(start, BOTTOM_RIGHT)) {
            throw new IllegalArgumentException(
                    String.format(
                            "Start has to be [0,0] or [%1$d,%1$d] ! current: [%2$d,%3$d]",
                            DIMENSIONS,
                            start[0], start[1])
            );
        }

        for (int i = 0; i < DIMENSIONS; i++) {
            if (start == TOP_LEFT) {
                diagonal[i] = tableau[i][i];
            }
            if (start == BOTTOM_RIGHT) {
                diagonal[i] = tableau[i][DIMENSIONS - 1 - i];
            }
        }
        return diagonal;
    }

    public void setSquare(int x, int y, Player player, GameLogic gameLogic) {
        if (isSquareEmpty(x, y) && isInBounds(x, y)) {
            tableau[x][y] = player;
        } else {
            new Cli().showIllegalInput("Square (" + x + "," + ")not Empty OR not an board !");
        }
    }

    boolean isSquareEmpty(int x, int y) {
        return tableau[x][y] == null;
    }

    public Player[] getColumn(int y, GameLogic gameLogic) {
        Player[] column = new Player[DIMENSIONS];
        for (int i = 0; i < DIMENSIONS; i++) {
            column[i] = tableau[i][y];
        }
        return column;
    }

    public Player[] getRow(int x, GameLogic gameLogic) {
        return tableau[x];
    }

    public ArrayList<int[]> getEmptySquares() {
        ArrayList<int[]> emptySquares = new ArrayList();
        for (int x = 0; x < tableau.length; x++) {
            for (int y = 0; y < tableau[x].length; y++) {
                if (tableau[x][y] == null) {
                    emptySquares.add(new int[]{x, y});
                }
            }
        }
        return emptySquares;
    }

    public Player[][] getTableau() {
        return tableau;
    }

    boolean isPurePlayer(Player[] squares, Player player) {
        Player[] purePlayerRow = new Player[DIMENSIONS];
        Arrays.fill(purePlayerRow, player);
        return Arrays.deepEquals(squares, purePlayerRow);
    }

    public void setTableau(Player[][] tableau) {
        this.tableau = tableau;
    }

    void serialize() {
        try {
            new File(RUNTIME_DIR).mkdirs();
            new File(SER_FILE_NAME).createNewFile();

            FileOutputStream fs = new FileOutputStream(SER_FILE_NAME);
            ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(this);
            os.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Board.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Board.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String toString() {
        return Arrays.deepToString(tableau);
    }

    boolean isValid(int[] move) {
        if (isSquareEmpty(move[0], move[1])) {
            return true;
        } else {
            new Cli().showIllegalInput("" + move[0] + "," + move[1] + " NOT empty");
            return false;
        }
    }

    Board unserialize() {
        FileInputStream fis;
        Board unserialize = null;
        try {
            fis = new FileInputStream(SER_FILE_NAME);
            ObjectInputStream ois = new ObjectInputStream(fis);
            unserialize = (Board) ois.readObject();
            ois.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Board.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Board.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Board.class.getName()).log(Level.SEVERE, null, ex);
        }

        return unserialize;
    }

}
