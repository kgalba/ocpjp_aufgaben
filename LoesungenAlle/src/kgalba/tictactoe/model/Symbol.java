/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.tictactoe.model;

/**
 *
 * @author werkstatt
 */
public enum Symbol {
    X, O;

    public Symbol getOther() {
        //System.out.println("(this.ordinal() + 1) % 2" + (this.ordinal() + 1));
        return Symbol.values()[(this.ordinal() + 1) % 2];
    }
}
