package kgalba.tictactoe.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import kgalba.tictactoe.view.View;

/**
 * @todo computer vs. computer
 * @todo refactor tableau to seperate class
 * @author kgalba
 */
public class GameLogic {

    private Player[] players = new Player[2];
    private Board board = new Board();
    private final View view;
    private Player winner;
    Player currentPlayer;

    int moveCounter = 0;
    int[] move = new int[2];

    public GameLogic(View view) {
        this.view = view;
        players[0] = new Human(Symbol.X, board);
        players[1] = new Computer(Symbol.O, board);
        currentPlayer = players[0];
    }

    public Player getOpponent(Player player) {
        Player opponent = null;
        for (Player p : players) {
            if (p == player) {
                continue;
            } else {
                opponent = p;
            }
        }
        return opponent;
    }

    public Board getBoard() {
        return board;
    }

    /**
     * @todo move to Controller ?!
     *
     * @return winner winning player or null on tie
     */
    public Player gameLoop() {
        do {
            //who is next
            changePlayer();

            //make your move
            playerMove();
            view.showMove(move, currentPlayer);

            //show result on board
            view.showBoard(this);
        } while (!isWinner(currentPlayer) && !isTie());

        //winner or tie
        return (isWinner(currentPlayer)) ? currentPlayer : null;
    }

    void changePlayer() {
        if (moveCounter > 0) {
            setCurrentPlayer(getOpponent(currentPlayer));;
        }
    }

    private void playerMove() {
        move = currentPlayer.move();
        board.setSquare(move[0], move[1], currentPlayer, this);
        moveCounter++;
    }

    /**
     *
     * @param player the player to check for winner
     * @return true if player is, false otherwise
     */
    boolean isWinner(Player player) {
        if ((moveCounter > 2) && (isHorizontalWinner(player) || isVerticalWinner(player) || isDiagonalWinner(player))) {
            this.winner = player;
            return true;
        } else {
            return false;
        }
    }

    public void setBeginner() {
        ArrayList<Player> playerList = new ArrayList(Arrays.asList(players));
        Collections.shuffle(playerList);
        players = playerList.toArray(players);
        setCurrentPlayer(players[0]);
    }

    private void setCurrentPlayer(Player player) {
        this.currentPlayer = player;
    }

    private boolean isVerticalWinner(Player player) {
        Player[] column = board.getColumn(move[1], this);

        if (board.isPurePlayer(column, player)) {
            return true;
        }
        return false;
    }

    private boolean isHorizontalWinner(Player player) {
        if (board.isPurePlayer(board.getRow(move[0], this), player)) {
            return true;
        }
        return false;
    }

    private boolean isDiagonalWinner(Player player) {
        ArrayList<Player[]> diagonals = board.getDiagonals(this);
        for (Player[] diagonal : diagonals) {
            if (board.isPurePlayer(diagonal, player)) {
                return true;
            }
        }
        return false;
    }

    boolean isTie() {
        return board.getEmptySquares().isEmpty();
    }

    public Player getFirstPlayer() {
        return players[0];
    }

    public Player getSecondPlayer() {
        return players[1];
    }

    public Player getWinner() {
        return winner;
    }

    public Player[] getPlayers() {
        return players;
    }

    public void setPlayers(Player[] players) {
        this.players = players;
    }
}
