package kgalba.tictactoe.controller;

import kgalba.tictactoe.model.Board;
import kgalba.tictactoe.model.GameLogic;
import kgalba.tictactoe.model.Human;
import kgalba.tictactoe.model.Player;
import kgalba.tictactoe.view.View;

/**
 *
 * @author kgalba
 */
public class TttController {

    View view;
    
    //models
    Board board;
    GameLogic game;

    public TttController(Board board, GameLogic gameLogic, View view) {
        this.view = view;
        this.board = board;
        this.game = gameLogic;
    }

    public void actionPlay() {
        //human choice
        actionSetPlayers();
        
        //show empty board with coordinates
        view.showBoardCoordinates(game);
        
        //radom beginner 
        game.setBeginner();
        view.showPlayer(game.getFirstPlayer());
        
        //the gamePlay
        game.gameLoop();
        
        //winner ?!
        view.victoryCeremony(game.getWinner());
    }

    public void actionSetPlayers() {
        Player player1 = new Human(view.requestHumanPlayer(), board);
        Player player2 = game.getOpponent(player1);
        game.setPlayers(new Player[] {player1, player2});
        this.view.showPlayers(player1, player2);        
    } 
    
    /*
    void actionTemplate() {
        //request/receive view-input
        //update model
        //update view
    }
    */
}
