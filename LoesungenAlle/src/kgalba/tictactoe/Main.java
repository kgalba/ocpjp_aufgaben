package kgalba.tictactoe;

import kgalba.tictactoe.controller.TttController;
import kgalba.tictactoe.model.Board;
import kgalba.tictactoe.model.Computer;
import kgalba.tictactoe.model.GameLogic;
import kgalba.tictactoe.model.Human;
import kgalba.tictactoe.model.Player;
import kgalba.tictactoe.model.Symbol;
import kgalba.tictactoe.view.Cli;
import kgalba.tictactoe.view.View;

/**
 *
 * Create needed instances and start the tic tac toe MVC-App
 *
 * @author werkstatt
 */
public class Main {

    public static void main(String[] args) {
        
        //models
        Board board = new Board();
        
        //view 
        View view = new Cli();

        //businessLogic
        GameLogic gameLogic = new GameLogic(view);
        
        TttController controller = new TttController(board, gameLogic, view);
        controller.actionPlay();
    }
}
