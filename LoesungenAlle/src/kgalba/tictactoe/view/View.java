package kgalba.tictactoe.view;

import kgalba.tictactoe.model.GameLogic;
import kgalba.tictactoe.model.Player;
import kgalba.tictactoe.model.Symbol;

/**
 *
 * @author werkstatt
 */
public interface View {

    Symbol requestHumanPlayer();

    void showPlayers(Player computer, Player human);

    void showPlayer(Player player);

    void showBoard(GameLogic game);

    int[] requestHumanMove();

    void victoryCeremony(Player winner);

    void showMove(int[] move, Player player);
    
    void showBoardCoordinates(GameLogic game);
    
    void showIllegalInput(String message);

//    static public void showIllegalInput(String messsage) {
//        String msg = "Wrong input: " + messsage;
//        JOptionPane.showMessageDialog(null, msg, "Input Error", JOptionPane.ERROR_MESSAGE);
//    }
}
