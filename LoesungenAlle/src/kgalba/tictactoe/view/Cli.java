package kgalba.tictactoe.view;

import java.util.Arrays;
import java.util.Scanner;
import kgalba.tictactoe.model.Board;
import kgalba.tictactoe.model.GameLogic;
import kgalba.tictactoe.model.Player;
import kgalba.tictactoe.model.Symbol;

/**
 *
 * @author werkstatt
 */
public class Cli implements View {

    Scanner scanner = new Scanner(System.in);

    @Override
    public Symbol requestHumanPlayer() {
        String input = null;

        String symbols = Arrays.toString(Symbol.values());
        do {
            System.out.println("Choose symbol, X or O :");
            input = scanner.nextLine().toUpperCase();
            if (input.isEmpty() || !new String(symbols).contains(input)) {
                showIllegalInput(String.format("\"%1$s\"", input));
                input = null;
            }
        } while (input == null);
        return Symbol.valueOf(input);
    }

    @Override
    public void showPlayers(Player computer, Player human) {
        System.out.print("Players: ");
        System.out.print(computer);
        System.out.println(" " + human);

    }

    @Override
    public void showPlayer(Player player) {
        System.out.println("Beginner:" + player);
    }

    @Override
    public void showMove(int[] move, Player player) {
        System.out.println(player + " sets square: " + move[0] + "," + move[1]);
    }

    private void showBoard(GameLogic game, String emptyTextFormat) {
        //System.out.println("Current game:");
        Player[][] tableau = game.getBoard().getTableau();

        for (int x = 0; x < tableau.length; x++) {   //rows
            for (int y = 0; y < tableau[x].length; y++) { //cols
                String cellText = (tableau[x][y] == null) ? String.format(emptyTextFormat, x, y) : String.valueOf(tableau[x][y].getSymbol());
                System.out.print("|" + cellText);
            }
            System.out.println("|");
        }
        System.out.println("");
    }
    
    @Override
    public void showBoard(GameLogic game) {
        showBoard(game, "_");
    }
    
    @Override
    public int[] requestHumanMove() {
        int[] move = new int[2];
        System.out.println("Choose square coordinates in format <x,y> <enter>");
        String xyInput = scanner.nextLine();

        try {
            String pattern = String.format("[0-%1$d],[0-%1$d]", Board.DIMENSIONS-1);
            if (xyInput.isEmpty() || !xyInput.matches(pattern)) {
                throw new IllegalArgumentException(xyInput);
            }
            move[0] = Integer.parseInt(xyInput.substring(0, 1));
            move[1] = Integer.parseInt(xyInput.substring(2));
        } catch (Exception e) {
            showIllegalInput(e.getMessage());
            return null;
        }
        return move;
    }

    @Override
    public void victoryCeremony(Player winner) {
        String winnerName = (winner == null) ? "NOBODY" : winner.toString();
        System.out.println("And the winner is : " + winnerName +" !");
    }

    @Override
    public void showBoardCoordinates(GameLogic game) {
        System.out.println("");
        System.out.println("Board coordinates x,y for input: ");
        showBoard(game, "%1$d,%2$d");
    }
    
    @Override
    public  void showIllegalInput(String message) {
        System.out.println("!!! Illegal input: " + message + " !!! Please repeat.");
    }
}
