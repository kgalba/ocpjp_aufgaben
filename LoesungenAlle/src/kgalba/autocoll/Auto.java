package kgalba.autocoll;

import kgalba.utils.Common;

/**
 *
 * @author kgalba
 */
public abstract class Auto<T extends Auto> implements Comparable<T> {

    public int getBaujahr() {
        return baujahr;
    }

    public void setBaujahr(int baujahr) {
        this.baujahr = baujahr;
    }

    public String getModell() {
        return modell;
    }

    public void setModell(String modell) {
        this.modell = modell;
    }

    public Auto(String modell, int baujahr) {
        this.baujahr = baujahr;
        this.modell = modell;
    }
    protected int baujahr;
    protected String modell;


    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " Modell: " + modell + " Baujahr: " + baujahr; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean equals(Object obj) {
        Auto a = (Auto) obj;
        //Common.printMethodName("(this.hashCode() ==  a.hashCode())" + (this.hashCode() ==  a.hashCode()));
        return (this.hashCode() ==  a.hashCode());
    }

    @Override
    public int hashCode() {
        //Common.printMethodName("modell.hashCode()):" + modell.hashCode() + " Integer.hashCode(baujahr))" +Integer.hashCode(baujahr) );
        return modell.hashCode() + baujahr;
    }
    
    /**
     * meaningless compareTo just to implement Comparable
     */
    @Override
    public int compareTo(T a2) {
        //Common.printMethodName(a2.toString());
        return this.hashCode()-a2.hashCode(); 
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return  super.clone(); //To change body of generated methods, choose Tools | Templates.
    }

}
