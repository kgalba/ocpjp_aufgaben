package kgalba.autocoll;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.TreeSet;

/**
 *
 * @author kgalba
 */
public class Main {

    private static BMW bmw1;
    private static BMW bmw2;
    private static VW vw1;
    private static VW vw2;
    private static VW vw3;

    public static void main(String[] args) throws CloneNotSupportedException {

        System.out.println("Aufgabe \"Collections\"");
        System.out.println("--------------------------------------------------");

        solvePart2();
        System.out.println("--------------------------------------------------");

        VW[] vwArr = solvePart3();
        System.out.println("--------------------------------------------------");

        ArrayList<Collection<VW>> allVwLists = solvePart4(vwArr);
        System.out.println("--------------------------------------------------");

        solvePart5(allVwLists);
        System.out.println("--------------------------------------------------");

        ArrayList<Collection<BMW>> bmwLists = solvePart6();
        System.out.println("--------------------------------------------------");

        solvePart7(bmwLists);
        System.out.println("--------------------------------------------------");

        solvePart8(bmwLists);
        System.out.println("--------------------------------------------------");

        solvePart9(allVwLists);
        System.out.println("--------------------------------------------------");

        solvePart10(allVwLists);
        System.out.println("--------------------------------------------------");

        solvePart11(allVwLists);
        System.out.println("--------------------------------------------------");

        solvePart121314(allVwLists);
        System.out.println("--------------------------------------------------");

    }

    private static void solvePart2() {
        /*
         2. Erstellen Sie eine Instanz vom Typ VW (Golf, Baujahr 1990) und eine Instanz
         vom Typ BMW (Z4, Baujahr 2000). Überschreiben Sie die toString-Methode für
         VW und BMW so, dass die Ausgaben mit System.out.println folgende Form haben:
        
         VW. Modell: Golf, Baujahr 1990
         BMW. Modell: Z4, Baujahr 2000*/

        VW vw = new VW("Golf", 1990);
        BMW bmw = new BMW("Z4", 2000);

        System.out.println("1, & 2.");
        System.out.println(vw);
        System.out.println(bmw);

    }

    private static VW[] solvePart3() {
        /*3. Erstellen Sie 3 Instanzen von VW. Weisen Sie dabei die Adressen der Objekte
         den Referenzen vw1, vw2 und vw3 zu.*/
        vw1 = new VW("Golf", 1990);
        vw2 = new VW("Polo", 1992);
        vw3 = new VW("Jetta", 1993);
        VW[] vwArr = new VW[]{vw1, vw2, vw3, new VW("Golf", 1990)};
        System.out.println("");
        System.out.println("3.");
        System.out.println("vwArr:" + Arrays.asList(vwArr));
        return vwArr;
    }

    private static ArrayList<Collection<VW>> solvePart4(VW[] vwArr) {
        /*
         4. Speichern Sie die 3 Referenzen aus dem Aufgabenteil 3 in LinkedList, HashSet,
         TreeSet und PriorityQueue. Beim Sortieren sollen die Objekte erst nach dem
         Modell und dann nach dem Baujahr verglichen werden. Die ggf. notwendige(n)
         hashCode-Methode(n) soll(en) korrekt (richtig, gültig) aber nicht unbedingt
         sinnvoll implementiert werden.
         */
        System.out.println("");
        System.out.println("4.");
        LinkedList<VW> vwLinkedList = new LinkedList<>(Arrays.asList(vwArr));
        HashSet<VW> vwHashSet = new HashSet<>(Arrays.asList(vwArr));
        TreeSet<VW> vwTreeSet = new TreeSet<>(Arrays.asList(vwArr));
        PriorityQueue<VW> vwPriorityQueue = new PriorityQueue<>(Arrays.asList(vwArr));
        ArrayList<Collection<VW>> allVwLists = new ArrayList<>();
        allVwLists.add(vwLinkedList);
        allVwLists.add(vwHashSet);
        allVwLists.add(vwTreeSet);
        allVwLists.add(vwPriorityQueue);
        return allVwLists;
    }

    private static void solvePart5(ArrayList<Collection<VW>> allVwLists) {
        /*
         5. Geben Sie alle erstellten Collections mit den foreach-Schleifen aus.
         */
        System.out.println("");
        System.out.println("5.");

        for (Collection vwList : allVwLists) {
            System.out.println("vwListType: " + vwList.getClass().getSimpleName() + "Elements:" + vwList);
        }
    }

    private static ArrayList<Collection<BMW>> solvePart6() {
        /*6. Erstellen Sie 2 Objekte von Typ 'BWM' und referenzieren Sie sie mit bmw1 und
         bmw2. Speicher Sie die Referenzen in ArrayList, HashSet und TreeSet. Geben
         Sie die neu erstellten Collections aus.*/
        System.out.println("");
        System.out.println("6.");
        bmw1 = new BMW("Z1", 2001);
        bmw2 = new BMW("Z2", 2002);
        BMW[] bmwArr = new BMW[]{bmw1, bmw2};
        System.out.println("bmwArr:" + Arrays.asList(bmwArr));

        ArrayList<BMW> bmwArrayList = new ArrayList<BMW>(Arrays.asList(bmwArr));
        HashSet<BMW> bmwHashSet = new HashSet<BMW>(Arrays.asList(bmwArr));
        TreeSet<BMW> bmwTreeSet = new TreeSet<BMW>(Arrays.asList(bmwArr));

        ArrayList<Collection<BMW>> bmwLists = new ArrayList<>();
        bmwLists.add(bmwArrayList);
        bmwLists.add(bmwHashSet);
        bmwLists.add(bmwTreeSet);

        for (Collection bmwList : bmwLists) {
            System.out.println("bmwListType: " + bmwList.getClass().getSimpleName() + "Elements:" + bmwList);
        }

        return bmwLists;
    }

    private static void solvePart7(ArrayList<Collection<BMW>> bmwLists) {
        /*
         7. Benutzen Sie die Methode 'contains', um in dem hashSet von BMW-Objekten nach
         bmw1 zu suchen.
         */
        System.out.println("");
        System.out.println("7.");
        HashSet<BMW> bmwHashSet = (HashSet<BMW>) bmwLists.get(1);
        System.out.println(bmwLists.get(1));
        System.out.println("HashSet.contains(bmw1) ? " + bmwHashSet.contains(bmw1));
    }

    /**
     * 8. Fügen Sie der Klasse BMW die Setter-Methode für das Attribut 'baujahr' zu. Benutzen Sie die neue Methode mit
     * der Referenz bmw1 um das Baujahr zu ändern. Versuchen Sie erneut mit der Methode 'contains' in dem HashSet von
     * BMW-Objekten nach bmw1 zu suchen. Was liefert die Methode 'contains' und warum?
     */
    private static void solvePart8(ArrayList<Collection<BMW>> bmwLists) {
        System.out.println("");
        System.out.println("8.");

        bmw1.setBaujahr(1970);
        System.out.println("bmw1 after bmw1.setBaujahr(1970):" + bmw1);
        System.out.println("bmw1.hashcode(): " + bmw1.hashCode());
        System.out.println("");

        HashSet<BMW> bmwHashSet = (HashSet<BMW>) bmwLists.get(1);
        System.out.println("HashSet.contains(bmw1) ? " + bmwHashSet.contains(bmw1) + " !!");
        System.out.println("Because, list bmwHashSet:");

        System.out.println("bmwHashSet.size(): " + bmwHashSet.size());
        System.out.println("   Iterate over bmwHashSet:");
        for (BMW bmw : bmwHashSet) {
            System.out.println("Set-element: " + bmw + ", element.hashCode():" + bmw.hashCode());
            System.out.println("element ==  bmw1 ? " + (bmw == bmw1));
            System.out.println("element.hashcode() ==  bmw1.hashcode (hashcollision)? " + (bmw.hashCode() == bmw1.hashCode()));
            //System.out.println("elment.equals(bmw1): " +bmw.equals(bmw1));
            System.out.println("");
        }

        System.out.println("HashSet.contains(bmwHashSet.toArray()[1]) ? " + bmwHashSet.contains(bmwHashSet.toArray()[1]));
        System.out.println("Hashmap.getNode() returns null!, and Auto.equals() not executed !");

    }

    /**
     * 9. Erstellen Sie eine Instanz VW (Polo, Baujahr 2200) und speichern Sie ihre Adresse in der Liste aus 4.
     * Speichern Sie dabei die Adresse in keiner weiteren Referenz.
     *
     * @param bmwLists
     */
    private static void solvePart9(ArrayList<Collection<VW>> allVwLists) {
        System.out.println("");
        System.out.println("9.");
        System.out.println("");

        //save to all lists
        for (Collection vwList : allVwLists) {
            vwList.add(new VW("Polo", 2200));
            System.out.println("Lists after vwList.add(new VW(\"Polo\", 2200)):");
            System.out.println("ListType: " + vwList.getClass().getName());
            System.out.println("List elements: " + vwList);
            System.out.println("");
        }
    }

    /**
     * 10.Benutzen Sie die Methode 'binarySearch' aus der Klasse 'Collections' und suchen Sie nach einem VW Polo,
     * Baujahr 2200 in der Liste aus 9. Geben Sie das Ergebnis aus.
     *
     * @param allVwLists
     */
    private static void solvePart10(ArrayList<Collection<VW>> allVwLists) {
        System.out.println("");
        System.out.println("10.");
        System.out.println("");

        //all lists
        for (Collection vwList : allVwLists) {
            if (vwList instanceof List) {
                System.out.println("ListType: " + vwList.getClass().getName());
                System.out.println("List elements: " + vwList);
                System.out.println("");
                System.out.println("Usorted binarySearch: " + Collections.binarySearch((List) vwList, new VW("Polo", 2200)));

            }
        }
    }

    /**
     *
     * 11.Benutzen Sie die Methode 'sort' aus der Klasse 'Collections' um die Liste aus dem Aufgabenpunkt 9 zu
     * sortieren. Geben Sie die sortierte Liste aus.
     *
     * @param allVwLists
     */
    private static void solvePart11(ArrayList<Collection<VW>> allVwLists) {
        System.out.println("");
        System.out.println("11.");
        System.out.println("");

        System.out.println("sort list");
        LinkedList<VW> vwLinkedList = (LinkedList<VW>) allVwLists.get(0);
        Collections.sort(vwLinkedList);
        System.out.println("List elements: " + vwLinkedList);

        System.out.println("Sorted binarySearch of  new VW(\"Polo\", 2200): " + Collections.binarySearch((List) vwLinkedList, new VW("Polo", 2200)));
    }

    /**
     * 12.Benutzen Sie die Methode 'sort' aus der Klasse 'Collections' um die Liste aus dem Aufgabenpunkt 9 in der
     * Umkehrreihenfolge zu sortieren. Geben Sie die Liste aus.
     *
     * @param allVwLists
     */
    private static void solvePart121314(ArrayList<Collection<VW>> allVwLists) {
        LinkedList<VW> vwList = (LinkedList<VW>) allVwLists.get(0);
        System.out.println("");
        System.out.println("12.");
        System.out.println("");
        //all lists

        System.out.println("ListType: " + vwList.getClass().getName());
        System.out.println("List elements: " + vwList);
        System.out.println("");
        System.out.println("sort reverse :");
        
        Comparator<VW> reverseComparator = Collections.reverseOrder();
        vwList.sort(reverseComparator);
        System.out.println("reversed list : " + vwList);

        System.out.println("--------------------------------------------------");
        System.out.println("");
        System.out.println("13.");
        System.out.println("");
        System.out.println("Pos of  binarySearch of new VW(\"Polo\", 2200): " + Collections.binarySearch((List) vwList, new VW("Polo", 2200)));

        System.out.println("--------------------------------------------------");
        System.out.println("");
        System.out.println("14.");
        System.out.println("");
        System.out.println("Pos of  binarySearch of new VW(\"Polo\", 3300): " + Collections.binarySearch((List) vwList, new VW("Polo", 3300)));

    }
}
