package kgalba.building;

/**
 * Solution to  jse_066260 / Aufgaben / aufgabe_InnereKlassen_Gebaeude.txt 
 * 
 * @author kgalba
 */
public class Main {

    public static void main(String[] args) {
        Building b = new Building("Hauptstrasse", 45, 3, 10);
        Building.Floor.Room room = b.getRoom(0, 2);
        System.out.println(room);
    }
}
