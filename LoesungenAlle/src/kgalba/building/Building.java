package kgalba.building;

/**
 * Solution to  jse_066260 / Aufgaben / aufgabe_InnereKlassen_Gebaeude.txt 
 * 
 * @author kgalba
 *
 */
public class Building {

    String street;
    int houseNumber;
    int roomCount;
    Floor[] floors; //floors in building

    public Building(String street, int houseNumber, int floorCount, int roomCount) {
        this.street = street;
        this.houseNumber = houseNumber;
        this.floors = new Floor[roomCount];
        for (int i = 0; i < this.floors.length; i++) {
            this.floors[i] = new Floor(roomCount, i);
        }
    }

    Floor.Room getRoom(int floorNumber, int roomNumber) {
        return floors[floorNumber].rooms[roomNumber];
    }

    class Floor {

        Room[] rooms;   //rooms on floor
        int floorNumber;

        public Floor(int roomCount, int floorNumber) {
            this.rooms = new Room[roomCount];
            this.floorNumber = floorNumber;

            for (int i = 0; i < this.rooms.length; i++) {
                rooms[i] = new Room(i);
            }
        }

        class Room {

            int roomNumber;

            public Room(int roomNumber) {
                this.roomNumber = roomNumber;
            }

            public String toString() {
                return "Full Roomadress: " + Building.this.street + " "
                        + Building.this.houseNumber + " "
                        + "FloorNr: " + Floor.this.floorNumber
                        + " RoomNr: " + Integer.toString(roomNumber);
            }
        }
    }

}
