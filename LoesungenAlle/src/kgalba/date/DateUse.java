package kgalba.date;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author kgalba
 */
public class DateUse {
    public static void main(String[] args) {
        Date date = new Date();
        System.out.println("Now: " + date);
        System.out.println("Now + 1day: " + addDay(date));
        
        
        // aufgabe_Date_DateFormat_Locale_Api.txt
        DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, Locale.ENGLISH);
        System.out.println("DateFormat English FULL: " + df.format(date));
        
        df = DateFormat.getDateInstance(DateFormat.DEFAULT, Locale.GERMAN);
        System.out.println("DateFormat Deutsch default: " + df.format(date));

    }
    
    static Date addDay(Date date) {
        Long time = date.getTime();
        Long dayInMillis = 24*3600*1000L;
        date.setTime(time+dayInMillis);
        return date;
    }
}
