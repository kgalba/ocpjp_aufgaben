package kgalba.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author kgalba
 */
public class NumericChecker {
    public static boolean isDecimalLiteral(String number) {
        String pattern = "[-+]?[0-9]*\\.?[0-9]*";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(number);
        return m.matches();
    }
    
    public static void main(String[] args) {
        NumericChecker nc = new NumericChecker();
        System.out.println("nc.isDecimalLiteral(\"10\")" +nc.isDecimalLiteral("10"));
        System.out.println("nc.isDecimalLiteral(\"-10\")" +nc.isDecimalLiteral("-10"));
        System.out.println("nc.isDecimalLiteral(\"+10\")" +nc.isDecimalLiteral("+10"));
        System.out.println("nc.isDecimalLiteral(\"abc10\")" +nc.isDecimalLiteral("abc10"));
        System.out.println("nc.isDecimalLiteral(\"10cab\")" +nc.isDecimalLiteral("10cab"));
        System.out.println("nc.isDecimalLiteral(\"-.3333\")" +nc.isDecimalLiteral("-.3333"));
        System.out.println("nc.isDecimalLiteral(\"-333.3333\")" +nc.isDecimalLiteral("-333.3333"));

    }
}
