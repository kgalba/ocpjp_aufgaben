package kgalba.regex;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * jse_066260 / Aufgaben / aufgabe_regex_philisophen.txt
 *
 * Aufgabe "Regex - Philosophen"
 *
 * Gegeben ist die Liste antiker Philosophen im Web: http://de.wikipedia.org/wiki/Liste_antiker_Philosophen
 *
 * - Speichern Sie den Html-Text der Seite bitte in einer Textdatei. Sie können es mit den Browser-Mitteln tun.
 * Alternativ kann die Klasse "java.net.URL" verwendet werden:
 *
 * java.net.URL url = new URL("http://de.wikipedia.org/wiki/Liste_antiker_Philosophen"); InputStream is =
 * url.openStream(); InputStreamReader reader = new InputStreamReader(is, "UTF-8"); BufferedReader in = new
 * BufferedReader(reader); ...
 *
 * - Laden Sie wieder den Html-Text aus der gespeicherten Textdatei in ein String
 *
 * - Parsen Sie den geladenen Html-Text, suchen Sie dabei die Namen der Philosophen - Benutzen Sie beim Parsen EINFACHE
 * reguläre Ausdrücke (nicht übertreiben!) - Benutzen Sie bei der Suche auch die Methoden der Klasse String, wenn es für
 * Sie sinnvoll und bequem erscheint. - Benutzen Sie beliebige andere Klassen/Methoden, die Ihnen bei der Suche nach den
 * Philosophennamen helfen können.
 *
 * Achtung! Der Html-Text einer fremden Html-Seite kann sich jeden Tag ändern. Bitte investieren Sie nicht zu viel Zeit
 * in das Ausarbeiten eines universellen regulären Ausdrückes, der evtl. morgen nicht mehr gelten wird.
 *
 * - Speichern Sie die gefundenen Philosophennamen in einer List
 *
 * - Geben Sie die Anzahl der gefundenen Namen und die Namen aus
 *
 * Optional: - Suchen Sie nach den Geburtsjahren/Todesjahren der Philosophen im Text - Suchen Sie nach Notizen, die es
 * zu einigen Philosophen gibt - Speichern Sie den Namen, das Geburtsjahr, das Todesjahr, die Notiz in einem
 * Person-Objekt - erstellen Sie eine List aller Philosphen-Personen
 *
 * @author kgalba
 */
public class Philosophers {
    
    static String resourceDir = "src/kgalba/resources/";
    
    public static void main(String[] args) throws MalformedURLException, IOException {
        java.net.URL url = new URL("https://de.wikipedia.org/wiki/Liste_antiker_Philosophen");
        String htmlSrc = getUrl(url);

        String rawNames = isolateRawNameList(htmlSrc);
        String[] names = extractNames(rawNames);
        printNames(names);

        saveNames(resourceDir, names);
        //System.out.println("Birth :"  + extractBirth(rawNames));
    }

    private static String[] extractNames(String rawNames) {
        //System.out.println("DEBUG rawNames: " + rawNames);
        ArrayList<String> nameList = new ArrayList<>();
        String namePattern = "<li>.*<a href=\"/wiki/(?!Liste).*</li>";
        String liSplit = "<li>(<i>)?(<b>)?<a href=\\\"/wiki/.*? title=\\\".*?\\\">";

        Pattern p = Pattern.compile(namePattern);
        Matcher m = p.matcher(rawNames);

        int nameCounter = 0;
        while (m.find()) {
            nameCounter++;
            String[] name = m.group().split(liSplit);
            String fullName = name[1].split("</a>?.*</li>")[0];
            nameList.add(fullName);

        }

        return nameList.toArray(new String[0]);
    }

    private static String isolateRawNameList(String htmlSrc) {
        String rawNames = "";
        //slice top off
        String beginPattern = "<h3><span class=\"mw-headline\" id=\"A\">A</span><span class=\"mw-editsection\">"
                + "<span class=\"mw-editsection-bracket\">"
                + "\\[</span><a href=\"/w/index.php\\?title=Liste_antiker_Philosophen&amp;action=edit&amp;section=2\""
                + " title=\"Abschnitt bearbeiten: A\">Bearbeiten</a><span class=\"mw-editsection-bracket\">\\]"
                + "</span></span></h3>";
        String[] topSplit = htmlSrc.split(beginPattern);
        //System.out.println("split length: " + topSplit.length);
        //System.out.println("topSplit[0]:  " +topSplit[0]);
        String remains = topSplit[1];
        //System.out.println("DEBUG topSplit remains: " + remains);
//
//        //slice bottom off
        String endPattern = "<h3><span class=\"mw-headline\" id=\"Siehe_auch_2\">Siehe auch</span>"
                + "<span class=\"mw-editsection\"><span class=\"mw-editsection-bracket\">"
                + "\\[</span><a href=\"/w/index.php\\?title=Liste_antiker_Philosophen&amp;action=edit&amp;section=25\""
                + " title=\"Abschnitt bearbeiten: Siehe auch\">Bearbeiten</a>"
                + "<span class=\"mw-editsection-bracket\">\\]</span></span></h3>";
        String[] bottomSplit = remains.split(endPattern);
        remains = bottomSplit[0];
//        
        //System.out.println("DEBUG bottomSplit : " + remains);

        return remains;
    }

    private static String getUrl(URL url) throws IOException, UnsupportedEncodingException {
        InputStream is = url.openStream();
        InputStreamReader reader = new InputStreamReader(is, "UTF-8");
        BufferedReader in = new BufferedReader(reader);
        String html;
        String htmlSrc = "";
        while ((html = in.readLine()) != null) {
            htmlSrc = htmlSrc + html + "\n";
        }

        //System.out.println("DEBUG htmlSrc: " +htmlSrc);
        //htmlSrc = IOUtils.toString(url, "UTF-8");
        return htmlSrc;
    }

    private static String extractBirth(String rawNames) {
        String birthPattern = "\\(?(\\*\\s|um |ca\\.?\\s)?\\d{1,4}(\\.|^%)*(\\d{1,3})?.*\\)";

        Pattern p = Pattern.compile(birthPattern);
        Matcher m = p.matcher(rawNames);

        int nameCounter = 0;
        while (m.find()) {
            nameCounter++;

            System.out.println(nameCounter + ". " + m.group());
        }

        return "";
    }

    private static void printNames(String[] names) {
        System.out.println(names.length + " Philosophen gefunden!");
        for (int i = 0; i < names.length; i++) {
            System.out.println(i + 1 + ". " + names[i]);
        }
    }

    /**
     * save names to dir
     *
     * @param dir
     */
    static void saveNames(String dir, String[] names) throws IOException {
        File file = new File(dir + "philosophers.txt");
        
        if (!file.createNewFile()) {
            throw new IOException("Could not create file: " + file.getAbsolutePath());
        }
        
        PrintWriter fw = new PrintWriter(file);
        for (String name : names) {
            fw.println(name);
        }
        fw.flush();
        fw.close();
    }
}
