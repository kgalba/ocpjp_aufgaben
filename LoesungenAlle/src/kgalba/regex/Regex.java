package kgalba.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author kgalba
 */
public class Regex {
    public static void search(String haystack, String needle) {
        Pattern p = Pattern.compile(needle);
        Matcher m = p.matcher(haystack);
        
        System.out.println("Gesucht wird im Text \"" + haystack + "\"" + "mit dem Regex \"" + needle + "\"." );
        for (int i = 0; i < haystack.split(needle).length; i++) {
            //System.out.println("i" +i);
            if (m.find()) {
                System.out.println("Treffer: " + m.group() + ". Von " + m.start() + " bis " +m.end());
            }       
        }
    }
    
    public static void main(String[] args) {
        search("Hallo Java", "a");
    }
}
