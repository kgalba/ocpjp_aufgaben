package kgalba.threads;

import static java.lang.Thread.sleep;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kgalba
 */
public class MyFirstThread extends Thread {

    public static void main(String[] args) {
        Thread myThread = new MyFirstThread();
        myThread.start();
        
        MyRunnable myRunnable = new MyRunnable();
        Thread myThread1 = new Thread(myRunnable);
        myThread1.start();

    }

    @Override
    public void run() {
        int tic = 0;
        for (int i = 0; i < 10; i++) {
            try {
                sleep(2000);
            } catch (InterruptedException ex) {
                Logger.getLogger(MyFirstThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println(this + " tic: " + tic++);
        }

    }

}

class MyRunnable implements Runnable {

    @Override
    public void run() {
        int tic = 0;
        for (int i = 0; i < 10; i++) {
            try {
                sleep(2000);
            } catch (InterruptedException ex) {
                Logger.getLogger(MyFirstThread.class.getName()).log(Level.SEVERE, null, ex);
            }
            System.out.println(this + " tic: " + tic++);
        }
    }
}
