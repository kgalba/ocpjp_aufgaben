package kgalba.threads;

import java.io.File;

/**
 * +﻿Aufgabe "Threads - Directory Observer"
 *
 * +
 *
 * +- Erstellen Sie eine EINFACHE Consolenanwendung, die die Anzahl der Dateien
 *
 * + in einem Verzeichnis überwachen kann.
 *
 * +
 *
 * +- Beim Starten wird die Anwendung den Namen des Verzichnisses und die aktuelle
 *
 * + Anzahl der Dateien ausgeben. Dann läuft die Anwendung endlos und ermittelt
 *
 * + in einem EXTRA-Thread immer wieder die Anzahl der Dateien im Verzeichnis.
 *
 * +
 *
 * +- Um die CPU nicht zu sehr zu belasten wird die Anwendung die Ermittlung der
 *
 * + Dateienanzahl nicht öfter als ein mal pro ca. 2 Sekunden durchführen. Wird eine Änderung
 *
 * + ermittelt, benachrichtigt die Anwendung den User, wie viele Dateien seit der * + letzten Ermittlung erzeugt oder
 * gelöscht wurden.
 *
 * +
 *
 * +- Der main Thread soll nach 1 Min. die Anwendund runterfahren. intterupt ausprobieren.
 *
 * +
 *
 * +!Die Anwendung soll nicht ermitteln, welche Dateien gelöscht oder erzeugt werden!
 *
 * +
 *
 * +- Optional. Erzeugen Sie eine Swing-Anwendung, die ähnlich funktioniert.
 *
 * + - Die Anwendung erlaubt es dem User das zu beobachtende Verzeichnis zu
 *
 * + bestimmen. Tipp: JFileChoose kann verwendet werden
 *
 * + - Das Überwachen soll gestartet und beendet werden können. Das Beenden einer
 *
 * + laufnden Überwachung soll möglichst ohne Verzögerung stattfinden.
 *
 * @author kgalba
 */
public class DirMonitor extends Thread {

    static String tmpPath = System.getProperty("java.io.tmpdir");
    static int fileCount = 0;

    private static int countFiles(String tmpPath) {
        return new File(tmpPath).listFiles().length;
    }

    public static void main(String[] args) throws InterruptedException {
        fileCount = countFiles(tmpPath);
        System.out.println("Monitoring dir : " + tmpPath);
        System.out.println("fileCount : " + fileCount);
        Thread th1 = new DirMonitor();
        th1.start();
        sleep(60000);
        th1.interrupt();
    }

    @Override
    public void run() {
        int loopCounter = 0;
        while (true) {
            try {
                System.out.println("loopCounter: " + this + loopCounter++);
                if (fileCount != countFiles(tmpPath)) {
                    System.out.println("Event in dir, fileCount delta :" + (fileCount - countFiles(tmpPath)));
                }
                fileCount = countFiles(tmpPath);
                sleep(2000);
            } catch (InterruptedException ex) {
                //Logger.getLogger(DirMonitor.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("Interrupted !" + " Bye,Bye !");
                break;
            }
        }
    }

}
