package kgalba.threads.weatherstation;

import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * Aufgabe "Wetterstation"
 *
 * - In einem Thread wird jede 100 mSek die Umgebungstemperatur gemessen. Simuliert wird jede Messung durch das
 * Generieren eines neuen Wertes als Zufallszahl, die im Bereich des alten Wertes +/- 5 Grad liegt.
 *
 * - In einem anderen Thread werden die drei letzten aktuellen Messungen analysiert (1 mal pro Sekunde). Falls in den 3
 * Messungen der stätige Abfall gefunden wird, wird die Tendenz auf der Konsole visualisiert. Dasselbe zur stätigen
 * Steigung.
 *
 * - Im dritten Thread werden Warnungen ausgegeben, falls die aktuelle Themperatur einen Grenzwert überschreitet (zu
 * warm) oder unterschreitet (zu kalt). Auch die Kontrollen werden nur periodisch (1 Mal in 2 Sek) durchgeführt
 *
 * - Threads bitte synchronisieren
 *
 * @author kgalba
 */
public class Main {


    public static void main(String[] args) throws InterruptedException {
        Weatherstation.init();

        Weatherstation weatherstation = new Weatherstation();
        Analyzer analyzer = new Analyzer();
        Alerter alerter = new Alerter();

        //start the processing in threads
        weatherstation.start();
        analyzer.start();
        alerter.start();

        //stop processing
        Thread.sleep(60000); //1 minute
        weatherstation.interrupt();
        analyzer.interrupt();
        alerter.interrupt();
        
        //main after end of processors
        weatherstation.join();
        analyzer.join();
        alerter.join();
    }

}

class Weatherstation extends Thread {
    static Random random = new Random();
    static ArrayList<Integer> measurements = new ArrayList<>();
    private static int centigrade = 0;
    static final int UPPER_BOUND = 30;
    static final int LOWER_BOUND = -30;

    public synchronized static int getCentigrade() {
        return centigrade;
    }

    public synchronized static void setCentigrade(int centigrade) {
        Weatherstation.centigrade = centigrade;
    }

    public synchronized static int[] getLast3Values() {
        return new int[] {
            measurements.get(Weatherstation.measurements.size() - 1),
            measurements.get(Weatherstation.measurements.size() - 2),
            measurements.get(Weatherstation.measurements.size() - 3)
        };
    }

    
    static void init() {
        centigrade = random.nextInt(100) - 50;
        for (int i = 0; i < 3; i++) {
            measurements.add(0);
        }
    }


    @Override
    public synchronized void run() {
        while (true) {
            try {
                sleep(100);
                measure();
                //System.out.println(this + " Measurement : " + centigrade);
            } catch (InterruptedException ex) {
                Logger.getLogger(Weatherstation.class.getName()).log(Level.INFO, "Interrupt", ex);
                break;
            }
        }
    }

    private synchronized void measure() {
        centigrade = centigrade - (random.nextInt(11) - 5);
        //System.out.println("random temp. :" +random);
        measurements.add(centigrade);
    }
}

class Analyzer extends Thread {

    @Override
    public synchronized void run() {
        while (true) {
            try {
                sleep(1000);
                showMonotonChange();
            } catch (InterruptedException ex) {
                Logger.getLogger(Analyzer.class.getName()).log(Level.INFO, "Interrupt", ex);
                break;
            }

        }
    }

    /**
     * are the last 3 Measurement show a monotonic change
     */
    private void showMonotonChange() {

        String direction = "";
        int[] lastThreeValues = Weatherstation.getLast3Values();

        if (lastThreeValues[2] > lastThreeValues[1] && lastThreeValues[1] > lastThreeValues[0]) {
            direction = "DOWN";
        }

        if (lastThreeValues[2] < lastThreeValues[1] && lastThreeValues[1] < lastThreeValues[0]) {
            direction = "UP";
        }

        if (direction != "") {
            System.out.println(this);
            System.out.printf(this.getClass().getName() + " INFO : Last 3 measurement in a row are going %s ! \n", direction);
            System.out.printf("Last 3 measurements, n-3: %d, n-2: %d, n-1: %d", lastThreeValues[2], lastThreeValues[1], lastThreeValues[0]);
            System.out.println("\n--------------------------------------------------------------------------------------");
        }
    }
}

class Alerter extends Thread {

    @Override
    public synchronized void run() {
        while (true) {
            try {
                sleep(2000);
                System.out.println(this);
                System.out.println("\n--------------------------------------------------------------------------------------");
                alert();
            } catch (InterruptedException ex) {
                Logger.getLogger(Alerter.class.getName()).log(Level.INFO, "Interrupt", ex);
                break;
            }
        }
    }

    private void alert() {
        if (Weatherstation.getCentigrade() < Weatherstation.LOWER_BOUND) {
            System.out.printf(this.getClass().getName() + " WARNING: current temperature %d lower than %d \n", Weatherstation.getCentigrade(),Weatherstation.LOWER_BOUND);
            System.out.println("\n--------------------------------------------------------------------------------------");

        }

        if (Weatherstation.getCentigrade() > Weatherstation.UPPER_BOUND) {
            System.out.printf(this.getClass().getName() + " WARNING: current temperature %d higher than %d \n ", Weatherstation.getCentigrade(), Weatherstation.UPPER_BOUND);
            System.out.println("\n--------------------------------------------------------------------------------------");
        }
    }
}
