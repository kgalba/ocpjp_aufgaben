package kgalba.threads.diningphilosophers;

import java.util.ArrayList;

/**
 *
 * @author werkstatt
 */
public class RoundTable {

    Philosoph[] guests;
    Fork[] forks;

    public RoundTable(Philosoph[] guests) {
        placeForks(guests.length);
        assignSeats(guests);
        startDinner();
    }

    final void startDinner() {

        System.out.println("Welcome all to the dinner !");

        //wakeup philosophers 
        ArrayList<Thread> threads = new ArrayList<>(guests.length);
        for (Philosoph philosoph : guests) {
            threads.add(new Thread(philosoph));

        }
        
        for (Thread thread : threads) {
            thread.start();
        }
    }

    private void assignSeats(Philosoph[] guests) {
        this.guests = guests.clone();
        for (int i = 0; i < guests.length; i++) {
            this.guests[i].setSeat(i);
            this.guests[i].setLeftFork(leftFork(i));
            this.guests[i].setRightFork(rightFork(i));
        }
    }

    /**
     * @param numberOfSeats
     */
    private void placeForks(int numberOfSeats) {
        int numberOfForks = Math.max(2, numberOfSeats - 1); //on round table
        forks = new Fork[numberOfForks];
        for (int i = 0; i < numberOfForks; i++) {
            forks[i] = new Fork(i);
        }
    }
 
    /**
     * round table: left of seat 0 is last fork
     * @param seatNr
     * @return 
     */
    private Fork leftFork(int seatNr) {
        int forkNr = (seatNr + forks.length - 1) % forks.length;
        //System.out.printf("leftForkNr(%d): %d \n",seatNr, forkNr);
        return forks[forkNr];

    }
    /**
     * round table: right of last seat is fork[0]
     * @param seatNr
     * @return 
     */
    private Fork rightFork(int seatNr) {
        int forkNr = seatNr % forks.length;
        //System.out.printf("rightFork(%d): %d \n",seatNr, forkNr);
        return forks[forkNr];
    }
}
