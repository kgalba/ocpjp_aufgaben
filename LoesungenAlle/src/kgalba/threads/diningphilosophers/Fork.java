package kgalba.threads.diningphilosophers;

/**
 * shared resource fork
 * @author werkstatt
 */
public class Fork {
    private int position;

    public Fork(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    } 

    @Override
    public String toString() {
        return "ForkNr " +this.position;
    }
}
