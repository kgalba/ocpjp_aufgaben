/*
 Aufgabe "Threads - Philosophenproblem"

 Beachten Sie bitte die Beschreibung des problems bei der Wikipedia:
 http://de.wikipedia.org/wiki/Philosophenproblem

 Erstellen Sie bitte eine Anwendung in der das Philosophenproblem auftaucht.
 Beachten Sie die weiteren Aufgabenpunkte. Die auf der Seite beschriebene
 weitere Variante des Problems soll nicht nachprogrammiert werden.

 - In Ihre Anwendung soll es mindestens 3 Philosophen geben. 

 - Jeder Philosoph hat einen eindeutigen Namen. Die Namen der Philosophen können
 Sie in der beiliegenden Datei "aufgabe_threads_philosophenproblem_namen.txt"
 finden.

 - Optional. Gestalten Sie die Anwendung flexibel genug, um die Anzahl der 
 Philosophen ohne viel Aufwand ändern zu können. Z.B.:

 List<Philosoph> philosophen = getPhilosophen(3); //hier wird die Anzahl auf 3 gesetzt
 ...
  
 Dabei können Sie einen Pool aus Philosophen im voraus erstellen. Die Namen kann
 man dabei aus einer Textdatei laden.

 - Simulieren Sie den Tagesablauf einzelner Philosophen mit entsprechenden
 Konsolenausgaben. Z.B.:

 Sokrates denkt nach...
 Sokrates hat Hunger
 Sokrates nimmt die linke Gabel
 Sokrates nimmt die rechte Gabel
 Sokrates isst...
 Sokrates legt die rechte Gabel ab
 Sokrates legt die linke Gabel ab

 - Der Tagesablauf eines Philosophen soll in einem Thread endlos wiederholt werden

 - Überlegen Sie, auf welche Ressourcen die Philosophen gleichzeitig angewiesen 
 sind. Synchronisieren Sie bitte den Zugriff auf die gemeinsamen Ressourcen
 so, dass es zu dem Deadlock kommen kann. 
 Tipp: Versuchen Sie Deadlock wahrscheinlicher zu gestalten, indem die Threads 
 an den Positionen künstrlich "gebremst" werden, wo das Anhalten zum Deadlock 
 führt.

 !!! Achtung! Die Aufgabe besteht nicht darin, das Philosophenproblem zu lösen!
 Es soll zum Deadlock kommen!

 */
package kgalba.threads.diningphilosophers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author kgalba
 */
public class Main {

    static int amount = 5;
    
    public static void main(String[] args) throws IOException {
        //create guests
        Philosoph[] guests = createPhilosophers(amount);
        //create seats
        RoundTable dinnerTable = new RoundTable(guests);
    }

    private static Philosoph[] createPhilosophers(int i) throws IOException {
        Philosoph[] philosophers = new Philosoph[i];

        for (int j = 0; j < i; j++) {
            philosophers[j] = new Philosoph(generateNames()[j]);
        }
        return philosophers;
    }

    private static String[] generateNames() throws FileNotFoundException, IOException {
        //get from file
        ArrayList<String> nameList = readNamesFromFile(new File("src/kgalba/resources/philosophers.txt"));
        Collections.shuffle(nameList);
        
        return nameList.subList(0, amount).toArray(new String[0]);
    }

    private static ArrayList<String> readNamesFromFile(File file) throws FileNotFoundException, IOException {
        ArrayList<String> names = new ArrayList<>();
        //readFile

        BufferedReader br = new BufferedReader(new FileReader(file));
        String name;
        while ((name = br.readLine()) != null) {
            names.add(name);
        }
        return names;
    }
    
    
}
