package kgalba.threads.diningphilosophers;

import static java.lang.Thread.sleep;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import kgalba.utils.Common;

/**
 *
 * @author kgalba
 */
public class Philosoph implements Runnable {

    private final String name;
    private int seat;
    private Fork leftFork;
    private Fork rightFork;
    private final Random random = new Random();

    public Philosoph(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%-26s on seat %s", name.toUpperCase(), seat);
    }

    public void setSeat(int seat) {
        this.seat = seat;
    }

    public int getSeat() {
        return seat;
    }

    public void setLeftFork(Fork leftFork) {
        this.leftFork = leftFork;
    }

    public void setRightFork(Fork rightFork) {
        this.rightFork = rightFork;
    }

    /**
     *      * - Der Tagesablauf eines Philosophen soll in einem Thread endlos wiederholt werden
     *
     *      * - Simulieren Sie den Tagesablauf einzelner Philosophen mit entsprechenden Konsolenausgaben. Z.B.:
     *
     * Sokrates denkt nach... Sokrates hat Hunger Sokrates nimmt die linke Gabel Sokrates nimmt die rechte Gabel
     * Sokrates isst... Sokrates legt die rechte Gabel ab Sokrates legt die linke Gabel ab
     *
     */
    @Override
    public void run() {
        while (true) {
            try {
                //daily routine
                thinking();
                hungry();
            } catch (InterruptedException ex) {
                Logger.getLogger(Philosoph.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void thinking() throws InterruptedException {
        performAction(Common.getCallingMethodName(), random.nextInt(400)); //fast thinker
    }

    private void hungry() throws InterruptedException {
        performAction(Common.getCallingMethodName(), 0);

        synchronized (leftFork) {
            grabingLeftFork();
            synchronized (rightFork) {
                grabingRightFork();
                eating();
                releasingRightFork();
            }
            releasingLeftFork();
        }
    }

    private void eating() throws InterruptedException {
        performAction(Common.getCallingMethodName(), 10000); //slow eater
    }

    private void grabingLeftFork() throws InterruptedException {
        performAction(Common.getCallingMethodName() + ", " + leftFork, 1);
    }

    private void grabingRightFork() throws InterruptedException {
        performAction(Common.getCallingMethodName() + ", " + rightFork, 1);
    }

    private void releasingLeftFork() throws InterruptedException {
        performAction(Common.getCallingMethodName() + ", " + leftFork, 1);
    }

    private void releasingRightFork() throws InterruptedException {
        performAction(Common.getCallingMethodName() + ", " + rightFork, 1);
    }

    private  void performAction(String action, long duration) throws InterruptedException {
        System.out.printf("\n" + this + " is " + action +" (" + duration + " time-units)");
        sleep(duration);
    }
}
