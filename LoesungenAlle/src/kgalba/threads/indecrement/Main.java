package kgalba.threads.indecrement;

/**
 *
 * @author kgalba
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 10; i++) {

            Thread inc = new Increment();
            Thread dec = new Decrement();

            inc.start();
            dec.start();

            inc.join();
            dec.join();

            System.out.println("count: " + Share.count);
        }
    }
}
