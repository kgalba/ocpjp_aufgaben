package kgalba.threads.indecrement;

/**
 *
 * @author kgalba
 */
public class Decrement extends Thread {

    @Override
    public void run() {
        for (int i = 0; i < Share.ITERATIONS; i++) {
            synchronized (Share.class) {
                Share.count--;
            }
        }
    }
}
