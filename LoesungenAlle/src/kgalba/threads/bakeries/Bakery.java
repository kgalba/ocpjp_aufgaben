package kgalba.threads.bakeries;

import static java.lang.Thread.sleep;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kgalba
 */
public class Bakery implements Runnable {

    String name;
    int store;
    int storeSize;

    public Bakery(String name, int storeSize) {
        this.name = name;
        this.store = 100;
        this.storeSize = storeSize;
    }

    
    @Override
    public String toString() {
        return name + ". buns in store: " + store;
    }

    public int sellBuns(int i) throws InterruptedException {
        int selled = (i >= store) ? store : i;
        store = (store >= i) ? store - i : 0;
        return selled;
    }
    
    @Override
    public void run() {
        while (true) {
            try {
                produce();
                synchronized (this) {
                    this.notify();
                }
            } catch (InterruptedException ex) {
                Logger.getLogger(Bakery.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void produce() throws InterruptedException {
        //produce if store is not full
        if (store < storeSize) {
            sleep(new Random().nextInt(100));
            System.out.println(this.name + " - Buns stored: " + addBun(10) + " " + this);
        }
    }

    /**
     * store buns until store is full
     *
     * @return buns added
     */
    private int addBun(int buns) throws InterruptedException {
        store = ((store + buns) > storeSize) ? storeSize : store + buns;
        return (buns > getCapacity()) ? getCapacity() : buns;
    }

    private int getCapacity() {
        return storeSize - store;
    }
}
