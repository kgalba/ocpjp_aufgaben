/*
 Aufgabe "Threads - wait/notify".txt

 Erstellen Sie bitte eine Anwendung wo die Klassensammlung getestet wird, die 
 den folgenden Beschreibungen entspricht:

 - Jede Bäckerei hat einen Namen

 - Jede Bäckerei kann Brötchen produzieren und im eigenen Lager halten. 
 Lagerkapazität: 100 Stück. 

 Optional: die Lagerkapazität kann für jede Bäckerei gesetzt werden.  

 - Die Produktion in jeder Bäckerei dauert 100 Millisekunden (simuliert durch 
 Thread.sleep). 

 Optional: Die Produktionsdauer kann für eine Bäckerei zufällig variiren.

 - Bei jedem Produktionsvorgang produziert eine Bäckerei 10 Brötchen. Wenn die 
 10 Brötchen das Lager überfüllen würden, werden nur so viel produziert, um das 
 Lager voll zu kriegen.  

 - Jede Bäckerei existiert im eigenen endlosen Thread, in dem die Produktion 
 immer wieder wiederholt wird.
 
 - Jede Person hat einen Namen und eine bestimmte Lieblingsbäckerei

 - Jede Person möchte 3 bis 15 Brötchen kaufen. Die gewünschte Anzahl der Brötchen
 wird vor jedem Einkauf zufällig ermittelt. Sollte es auf dem Lager nicht genug
 Brötchen geben, wird einfach der Restbestand eingekauft - der aktuelle Einkauf
 ist danach erledigt.

 - Jede Person existiert im eigenen endlosen Thread und versucht immer wieder neue 
 Brötchen einzukaufen.

 - Bitte synchronisieren Sie die Threads sinnvoll.

 - Bitte lassen Sie die Personen keine unnötigen Einkäufe versuchen, wenn das 
 Bäckerei-Lager leer ist (wait).

 - Bitte lassen Sie die Personen nicht unnötig warten, wenn es auf dem Lager 
 Brötchen gibt (notify).

 - Bitte erstellen Sie eine Bäckerei "Agethen" und zwei-drei Personen, die dort 
 einkaufen. 

 - Visualisieren Sie bitte mit den Consolenausgaben die Produktion und den Einkauf 
 der Brötchen.
 */
package kgalba.threads.bakeries;

import java.util.Random;

/**
 *
 * @author kgalba
 */
public class Main {

    public static void main(String[] args) {
        new Main();
    }

    Bakery[] bakeries = new Bakery[3];
    Customer[] customers = new Customer[10];

    public Main() {
        initBakeries();
        initCustomers();

        startProductions();
        startConsumers();
    }

    private void initBakeries() {
        bakeries[0] = new Bakery("Agethen", 100);

        for (int i = 1; i < bakeries.length; i++) {
            bakeries[i] = new Bakery("Bakery" + i, 100);
        }
    }

    private void initCustomers() {
        customers[0] = new Customer("agethen-lover0", bakeries[0]);
        customers[1] = new Customer("agethen-lover1", bakeries[0]);
        customers[2] = new Customer("agethen-lover2", bakeries[0]);

        for (int i = 3; i < customers.length; i++) {
            int bak = new Random().nextInt(bakeries.length);
            customers[i] = new Customer("Bakery" + bak + "-lover", bakeries[bak]);;
        }

    }

    private void startProductions() {
        Thread[] producers = new Thread[bakeries.length];

        for (int i = 0; i < bakeries.length; i++) {
            producers[i] = new Thread(bakeries[i]);
        }

        for (Thread producer : producers) {
            producer.start();
        }
    }

    private void startConsumers() {
        Thread[] consumers = new Thread[customers.length];

        for (int i = 0; i < consumers.length; i++) {
            consumers[i] = new Thread(customers[i]);
        }

        for (Thread consumer : consumers) {
            consumer.start();
        }
    }
}
