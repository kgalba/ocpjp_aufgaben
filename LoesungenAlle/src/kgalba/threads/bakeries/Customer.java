package kgalba.threads.bakeries;

import static java.lang.Thread.sleep;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import kgalba.collections.Person;

/**
 *
 * @author kgalba
 */
public class Customer extends Person implements Runnable {

    Bakery preferedBakery;

    public Customer(String lastname, Bakery preferedBakery) {
        super("firstName", lastname, 1970);
        this.preferedBakery = preferedBakery;
    }

    @Override
    public void run() {
        while (true) {
            try {
                buyBuns();
                sleep(100);

            } catch (InterruptedException ex) {
                Logger.getLogger(Customer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void buyBuns() throws InterruptedException {
        synchronized (preferedBakery) {
            if (preferedBakery.store == 0) {
                preferedBakery.wait();
            }
        }
        sleep(new Random().nextInt(10));
        int buied = preferedBakery.sellBuns(new Random().nextInt(13) + 3);
        System.out.println(this + " buied buns:" + buied);
    }

    @Override
    public String toString() {
        return "Customer: " + this.surName;
    }

}
