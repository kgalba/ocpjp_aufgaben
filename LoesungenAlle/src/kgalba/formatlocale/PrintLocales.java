package kgalba.formatlocale;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Locale;

/**
 * aufgabe_format_Locale.txt
 *
 * @author kgalba
 */
public class PrintLocales {
    int maxColWidth = 3;
    Column[] columns;
    Locale[] allLocales = Locale.getAvailableLocales();
    
    public static void main(String[] args) {
        PrintLocales printlocales = new PrintLocales();

        /*
        - Testen Sie bitte die neue Methode mit dem Array aller geladenen Locales.
        Dieses Array erhält man mit dem Aufruf 'Locale.getAvailableLocales()'.
        */
        System.out.println("");
        System.out.println("No sort: ----------------------------------------------------------------------------");

        printlocales.columns = printlocales.collectLocalesData();
        printlocales.printTable();
        
        //- Das Array aufsteigend nach der Spalte 'Land' (und nach 'Sprache' bei gleichen 
        //Landesnamen) sortieren und ausgeben
        System.out.println("");
        System.out.println("sort ASC: ----------------------------------------------------------------------------");
        Arrays.sort(printlocales.columns);
        printlocales.printTable();
        
        /*- Das Array absteigend sortieren und ausgeben. Bei der Lösung wenn möglich
        bitte anonyme Klasse(n) einsetzen.*/
        System.out.println("");
        System.out.println("sort DESC: --------------------------------------------------------------------------------");

//        class DescComparator implements Comparator<Column> {
//
//            @Override
//            public int compare(Column c1, Column c2) {
//                return (c2.displayCountry + c2.language).compareTo(c1.displayCountry + c1.language);
//            }
//        }
        Arrays.sort(printlocales.columns, new Comparator<Column>() {
            public int compare(Column c1, Column c2) {
                return (c2.displayCountry + c2.language).compareTo(c1.displayCountry + c1.language);
            }
        });

        printlocales.printTable();
    }
    
    private void printTable() {
        maxColWidth = getMaxColWidth();
        String colFormat = "|%1$3s |%2$" + maxColWidth + "s |%3$" + maxColWidth + "s |%4$" + maxColWidth + "s";
        String header = String.format(colFormat, "Nr.", "Länder-Code", "Land", "Sprache");
        System.out.println(header);

        for (int i = 1; i < columns.length; i++) {
            System.out.printf(colFormat, i, columns[i].country, columns[i].displayCountry, columns[i].language);
            System.out.println("");
        }
    }

    private Column[] collectLocalesData() {
        Column[] columns = new Column[allLocales.length];
        //collect locales-data
        for (int i = 0; i < columns.length; i++) {
            columns[i] = new Column(
                    allLocales[i].getDisplayCountry(), allLocales[i].getDisplayLanguage(), allLocales[i].getCountry());
        }
        return columns;
    }

    private int getMaxColWidth() {

        //merorize maxColWidth
        int maxColWidth = 3;
        for (Column column : columns) {
            int maxLength = Math.max(column.displayCountry.length(), column.language.length());
            maxColWidth = Math.max(maxColWidth, maxLength);
        }
        return (maxColWidth + 2);
    }
}
