/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.formatlocale;

/**
 *
 * @author kgalba
 */
class Column implements Comparable<Column> {
    String displayCountry;
    String language;
    String country;

    public Column(String displayCountry, String language, String country) {
        this.displayCountry = displayCountry;
        this.language = language;
        this.country = country;
    }

    @Override
    public int compareTo(Column column) {
        return (displayCountry + language).compareTo(column.displayCountry + column.language);
    }
}
