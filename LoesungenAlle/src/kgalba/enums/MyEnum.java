package kgalba.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 *
 * @author kgalba
 */

enum Emoticon {

    WHITE_SMILING_FACE("\u263A", 'A'), NEUTRAL_FACE(":-|", 'B'), UNAMUSED_FACE(":-(", 'C');

    static Emoticon[] elements = values();
    
    private String unicode;
    private String choice;


    private Emoticon(String unicode, char choice) {
        this.unicode = unicode;
    }

    public String getUnicode() {
        return this.unicode;
    }

    public String getChoice() {
        return this.choice;
    }
    
    static public ArrayList getChoices() {
        ArrayList choices = new ArrayList();
        for (Emoticon element : elements) {
            choices.add(element.ordinal());
        }
        return choices;
    }
    
    @Override
    public String toString() {
        return this.name() + " Emoticon: " + this.unicode;
    }
}

public class MyEnum {
    static Scanner scan = new Scanner(System.in);
    static String input;
    
  
    public static void main(String[] args) {

        for (Emoticon emoticon : Emoticon.values()) {
            System.out.println("Emoticon: " + emoticon);
        }
        Integer inputInt = 0;
        do {            
            System.out.println("Choose emoticon : " + Arrays.asList(Emoticon.getChoices()));
            input = scan.nextLine();
            inputInt = Integer.parseInt(input);
            System.out.println("inputInt:" + inputInt);
            System.out.println(Emoticon.elements[inputInt].getUnicode());
        } while (Arrays.asList(Emoticon.getChoices()).contains(inputInt));
        
        System.out.println("You have chosen :" + Emoticon.elements[inputInt].getUnicode());
        
    }
}
