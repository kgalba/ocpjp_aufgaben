package kgalba.lotto;

/**
 *
 * @author kgalba
 */
public class LottoTipp extends Lotto {

    
    public LottoTipp(int balls, int numberOfBalls) {
        super(balls, numberOfBalls);
    }
    
    public void abgeben() {
        draw();
//        this.drawing = super.drawing;
//        drawing = super.drawing;
//        System.out.println("DEBUG: " + Thread.currentThread().getStackTrace()[1].getClassName());
//        System.out.println("DEBUG: this.tipp: " +this);
    }
    
    public String toString() {
        return super.toString().replaceFirst("Spiel", "Tipp");
    }
}
