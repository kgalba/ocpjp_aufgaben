package kgalba.lotto;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author kgalba
 */
public class Lotto {

    public static void main(String[] args) {
        Lotto lotto = new Lotto(7, 49);

        LottoTipp tipp = new LottoTipp(7, 49);
        tipp.abgeben();
        System.out.println(tipp);

        int profit = 0;
        int profitSum = 0;
        int maxGames = 100000;
        for (int i = 0; i < maxGames; i++) {
            lotto.draw();
            profit = lotto.vergleichen(tipp.drawing);
            profitSum += profit;

            if (profit > 0) {
                System.out.println(lotto);
                System.out.println("Gewinn €: " + lotto.vergleichen(tipp.drawing));
                System.out.println("Versuch : " + (i + 1) + " € Einsatz");
                System.out.println("Gewinn : " + profit + " €");
                System.out.println("Gesamt-Gewinn:" + profitSum);
            }

        }
        System.out.println(tipp);
        System.out.println("Gesamt-Gewinn €: " + profitSum + " nach " + maxGames + " € Einsatz !");
    }

    int balls = 7;
    int allBalls = 49;
    int[] drawing;
    Random random = new Random();

    Lotto(int drawing, int allBalls) {
        this.balls = balls;
        this.allBalls = allBalls;
        this.drawing = new int[balls];
    }

    void draw() {

        for (int i = 0; i < balls; i++) {
            drawing[i] = random.nextInt(allBalls) + 1;
            drawing[i] = nextUniqueRandom();

//            System.out.println("DEBUG ball: " + drawing[i]);
        }
        Arrays.sort(drawing);
    }

    public String toString() {
        Arrays.sort(drawing);
        return "Spiel 7 aus 49.:" + Arrays.toString(drawing);
    }

    public int vergleichen(int[] tipp) {

        int match = 0;

        //count matches in sorted drawing
        Arrays.sort(drawing);
        for (int t : tipp) {
            if (Arrays.binarySearch(drawing, t) == 1) {
                match++;
            };
        }

        int profit = 0;
        if (match > 0) {
            profit = (int) Math.pow(10, match);
        }
        return profit;
    }

    int nextUniqueRandom() {
        //generate until new unique radom found
        int unique = random.nextInt(allBalls) + 1;

        while (Arrays.binarySearch(drawing, unique) > 0) {
            unique = random.nextInt(allBalls) + 1;
        }

        return unique;
    }
}
