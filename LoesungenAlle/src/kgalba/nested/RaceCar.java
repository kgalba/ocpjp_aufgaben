package kgalba.nested;

/**
 *
 * @author kgalba
 */
public class RaceCar {

    String brand;
    Driver driver;
    Engine engine;

    public RaceCar(String brand) {
        this.brand = brand;
        this.engine = new Engine("Type1");
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    Engine getEngine() {
        return engine;
    }

    public String toString() {
        return "I'm the " + this.getClass().getSimpleName()
                + " with ID:" + this.hashCode() + "\n"
                + this.brand + " Driver: " + this.driver;
    }

    class Engine {

        String type;

        private Engine(String type) {
            this.type = type;
        }

        public String toString() {
            return "Motor " + type + " RaceCar brand: " +brand;
        }

    }

    static class Driver {

        String forename = "";
        String surname = "";

        public Driver(String forename, String surname) {
            this.forename = forename;
            this.surname = surname;
        }

        public String toString() {
            return forename + surname;
        }
    }

}