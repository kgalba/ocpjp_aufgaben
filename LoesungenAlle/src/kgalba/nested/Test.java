package kgalba.nested;

/**
 *
 * @author kgalba
 */
public class Test {
    public static void main(String... args){
        RaceCar rw = new RaceCar("Mercedes");

        RaceCar.Driver f =  new RaceCar.Driver("M.", "Schumacher");
        rw.setDriver(f);
        
        RaceCar.Engine m = rw.getEngine();

        System.out.println(rw); 	//Zeile 1
        System.out.println(m);	//Zeile 2
    }
}
