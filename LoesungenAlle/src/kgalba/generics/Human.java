package kgalba.generics;

/**
 * human is NOT an animal ;-)
 * @author kgalba
 */
public class Human extends LifeForm {

    Boolean healthy;
    
    @Override
    public void setHealthy(boolean healthy) {
        this.healthy = healthy;
    }

    @Override
    public boolean isHealthy() {
        return this.healthy;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + ".healthy: " +healthy;
    }
}
