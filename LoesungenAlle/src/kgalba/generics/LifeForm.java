package kgalba.generics;

/**
 *
 * @author kgalba
 */
public abstract class LifeForm implements Treatable {
    Boolean healthy;

    @Override
    public void setHealthy(boolean healthy) {
        this.healthy = healthy;
    }

    @Override
    public boolean isHealthy() {
        return this.healthy;
    }

    @Override
    public String toString() {
        return this.getClass().getName() + ".healthy: " + healthy;
    }

}
