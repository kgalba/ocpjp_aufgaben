package kgalba.generics;

/**
 *
 * @author kgalba
 */
public class Doctor<T extends Treatable> extends Human {
        
    public Doctor () {
    }
    
    public void treat(T treatable) {
        treatable.setHealthy(true);
    }
}
