package kgalba.generics;

/**
 *
 * @author kgalba
 */
public interface Treatable {
    public void setHealthy(boolean cured);
    public boolean isHealthy();
}
