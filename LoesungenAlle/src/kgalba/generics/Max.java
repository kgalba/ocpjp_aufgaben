package kgalba.generics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;
import tverrbjelke.collections.Benchmark;

/**
 *
 * @author kgalba
 */
public class Max {

    static <T extends Comparable<? super T>> T getMax(T param1, T param2) {
        T max;
        ArrayList<T> sortList = new ArrayList<T>();

        sortList.add(param1);
        sortList.add(param2);
        Collections.sort(sortList);

        return sortList.get(1);
    }

    public static void main(String[] args) {

        System.out.println("============================================================");

        int i = getMax(2, 3);
        System.out.println("int i = getMax(2, 3) " + i);

        System.out.println("============================================================");

        double d = getMax(2.0, 3.0);
        System.out.println("double d = getMax(2.0, 3.0) " + d);

        System.out.println("============================================================");

        long l = getMax(new Long(Integer.MAX_VALUE), Long.MAX_VALUE);
        System.out.println("getMax( new Long(Integer.MAX_VALUE), Long.MAX_VALUE) " + l);

        System.out.println("============================================================");

        Byte b = getMax((byte) 126, Byte.MAX_VALUE);
        System.out.println("getMax( new Long(Integer.MAX_VALUE), Long.MAX_VALUE) " + b);

        System.out.println("============================================================");

        String s = getMax("foo", "bar");
        System.out.println("getMax(\"foo\", \"bar\") " + s);

        //Number n = getMax((Number)2.0, (Number) 3); //Number is NOT comparable
        //System.out.println("getMax( new Long(Integer.MAX_VALUE), Long.MAX_VALUE) " + n);        
        System.out.println("============================================================");

        String s2 = getMax("22bar", "bar22");
        System.out.println("String s2 = getMax(\"22bar\", \"bar22\") " + s2);

        MyNumber mNmax = getMax(new MyNumber("999.0"), new MyNumber("1000.000001"));
        System.out.println("MyNumber mNmax = getMax(new MyNumber(\"34334.0\"), new MyNumber(\"34334.1\")); " + mNmax);
        System.out.println("============================================================");

        MyNumber mNmax1 = getMax(new MyNumber("AA"), new MyNumber("34334.000001"));
        //throws java.lang.NumberFormatException 
        System.out.println("MyNumber mNmax = getMax(new MyNumber(\"AA\"), new MyNumber(\"34334.000001\")); " + mNmax1);
    }

}

class MyNumber extends Number implements Comparable<Number> {

    private String numberStr;

    public MyNumber(String numberStr) {
        this.numberStr = numberStr;
        isNumericRegEx();
    }

    @Override
    public int intValue() {
        return Integer.valueOf(numberStr);
    }

    @Override
    public long longValue() {
        return Long.valueOf(numberStr);
    }

    @Override
    public float floatValue() {
        return Float.valueOf(numberStr);
    }

    @Override
    public double doubleValue() {
        return Double.valueOf(numberStr);
    }

    private boolean isNumeric() {
        try {
            double b = Double.parseDouble(numberStr);
            return true;
        } catch (NumberFormatException ex) {
            Logger.getLogger(MyNumber.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    private boolean isNumericRegEx() {
        boolean match = numberStr.matches("[-+]?[0-9]*.?[0-9]*");
        if (!match) {
            throw new NumberFormatException(numberStr + " is NaN");
        }
        return match;
    }

    @Override
    public int compareTo(Number n) {
        return numberStr.compareTo(n.toString());
    }

    @Override
    public String toString() {
        return numberStr;
    }
}
