package kgalba.generics;

/**
 * jse_066260 / Aufgaben / aufgabe_Generics_Zoo.txt
 *
 * @author kgalba
 */
public class Main {

    public static void main(String[] args) {
        Zoo zoo = new Zoo();
        Monkey monkey = new Monkey();
        Zebra zebra = new Zebra();
        Human human = new Human();
        zoo.add(monkey);
        zoo.add(zebra);
        System.out.println("All zoo animals:" + zoo.getAll());
        

        Doctor<Monkey> monkeyDoc = new Doctor<Monkey>();
        monkey.setHealthy(false);
        monkeyDoc.treat(monkey);
        System.out.println("monkey" + monkey);

        Doctor<Animal> animalDoc = new Doctor<Animal>();
        zebra.setHealthy(false);
        animalDoc.treat(zebra);
        System.out.println("zebra" + zebra);

        Doctor<Human> humanDoc = new Doctor<Human>();
        human.setHealthy(false);
        humanDoc.setHealthy(false);
        humanDoc.treat(humanDoc);
        System.out.println("humanDoc" + human);
        
        //not possible 
        // humanDoc.treat(monkey);
        //animalDoc.treat(humanDoc);

    }
}
