package kgalba.utils;

import kgalba.threads.diningphilosophers.Fork;

/**
 * utilitys to help testing
 *
 * @author kgalba
 */
abstract public class Common {

    /**
     * helper to print out testMethod-name
     */
    public static void printMethodName(String msgSuffix) {
        StackTraceElement caller = Thread.currentThread().getStackTrace()[2];
        String msg = "DEBUG method-name: " + caller.getClassName() + "::" + caller.getMethodName() + "()  " + msgSuffix;
        System.out.println(msg);
    }

    public static String getClassName() {
        return Thread.currentThread().getStackTrace()[2].getClassName();
    }

    public static void printMethodName(Fork rightFork) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public static String getCallingMethodName() {
        StackTraceElement caller = Thread.currentThread().getStackTrace()[2];
        return caller.getMethodName();
    }
}
