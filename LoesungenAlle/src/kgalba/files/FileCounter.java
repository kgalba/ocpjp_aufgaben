package kgalba.files;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.util.Arrays;

/**
 * jse_066260 / Aufgaben / aufgabe_File_FilesCounter.txt
 *
 * @author werkstatt
 */
public class FileCounter {

    static String os = System.getProperty("os.name").toLowerCase();
    static File fsRoot = File.listRoots()[0];

    public static void main(String[] args) {
        /*
         - Folgender Aufruf muss die Dateien im Verzeichnis "C:\\Windows"
         (s. den Konstruktoraufruf) (aber nicht in Unterverzeichnissen) zählen:
        
         int anzahl = fc.count("txt");
        
         Geben Sie die ermittelte Anzahl der txt-Dateien auf der Console aus*/
        FileCounter fCounter = new FileCounter(fsRoot);
        String extension = "txt";

        System.out.printf("Number of '*.%s' Files in '%s' is '%d'.", extension, fsRoot, fCounter.count(extension));
        System.out.println("");

        /*- Optional:
         - Folgender Aufruf muss rekursiv die Dateien im Verzeichnis "C:\\Windows"
         (s. den Konstruktoraufruf) und allen Unterverzeichnissen zählen:

         int anzahl = fc.countDeep("txt");

         - Geben Sie die ermittelte Anzahl der txt-Dateien auf der Console aus*/
        
        System.out.printf("Recursive Number of '*.%s' Files in '%s' is '%d'.", extension, fsRoot, fCounter.countDeep(extension));
        System.out.println("");
    }
    
    File dir;

    public FileCounter(File dirName) {
        this.dir = dirName;
    }

    /**
     * count files with extension in this.dir
     *
     * @param extension of the files to count
     * @return number of found files
     */
    int count(String extension) {
        return count(this.dir, extension);
    }

    /**
     * count files with extension in given dir
     *
     * @param directory to search in
     * @param extension of the files to count
     * @return number of found files
     */
    int count(File directory, String extension) {
        FilenameFilter fnf = createFileNameFilter(extension);
        return directory.listFiles(fnf).length;
    }

    private FilenameFilter createFileNameFilter(String extension) {
        return (File path, String name) -> {
            if (!path.isDirectory()) {
                return false;
            }
            return name.toLowerCase().endsWith(extension);
        };
    }

    private FileFilter createDirectoryFilter() {
        return new FileFilter() {
            @Override
            public boolean accept(File path) {
                if (path.isDirectory() && path.canRead()) {
                    return true;
                }
                return false;
            }
        };
    }

    int countDeep(File directory, String extension) {
        int fileCounter = count(directory, extension);

        File[] directories = directory.listFiles(createDirectoryFilter());
        //System.out.println("dirs:" + Arrays.asList(directories));

        for (File dir : directories) {
            //System.out.println("dir:" + dir.canRead());

            fileCounter += countDeep(dir, extension);
        }
        return fileCounter;
    }

    int countDeep(String extension) {
        return countDeep(this.dir, extension);
    }
}
