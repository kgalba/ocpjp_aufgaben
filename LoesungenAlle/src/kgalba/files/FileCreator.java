package kgalba.files;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

/**
 * jse_066260 / Aufgaben / aufgabe_File_createFiles.txt
 *
 * @author kgalba
 */
public class FileCreator {

    static String tempDir = System.getProperty("java.io.tmpdir");
    static String workingDir = tempDir+FileCreator.class.getCanonicalName()+new Date().getTime();

    static File[] createFiles(File root, String prefix, String extension, int count) throws IOException {
        root.mkdirs();
        File[] createdFiles = new File[count];
        for (int i = 0; i < count; i++) {

            String fileName = String.format("%s%03d.%s", prefix, i, extension);
            createdFiles[i] = new File(root.getAbsolutePath() + "/" + fileName);
            createdFiles[i].createNewFile();
            //System.out.println(createdFiles[i]);
        }
        return createdFiles;
    }

    static File[] deleteFiles(File root, String prefix, String extension) {
        FileFilter ff = createFileFilter(root, prefix, extension);
        ArrayList<File> deletedFiles = new ArrayList<>();
        
        File[] filesToDelete = root.listFiles();
        for (File delFile : filesToDelete) {
            if (delFile.delete()) {
                deletedFiles.add(delFile);
                //System.out.println(delFile);
            }
        }
        
        return deletedFiles.toArray(filesToDelete);
    }

    public static void main(String[] args) throws IOException {
        System.out.println("createdFiles:");
        createFiles(new File(workingDir), "file", "txt", 30);
        System.out.println("deleteFiles:");
        deleteFiles(new File(workingDir), "file", "txt");

    }

    private static FileFilter createFileFilter(File directory, String prefix, String extension) {
        FileFilter fFilter = new FileFilter(){
            @Override
            public boolean accept(File directory) {
                if(directory.isDirectory()) {
                    return false;
                }
                
                return  directory.getName().toLowerCase().startsWith(prefix) &&
                        directory.getName().toLowerCase().endsWith(extension);
            }
        };
        return fFilter;
    }
}
