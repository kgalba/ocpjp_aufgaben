package kgalba.files;

import java.io.File;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Comparator;
import kgalba.utils.Common;

/**
 * @see Aufgaben/aufgabe_File_Laufwerke.txt
 * @author kgalba
 */
public class ListDrives {

    static class SizeUnit {

        String name;
        int divisor;

        public SizeUnit(String name, int divisor) {
            this.name = name;
            if (divisor == 0) {
                throw new IllegalArgumentException("Zero not allowed for a divisor !");
            }
            this.divisor = divisor;
        }

        final long getCustomSize(long rawSize) {
            return rawSize / this.divisor;
        }

    }

    static int maxColWidth = 0;
    static final SizeUnit megaByte = new SizeUnit("MB", 1024);

    static String print() {
        String[] headerRow = headerRow();

        String[][] bodyTable = bodyTable();

        String header = formatRow(headerRow);
        String body = "";
        for (int i = 0; i < bodyTable.length; i++) {
            body += formatRow(bodyTable[i]);
        }

        return header + body;
    }

    private static String[][] bodyTable() {

        //table-body
        String body = "";
        File file = new File("/");
        File[] roots = File.listRoots();

        String[][] rows = new String[roots.length][];
        String[] columns = new String[4];
        for (int i = 0; i < roots.length; i++) {

            columns[0] = roots[i].getAbsolutePath();
            columns[1] = String.valueOf(megaByte.getCustomSize(roots[i].getFreeSpace()));
            columns[2] = String.valueOf(megaByte.getCustomSize((roots[i].getTotalSpace() - roots[i].getUsableSpace())));
            columns[3] = String.valueOf(megaByte.getCustomSize(roots[i].getTotalSpace()));
            rows[i] = columns.clone();
            setMaxColWith(columns);
        }

        rows = sort(rows);
        return rows;
    }

    private static String[] headerRow() {
        String[] headerRow = new String[]{"LW ", "Frei(MB)", "Belegt(MB)", "Gesamt(MB)"};
        return headerRow;
    }

    public static void main(String[] args) {
        System.out.println(print());
    }

    private static void setMaxColWith(String[] columns) {
        for (String column : columns) {
            maxColWidth = (column.length() > maxColWidth) ? column.length() : maxColWidth;
        }
    }

    private static String formatRow(String[] row) {
        for (int i = 0; i < row.length; i++) {
            row[i] = formatNumber(row[i]);
        }

        String colFormat = "|%1$5s |%2$" + maxColWidth + "s |%3$" + maxColWidth + "s |%4$" + maxColWidth + "s" + "|\n";
        String formatedRow = String.format(colFormat, row);
        return formatedRow;
    }

    private static String formatNumber(String col) {
        NumberFormat nf = NumberFormat.getInstance();
        try {
            col = nf.format(Long.valueOf(col));
        } catch (NumberFormatException e) {
            return col;
        }
        return col;
    }

    /**
     * sort table by col ASC
     */
    private static String[][] sort(String[][] table) {
        /**
         * inner class have custom sort
         */
        class ColumnComparator implements Comparator {

            public ColumnComparator() {
                this.colNr = 1;
            }
            int colNr;

            /**
             * ASC sorting
             *
             * @param o1
             * @param o2
             * @return
             */
            @Override
            public int compare(Object o1, Object o2) {
                String[] table1 = (String[]) o1;
                String[] table2 = (String[]) o2;
                String o1Str = String.valueOf(table1[colNr]);
                String o2Str = String.valueOf(table2[colNr]);
                return o1Str.compareTo(o2Str);
            }
        }

        Arrays.sort(table, new ColumnComparator());

        return table;
    }
}
