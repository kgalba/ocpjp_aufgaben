package kgalba.files;

import java.io.File;

/**
 *
 * @author werkstatt
 * @todo crossPlatform 
 */
public class Directorys {
    static public boolean createDirectories(String path) {
        boolean result = false;
        File dir = new File(path);
        if (!dir.exists()) {
            result = dir.mkdirs();
        }
        return result;
    }
    
        static public boolean deleteDirectories(String path) {
        boolean result = false;
                File dir = new File(path);
        if (dir.exists()) {
            result = dir.delete();
        }
        return result;
    }
}
