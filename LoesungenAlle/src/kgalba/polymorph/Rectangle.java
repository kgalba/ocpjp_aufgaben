
package kgalba.polymorph;

/**
 *
 * @author kgalba
 */
public class Rectangle extends Figure {

    int width;
    int height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    @Override
    double getArea() {
        return width * height;
    }
    
    
}
