/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.polymorph;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kgalba
 */
public abstract class Figure {

    public Figure() {
    }

    public String toString() {
        return "I'm the " + this.getClass().getSimpleName()
                + " with ID:" + this.hashCode() + "\nMy properties are: "
                + getProperties();
    }

    String getProperties() {
        
        Field[] fields = getClass().getDeclaredFields();
        String propertyMap = "";
        for (Field field : fields) {
            try {
//                System.out.println("field" + field);
                propertyMap += field.getName() + ": " + field.get(this) +" ";
            } catch (Exception ex) {
                ex.printStackTrace();
                return null;
            } 
        }
        return propertyMap;
    }

    abstract double getArea();
}
