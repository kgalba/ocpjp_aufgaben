package kgalba.polymorph;

import java.util.Random;

/**
 *
 * @author kgalba
 */
public class Main {

    public static void main(String[] args) {
        Random random = new Random();
        
        
        //100 rectangles
        for (int i = 0; i < 100; i++) {
            Rectangle rectangle = new Rectangle(random.nextInt(20) + 1, random.nextInt(20) + 1);
            System.out.println(rectangle);
            System.out.println("Area: " + rectangle.getArea());

        }
        
        //circle
        Circle circle = new Circle(random.nextInt(20) + 1);
        System.out.println(circle);
        System.out.println("Area: " + circle.getArea());
        
        //100 rectangle or circles
        int figureCount = 100;
        Figure[] figures = new Figure[figureCount];
        for (int i = 0; i < figureCount; i++) {
            if (random.nextInt(2) == 1) {
                figures[i] = new Rectangle(random.nextInt(20) + 1, random.nextInt(20) + 1);
            } else {
                figures[i] = new Circle(random.nextInt(20) + 1);
            }
        }
        
        //output areas of 100 figures
        calculateAreas(figures);
    }
    
    static void calculateAreas(Figure[] figures) {
        int count = 0; 
        for (Figure figure : figures) {
            System.out.println("\n" + "Figure " + ++count);
            System.out.println(figure);
            System.out.println("Area: " +figure.getArea());
        }       
    }
 }
