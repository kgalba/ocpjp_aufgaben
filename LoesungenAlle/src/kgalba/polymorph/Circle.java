/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kgalba.polymorph;

/**
 *
 * @author kgalba
 */
public class Circle extends Figure{

    int radius;

    public Circle(int radius) {
        this.radius = radius;
    }
    
    double getArea() {
        return  Math.PI * radius * radius;
    }
}
