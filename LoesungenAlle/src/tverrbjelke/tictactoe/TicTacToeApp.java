/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.tictactoe;

import tverrbjelke.tictactoe.model.Game;
import tverrbjelke.tictactoe.model.GameLogic;
import tverrbjelke.tictactoe.presenter.CliPresenter;
import tverrbjelke.tictactoe.view.CliView;

/**
 *
 * @author tverrbjelke
 */
public class TicTacToeApp {
    private static int DEFAULT_DIMENSION = 3;
    
    public static void main(String[] args) {
        
        CliView cliView = new CliView(DEFAULT_DIMENSION);
        Game gameModel = new Game(DEFAULT_DIMENSION);
        GameLogic logic = new GameLogic(gameModel);
        CliPresenter presenter = new CliPresenter(gameModel, logic, cliView);

        presenter.init();
        presenter.run();
    }

}
