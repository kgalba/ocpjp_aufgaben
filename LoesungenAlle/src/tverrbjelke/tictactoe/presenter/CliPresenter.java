/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.tictactoe.presenter;

import tverrbjelke.tictactoe.model.Game;
import tverrbjelke.tictactoe.model.GameLogic;
import tverrbjelke.tictactoe.model.Player;
import tverrbjelke.tictactoe.model.Player;
import tverrbjelke.tictactoe.view.BoardCliView;
import tverrbjelke.tictactoe.view.CliView;
/**
 *
 * @author tverrbjelke
 */
public class CliPresenter {
    private final Game myGame;
    private final CliView cliView;
    private final GameLogic logic;

    private boolean abortStatus;
    
    // member functions ############################ 
    
    public CliPresenter(Game gameModel, GameLogic logic, CliView gameView) {

        this.myGame = gameModel;
        this.logic = logic;
        this.cliView = gameView;
        this.abortStatus = false;
    }

    /** 
     * initualize GUI and if needed the model.
     */
    public void init() {
        System.out.println("initializing tic tac toe game...");

        //this.cliView.init(this); // now the view knows its presenter.
        // propagate Model defaults to the GUI
        
        
    }

    /**
     * runs the current game with its settings.
     */
    public void run(){
        System.out.println("Starting tic tac toe game on commandline...");

        this.abortStatus = false;
        Game.Move move;
        do {
            cliView.showBoard();
            Player player = myGame.getCurrentplayer();
            move = readNewCoordinate(player);
            doMove(move);
            myGame.setCurrentPlayer( logic.getOtherPlayer(player));
        }while (! logic.isDrawSituation() && !logic.isWinSituation(move) && !this.abortStatus);
        showResults(move);
    }

    /** 
     * 
     * Can set abortStatus to true.
     * @param player
     * @return the next move to be performed.
     */
    Game.Move readNewCoordinate(Player player){
        Game.Move move = null; // stupid, only to overcome the "maybeuninitialized" warning
        if ( player.isComputerPlayer ) {
            move = doAIMove(player);
        }
        else {
            boolean isValidMove = false;
            while ( !isValidMove && !abortStatus )  {
                String msgRow = player.name + " Row: ";
                String msgColumn = player.name + " Column: ";
                CliView.FieldCoordinate coord = cliView.readCoordinates(msgRow, msgColumn);
                move = createMove(player, coord);
                isValidMove = logic.isValidMove(move);
                if (!isValidMove) {
                    if (! cliView.dialogYesNo("Ungültige Eingabe! Wollen Sie nochmal probieren (ja) oder aufgeben (nein)? ", "ja")){
                        this.abortStatus = true;
                    };
                }
            }
        }
        return move;
    }
    
    void doMove(Game.Move move){
        if (!this.abortStatus ) {
            logic.doMove(move);
            this.cliView.updateBoardField( new CliView.FieldCoordinate(move.x, move.y), 
                                           getFieldTokenForPlayer(move.player) );
            this.cliView.showBoard();
        }
    }

    /** 
     * todo implement
     */
    Game.Move doAIMove(Player player){
        return null;
    }
    

    BoardCliView.FieldToken getFieldTokenForPlayer(Player player){
        // now visualize the move
        switch (player.role){
            case PLAYER1:
                return BoardCliView.FieldToken.PLAYER1;
            case PLAYER2:
                return BoardCliView.FieldToken.PLAYER2;
            default:
                throw new IllegalArgumentException ("Ups error, cannot handle player properly..." + player.toString());
        }       
    }
    
    void showAbort(){
        String msg = "Ups noone won and no draw! Someone aborted the game?";
        this.cliView.displayAbort(msg);        
    }
    
    void showResults(Game.Move lastMove) {
        if (logic.isDrawSituation() ){
            String msg = "Draw, nobody won this game!";
            this.cliView.displayDrawSituation(msg);
        }
        else if (logic.isWinSituation(lastMove)) {
            Player winner = lastMove.player;
            Player looser = logic.getOtherPlayer(winner);
            String msg = "The Winner of the game is " + winner.name + 
                    " with the mighty move [" + lastMove.x + ", " + lastMove.y 
                    + "].\n" +
                    "The lousy loser is " + looser.name + "!";
            this.cliView.displayWinSituation(msg);
        }
        else if ( abortStatus ) { 
            showAbort();
        }
    }

/** 
     * Adapter to convert Move into Model domain.
     * @param p
     * @param coord
     * @return Move
     */
    Game.Move createMove(Player p, CliView.FieldCoordinate  coord){
        Game.Move move = new Game.Move(p, coord.x, coord.y); 
        return move;
    }
}
