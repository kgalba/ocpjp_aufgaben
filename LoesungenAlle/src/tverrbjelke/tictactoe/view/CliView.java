/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.tictactoe.view;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;

import java.util.Scanner;
import tverrbjelke.tictactoe.presenter.CliPresenter;

/**
 * Is a (commandline interface) view of TicTacToe.
 * Creates and holds the input scanner itself.
 * @author tverrbjelke
 * @see see http://alvinalexander.com/java/edu/pj/pj010005
 */
public class CliView implements View{

    public static class FieldCoordinate {
        public int x,y;

        public FieldCoordinate() { super(); }
        
        public FieldCoordinate(int x, int y) {
            super();
            this.x = x;
            this.y = y;
        }
    }

    
    private Scanner cliScanner;
//    private CliPresenter presenter;
    private BoardCliView board;
    
    // member functions ########################
    
    /** 
     * @param boardDimension >0 is normally 3 (= 3x3 board).
     */
    public CliView(int boardDimension){
        this.cliScanner = new Scanner(System.in);
        this.board = new BoardCliView(boardDimension);
    }
    
//    public void init(CliPresenter presenter){
//        this.presenter = presenter;
//    }

    public void updateBoardField( FieldCoordinate coord, BoardCliView.FieldToken  value ){
        this.board.field[coord.x][coord.y] = value.getToken();
    }
    
    public void showBoard() {
        System.out.println(this.board.toString());
    }

    public  FieldCoordinate readCoordinates(String msgX, String msgY){
        FieldCoordinate coord = new FieldCoordinate();
        try {
            System.out.println(msgX);
            coord.x = this.cliScanner.nextInt();
            System.out.println(msgY);
            coord.y = this.cliScanner.nextInt();
        }
        catch ( NoSuchElementException|IllegalStateException ex ){ // InputMismatchException is subclassing
            System.err.println(ex);
            coord = null;
        }
        return coord;
    }
    
    /** 
     * @param msg asking for Yes / No feedback from user.
     * @return true for yes
     */
    public boolean dialogYesNo(String msg, String yesMatchString){
        System.out.println(msg);
        String answerStr = this.cliScanner.next();
        if (answerStr.equals(yesMatchString)){
            return true;
        }
        else { 
            return false;
        }
    }
    
    public void displayAbort(String msg) {
        System.out.println(msg);
    }

    public void displayDrawSituation(String msg){
        System.out.println(msg);
    }

    /**
     * @todo hiere we could pass over a player and maybe some pics/avatars of him or whatever...
     * @param msg 
     */
    public void displayWinSituation(String msg){
        System.out.println("Muhaha the game has ended, we have a WINNER!!!");
        System.out.println(msg);
    }


}
