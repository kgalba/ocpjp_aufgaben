/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.tictactoe.view;

import java.util.ArrayList;

// member variables ########################

public class BoardCliView {
    public char[][] field;

    public static enum FieldToken {

        PLAYER1('X'), PLAYER2('O'), EMPTY(' ');//, HSEP('|'), VSEP('-');
        private final char token;

        FieldToken(char token) {
            this.token = token;
        }

        char getToken() {
            return this.token;
        }

        @Override
        public String toString() {
            return Character.toString(this.token);
        }
    }
    
    private static final char hSep = '|';
    private static final char vSep = '-';
    private final String hSepLine;
    private final int dim;

    BoardCliView(int dim) {
        this.dim = dim;
        this.field = new char[dim][dim];
        for (int x=0; x < this.dim; x++){
            for (int y=0; y < this.dim; y++){
                this.field[x][y] = FieldToken.EMPTY.getToken();
            }
        }
        // create separation line string
        ArrayList<Character> rowSepLine = new ArrayList<Character>(dim * 2 + 2);
        for (int row = 0; row < dim; row++) {
            rowSepLine.add(vSep);
            rowSepLine.add(vSep);
        }
        rowSepLine.add(vSep);
        rowSepLine.add('\n');
        this.hSepLine = getStringRepresentation(rowSepLine);
    } // create separation line string

    /**
     * see http://stackoverflow.com/questions/6324826/converting-arraylist-of-characters-to-a-string
     * @param list holds the chars 
     * @return all chars simply ins series as String 
     */
    private String getStringRepresentation(ArrayList<Character> list)
{    
    StringBuilder builder = new StringBuilder(list.size());
    for(Character ch: list)
    {
        builder.append(ch);
    }
    return builder.toString();
}
    @Override
    public String toString() {
        StringBuilder boardStr = new StringBuilder();
        for (int row = 0; row < this.dim; row++) {
            boardStr.append(this.hSepLine);
            boardStr.append(rowToString(row));
        }
        boardStr.append(this.hSepLine);
        return boardStr.toString();
    }

    private String rowToString(int rowNr) {
        // each field also has left sep, plus 1 closing sep
        final int len = this.dim * 2 + 2;
        ArrayList<Character> rowArray = new ArrayList<Character>(len);
        for (int i = 0; i < this.dim; i++) {
            rowArray.add(BoardCliView.hSep);
            rowArray.add(this.field[rowNr][i]);
        }
        rowArray.add(BoardCliView.hSep); // closing separator
        rowArray.add('\n'); 
        return getStringRepresentation(rowArray);
    }
    
}
