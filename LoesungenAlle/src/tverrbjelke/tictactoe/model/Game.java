/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.tictactoe.model;

import static java.lang.Integer.min;

/**
 *
 * @author tverrbjelke
 */
public class Game {


    
    /** 
     * Is a single move of the game. pure dataclass 
     */
    static public class Move{
        public int x, y;
        public Player player;
        
        public Move(Player p, int x, int y){
            super();
            this.player = p;
            this.x = x;
            this.y = y;
        }
    }
    
    // member variables ###########################
    
    /**
     * reference to one of the two players.
     */
    private Player currentPlayer; 
    
    /**
     * player 1 and player2
     */
    private final Player[] players;

    /** 
     * the one board of the game.
     */
    private final Board board;

    
    // member functions ####################################

    public Game(int boardSize) {
        this.board = new Board(boardSize);
        
        Player p1 = new Player(Player.PlayRole.PLAYER1, "Player 1");
        
        Player p2 = new Player(Player.PlayRole.PLAYER2, "Player 2");
        
        this.players = new Player[] {p1,p2};

        this.currentPlayer = this.players[0];
    }
   
    public Board getBoard() {
        return board;
    }
    
    /**
     * who is doing the next move?
     */
    public void setCurrentPlayer(Player currentPlayer) {
        this.currentPlayer = currentPlayer;
    }

    /**
     * who is doing the next move?
     */
    public Player getCurrentplayer() {
        return this.currentPlayer;
    }

    public Player[] getPlayers(){
        return this.players;
    }
    

    /**
     * Adapter to match the types.
     * @param role of a player
     * @returns a field status that mathces the role. (palyer1 or player2)
     */
    public static Board.PlayFieldStatus playerRoleToFieldStatus(Player.PlayRole role) throws IllegalArgumentException
    {
        switch (role){
            case PLAYER1:
                return Board.PlayFieldStatus.PLAYER1;
            case PLAYER2:
                return Board.PlayFieldStatus.PLAYER2;
            default:
                throw ( new IllegalArgumentException("cannot convert role to field status"));
        }
    }

}
