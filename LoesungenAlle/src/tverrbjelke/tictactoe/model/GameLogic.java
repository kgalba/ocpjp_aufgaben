/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.tictactoe.model;

import tverrbjelke.tictactoe.model.Game.Move;

/**
 *
 * @author tverrbjelke
 */
public class GameLogic {

    private final Game game;
    
    public GameLogic(Game game){
        super();
        this.game = game;
    }
    
    /**
     *
     * @param move the value of move
     * @param game the value of game
     * @return the boolean
     */
    private boolean isColumnWin(Move move) {
        for (int y = 0; y < game.getBoard().BOARDSIZE_Y; y++) {
            if (!game.getBoard().getField(move.x, y).equalsTo(move.player.role)) {
                return false;
            }
        }
        return true; // all fields are owned by the player
    }

    /**
     *
     * @param move the value of move
     * @param game the value of game
     * @return the boolean
     */
    private boolean isDiagonalWinNW2SE(Move move) {
        if (game.getBoard().BOARDSIZE_X != game.getBoard().BOARDSIZE_Y) {
            throw new IndexOutOfBoundsException("Only suare boards implemented yet!");
        }
        for (int i = 0; i < game.getBoard().BOARDSIZE_X; i++) {
            if (!game.getBoard().getField(i, i).equalsTo(move.player.role)) {
                return false;
            }
        }
        return true; // all fields are owned by the player
    }

    /**
     * checks if this move is valid or not.
     *
     * @return true if the move is OK.
     */
    public boolean isValidMove(Move testMove) {
        boolean isInBoundaries = (testMove.x >= 0) && (testMove.x < game.getBoard().BOARDSIZE_X) && (testMove.y >= 0) && (testMove.y < game.getBoard().BOARDSIZE_Y);
        if ((isInBoundaries) && (game.getBoard().getField(testMove.x, testMove.y) == Board.PlayFieldStatus.EMPTY)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param move the value of move
     * @param game the value of game
     * @return the boolean
     */
    private boolean isDiagonalWinSW2NE(Move move) {
        if (game.getBoard().BOARDSIZE_X != game.getBoard().BOARDSIZE_Y) {
            throw new IndexOutOfBoundsException("Only suare boards implemented yet!");
        }
        for (int x = 0; x < game.getBoard().BOARDSIZE_X; x++) {
            int y = game.getBoard().BOARDSIZE_Y - x - 1;
            if (!game.getBoard().getField(x, y).equalsTo(move.player.role)) {
                return false;
            }
        }
        return true; // all fields are owned by the player
    }

    //    /**
    //     * Asumes that after a move the current player was swapped already,
    //     * so the winner is the other player.
    //     * @return null if none has won, instance of the player who has won.
    //     */
    //    public Player getWinner(){
    //        if (this.isWinSituation())
    //        {
    //            return this.getOtherPlayer(this.getCurrentplayer() );
    //        }
    //        else
    //        {
    //            return null;
    //        }
    //    }
    //
    /**
     * @return true if the last doMove() resulted in a draw situation.
     */
    /**
     *
     * @param game the value of game
     * @return the boolean
     */
    public boolean isDrawSituation() {
        for (int x = 0; x < game.getBoard().BOARDSIZE_X; x++) {
            for (int y = 0; y < game.getBoard().BOARDSIZE_Y; y++) {
                if (game.getBoard().getField(x, y) == Board.PlayFieldStatus.EMPTY) {
                    return false;
                }
            }
        }
        // ok we really did not find any empty field...
        return true;
    }

    /**
     * Gets the other player ( assumes that we are in a 2player game).
     *
     * @param currentPlayer
     * @return null if no other player was found or teh first different player in this game.
     */
    public Player getOtherPlayer(Player currentPlayer) {
        for (Player p : game.getPlayers()) {
            if (p != currentPlayer) {
                return p;
            }
        }
        return null;
    }

    
    /**
     * @return true if the last doMove() resulted in a win situation.
     */
    public boolean isWinSituation(Move move) {
        return isRowWin(move) || isColumnWin(move) || isDiagonalWinSW2NE(move) || isDiagonalWinNW2SE(move);
    }

    /**
     *
     * @param move the value of move
     * @param game the value of game
     * @return the boolean
     */
    private boolean isRowWin(Move move) {
        for (int x = 0; x < game.getBoard().BOARDSIZE_X; x++) {
            if (!game.getBoard().getField(x, move.y).equalsTo(move.player.role)) {
                return false;
            }
        }
        return true; // all fields are owned by the player
    }

    /**
     *
     *
     * @param move
     * @return true if the move was actually performed, false on failed move...
     */
    public boolean doMove(Move move) {
        if (isValidMove(move)) {
            try {
                game.getBoard().setField(move.x, move.y, Game.playerRoleToFieldStatus(move.player.role));
                return true;
            } catch (IllegalArgumentException e) {
                System.out.println(e);
                return false;
            }
        } else {
            return false;
        }
    }
    
}
