/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.tictactoe.model;

/**
 *
 * @author tverrbjelke
 */
public class Board {

    public enum PlayFieldStatus { 
        EMPTY, PLAYER1, PLAYER2 ;
        
        /**
         * We need to compare Player roles to board field status.
         *  http://stackoverflow.com/questions/15434158/overloading-equals-method-in-enum-any-pitfalls
         * @param player role to compare to.
         * @return 
         */
        public boolean equalsTo(Player.PlayRole player){
            switch (player){
                case PLAYER1:
                    if ( this.name() == PLAYER1.name()) { return true; }
                    break;
                case PLAYER2:
                    if ( this.name() == PLAYER2.name()) { return true; }
                    break;
                default: // all others are false...
                    break;
            }
            return false; // no match
        }
    }
    

    // ****** Board members
    
    public final int BOARDSIZE_X;
    public final int BOARDSIZE_Y;

    /**
     *  holds all fields of the game board
     */
    private PlayFieldStatus[][] boardFields;

    /** 
     * 
     * @param dimension board size in x and y direction, >0
     * @todo sanity checks
     */
    public Board(int dimension) {
        this.BOARDSIZE_X = dimension;
        this.BOARDSIZE_Y = dimension;
        this.boardFields = new  PlayFieldStatus[BOARDSIZE_X][BOARDSIZE_Y];
        sweepBoard(Board.PlayFieldStatus.EMPTY);
        }
    
    /**
     ** Occupies one field of the board with new value.
     * @param x x coordinate from [0..BOARDSIZE_X[
     * @param y y coordinate from [0..BOARDSIZE_Y[
     * @param status new value of the field.
     */
    public void setField( int x, int y, PlayFieldStatus status){
        this.boardFields[x][y] = status;
    }
    
    /**
     * Gets the existing value of the field on that coordinate.
     * @param x x coordinate from [0..BOARDSIZE_X[
     * @param y y coordinate from [0..BOARDSIZE_Y[
     * @return status of the field at this coordinate.
     */
    public PlayFieldStatus getField( int x, int y){
        return this.boardFields[x][y];
    }
    
    /** 
     * clears the already existing board fields with status.
     * @param status is the value teh whole board will be filled with.
     */
    protected void sweepBoard(PlayFieldStatus status){
        for ( int x =0; x < this.boardFields.length; x++){
            for ( int y = 0; y < this.boardFields[x].length; y++) {
                setField(x, y, status);
            }
        }
    }



}
