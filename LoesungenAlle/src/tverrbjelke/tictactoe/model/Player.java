/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.tictactoe.model;

// sub-types #######################################

/**
 * TicTacToe has two roles: player1 and player2. Each has a name.
 */
public class Player {

    public enum PlayRole {

        PLAYER1, PLAYER2
    }
    public PlayRole role;
    public String name;
    public boolean isComputerPlayer;

    /**
     * Creates Player with name as its role e.g. "PLAYER1"
     * @param role which role the player takes PLAYER1 or PLAYER2
     */
    public Player(PlayRole role) {
        this(role, role.name());
    }

    public Player(PlayRole role, String name) {
        this.role = role;
        this.name = name;
        this.isComputerPlayer = false;
    }
    
}
