/*
 Aufgabe "String - Namen"

- Definieren Sie ein Array mit 10-20 unterschiedlichen Silben ihrer Wahl. Z.B.:
 "pro", "gi", "idre" ...

- Definieren Sie eine Methode 'generiereName(String[], int)', die man so
  verwenden kann:
    
    String neuerName = generiereName(arrayMitSilben, 4);

  Die Methode erhält das Array mit Silben, die gewünschte Anzahl der Silben 
  und generiert einen neuen Namen. 

- Generieren Sie 50 neue Namen und geben Sie die aus
 */
package tverrbjelke.Strings;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author tverrbjelke
 */
public class StringNamen {
    
    private static String[] silbenRepository = { "pro", "gi", "idre", "al", "liu", "bats", "nik", "um", "lu", "li", "nux" };
        
    public static String generiereNamen( String[] silben, int numSilben) {
        // Random r;
        Random r = new Random();
        
        //StringBuilder name;
        StringBuilder name = new StringBuilder(); 
        for (int i =0; i< numSilben; i++){
             name.append(silben[r.nextInt(silben.length)]);
        }
        
        return name.toString();
    }
    
    public static void main (String[] args){
     
        System.out.println("dies sind alle silben:" + Arrays.deepToString( StringNamen.silbenRepository ) );
        
        for (int i = 0; i<50; i++) {
            System.out.println(StringNamen.generiereNamen(StringNamen.silbenRepository, 4) );
        }
    }
}
