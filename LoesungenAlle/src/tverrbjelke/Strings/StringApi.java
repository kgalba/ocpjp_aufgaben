/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.Strings;

/**Aufgabe "String Api"

* Definiren Sie ein String mit einem Literal
* @
* Definiren Sie ein String mit dem new-Operator
@
* Wie ist es möglich, dass in String-Objekten unterschiedliche Anzahl der Zeichen
  gespeichert werden kann?
  *@
  * Intern wird ein chararray als privates datenelement gepflegt.
  * 
- Testen Sie folgende Methoden. Sehen Sie sich dabei die Methodendedklarationen an.

    - concat(String)
      Kann man concat durch den Konkatenationsoperator (+) ersetzen?

    - charAt(int)
    - length()
    - isEmpty()  

    - toUpperCase()
    - toLowerCase()

    - endsWith(String)
    - startsWith(String prefix) 
    - startsWith(String prefix, int toffset) 

    - equals(Object)
      Kann man die equals durch den Vergleichsoperator (==) ersetzen?
    - equalsIgnoreCase(String)

    - indexOf(int 
    - indexOf(int ch, int 
    - indexOf(String str) 
    - indexOf(String str)  

    - lastIndexOf(int ch)  
    - lastIndexOf(int ch, int fromIndex)  
    - lastIndexOf(String str)  
    - lastIndexOf(String str, int fromIndex) 

    - replace(char oldChar, char newChar) 

    - substring(int beginIndex) 
    - substring(int beginIndex, int endIndex)  

- Testen Sie folgende statische Methoden. Sehen Sie sich bitte die Methodendedklarationen an.

    - valueOf(boolean b)
    - valueOf(char c)
    - valueOf(char[] data)
    - valueOf(double d)
    - valueOf(float f)
    - valueOf(int i)
    - valueOf(long l)
    - valueOf(Object obj)
 *
 * @author tverrbjelke
 */
public class StringApi {
    public static void main (String [] args){
        String  s1 = "olla";
        String s2 = "lao";
        String s3 = s1 + s2;
        String s4 = s1.concat(s2);
        System.out.println(s3);
        System.out.println(s4);
    } 
}
