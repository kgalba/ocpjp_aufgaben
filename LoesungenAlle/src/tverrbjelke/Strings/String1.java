/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.Strings;

/** Aufgabe "String"
 * - Gegeben ist ein String "Java ist toll". Geben Sie bitte den String rückwärts auf der Konsole aus.
 * 
 * - Erstellen Sie eine Methode 'isTextFile', an die man einen Dateinamen als String übergeben kann. 
 * Die Methode liefert true, wenn die Dateierweiterung "txt" (in allen Kombinationen der Klein- und Großbuchstaben) ist.
 * Sonst liefert die Methode false. Testen Sie Ihre Lösung mit unterschiedlichen Strings.
 *
 * @author tverrbjelke
 */
public class String1 {
    public static void main (String args[]) {
        String tollString = "Java ist toll";
        System.out.println( String1.reverseString(tollString) );

        String[] testSet_isTextFile = { "", "Hallo", ".txt", ".TxT", "dateiname.text", "datei.txt.nich", "datei.nic.tXT", null  };
        for (String test: testSet_isTextFile) {
            System.out.println(test + " is text? "+ isTextFile(test));
        }
    }
    
    /** reverses the String s
     * 
     * @param s string to be reverted
     * @return reversed s
     */
    public static String reverseString(String s) {
        if (s.length() ==0 ){
            return "";
        }
        char [] destArr = new char[s.length()];
        //revert
        for(int i = 0; i< s.length(); i++) {
            int destI = s.length() -1 - i ; // -1 offset is because length 1 is index 0
            destArr[destI] = s.charAt(i);
            //System.out.println("i=" + String.valueOf(i) + " destI=" + String.valueOf(destI) + "Value=" + String.valueOf(destArr[destI]) );
        }
        return String.valueOf(destArr );
    }
    
    public static boolean isTextFile(String s){
        if (s == "" || s == null ) return false; // endsWith ("" ) returns true, we need false

        final String suffix = ".txt";
        return (s.toLowerCase().endsWith(suffix)); 
    }
}
