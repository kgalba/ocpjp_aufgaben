/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.innereKlassen.gebaude;

import java.util.ArrayList;

/**
 *2.  Definieren Sie für die Klasse "Gebaeude" einen Konstruktor, an den Sie die 
den Strassennamen, die Hausnummer, die Anzahl von Stockwerke und die Anzahl 
von Räumen pro Stockwerk übergeben können.

 * @author tverrbjelke
 */
public class CubicBuilding {
   
    /** 
     * All what is realated to the content od each level. 
     * Like Number of rooms. And like the rooms in that level.
     */
    public class Level{

        /** 
         * All what is related to the content of  each room.
         */
        public class Room{

            private int roomNr;
            
            private Room(int roomNr){
                this.roomNr = roomNr;
            }
            private int getRoomNr() {
                return this.roomNr;
            }
            
            /** 
             * @return Raum 0.2 / Hauptstr. 45
             */
            @Override
            public String toString(){
                return "Raum " + Level.this.levelNr + "." + this.roomNr;
            }
        }

        //****************************************
        
        private int levelNr;
        protected ArrayList<Room> roomsOfLevel;

        public Level(int levelNr, int numberOfRooms) throws InstantiationException{
            if ( numberOfRooms < 1 )  {
                throw new InstantiationException("Arguments invalid");
            }    
            this.levelNr = levelNr;
            this.roomsOfLevel = new ArrayList<Room>(numberOfRooms);
            for (int i=0; i< numberOfRooms; i++) {
                this.roomsOfLevel.add( new Room(i));
            }
        }

        public int getLevelNr() {
            return levelNr;
        }
        
        private Room getRoom(int roomNr) {
            Room resultRoom = null;
            for (Room currRoom : this.roomsOfLevel) {
                if (currRoom.getRoomNr() == roomNr){
                    resultRoom = currRoom;
                    break;
                }
            }
            return resultRoom;
        }

    }

    // ************************************************************************
    
    protected ArrayList<Level> levelsOfBuilding;
    protected String streetName, houseNumber;

    public CubicBuilding(   String streetName, 
                            String houseNumber, 
                            int numberOfLevels, 
                            int roomsPerLevel) throws InstantiationException {
        boolean areArgsOK = false;
        if ( (numberOfLevels >=1 ) && ( roomsPerLevel >=1 )) {
            areArgsOK = true;
        }

        if (!areArgsOK )  {
            throw new InstantiationException("Arguments invalid");
        }    
            
        this.streetName = streetName;
        this.houseNumber = houseNumber;
        this.levelsOfBuilding = new ArrayList<Level>(numberOfLevels);

        // now create each level
        for (int i=0; i < numberOfLevels; i++) {
            Level currLevel = new Level( i, roomsPerLevel);
            this.levelsOfBuilding.add(currLevel);
        }
    }

    // @todo sanity chekcs 
    // if ()
    public Level.Room getRoom(int levelNr, int roomNr){
        return this.getLevel(levelNr).getRoom(roomNr);
    }

    /** 
     * @param levelNr which level tosearch for
     * @return the 1st found level with this number, or null if not found.
     */
    private Level getLevel(int levelNr) {
        
        Level resultLevel = null;
        for (Level level: this.levelsOfBuilding){
            if (level.getLevelNr() == levelNr)
            {
                resultLevel = level;
                break;
            }
        }
        return resultLevel;            
    }

    
}
