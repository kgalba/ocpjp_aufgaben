/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.innereKlassen.gebaude;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tverrbjelke
 */
public class TestCubicBuilding {
    
    /**
     * 3.  Erstellen Sie ein Objekt von Typ "Gebaeude". 
     * Dabei soll ein Gebäude auf der Hauptstr. 45 
     * mit 3 Stockwerken und 10 Räume pro Stockwerk entstehen. 
     * @param args not used
     */
    public static void main(String[] args) {
        try {
            CubicBuilding gebaude = new CubicBuilding("Hauptstr.", "45", 3, 10);
            
             CubicBuilding.Level.Room myRoom = gebaude.getRoom(0,2);
             // 6.  Geben Sie die von der Methode gelieferte Referenz auf der Konsole aus. 
             // Die Ausgabe soll dabei folgende Form haben:
             // Raum 0.2 / Hauptstr. 45
             System.out.println(myRoom);
             
        } catch (InstantiationException ex) {
            System.out.println("Exception " + ex );
            Logger.getLogger(TestCubicBuilding.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
