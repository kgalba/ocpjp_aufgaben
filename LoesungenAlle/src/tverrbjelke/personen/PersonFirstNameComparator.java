/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.personen;

import java.util.Comparator;

/**
 * @see http://www.programcreek.com/2013/11/arrays-sort-comparator/
 * @author tverrbjelke
 */
public class PersonFirstNameComparator implements Comparator<Person> {

    @Override
    public int compare(Person o1, Person o2) {
        return o1.getFirstName().compareTo( o2.getFirstName() );
    }
    
    // todo equals() and xxhashxx()
    
}
