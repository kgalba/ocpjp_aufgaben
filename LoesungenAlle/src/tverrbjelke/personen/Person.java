/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.personen;

import java.util.Arrays;

/**
 *
 * @author tverrbjelke
 */
public class Person implements Comparable<Person>{

    int yearOfBirth;

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }
    String lastName;
    String firstName;

    public Person(String firstName, String lastName, int yearOfBirth ) {
        this.yearOfBirth = yearOfBirth;
        this.lastName = lastName;
        this.firstName = firstName;
    }
    
    @Override
    public int compareTo(Person o) {
        return Integer.compare(this.yearOfBirth, o.yearOfBirth);
    }
    
    @Override
    public String toString(){
        return "Person " + getFirstName() + " "+ getLastName() + ", geb. " + getYearOfBirth();
                
    }
    public static void main(String[] args) {
        Person[] arrayPersonen = {
                new Person("Alfred Hans", "Bechler", 1906),
                new Person("Berta", "Adler", 1907),            
                new Person("Max", "Adler", 1907),
                new Person("Bernd", "Pferd", 1902),
                new Person("Ute", "Vogel", 1903),
            };
        
        System.out.println("Unordered Personen = " + Arrays.deepToString(arrayPersonen));

        Arrays.sort(arrayPersonen);
        System.out.println("Simple Arrays.sort() = " + Arrays.deepToString(arrayPersonen));

        java.util.Arrays.sort(arrayPersonen, new PersonFirstNameComparator() );
        System.out.println("Sorted by FirstName = " + Arrays.deepToString(arrayPersonen));

        java.util.Arrays.sort(arrayPersonen, new PersonLastThenFirstNameComparator() );
        System.out.println("sorted by LastThenFirstname = " + Arrays.deepToString(arrayPersonen));                
    }
}
