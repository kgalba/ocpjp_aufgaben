/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.personen;

import java.util.Comparator;

/**
 *
 * @author tverrbjelke
 */
public class PersonLastThenFirstNameComparator implements Comparator<Person> {

    @Override
    public int compare(Person o1, Person o2) {
        int compareVal = o1.getLastName().compareTo(o2.getLastName());

        if (compareVal == 0 ){ // equal?
            return o1.getFirstName().compareTo( o2.getFirstName());
        }
        else
        {
            return compareVal;
        }
    }
    
    
}
