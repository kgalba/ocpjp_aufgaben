/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.generics;

/**
 * Watch out EU is pluggable to CHE but not electrical compatibel! 
 * EU apliances connected to CHE plugs will smoke out.
 * @author tverrbjelke
 */
enum PlugNorm { CHE, EU, JP, UK, US,}

class PlugNormClass {
    @Override
    public String toString(){ return this.getClass().getName(); }
}

class PlugNormCHE extends PlugNormClass{}
class PlugNormEU extends PlugNormClass{}
class PlugNormJP extends PlugNormClass{}
class PlugNormUK extends PlugNormClass{}
class PlugNormUS extends PlugNormClass{}

class TV<T>
{
    T norm;
    
    TV(T norm) { this.norm = norm; }
    
    public String toString (){
        return String.format("TV<%s>" , norm.toString());
    }
}

class Plug<T>{
    T norm;
    
    Plug(T norm) { this.norm = norm; }
    
    void doPlug(TV<T> telli) {
        System.out.printf("We plug telly %s in plug<%s>.\n", telli.toString() , this.norm.toString());
    }

    public String toString (){
        return String.format("Plug<%s>" , norm.toString());
    }
}

/**
 *
 * @author tverrbjelke
 */
public class Plugs {
    
    static void pluggWithEnumType(){
        System.out.println("pluggWithEnumType()");
        TV<PlugNorm> deTV = new TV<>(PlugNorm.EU);
        TV<PlugNorm> ukTV = new TV<>(PlugNorm.UK);

        Plug<PlugNorm> dePlug = new Plug<>(PlugNorm.EU);

        dePlug.doPlug(deTV); // We plug telly TV<EU> in plug<EU>.
        // Ups it compiles, because all use same enum type!
        dePlug.doPlug(ukTV); // We plug telly TV<UK> in plug<EU>.        
    }
    
    static void pluggWithClassTypes(){
        System.out.println("pluggWithClassTypes()");
        PlugNormCHE normCHE = new PlugNormCHE();
        PlugNormUK normUK = new PlugNormUK();
        PlugNormClass norm = normUK;
        
        TV<PlugNormCHE> cheTV = new TV<>(normCHE);
        TV<PlugNormUK> ukTV1 = new TV<>( (PlugNormUK) norm); // not compiles without cast!
        TV<PlugNormUK> ukTV2 = new TV<>(normUK); // not compiles!
        
        Plug<PlugNormCHE> chePlug = new Plug<>(normCHE);
        chePlug.doPlug(cheTV);
        //chePlug.doPlug(ukTV1); // does not compile!
        
        Plug<PlugNormUK> ukPlug = new Plug<>(normUK); // We plug telly TV<tverrbjelke.generics.PlugNormCHE> in plug<tverrbjelke.generics.PlugNormCHE>.
        ukPlug.doPlug(ukTV1); // We plug telly TV<tverrbjelke.generics.PlugNormUK> in plug<tverrbjelke.generics.PlugNormUK>.
        ukPlug.doPlug(ukTV2); // We plug telly TV<tverrbjelke.generics.PlugNormUK> in plug<tverrbjelke.generics.PlugNormUK>.
        //ukPlug.doPlug(cheTV); // not compiles!
        
    }
    
    public static void main(String[] args) {
        System.out.println("Plugs main");
        System.out.println("----------------");

        
        pluggWithEnumType();
        System.out.println("----------------");
        pluggWithClassTypes();
        System.out.println("----------------");
    }
}
