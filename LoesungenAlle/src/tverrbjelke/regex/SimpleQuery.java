/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.regex;

import java.util.regex.Pattern;

/**
 *
 * @author tverrbjelke
 */
public class SimpleQuery {
    public static void main(String[] args) {
        String source = "Hallo Java";
        String exp = "a";
        java.util.regex.Pattern p = Pattern.compile(exp);
        java.util.regex.Matcher m = p.matcher(source);
        
        while (m.find() ){
            System.out.printf("Treffer: \"%s\" at [%d - %d] \n", m.group(), m.start(), m .end());
        }
    }
}
