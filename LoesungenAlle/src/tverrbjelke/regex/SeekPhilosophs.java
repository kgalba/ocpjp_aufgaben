///*
//Aufgabe "Regex - Philosophen"
//
//Gegeben ist die Liste antiker Philosophen im Web:
//    http://de.wikipedia.org/wiki/Liste_antiker_Philosophen
//
//- Speichern Sie den Html-Text der Seite bitte in einer Textdatei. Sie können 
//  es mit den Browser-Mitteln tun. Alternativ kann die Klasse "java.net.URL"
//  verwendet werden:
//
//    java.net.URL url = new URL("http://de.wikipedia.org/wiki/Liste_antiker_Philosophen");
//    InputStream is = url.openStream();
//    InputStreamReader reader = new InputStreamReader(is, "UTF-8");
//    BufferedReader in = new BufferedReader(reader);
//    ...
//
//- Laden Sie wieder den Html-Text aus der gespeicherten Textdatei in ein String
//
//- Parsen Sie den geladenen Html-Text, suchen Sie dabei die Namen der Philosophen
//    - Benutzen Sie beim Parsen EINFACHE reguläre Ausdrücke (nicht übertreiben!)
//    - Benutzen Sie bei der Suche auch die Methoden der Klasse String, wenn es 
//      für Sie sinnvoll und bequem erscheint.
//    - Benutzen Sie beliebige andere Klassen/Methoden, die Ihnen bei der Suche
//      nach den Philosophennamen helfen können.
//
//  Achtung! Der Html-Text einer fremden Html-Seite kann sich jeden Tag ändern. 
//  Bitte investieren Sie nicht zu viel Zeit in das Ausarbeiten eines universellen
//  regulären Ausdrückes, der evtl. morgen nicht mehr gelten wird.
//
//- Speichern Sie die gefundenen Philosophennamen in einer List
//
//- Geben Sie die Anzahl der gefundenen Namen und die Namen aus
//  
//Optional:
//   - Suchen Sie nach den Geburtsjahren/Todesjahren der Philosophen im Text
//   - Suchen Sie nach Notizen, die es zu einigen Philosophen gibt
//   - Speichern Sie den Namen, das Geburtsjahr, das Todesjahr, die Notiz in einem
//     Person-Objekt
//   - erstellen Sie eine List aller Philosphen-Personen
// */
//package tverrbjelke.regex;
//
//import java.io.BufferedOutputStream;
//import java.io.BufferedReader;
//import java.io.BufferedWriter;
//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.FileOutputStream;
//import java.io.FileReader;
//import java.io.FileWriter;
//import java.io.IOException;
//import java.io.OutputStream;
//import java.net.MalformedURLException;
//import java.net.URL;
//import java.util.ArrayList;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
//
///**
// *
// * @author tverrbjelke
// */
//public class SeekPhilosophs {
//
//    static final String url =  "https://de.wikipedia.org/wiki/Liste_antiker_Philosophen";
//    static final String fileName = "philosophers.html";
//    
//    public static void main(String[] args) {
//        //this config is to be adopted
//        String sourceText = getSourceText(false);
//        //System.out.println(sourceText);        
//        System.out.println("parsing philosophers from text...");        
//        ArrayList<String> philosophers = parsePhilosophers(sourceText);
//        System.out.printf("Found %d philosophers:\n", philosophers.size());        
//    
//        System.out.println(philosophers);
//    }
//
//    static private String getSourceText(boolean isFromInternet){
//        final boolean isFromFile = !isFromInternet;
//        String sourceText = null;
//        File sourceFile = new File(fileName);
//
//        if(isFromInternet){
//            try{
//                System.out.println("Reading from URL" + url);
//                sourceText = Internet.ReadFromURL(url);
//            }
//            catch ( MalformedURLException ex){
//                Logger.getLogger(SeekPhilosophs.class.getName()).log(Level.SEVERE, "Alas the url was malformed, could not read stuff" );
//                Logger.getLogger(SeekPhilosophs.class.getName()).log(Level.SEVERE, null, ex);                
//            }
//            catch (IOException ex){
//                Logger.getLogger(SeekPhilosophs.class.getName()).log(Level.SEVERE, "Alas I could not read stuff " );
//                Logger.getLogger(SeekPhilosophs.class.getName()).log(Level.SEVERE, null, ex);
//            }
//                
//            System.out.println("Writing text to file " + sourceFile);
//            writeToFile(sourceText, sourceFile);                
//        }
//        
//        if (isFromFile){
//            System.out.println("reading text form file " + sourceFile);
//            sourceText = getSourceTextFromFile(sourceFile);
//        }        
//        return sourceText;
//    }
//    
//    private static void writeToFile(String sourceText, File sourceFile) {
//        try ( FileWriter fw = new FileWriter( sourceFile); BufferedWriter bw = new BufferedWriter(fw )){
//            bw.write(sourceText);
//        }
//        catch (IOException ex) {
//            Logger.getLogger(SeekPhilosophs.class.getName()).log(Level.SEVERE, "writing failed!", ex);
//        }
//    }
//
//    private static String getSourceTextFromFile(File sourceFile) {
//        String sourceText = "";
//        try ( FileReader fr = new FileReader( sourceFile); BufferedReader  br = new BufferedReader(fr )){
//            String line;
//            while ( ( line = br.readLine()) != null){
//                sourceText = sourceText + line + "\n";
//            }
//        }
//        catch (IOException ex) {
//            Logger.getLogger(SeekPhilosophs.class.getName()).log(Level.SEVERE, "reading from file failed!" + sourceFile.toString(), ex);
//            return null;
//        }
//        return sourceText;
//    }
//
//    private static ArrayList<String> parsePhilosophers(String sourceText) {
//        ArrayList<String> philosophersList = new ArrayList<>();
//        Pattern consumeHeaderPattern = Pattern.compile("<body.+>.+?<h2>Inhaltsverzeichnis</h2>", Pattern.DOTALL);
//        Matcher matcher = consumeHeaderPattern.matcher(sourceText);
//        int postHeader;
//        if (matcher.find()){
//            System.out.println("groupCount=" + matcher.groupCount());
//                
//            for (int i = 0; i <= matcher.groupCount(); i++){ 
//                System.out.println( "Group: " + i + " |" +  matcher.group(i) + "| ");
//                //System.out.printf("index [%d, %d]: |%s|\n"  , matcher.start(i) 
//                //                                            , matcher.end(i)
//                //                                            , sourceText.substring(matcher.start(i), matcher.end(i)) ) ;
//            }
//            
//        }
//        else{
//            System.out.println("No header found.");
//        }
//        
//        return philosophersList;
//    }
//    
//}
