/* Aufgabe "Arrays 01"

Bitte definieren Sie eine Methode 'createArray', die ein int-Array erzeugt 
und mit Zufallswerten belegt. Die neue Methode soll aus der main-Methode 
folgendermassen aufgerufen werden koennen:
    
    int[] arr = createArray(2, 15, 30);

In diesem Beispiel wird ein Array der Länge 30 erzeugt und mit den 
Zufallswerten aus dem Bereich [2 .. 15] belegt.

Bitte geben Sie das erzeugte Array aus. Dafür definieren Sie eine weitere 
Methode 'printArray', an die Sie das Array übergeben.
*/
package tverrbjelke.Arrays;

/**
 *
 * @author tverrbjelke
 */
public class Array01 
{
    public static void main( String[] args)
    {
        // todo where to set seed for random? HERE? or is there a nicer place in java?
        
        // 1st thing to do is - hello world programm!
        int[] arr = createArray(2, 15, 30);
        for( int i = 0;i< arr.length; i++)
        {
            String msg = "Array Element " + Integer.toString(i) +  " is " + Integer.toString( arr[i] );
            System.out.println (msg);
        }
        
    }
   
    /** ok this expects useful arguments, like min<max and len>0 
     *  @returns returns array filled with random values or null 
     *  or in case arguments are insane - throws exception? (which?)
     * 
     *  we use Math.random() for now, so you might know where to change the seed.
     */
    public static int[] createArray( int min_val, int max_val, int array_length)
    {
        double tmp_delta = (double) (max_val - min_val);
        
        int arr_val[] = null;
        if (array_length >0)
        {
            arr_val = new int [array_length];
        }

        for (int i = 0; i< arr_val.length; i++)
        {
            arr_val[i] = min_val + (int) (Math.random() * tmp_delta); // omg - all that range calculation by hand???       
        }
        return arr_val;
    }

}
