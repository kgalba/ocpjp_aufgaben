/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.thread.philosophenproblem;

import java.util.Random;

/**
 *
 * @author tverrbjelke
 */
class Philosoph implements Runnable {
    boolean doSuicide = false;
    String name;
    Object leftFork = null;
    Object rightFork = null;

    public Philosoph(String name, Object leftFork, Object rightFork) {
        this.name = name;
        this.leftFork = leftFork;
        this.rightFork = rightFork;
    }

    @Override
    public void run() {
        while (!doSuicide) {
            think();
            System.out.println("Philosoph " + name + " will try to pickup left fork...");
            synchronized (this.leftFork) {
                pickLeftFork();
                System.out.println("Philosoph " + name + " will try to pickup right fork...");
                synchronized (this.rightFork) {
                    pickRightFork();
                    eat();
                    dropRightFork();
                }
                dropLeftFork();
            }
        }
        doAction("performs suicide", 10);
    }

    private void doAction(String msg, int milliSecs) {
        try {
            System.out.println("Philosoph " + name + " " + msg);
            Thread.sleep(milliSecs);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void think() {
        // thinking is sleeping, lazy bastard!
        doAction("thinking", new Random().nextInt(100));
    }

    public void eat() {
        doAction("eating", new Random().nextInt(200));
    }

    public void pickLeftFork() {
        doAction("picking up left fork " + leftFork.toString(), 200);
    }

    public void pickRightFork() {
        doAction("picking up right fork " + rightFork.toString(), 200);
    }

    public void dropLeftFork() {
        doAction("dropping left fork " + leftFork.toString(), 10);
    }

    public void dropRightFork() {
        doAction("dropping right fork " + rightFork.toString(), 10);
    }
    
}
