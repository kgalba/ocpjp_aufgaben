/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.thread.philosophenproblem;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tverrbjelke
 */
class PhilosophenRunde implements Runnable {
    ArrayList<Philosoph> philosophs;
    ArrayList<Object> forks;

    PhilosophenRunde(ArrayList<String> namen) {
        createForks(namen.size());
        this.philosophs = new ArrayList<>(namen.size());
        for (int i = 0; i < namen.size(); ++i) {
            Philosoph p = new Philosoph(namen.get(i), getLeftFork(i), getRightFork(i));
            this.philosophs.add(p);
        }
    }

    @Override
    public void run() {
        // todo all in separate thread + join
        LinkedList<Thread> ts = new LinkedList<Thread>();
        for (Philosoph p : this.philosophs) {
            ts.add(new Thread(p));
        }
        for (Thread t : ts) {
            t.start();
        }
        System.out.println("waiting for all philosophers to terminate...");
        for (Thread t : ts) {
            while (t.isAlive()) {
                try {
                    t.join();
                } catch (InterruptedException ex) {
                    System.out.println("interrupted, cold not join with t ");
                }
            }
        }
    }

    public void comandGenozide() {
        for (Philosoph p : philosophs) {
            p.doSuicide = true;
        }
    }

    private void createForks(int number) {
        this.forks = new ArrayList<>(number);
        for (int i = 0; i < number; ++i) {
            this.forks.add(new Integer(i));
        }
    }

    int size() {
        return philosophs.size();
    }

    private Object getLeftFork(int n) {
        final int index;
        if (0 >= n) {
            index = this.forks.size() - 1;
        } else {
            index = n - 1;
        }
        return this.forks.get(index);
    }

    private Object getRightFork(int n) {
        return this.forks.get(n);
    }
    
}
