/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.thread.philosophenproblem;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tverrbjelke
 */
public class PhilosophenProblem {
    PhilosophenRunde runde;

    public PhilosophenProblem() {
        ArrayList<String> namen = new ArrayList<>(7);
        namen.add("Rote Zora");
        namen.add("Hans Albers");
        namen.add("Eric von Däneken");
        namen.add("Hägar");
        namen.add("Alfred E. Neumann");
        namen.add("Zorro");
        namen.add("Ted");
        
        this.runde = new PhilosophenRunde(namen);
    }
    

    private static void philoTest() throws InterruptedException{
        Integer links = new Integer(0);
        Integer rechts = new Integer(1);
        Philosoph p = new Philosoph("Obelix", links, rechts);
        Thread t = new Thread(p);
        System.out.println("P laufen lassen...");
        t.start();
        Thread.sleep(4000);
        System.out.println("P killen lassen...");
        p.doSuicide = true;
        System.out.println("P killed...");
        while (t.isAlive()){
            System.out.println("P still alive...");
            Thread.sleep(200);
        }
        
    }
    
    static public void main(String[] args) throws InterruptedException{
        //philoTest();
        
        PhilosophenProblem problem = new PhilosophenProblem();
        
        Thread t = new Thread(problem.runde);
        t.start();
        System.out.println("Runde gestartet...");
        if (true){ while (true){}}
        Thread.sleep(66666);

        System.out.println("Runde killt sich...");
        problem.runde.comandGenozide();
        while (t.isAlive()){
            System.out.println("P still alive...");
            Thread.sleep(500);
        }
        
    }
    
    
}



