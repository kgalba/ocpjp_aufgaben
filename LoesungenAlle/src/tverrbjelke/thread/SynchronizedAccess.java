/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.thread;


/**
 *
 * @author tverrbjelke
 */
public class SynchronizedAccess {
    
    static class SafeShare{

        static private int count = 0;
        static private Object syncLock = new Object();

        static public int getCount(){
            synchronized(syncLock){return count;}
        }
        
        static public int decCount(){
            synchronized(syncLock){ count--; return count;}
        }

        static public int incCount() {
            synchronized(syncLock){ count++; return count;}
        }
        
    }

    ////////////////////

    static class Increment extends Thread{
        private final int maxIterations;

        public Increment(int numIterations) {
            this.maxIterations = numIterations;
        }

        @Override
        public void run(){
            for (int i=0; i<maxIterations; ++i){
                SafeShare.incCount();
            }
        }
    }

    //////////////////////

    static class Decrement extends Thread{
        private final int maxIterations;

        public Decrement(int maxIterations) {
            this.maxIterations = maxIterations;
        }

        @Override
        public void run(){
            for (int i=0; i< this.maxIterations; ++i){
                SafeShare.decCount();
            }
        }
    }

    /////////////////////////////
    
    public static void main(String[] args) throws InterruptedException {
        int numIterations =1000*1000*1000;
        
        Thread inc = new Increment(numIterations);
        Thread dec = new Decrement(numIterations);
        System.out.println("Initial count:" + SafeShare.getCount());
        
        inc.start();
        dec.start();
        inc.join();
        dec.join();
        System.out.println("End count: (should be 0)" + SafeShare.getCount());
//        Initial count:0
//        End count: (should be 0)0
//        BUILD SUCCESSFUL (total time: 1 minute 34 seconds)
        
    }
}    

