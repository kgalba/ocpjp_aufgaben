/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.thread.WeatheStation;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author tverrbjelke
 */
class LimitedStack {
    private final int maxSize;
    private final LinkedList<Double> data;

    public LimitedStack(int maxSize) {
        this.maxSize = maxSize;
        this.data = new LinkedList<>();
    }

    /**
     * implement later:  return last fallen over element.
     * @param element
     */
    public void add(Double element) {
        while (data.size() >= maxSize) {
            data.removeLast();
        }
        data.addFirst(element);
    }

    public Double get(int index) {
        return data.get(index);
    }

    public int size() {
        return data.size();
    }

    @Override
    public String toString() {
        String val = "LimitedStack: ";
        for (Double e : this.data) {
            val += e.toString() + ", ";
        }
        return val;
    }

    public List<Double> subFromFirst(int end){
        return data.subList(0, end);
    }
}
