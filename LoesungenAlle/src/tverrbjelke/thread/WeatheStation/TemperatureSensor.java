/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.thread.WeatheStation;

/**
 *
 * @author tverrbjelke
 */
class TemperatureSensor implements Runnable {
    LimitedStack buffer;
    double lastTemperature;
    boolean stayAlive;

    public TemperatureSensor(LimitedStack buffer) {
        this.buffer = buffer;
        this.lastTemperature = 0.0;
        this.stayAlive = false;
    }

    private Double getTemperature() {
        final double range = 10.0;
        final double delta = (Math.random() * range) - (range / 2.0);
        double newTemp = this.lastTemperature + delta;
        return newTemp;
    }

    @Override
    public void run() {
        this.stayAlive = true;
        while (this.stayAlive) {
            if (buffer != null){
                synchronized (buffer) {
                    buffer.add(getTemperature());
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    System.out.println("TemperatureSensor was interrupted!");
                    e.printStackTrace();
                }
            }
        }
        System.out.println(Thread.currentThread().getName() + " ends.");

    }
    
}
