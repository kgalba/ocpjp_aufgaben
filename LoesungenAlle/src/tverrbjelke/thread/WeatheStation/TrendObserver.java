/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.thread.WeatheStation;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tverrbjelke
 */
class TrendObserver implements Runnable {
    private final LimitedStack buffer;
    public boolean stayAlive;
    private int trendDuration;

    public TrendObserver(LimitedStack buffer, int tendencyDuration) {
        this.buffer = buffer;
        this.trendDuration = tendencyDuration;
        this.stayAlive = false;
    }

    @Override
    public void run() {
        this.stayAlive = true;
        while (stayAlive) {
            if (this.buffer != null){
                if (isTrendUp()) {
                    System.out.println("TrendObserver reports a trend up: ");
                    List<Double> trendData = reverseList( buffer.subFromFirst(trendDuration) );
                    System.out.println(Arrays.deepToString( trendData.toArray()));
                }
                if (isTrendDown()) {
                    System.out.println("TrendObserver reports a trend down: ");
                    List<Double> trendData = reverseList( buffer.subFromFirst(trendDuration) );
                    System.out.println(Arrays.deepToString( trendData.toArray()));
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    System.out.println("TrendObserver was interrupted" );
                    Thread.currentThread().interrupt();
                }
            }
        }
        System.out.println(Thread.currentThread().getName() + " ends.");
    }

    
    /**
     * since the list is newest lowest index, we want to reverse order in output...
     * @param trend
     * @return inversed Order list
     */
    private <E> List<E> reverseList(List<E> trend){
        LinkedList<E> reversedCopy = new LinkedList<>();
        for (E e : trend){
            reversedCopy.addFirst(e); 
        }
        return reversedCopy; // it is a linked list
    }
    
    boolean isTrendUp() {
        return isTrend( (Double a, Double b) -> { return Double.compare(a, b);} );
    }

    boolean isTrendDown() {
        return isTrend( (Double a, Double b) -> { return Double.compare(b, a);} );
    }

    private boolean isTrend(Comparator<Double> comp){
        boolean isTrend;
        synchronized (this.buffer) {
            // we have a trend only if collected data series already is sufficient
            if (this.buffer.size() >= this.trendDuration) {
                isTrend = true;
                for (int i = 1; i < this.trendDuration; i++) {
                    Double newer = buffer.get(i-1);
                    Double older = buffer.get(i);
                    if ( comp.compare(older, newer) > 0 ){
                        isTrend= false;
                        break;
                    }
                }
            }
            else { isTrend = false; }
        }
        return isTrend;
    }
}
