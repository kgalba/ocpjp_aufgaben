/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.thread.WeatheStation;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tverrbjelke
 */
public class ExtremeObserver implements Runnable{
    public boolean stayAlive; // to false shuts down this runnable.
    private LimitedStack buffer;
    private double lowAlert;
    private double highAlert;
    
    public ExtremeObserver(LimitedStack buffer, double lowAlert, double highAlert) {
        this.buffer = buffer;
        this.lowAlert = lowAlert;
        this.highAlert = highAlert;
        this.stayAlive = false;
    }
    
    
    @Override
    public void run() {
        this.stayAlive = true;
        while(this.stayAlive){
            synchronized(buffer){
                if ( buffer != null && buffer.size()>0 ){
                    Double temp = buffer.get(0);
                    if (temp <= this.lowAlert){
                        System.out.println("Warning! Temperature critically low: " + temp);
                    }
                    if (temp >= this.highAlert){
                        System.out.println("Warning! Temperature critically high: " + temp);                        
                    }
                }
            }
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                System.out.println("ExtremeObserver was interrupted...");
                Thread.currentThread().interrupt();
            }
        }
        System.out.println(Thread.currentThread().getName() + " ends.");
    }
}
