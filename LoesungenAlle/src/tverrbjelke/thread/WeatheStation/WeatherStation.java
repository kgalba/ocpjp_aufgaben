/*
Aufgabe "Wetterstation"

- In einem Thread wird jede 100 mSek die Umgebungstemperatur gemessen. 
Simuliert wird jede Messung durch das Generieren eines neuen Wertes als Zufallszahl, 
die im Bereich des alten Wertes +/- 5 Grad liegt.

- In einem anderen Thread werden die drei letzten aktuellen Messungen analysiert (1 mal pro Sekunde).
  Falls in den 3 Messungen der stätige Abfall gefunden wird, wird die Tendenz 
  auf der Konsole visualisiert. Dasselbe zur stätigen Steigung.

- Im dritten Thread werden Warnungen ausgegeben, falls die aktuelle Themperatur
  einen Grenzwert überschreitet (zu warm) oder unterschreitet (zu kalt). Auch die
  Kontrollen werden nur periodisch (1 Mal in 2 Sek) durchgeführt

- Threads bitte synchronisieren
 */
package tverrbjelke.thread.WeatheStation;

import java.util.Date;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;






/**
 *
 * @author tverrbjelke
 */
public class WeatherStation {
    public static void main(String[] args) {
        LimitedStack buffer = new LimitedStack(3);

        TemperatureSensor sensor = new TemperatureSensor(buffer);
        Thread tSensor = new Thread(sensor);
        tSensor.start();
        System.out.println("TemperatureSensor up...");
        
        TrendObserver trend = new TrendObserver(buffer, 3);
        Thread tTrend = new Thread(trend);
        tTrend.start();
        System.out.println("TrendObserver up...");

        ExtremeObserver extreme = new ExtremeObserver(buffer, -3.0, 3.0);
        Thread tExtreme = new Thread(extreme);
        tExtreme.start();
        System.out.println("ExtremeObserver up...");
                
        System.out.println("Press <Enter> to quit...");
        Scanner s = new Scanner (System.in);
        String inputStr = s.nextLine();
        
        System.out.println("Shutting system down...");
        sensor.stayAlive = false;
        trend.stayAlive = false;
        extreme.stayAlive = false;
        try {
            tSensor.join();
            tTrend.join();
            tExtreme.join();
        } catch (InterruptedException ex) {
            System.out.println("WeatherStation.main() got interrupted during join()...");
            //Thread.currentThread().interrupt(); // pass signal on
        }
        
        System.out.println("System is now shut down!");
        
    }
}

