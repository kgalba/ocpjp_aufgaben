/*
Aufgabe "Threads - Directory Observer"

- Erstellen Sie eine EINFACHE Consolenanwendung, die die Anzahl der Dateien
  in einem Verzeichnis überwachen kann.

- Beim Starten wird die Anwendung den Namen des Verzichnisses und die aktuelle
  Anzahl der Dateien ausgeben. Dann läuft die Anwendung endlos und ermittelt
  in einem EXTRA-Thread immer wieder die Anzahl der Dateien im Verzeichnis.

- Um die CPU nicht zu sehr zu belasten wird die Anwendung die Ermittlung der
  Dateienanzahl nicht öfter als ein mal pro ca. 2 Sekunden durchführen. Wird eine Änderung
  ermittelt, benachrichtigt die Anwendung den User, wie viele Dateien seit der 
  letzten Ermittlung erzeugt oder gelöscht wurden.

- Der main Thread soll nach 1 Min. die Anwendund runterfahren. intterupt ausprobieren.

!Die Anwendung soll nicht ermitteln, welche Dateien gelöscht oder erzeugt werden!

- Optional. Erzeugen Sie eine Swing-Anwendung, die ähnlich funktioniert.
    - Die Anwendung erlaubt es dem User das zu beobachtende Verzeichnis zu
      bestimmen. Tipp: JFileChoose kann verwendet werden
    - Das Überwachen soll gestartet und beendet werden können. Das Beenden einer
      laufnden Überwachung soll möglichst ohne Verzögerung stattfinden.

*/
package tverrbjelke.thread;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author tverrbjelke
 */
public class DirectoryObserver implements  Runnable {
    String folderPath;
    Integer numberOfFilesCurrent;
    Integer numberOfFilesLast;
    private boolean isSubthreadRunning;
            
    public DirectoryObserver(String folderPath) {
        this.folderPath = folderPath;
        this.numberOfFilesCurrent = null;
        this.numberOfFilesLast = null;
        this.isSubthreadRunning = false;
    }
    
    public static void main(String[] args) throws InterruptedException {
        
        String folder = null;
        if (args!= null && args.length > 0){
            folder = args[0];
        }
        if (folder == null){
            File root = File.listRoots()[0];
            folder = root.getPath();
        }
        folder = ".";
        try {
            folder = new File(folder).getCanonicalPath();
        } catch (IOException ex) {
            Logger.getLogger(DirectoryObserver.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println( "Using folder " + folder);
        DirectoryObserver observer = new DirectoryObserver(folder);
        
        Thread subThread;
        subThread = new Thread(observer);
        System.out.println("Starting subthread");
        subThread.start();
        Thread.sleep(60*1000);
        System.out.println("Stopping subthread...");
        observer.isSubthreadRunning = false;
        
    }

    void updateStatus(){
        try {
            // only update when succesfully got new numbers from filesystem
            int tmp = retrieveNumberOfFiles();
            this.numberOfFilesLast = this.numberOfFilesCurrent;
            this.numberOfFilesCurrent = tmp;
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
    
    void printStatus() {
        if ( this.numberOfFilesCurrent != null ) { // do we have something at all?
            if (this.numberOfFilesLast == null) { // fetched 1st time 
                System.out.println( "Folder:"  + this.folderPath + " contains "
                                    + this.numberOfFilesCurrent + " files." );
            }
            else
            {
                int diff = numberOfFilesCurrent - numberOfFilesLast;
                if ( diff > 0){
                    System.out.println("Folder:"  + this.folderPath + " has " + diff + " more Files." );
                }            
                if ( diff < 0){
                    System.out.println("Folder:"  + this.folderPath + " has " + (-1 * diff) + " less Files." );
                }            
            }
        }
    };

    private Integer retrieveNumberOfFiles() throws IOException{
        File folder = new File(folderPath);
        
        if ( folder.exists() && folder.isDirectory() ){
            return folder.listFiles().length; // SecurityException
        }
        else {
            throw new IOException("Invalid Folder " + this.folderPath );
        }

    }

    @Override
    public void run() {
        this.isSubthreadRunning = true;
        while(this.isSubthreadRunning){
            updateStatus();
            printStatus();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                System.out.println("We were interrupted ");
                Logger.getLogger(DirectoryObserver.class.getName()).log(Level.SEVERE, "got a Interrupted signal ", ex);
            }
        }
    }


} // class DirectoryObserver 

