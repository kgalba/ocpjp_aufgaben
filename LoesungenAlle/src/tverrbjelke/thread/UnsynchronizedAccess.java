/*
* Klasse Increment erweitert Thread.   
- in der run wird die statische int Share.count in einer Schleife 100_000_000 Mal inkrementiert   
* Klasse Decrement erweitert Thread.   
- in der run wird dieselbe statische int Share.count in einer Schleife 100_000_000 Mal dekrementiert   
* in der main Increment und Decrement Threads starten. main soll sich den beiden Threads 'joinen'   
* in der main Share.count ausgeben

* Optional: synchronized-Blöcke 
*/
package tverrbjelke.thread;

import java.util.logging.Level;
import java.util.logging.Logger;

class Increment extends Thread{
    
    private final int maxIterations;
    
    public Increment(int numIterations) {
        this.maxIterations = numIterations;
    }
    
            
    @Override
    public void run(){
        for (int i=0; i<maxIterations; ++i){
            --Share.count;
        }
    }

}

class Decrement extends Thread{
    private final int maxIterations;

    public Decrement(int maxIterations) {
        this.maxIterations = maxIterations;
    }
    
    @Override
    public void run(){
        for (int i=0; i< this.maxIterations; ++i){
            --Share.count;
        }
    }
    
}

class Share{
    static int count = 0;
}


/**
 *
 * @author tverrbjelke
 */
public class UnsynchronizedAccess {
    public static void main(String[] args) {
        System.out.println("Result should be exactly 0: is:" + Share.count);
            int iterations = 1000*1000;//*1000;
            Increment i = new Increment(iterations);
            Decrement d = new Decrement(iterations);

            i.start();
            d.start();
            
        try {
            i.join();
            d.join();
        } catch (InterruptedException ex) {
            
            Logger.getLogger(UnsynchronizedAccess.class.getName()).log(Level.SEVERE, "Somebody interrupted subtask or me", ex);
            
        }
        System.out.println("Result should be exactly 0: is:" + Share.count);
    }
}
