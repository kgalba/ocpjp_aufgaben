/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.files;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *Aufgabe "File - Laufwerke".

- Definieren Sie in einer Klasse "Laufwerke" eine statische Methode "print". 
  Die Methode soll für alle Laufwerke eine formatierte Tabelle auf der Konsole 
  ausgeben. Die Tabelle soll folgende Form haben:

|   LW  |       Frei(MB)|    Belegt(MB) |     Gesamt(MB)|
|   C:\ |            ...|            ...|            ...|
|   D:\ |            ...|            ...|            ...|
...

  Die Spalte "LW" beinhaltet den Laufwerknamen. In den restlichen Spalten werden
  der freie, der belegte und der gesamte Speicher für das jeweilige Laufwerk 
  angezeigt. Die Werte für die Spalten "Frei", "Belegt" und "Gesamt" ermitteln 
  Sie bitte mit den Methoden "getFreeSpace" und "getTotalSpace" der Klasse 
  java.io.File. Die Ausgaben erfolgen in Megabyte (MB). 

Tipp: Benutzen Sie die Methode "listRoots" um alle aktuellen Laufwerke zu 
ermitteln.

- Sortieren Sie bitte die Daten nach der Spalte "Frei" und geben Sie die aus.

- Optional. Gestalten Sie bitte die Anwendung interaktiv. Der Benutzer darf sich
  entscheiden ob die Speicherangaben in Gigabyte, Megabyte oder Kilobyte 
  erscheinen. 

Tipp: Klasse java.util.Scanner für das Auslesen der Benutzereingaben einsetzen

 * @author tverrbjelke
 */
public class Drives {
    static final String colName = "Name";
    static final String colFree = "Frei";
    static final String colOccupied = "Belegt";
    static final String colTotal = "Gesamt";

    public enum SizeUnit { KB(1024), MB(1024*1024), GB(1024*1024*1024);
        final long value;
        
        private SizeUnit(long value){
            this.value = value;
        }
    };

    static class DriveStats{
        String name;
        long free;
        long occupied;
        long total;
        
        DriveStats(String name, long free, long occupied, long total){
            this.name = name;
            this.free = free;
            this.occupied = occupied;
            this.total = total;
        }
    }

    // ##############################################
    
    static final char colSep ='|'; 
    static final int nColumns  = 4;

    /** 
     * warning to all readers: Design bloat because of the hot weather!
     */
    static public void print(){
        print(SizeUnit.MB);
    } 

    static public void print(SizeUnit unit){
        ArrayList<StringBuilder[]> table = createTableFromDrives( getDrives(), unit);
        formatTable(table);
        for (StringBuilder[] row : table){
            String rowStr = "" + colSep;
            for (StringBuilder col : row){
                rowStr = rowStr + col.toString() + colSep;
            }
            System.out.println(rowStr);
        }                
    }

    public static void main(String[] args) {
        SizeUnit unit = readInput();
        System.out.println(unit.name()); 
        print(unit);
    }

    
    static SizeUnit readInput(){
        Scanner scanner = new Scanner(System.in);
        String usage = "Welche Einheit (" + SizeUnit.KB 
                       + ", " + SizeUnit.MB 
                       + ", "+ SizeUnit.GB + ")?";
        System.out.println(usage);
        String input = scanner.next();

        SizeUnit unit;
        if (input.equals(SizeUnit.KB.name())){
            unit = SizeUnit.KB;
        } else if (input.equals(SizeUnit.MB.name() )){
            unit = SizeUnit.MB;
        } else if (input.equals(SizeUnit.GB.name() )){
            unit = SizeUnit.GB;
        }
        else {
            unit = SizeUnit.MB;
        }
        return unit;
    }

    static StringBuilder alignRight(StringBuilder item, int width){
        return new StringBuilder( String.format("%"+width + "s", item));
    }

    /** 
     * Formats the width of each field.
     * @param table table of StringBuilders - to be formatted
     */
    static void formatTable( ArrayList<StringBuilder[]> table){
        for (int col = 0; col < Drives.nColumns; ++col){
            int maxWidth = getMaxColumnWidth(table, col);
            for (int rowNum = 0; rowNum < table.size(); ++rowNum){
                StringBuilder[] row = table.get(rowNum);
                StringBuilder item = row[col];
                StringBuilder newItem = alignRight(item, maxWidth);
                row[col] = newItem;
            }
                
        }
    }
    
    
    static ArrayList<StringBuilder[]> createTableFromDrives( ArrayList<DriveStats> drives, 
                                                    SizeUnit unit ){
        int nRows = drives.size() +1; // incl. header
        ArrayList<StringBuilder[]> rows = new ArrayList<StringBuilder[]>(nRows);

        rows.add ( getHeader(unit) );
        for (DriveStats d :drives){
            rows.add(drive2RowOfStringBuilders(d, unit.value));
        }
        return rows;
    }
    
    /** 
     * Deternins the maxlength of given column, over rows.
     * @param table the (unformatted) table of StringBuilders 
     * @param column get maxlen for which column
     * @return the max len of given column
     */
    static int getMaxColumnWidth(ArrayList<StringBuilder[]> table, int column){
        int maxWidth=0;
        for (StringBuilder[] row:table){
            if (row[column].length() > maxWidth){
                maxWidth = row[column].length() ;
            }                    
        }
        return maxWidth;
        
    }
    
    static StringBuilder[] getHeader(SizeUnit unit ){
        StringBuilder[] header = new StringBuilder[]{
            new StringBuilder(colName), 
            new StringBuilder(String.format("%s(%s)", colFree, unit.name() )),
            new StringBuilder(String.format("%s(%s)", colOccupied, unit.name() )),
            new StringBuilder(String.format("%s(%s)", colTotal, unit.name())),
        };
        return header;
    }
    
    static StringBuilder[] drive2RowOfStringBuilders(DriveStats d, long base){
        StringBuilder[] columns = new StringBuilder[nColumns]; 
        assert nColumns == 4; // we manually poke into the column array
        columns[0] = new StringBuilder(d.name);
        columns[1] = new StringBuilder( ).append( d.free / base );
        columns[2] = new StringBuilder( ).append( d.occupied / base);
        columns[3] = new StringBuilder( ).append( d.total /base );
        return columns;
    }
    
    static ArrayList<DriveStats> getDrives(){
        File[] rootFiles = File.listRoots();
        if (rootFiles == null) {
            System.out.println("No access to the root drives!");
            System.exit(1); // what else?
        }
        ArrayList<DriveStats> rootDrives = new ArrayList<DriveStats>(rootFiles.length);
        for ( File f : rootFiles){
            DriveStats d = parseFileIntoDrive(f);
            if (d != null ){
                rootDrives.add(d);
            };
        }                
        return rootDrives;
    }
    
    static DriveStats parseFileIntoDrive(File f){
        try {
             // hope that always total>free.            
            DriveStats d = new DriveStats(f.getName(), 
                                          f.getFreeSpace(),
                                          f.getTotalSpace() - f.getFreeSpace(), 
                                          f.getTotalSpace() );
            return d;
        }
        catch (SecurityException e){
            System.out.println("Accessing drive caused trouble... this drive is not accessible!");
            System.out.println(e.getCause()); 
            return null;
       }
    }
        
//    static private ArrayList<Drive> getAllDrives(){
//        ArrayList<Drive> allDrives = new ArrayList<Drive>(drives.length);
//
//        return null;
//    }
            
}
