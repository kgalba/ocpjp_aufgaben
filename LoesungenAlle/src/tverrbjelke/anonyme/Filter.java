/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.anonyme;
import tverrbjelke.anonyme.Person;

/**
 *
 * @author tverrbjelke
 */
public interface Filter {
    boolean accept(Person p);
}
