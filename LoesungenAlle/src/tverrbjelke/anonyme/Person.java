/*
- Jede Person hat Vornamen, Nachnamen und Geburtsjahr

- Das Interface Filter hat eine Methode 
        'boolean accept(Person)'

- Erzeugen Sie in der main-Methode einer Test-Klasse mindestens 4 Personen und
  speichern sie ein einem Array

- Definieren Sie in der Test-Klasse eine weitere Methode 
    Person[] filtern(Person[], Filter)
  Sie liefert ein Array zurück, in dem nur die Personen gespeichert sind, die
  mit dem übergebenem Filter (2. Parameter) akzeptiert werden. Die Methode
  sucht in dem Array, das als 1. parameter übergeben wird

- Testen Sie die neue Methode 'filtern' mit unterschiedlichen Filter. Z.B.
  kann ein Filter die Personen akzeptieren, die nach einem bestimmten Jahr
  geboren wurden. Benutzen Sie dabei, wenn möglich, anonyme Klassen.
 */
package tverrbjelke.anonyme;

/**
 *
 * @author tverrbjelke
 */
public class Person {
    String firstName, lastName;
    int yearOfBirth;

    public Person(String firstName, String lastName, int yearOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.yearOfBirth = yearOfBirth;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    @Override
    public String toString(){
        return getFirstName() + " " + getLastName() + ", geb. " + String.valueOf(getYearOfBirth());
    }
}
