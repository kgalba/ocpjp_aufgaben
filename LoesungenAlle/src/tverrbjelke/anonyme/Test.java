/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.anonyme;

import java.time.Clock;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

/**
 *
 * @author tverrbjelke
 */
public class Test {
    public static void main(String[] args) {
        Person[] testpersonen = { new Person("Hans", "A", 2015) 
                                  , new Person("Jesus", "von Nazareth", 0)
                                  , new Person("Jesus", "von Guinea", 1432)
                                  , new Person("Hans", "von Guinea", 1754)
                                  };
        
        
        System.out.println("Person Database" + Arrays.deepToString(testpersonen));
        
        
        Person[] filteredPersons = Test.filtern(testpersonen, 
            new Filter() {

                @Override
                public boolean accept(Person p) {
                    return true;
                };
            });
    
        System.out.println("Filtered by true: " + Arrays.deepToString(filteredPersons));
        
        filteredPersons = Test.filtern(testpersonen,
                                       new Filter(){
                                           @Override
                                           public boolean accept(Person p) {
                                               return false;
                                           }
                                       }
                                       );
        System.out.println("Filtered by false: " + Arrays.deepToString(filteredPersons));

       filteredPersons = Test.filtern(testpersonen,
                                      new Filter() {
                                          @Override
                                          public boolean accept(Person p){
                                              return p.firstName == "Jesus";
                                          }
                                      });

        System.out.println("Filtered by Jesus: " + Arrays.deepToString(filteredPersons));

        Filter f = (Person p) -> { return p.getYearOfBirth() > 1000;};
        filteredPersons = Test.filtern(testpersonen, f);
        System.out.println("Filtered by lambda >1000y: " + Arrays.deepToString(filteredPersons));

        java.util.function.Predicate<Person> accept = (Person p) -> { return p.yearOfBirth > 1500;};
        filteredPersons = Test.filternPredicate(testpersonen, accept);
        System.out.println("Filtered by lambda >1500y: " + Arrays.deepToString(filteredPersons));
    }
    
    
    /**
     *
     * @param personsToFilter
     * @param f
     * @return
     */
    public static Person[] filtern(Person[] personsToFilter, Filter f ){
        ArrayList<Person> list = new ArrayList<>();
        for (Person p :  personsToFilter){
            if (f.accept(p)) {
                list.add(p);
            }
            
            
        }
        // create array from that list
        Person[] filteredPersons = list.stream().toArray(Person[]::new);
        return filteredPersons;
    }

    public static Person[] filternPredicate(Person[] personsToFilter, java.util.function.Predicate<Person> f ){
        ArrayList<Person> list = new ArrayList<>();
        for (Person p :  personsToFilter){
            if (f.test(p)) {
                list.add(p);
            }
            
            
        }
        // create array from that list
        Person[] filteredPersons = list.stream().toArray(Person[]::new);
        return filteredPersons;
    }

}
