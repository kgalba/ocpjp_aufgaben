/*
Aufgabe "Enums / Hunderasse"

- Bitte definieren Sie eine Enum-Klasse 'Hunderasse' mit drei Konstanten 
    DACKEL, COLLIE und DOGGE. 
 
- Jede Hunderasse soll eine readonly-Property 'maxGroesse' bekommen: 
    0.5 für DACKEL, 1.0 für COLLIE und 1.5 für DOGGE.   

- Geben Sie alle statischen Konstanten der Klasse 'Hunderasse' aus. 
  Die Ausgaben sollen folgendermaßen aussehen:
    Dackel, max. Größe: 0.5
    Collie, max. Größe: 1.0
    Dogge, max. Größe: 1.5
 */
package tverrbjelke.enums;

/**
 *
 * @author tverrbjelke
 */
public enum DogRace{
     DACKEL(0.5), COLLIE(1.0), DOGGE(1.5);
     
     double maxSize;
     DogRace(double maxSize){
         this.maxSize = maxSize;
     }

     @Override
     public String toString(){
         return name() + ", max. Größe: " + maxSize;
     }
} 
