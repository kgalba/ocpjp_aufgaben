/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.auto;

/**
 *     Die Zeile 1 soll folgende Ausgabe erzeugen:
	Rennwagen Mercedes. Fahrer: M. Schumacher
	
    Die Zeile 2 soll folgende Ausgabe erzeugen:
	Motor Type1 aus dem Rennwagen Mercedes
 *
 * @author tverrbjelke
 */
public class Test {

    public static void main(String... args)
    {
        Rennwagen rw = new Rennwagen("Mercedes");

        Rennwagen .Fahrer f = new Rennwagen.Fahrer("M.", "Schumacher");
        rw.setFahrer(f);

        Rennwagen.Motor m = rw.getMotor();

        System.out.println(rw); 	//Zeile 1
        System.out.println(m);	//Zeile 2
    }

}
