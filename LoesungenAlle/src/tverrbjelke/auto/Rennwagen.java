/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.auto;

/**
 *
 * @author tverrbjelke
 */
public class Rennwagen {

   /** 
    * Fahrer-Instanzen existieren auch außerhalb des Rennwagens.
    */
    static class Fahrer {

        String firstName, lastName;
        
        public Fahrer(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }
        
        @Override
        public String toString(){
            return "Fahrer: " + this.firstName + " " + this.lastName;
        }
    }

    /** 
     * Instanzen existieren nur innerhalb von Instanzen von Rennwagens
     */
    public class Motor {

        String typ = null;
        public Motor(String typ) {
            this.typ = typ;
        }
        
        @Override
        public String toString (){
            return "Motot " + typ + " aus dem Rennwagen " + name;
            // same: 
            // return "Motot " + typ + "aus dem Rennwagen " + Rennwagen.this.name;
            
        }
    }

    private String name;
    private Fahrer fahrer = null;
    private Motor motor = null;
    
    public Rennwagen( String name){
        this.name = name;
        this.motor = this.new Motor("Type1");
    }

    public Fahrer getFahrer() {
        return fahrer;
    }

    public void setFahrer(Fahrer fahrer) {
        this.fahrer = fahrer;
    }

    public Motor getMotor() {
        return motor;
    }
    
    
    @Override
    public String toString(){
        return "Rennwagen " + this.name + ". " + this.fahrer;
    }
}
