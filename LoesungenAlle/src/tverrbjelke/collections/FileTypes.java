/*
Aufgabe "Collections/HashSet - FileTypes"

- Erstellen Sie eine Klasse 'FileTypes'

- Definieren Sie einen Konstruktor für die Klasse 'FileTypes', an den man ein 
  String übergeben kann:

        FileTypes ft = new FileTypes("C:\\Windows");

- Definieren Sie eine Methode 'getFileTypes' in der Klasse 'FileTypes', die 
  eine Collection von Strings liefert, in der die alle auftretenden 
  Datei-Erweiterungen eines Verzeichnisses (s. Konstruktor) aufgelistet sind 
  und einmalig auftauchen:

        Collection<String> extColl = ft.getFileTypes();

- Geben Sie die gefundenen Datei-Erweiterungen aus
 */
package tverrbjelke.collections;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FilenameUtils;

/**
 *
 * @author tverrbjelke
 */
public class FileTypes {
    final String folder;

    FileTypes(String folder){
        this.folder = folder;
    }
    
    @Override
    public String toString(){
        return String.format("FileTypes[%s]", folder);
    }
    /**
     * calculates all different file types (postfixes) of this folder. 
     * Currently flat only THIS folder. Not recursive.
     * Stupid implementation: get all files in a collection and work with them...
     * @return a set of unique entries of all occuring filetypes. or null if problems with the folder.
     */
    public Collection<String> getFileTypes(){
        Collection<String> allFiles = getFiles();
        if (allFiles == null){
            return null;
        }
        
        HashSet<String> types = new HashSet<>();
        for (String f : allFiles){
            String typ = FilenameUtils.getExtension(f);
            if (!typ.equals("") && typ != null)  // we do not want to add nulls
            {
                types.add(typ);
            }
        }
        return types;
    }
    
    public Map<String, Integer> getFileTypesAndOccurences(){
        Collection<String> allFiles = getFiles();
        if (allFiles == null){
            return null;
        }
        HashMap<String, Integer> types = new HashMap();
        for (String f: allFiles ){
            String typ = FilenameUtils.getExtension(f);
            if (!typ.equals("") && typ != null)  // we do not want to add nulls
            {
                Integer number =  types.get(typ);
                if (number == null){
                    number = 1;
                }
                else{
                    number ++;
                }
                
                types.put(typ,number);
            }
        }
        return types;
    }
    
    
    Collection<String> getFiles(){
        File folderFile = new File(this.folder);
        List<String> asList = Arrays.asList( folderFile.list() );
        return asList;
    }
    
    public static void main(String[] args) {
        FileTypes ft = new FileTypes("C:\\Windows");
        Collection<String> extensions = ft.getFileTypes();
        System.out.println( ft + " has this extensions:" +  extensions  );

        System.out.println("--------------");
        Map<String, Integer> extensionsWithOccurrences = ft.getFileTypesAndOccurences();
        System.out.println( ft + " has this extensions:" +  extensionsWithOccurrences  );
    }
    
}