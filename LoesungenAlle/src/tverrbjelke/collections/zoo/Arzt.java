/*
Aufgabe "Generics / Zoo"

1. Entwerfen Sie eine Klassensammlung, die folgender Beschreibung entspricht:

    - Das Interface 'KannBehandeltWerden' deklariert eine Methode 'void setGesund(boolean)'
    - Das Interface 'KannBehandeltWerden' deklariert eine Methode 'boolean isGesund()'

    - Mensch ist eine Klasse, die das Interface 'KannBehandeltWerden' realisiert

    - Klasse Tier ist abstract
    - alle Tiere können behandelt werden (s. das Interface 'KannBehandeltWerden')
    - Zebras und Affen sind Tiere

    - Ein Zoo kann mehrere Tiere haben (HAS-A)
    - Einem Zoo können beliebige Tiere einzeln hinzugefügt werden
    - Ein Zoo kann eine Collection aller seine Tiere mit einer Methode zurückliefern
    
    - Ein Arzt ist ein Mensch

2. Passen Sie die Klasse 'Arzt' so an, dass mit der folgende Aufgaben lösen lassen:

    - Es ist möglich, einen Arzt zu erzeugen, der nur Affen behandeln kann (keine anderen Tiere)
    - Es ist möglich, einen Arzt zu erzeugen, der beliebige Tiere behandeln kann (keine Menschen)
    - Es ist möglich, einen Arzt zu erzeugen, der sowohl Tiere als auch Menschen behandeln kann

    - Die drei oben beschriebenen Ärzte werden der selben Klasse 'Arzt' erzeugt,
      sollten aber zueinander 'nicht kompatibel' sein

    
 */
package tverrbjelke.collections.zoo;

/**
 *
 * @author tverrbjelke
 */
public class Arzt<T extends KannBehandeltWerden> extends Mensch {
    void behandelt(T t){
        t.setGesund(true);
    }

    public static void main(String[] args) {
        Arzt<Affe> affenArzt = new Arzt();
        Affe a = new Affe();
        affenArzt.behandelt(a);
        
        Zebra z = new Zebra();
        Mensch m = new Mensch();
        Arzt<Tier> tierArzt = new Arzt();
        tierArzt.behandelt(z);
        tierArzt.behandelt(a);
        //tierArzt.behandelt(m); // should not compile!
        
        Arzt<KannBehandeltWerden> allgemeinArzt = new Arzt();
        allgemeinArzt.behandelt(z);
        allgemeinArzt.behandelt(a);
        allgemeinArzt.behandelt(m); // TODO should not compile!
        
    }

}


