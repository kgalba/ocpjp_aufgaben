/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.collections;

import tverrbjelke.personen.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeSet;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 *
 * @author tverrbjelke
 */
public class Person implements Comparable<Person>{

    int yearOfBirth;
    String lastName;
    String firstName;

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public Person(String firstName, String lastName, int yearOfBirth ) {
        this.yearOfBirth = yearOfBirth;
        this.lastName = lastName;
        this.firstName = firstName;
    }
    
    @Override
    public int compareTo(Person o) {
        int comp = lastName.compareTo(o.lastName);
        if (comp == 0){
            comp = firstName.compareTo(o.firstName);
            if (comp == 0){
                comp = Integer.compare(yearOfBirth, o.yearOfBirth);
            }
        }
        return comp;
    }
    
    @Override
    /**
     * did not use  http://commons.apache.org/proper/commons-lang/apidocs/org/apache/commons/lang3/builder/EqualsBuilder.html
     */
    public boolean equals(Object o){
        if ( o instanceof Person ) {
            if (   ( yearOfBirth == ((Person) o).yearOfBirth)
                && ( lastName.equals( ((Person) o).lastName) )
                && ( firstName.equals( ((Person) o).firstName) ))
            {
                return true;
            }
        }
        return false;
    }
    
    @Override
    /**
     * did use http://commons.apache.org/proper/commons-lang/apidocs/org/apache/commons/lang3/builder/HashCodeBuilder.html
     */
    public int hashCode(){
        // you pick a hard-coded, randomly chosen, non-zero, odd number
        // ideally different for each class
        return new HashCodeBuilder(17, 37).
        append(yearOfBirth).
        append(firstName).
        append(lastName).
        toHashCode();

//        return Integer.hashCode(yearOfBirth)
//                + firstName.hashCode()
//                + lastName.hashCode();
    }
    
    @Override
    public String toString(){
        return "Person " + getFirstName() + " "+ getLastName() + ", geb. " + getYearOfBirth();
                
    }
    public static void main(String[] args) {
        Person[] arrayPersonen = {
                new Person("Alfred Hans", "Bechler", 1906),
                new Person("Berta", "Adler", 1907),            
                new Person("Berta", "Adler", 1907),            
                new Person("Max", "Adler", 1907),
            };

        List<Person> personenList = new LinkedList<>();
        HashSet<Person> personenHashSet = new HashSet<>();
        TreeSet<Person> personenTreeSet = new TreeSet<>();
        for (Person p : arrayPersonen) {
            personenList.add(p);
            personenHashSet.add(p);
            personenTreeSet.add(p);
        };


        
        System.out.println("Original array of Personen = " + Arrays.deepToString(arrayPersonen));// Arrays.deepToString(arrayPersonen));

        Arrays.sort(arrayPersonen);
        System.out.println("Sorted original  = " + Arrays.deepToString(arrayPersonen));

        System.out.println("List of Personen= " + personenList);
        System.out.println("Hash of Personen= " + personenHashSet);
        System.out.println("TreeSet of Personen= " + personenTreeSet);
        

    }
}
