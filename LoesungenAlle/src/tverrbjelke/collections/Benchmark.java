/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

class Element{
    long valL=100;
    double valF=100.0;
}

/**
 * see http://stackoverflow.com/questions/17940200/how-to-find-the-duration-of-difference-between-two-dates-in-java
 * @author tverrbjelke
 * @returns duration of the loop in milliseconds
 */
public class Benchmark {
 
    static long testInsertListFromBegin(List<Long> instance, long numLoops){
        Date startTime = new Date();
        for (long i=0; i< numLoops; i++){
            instance.add(0,i);
        }
        Date endTime = new Date();
        long duration = endTime.getTime() - startTime.getTime();
        
        Runtime.getRuntime().gc();
        try {
            Thread.sleep(5000); //GC
        } catch (InterruptedException ex) {
            Logger.getLogger(Benchmark.class.getName()).log(Level.SEVERE, null, ex);
        }
        return duration; 
    }

    static long testInsertListAtEnd(List<Long>  instance, long numLoops){
        Date startTime = new Date();
        for (long i=0; i< numLoops; i++){
            instance.add( i);
        }
        Date endTime = new Date();
        long duration = endTime.getTime() - startTime.getTime();

        Runtime.getRuntime().gc();
        try {
            Thread.sleep(5000); //GC
        } catch (InterruptedException ex) {
            Logger.getLogger(Benchmark.class.getName()).log(Level.SEVERE, null, ex);
        }

        return duration; 
    }


    
    
    public static void main(String[] args){
        System.out.println("Starte benchmark...");
        int initialCapacity = 1;
        long loops = 1000;
        double durationperLoop;
        long incFactor = 10;

        while (loops <= 1000 * 1000){
            Date startTime = new Date();

            System.out.println("Test with Number of loops=" + loops);
            ArrayList<Long> instanceArrayBegin = new ArrayList<>(initialCapacity);
            durationperLoop= (double) testInsertListFromBegin(instanceArrayBegin, loops) / (double) loops;
            System.out.printf("testInsertArrayListFromBegin: %.6f\n", durationperLoop);

            ArrayList<Long> instanceArrayEnd = new ArrayList<>(initialCapacity);
            durationperLoop = (double) testInsertListAtEnd(instanceArrayEnd, loops) / (double) loops;
            System.out.printf("testInsertArrayListAtEnd: %.6f\n", durationperLoop);

            LinkedList<Long> instanceLinkedLBegin = new LinkedList<>();
            durationperLoop= (double) testInsertListFromBegin(instanceLinkedLBegin, loops) / (double) loops;
            System.out.printf("Insert LinkedList at Begin: %.6f\n", durationperLoop);

            LinkedList<Long> instanceLinkedLEnd = new LinkedList<>();
            durationperLoop = (double) testInsertListAtEnd(instanceLinkedLEnd, loops) / (double) loops;
            System.out.printf("inster LinkedList at End: %.6f\n", durationperLoop);

            Date endTime = new Date();
            long deltaTime = endTime.getTime() - startTime.getTime();
            double deltaTimeSec = (double) deltaTime/1000.0;//  TimeUnit.MILLISECONDS.toSeconds(deltaTime);
            System.out.printf("This iteration with %d loops took %.3f seconds\n", loops, deltaTimeSec );
            System.out.println("----------------");
            loops = loops*incFactor;
        }        
    }
}
