/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.polymorphismus;

/**
 *
 * @author tverrbjelke
 */
public class Rechteck implements GeometrischeForm{

    double width, height;
    
    public Rechteck(double breite, double hoehe) {
        this.width = breite;
        this.height = hoehe;
    }
   
    @Override
    public double getFlaeche() {
        double area = this.width * this.height;
        return area;
    }
    
    @Override
    public String  toString(){
        return "Rechteck mit Breite=" + this.width + " und Höhe=" + this.height;                
    }
    
        
//    @Override
//    public int compareTo(GeometrischeForm other) {
//        return Double.compare(this.getFlaeche(), other.getFlaeche());
//    }
}
