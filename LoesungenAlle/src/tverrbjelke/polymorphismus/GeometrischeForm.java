/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.polymorphismus;

/**
 *
 * @author tverrbjelke
 */
public interface GeometrischeForm extends Comparable<GeometrischeForm> {
    public double getFlaeche();
   
//    @Override
    default public int compareTo(GeometrischeForm other) {
        return Double.compare(this.getFlaeche(), other.getFlaeche());
    }


}
