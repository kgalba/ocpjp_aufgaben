/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.polymorphismus;

/**
 *
 * @author tverrbjelke
 */
public class Kreis implements GeometrischeForm{

    double radius;
    
    public Kreis( double radius) {
        this.radius = radius;
    }

    @Override
    public double getFlaeche() {
        double area = Math.PI * this.radius * this.radius;
        return area;
    }
    
    @Override
    public String toString(){
        return "Kreis mit Radius=" +this.radius;
    }
    
 //   @Override
 //   public int compareTo(GeometrischeForm other) {
 //       return Double.compare(this.getFlaeche(), other.getFlaeche());
 //   }
    
}
