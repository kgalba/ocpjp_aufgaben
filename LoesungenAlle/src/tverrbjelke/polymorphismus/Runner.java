/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.polymorphismus;

import static java.util.Arrays.sort;
import java.util.Random;

/**
 *
 * @author tverrbjelke
 */
public class Runner {
    public static void main(String[] args) {
        GeometrischeForm formen[] = new GeometrischeForm[100];
        Random r = new Random();
        double minBreite = 1;
        double maxBreite = 20;
        double minHoehe = 1;
        double maxHoehe = 20;
        double maxRadius = 10;
        
        for (int i=0; i<10; i++){
            double hoehe = r.nextDouble() * (maxHoehe - minHoehe) + minHoehe;
            double breite = r.nextDouble() * (maxBreite - minBreite) + minBreite;
            
            formen[i] = Runner.createRechteck(breite, hoehe);
        }
        
        
        Kreis k = new Kreis(1.3);
        System.out.println(k);

        formen = new GeometrischeForm[100];
        for (int i=0; i<100; i++){
            if ( r.nextBoolean() ) // true = Kreis, faalse = rechteck
            {
                double radius = r.nextDouble() * maxRadius;
                formen[i] = createKreis(radius);
            }
            else
            {
                double hoehe = r.nextDouble() * (maxHoehe - minHoehe) + minHoehe;
                double breite = r.nextDouble() * (maxBreite - minBreite) + minBreite;
                formen[i] = Runner.createRechteck(breite, hoehe);
            }
        }
        
        printForms(formen);
        System.out.println("---------------------------------");
        sort (formen);
        printForms(formen);
        
    }
    
    static Rechteck createRechteck (double breite, double hoehe ){
        return new tverrbjelke.polymorphismus.Rechteck(breite, hoehe);
    }

    static Kreis createKreis(double radius ){
        return new tverrbjelke.polymorphismus.Kreis(radius);
    }

    static void printForms (GeometrischeForm formen[]){
        for (GeometrischeForm form : formen){
            System.out.println(form + " Fläche=" + form.getFlaeche()) ;
        }
        
    }
}
