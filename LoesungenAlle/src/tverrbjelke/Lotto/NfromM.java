/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tverrbjelke.Lotto;

import java.util.Arrays;
import java.util.Random;

/**
 * Superklasse
 * @author tverrbjelke
 */
public class NfromM {

    /**
     * creates an instance with unique numbers in its drawPool.
     * @param n a draw of n numbers between 1..m, expect n already beensanity checked.
     * @param m maximum of the drawn numbers, expect n already beensanity checked.
     * @throws IllegalArgumentException if not 1 <= n <= m
     */

    public NfromM(int n, int m) {
        //sanity check
        if (n <1 || m < n )  {
            String msg = "Illegal arguments n=" + n + " m=" + m;
            throw new IllegalArgumentException(msg);
        }

        this.m = m;
        this.n = n;
        this.drawnPool = new int[this.n]; // zero initialized, fulfill prerequisites for draw
        this.draw();
    }
    
    /** max mumber to be draw, inclusive */
    private int m;
    
    /** how many numbers the pools holds, each number is [1..m] (inclusive) */
    private int n;
    
    /** 
     * the (sorted) pool of numbers
     * //initialized here with null ?
     */
    private int[] drawnPool; 

    /** 
     * getter of underlying drawPool. Do not modify array!
     * @return drawPool.
     */
    public final int [] getPool(){
        return this.drawnPool;
    }
    
    /**
     * fills the drawnPool with new random unique ints, each in [1..m], inclusive.
     * The new values in drawPool is sorted (ascending) .
     */
    public void draw(){
        // clear the pool by filling zeroes, to allow proper isUnique() in getNextNumber()
        for (int i=0; i< this.drawnPool.length; i++){
            this.drawnPool[i]=0;
        }

        //throw the mighty dice
        for (int i=0; i < this.n; i++) {
            this.drawnPool[i] = getNextNumber();
        }        
        Arrays.sort (this.drawnPool);
    }

    
        /** 
     * Simulated drawinf a single unique number from [1..m] (inclusive).
     * Expects that in pool all not drawn numbers have value 0.
     * 
     * @return unique value in 1..m (inclusive) 
     *         or -1 if pool is already filled.
     * 
     * @todo this implementation will get inefficcient with n aprox= m 
     * then we should create a (array/list) pool of numbers 1..m,
     * just dice for their index
     * and for each draw remove that number from pool.
     */
    private int getNextNumber(){
        // @dozent: noone can externally inject a fixed seed to test against fixed test suit...
        
        Random r = new Random(); // howto test such things?
        int value; // 0 is a nice "yet unititialized" marker
        do{ 
            value = r.nextInt(m ) +1; // [0..49[ +1 -> [1..49]
        } while (! isUniqueValue(value));
        return value;
    }

    public int getM() {
        return m;
    }

    public int getN() {
        return n;
    }

    /**
     * is this value alrady in pool?
     * @param value checks out, if this value is already in pool.
     *        assumes: 0 < value <= m
     * @return true if value not in pool, otherwiese false
     */
    private boolean isUniqueValue(int value){
        boolean isUnique = true;
        for (int poolValue: this.drawnPool) {
            if (value == poolValue) { 
                isUnique= false;
                break;
            }
        }
        return isUnique;
    }

    /** returns the drawPool as string (comma separated). */
    @Override
    public String toString(){
        return Arrays.toString(drawnPool);
    }

}
