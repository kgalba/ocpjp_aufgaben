package tverrbjelke.Lotto;

import java.util.Arrays;
import java.util.Random;

/**
 * Represents a single draw of anzahlKugel from a given pool of total anzahlKugelGesamt.
 * @throws IllegalArgumentException when anzahlKugel < 1 
 *         or anzahlKugelGesamt <  anzahlKugel
 *         frage an Dozent: sollte ich das hier ind er doku angeben, 
 *         da die auch nicht explizit in der signatur angegeben wird (siehe inselbuch S.560)
 * @author tverrbjelke
 */
class LottoSpiel extends NfromM {
    LottoSpiel(int anzahlKugeln, int anzahlKugelnGesamt) {
        super(anzahlKugeln, anzahlKugelnGesamt);
    }

    /** 
     * simuliert das ziehen der Kugeln. Alle Kugeln-Slots werden neu gezogen.
     */
    public void ziehen(){
        draw();
    }

    /** gibt die Ziehung als String zurück. */
    @Override
    public String toString(){
        StringBuilder ziehungString = new StringBuilder("Spiel ");
        ziehungString.append( String.valueOf(super.getN())).append(" aus ").append(String.valueOf(super.getN())).append(". ");
        return ziehungString.toString() + super.toString();
    }
    
    private int numMatches(LottoTipp tipp){
        int numMatches = 0;
        for (int numberTip : tipp.getPool() ){
            for ( int numberZiehung: this.getPool() ){
                if (numberTip == numberZiehung) {
                    numMatches++;
                    break; //inner only
                }
            }
        }
        return numMatches;
    }
    
    /** Dabei soll der Gewinn nach folgendem Muster berechnet werden:

        0 richtige: 0 Euro
        1 richtige: 1 Euro
        2 richtige: 10 Euro
        3 richtige: 100 Euro
        4 richtige: 1000 Euro
        usw.
      */
    public int vergleichen(LottoTipp tipp){
        int numMatches = this.numMatches(tipp);

        if (numMatches==0) { return 0; };
        int win = 1;
        for (int i = 1; i< numMatches; i++){
            win = win*10;
        }
        return win;
    }
   
}
