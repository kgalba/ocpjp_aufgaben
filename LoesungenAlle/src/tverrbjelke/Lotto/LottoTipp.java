package tverrbjelke.Lotto;

/**
 * Ein LottoTipp ist das simulieren eines LottoTipps, 
 * also das abgeben einer wette über die zukünftige LottoSpiel-Ziehung.
 * @author tverrbjelke
 */
public class LottoTipp extends NfromM{
    int anzahlKugeln;
    int anzahlKugelnGesamt;
    
    LottoTipp(int anzahlKugel, int anzahlKugelGesamt){
        super(anzahlKugel, anzahlKugelGesamt);
    }
    
    void abgeben(){
        super.draw();
    }
    
    /** gibt die Ziehung als String zurück. */
    @Override
    public String toString(){
        StringBuilder ziehungString = new StringBuilder("Tipp ");
        ziehungString.append( String.valueOf(super.getN())).append(" aus ").append(String.valueOf(super.getN())).append(". ");
        return ziehungString.toString() + super.toString();
    }
    
    
}
