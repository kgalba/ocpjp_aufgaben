package mkeck.Strings;

/**
 *
 * @author mkeck
 */
public class Strings01 {
    
    public static String reverse(String s) {
        String reverse = "";
        
        for (int i = 0; i < s.length(); i++)
            reverse = s.charAt(i) + reverse;
        return reverse;
    }
    
    public static boolean isTextFile(String s) {
        // check if the file name (including extension) is long enough
        if (s == null || s.length() < 5)
            return false;
//        else if ("txt.".equalsIgnoreCase(reverse(s).substring(0, 4)))
        else if (s.toLowerCase().endsWith(".txt"))
            return true;
        
        return false;
    }
    
    public static void main(String[] args) {
        
        // reverse string
        String string = "Java ist toll";
        System.out.println(string);
        System.out.println(reverse(string));
        System.out.println("-----------------------");
        
        // is the file a text file?
        String text1 = "test";
        System.out.println("Is " + text1 + " a text file?");
        System.out.println(isTextFile(text1));
        System.out.println("-----------------------");
        
        String text2 = "test.txt";
        System.out.println("Is " + text2 + " a text file?");
        System.out.println(isTextFile(text2));
        System.out.println("-----------------------");
        
        String text3 = "test.TXT";
        System.out.println("Is " + text3 + " a text file?");
        System.out.println(isTextFile(text3));
        System.out.println("-----------------------");
        
        String text4 = ".txt";
        System.out.println("Is " + text4 + " a text file?");
        System.out.println(isTextFile(text4));
        System.out.println("-----------------------");
        
    }
}
