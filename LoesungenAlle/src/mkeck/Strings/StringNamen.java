package mkeck.Strings;

import java.util.Random;

/**
 *
 * @author mkeck
 */
public class StringNamen {
    
    public static String generiereName(String[] syllables, int number) {          
        
        StringBuilder builder = new StringBuilder();
        Random random         = new Random();
        
        for (int i = 0; i < number; i++)
            builder.append(syllables[random.nextInt(syllables.length)]);

        return builder.toString();
    }
    
    public static void main(String[] args) {
        
        String[] syllables = {"pro", "gi", "idre", "post", "me", "per", "hy", 
                              "test", "ga", "check", "top"};
        
        String neuerName = generiereName(syllables, 4);
        System.out.println(neuerName);
        System.out.println("---------------------------------------------");
        
        Random random = new Random();
        
        for (int i = 0; i < 50; i++) {
            neuerName = generiereName(syllables, random.nextInt(10) + 1);
            System.out.println(neuerName);
            System.out.println("---------------------------------------------");
        }
        
        
        
    }
}
