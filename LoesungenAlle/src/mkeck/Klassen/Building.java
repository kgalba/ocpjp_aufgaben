package mkeck.Klassen;

import java.util.Arrays;

/**
 *
 * @author mkeck
 */
public class Building {
//    VARIABLES
    private String streetName;
    private String streetNumber;
    private Floor[] floors;
    
//    CONSTRUCTORS
    public Building(String streetName, String streetNumber,
                    int numberOfFloors, int roomsPerFloor) {
        this.streetName   = streetName;
        this.streetNumber = streetNumber;
        
        floors = new Floor[numberOfFloors];
        
        for (int i = 0; i < floors.length; i++) {
            floors[i] = new Floor(roomsPerFloor);
        }
    }
    
//    METHODS
    public Floor.Room getRoom(int floorNumber, int roomNumber) {
        return floors[floorNumber].rooms[roomNumber];
    } 
    
//------------------------------------------------------------------------------
//    INNER CLASSES
    private class Floor {
//        VARIABLES
        private Room[] rooms;
        
//        CONSTRUCTORS
        private Floor(int roomsPerFloor) {
            
            rooms = new Room[roomsPerFloor];
            
            for (int i = 0; i < rooms.length; i++) {
                rooms[i] = new Room();
            }
        }
        
        private class Room {        
//            METHODS
            @Override
            public String toString() {
                return "Room " +
                       Arrays.asList(floors).indexOf(Building.Floor.this) +
                        "." + Arrays.asList(rooms).indexOf(this) + " / " + 
                       streetName + " " + streetNumber;
            }
        } // end of Room
    } // end of Floor
    
} // end of Building
