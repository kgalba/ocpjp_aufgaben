package mkeck.Klassen;

/**
 *
 * @author mkeck
 */
public class TestPersonLambda {
    
    static Person[] filter(Person[] people, Filter filter) {
        Person[] filteredPeople = null;
        int counter = 0;
        
        for (Person person : people) {
            if (filter.accept(person))
                counter++;
        }
        
        if (counter > 0) {
            filteredPeople = new Person[counter];
            counter = 0;
            
            for (int i = 0; i < people.length; i++) {
                if (filter.accept(people[i])) {
                    filteredPeople[counter] = people[i];
                    counter++;
                }
            }
        }

        return filteredPeople;
    }
    
    public static void main(String[] args) {
        
        Person[] people = new Person[4];
        
        people[0] = new Person("Albert",   "Einstein", 1879);
        people[1] = new Person("Stephen",  "Hawking",  1942);
        people[2] = new Person("Johannes", "Kepler",   1571);
        people[3] = new Person("Richard",  "Feynman",  1918);
        
//        save people that are older than 99 years in filteredPeople
        Person[] filteredPeople = 
            filter(people, (Person person) -> 
                    { return person.getYearOfBirth() < 1915; } );
        
        for (int i = 0; i < filteredPeople.length; i++) {
            System.out.println(filteredPeople[i]);
        }
    }
}
