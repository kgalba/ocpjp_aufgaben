package mkeck.Klassen;

/**
 *
 * @author mkeck
 */
public class Person {
    
//    INSTANCE VARIABLES
    String firstName;
    String lastName;
    int yearOfBirth;

//    CONSTRUCTORS
    public Person(String firstName, String lastName, int yearOfBirth) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.yearOfBirth = yearOfBirth;
    }
    
//    SETTER AND GETTER

    public int getYearOfBirth() {
        return yearOfBirth;
    }
    
//    OTHER METHODS
    @Override
    public String toString() {
        return firstName + " " + lastName + " (" + yearOfBirth +")";
    }
}
