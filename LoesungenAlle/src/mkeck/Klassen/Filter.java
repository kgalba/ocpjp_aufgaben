package mkeck.Klassen;

/**
 *
 * @author mkeck
 */
public interface Filter {
    
    boolean accept(Person person);
}
