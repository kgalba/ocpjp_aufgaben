package mkeck.Klassen;

import java.util.Random;

/**
 *
 * @author mkeck
 */
public class LottoSpiel {
    
    // Variables
    protected int anzahlKugel;
    protected int anzahlKugelGesamt;
    protected int[] zahlen;
    
    // Constructors
    /**
     * 
     * @param anzahlKugel
     * @param anzahlKugelGesamt 
     * @throws IllegalArgumentException
     */
    LottoSpiel(int anzahlKugel, int anzahlKugelGesamt) {
        
        if (anzahlKugel < 1 || anzahlKugelGesamt < anzahlKugel) {
            throw new IllegalArgumentException("Die Anzahl der Kugeln muss "
                    + "mindestens 1 betragen und die Gesamtanzahl der "
                    + "Kugeln darf nicht kleiner sein als die Anzahl der "
                    + "gezogenen Kugeln");
        }
        
        this.anzahlKugel       = anzahlKugel;
        this.anzahlKugelGesamt = anzahlKugelGesamt;
        zahlen = new int[anzahlKugel];
    }
    
    // Methods
    public void ziehen() {
        Random random = new Random();
        int check;
        
        for (int i = 0; i < this.anzahlKugel; i++) {
            test:
            while(true) {
                check = random.nextInt(this.anzahlKugelGesamt) + 1;
                
                for (int j = 0; j < i; j++) {
                    if (check == zahlen[j]) {
                        continue test;
                    }
                }
                zahlen[i] = check;
                break;
            } 
        }        
    }
    
    public int vergleichen(LottoTipp tipp) {
        int counter = 0;
        
        for (int i = 0; i < this.zahlen.length; i++) {
            for (int j = 0; j < tipp.zahlen.length; j++) {
                if (this.zahlen[i] == tipp.zahlen[j]) {
                    counter++;
                    break;
                }
            }
        }
        
        if (counter > 0)
            return (int) Math.pow(10, counter-1);
        
        return 0;
    }
    
    public String toString() {
        java.util.Arrays.sort(zahlen);
        return "Spiel 7 aus 49. " +
                java.util.Arrays.toString(zahlen);
    }

}