package mkeck.Klassen;

/**
 *
 * @author mkeck
 */
public class Rennwagen {
    
//    VARIABLEN
    private String rennwagen;
    private Fahrer fahrer;
    private Motor  motor;
    
//    CONSTRUCTORS
    public Rennwagen(String rennwagen) {
        this.rennwagen = rennwagen;
        this.motor     = new Motor("Type1");
    }
    
//    SETTER AND GETTER
    public void setFahrer(Fahrer fahrer) {
        this.fahrer = fahrer;
    }
    
    public Motor getMotor() {
        return this.motor;
    }
    
//    METHODEN
    @Override
    public String toString() {
        return "Rennwagen " + rennwagen + ". Fahrer: " + fahrer.vorname +
               " " + fahrer.nachname;
    }
    
//    INNERE KLASSEN
    class Motor {
        private String type;

        Motor(String type) {
            this.type = type;
        }
        
        @Override
        public String toString() {
            return "Motor " + motor.type + " aus dem Rennwagen " + rennwagen;
    }
    }
    
    static class Fahrer {
        private String vorname;
        private String nachname;

        public Fahrer(String vorname, String nachname) {
            this.vorname = vorname;
            this.nachname = nachname;
        }
        
    }
   
}

class Test {
    
    public static void main(String[] args) {
        
        Rennwagen rw = new Rennwagen("Mercedes");
        
        Rennwagen.Fahrer f = new Rennwagen.Fahrer("M.", "Schumacher");
        rw.setFahrer(f);
        
        Rennwagen.Motor m = rw.getMotor();
        
        System.out.println(rw); 	//Zeile 1
        System.out.println(m);	//Zeile 2
    }
}
