package mkeck.Klassen;

/**
 *
 * @author mkeck
 */
public class LottoSpielSimulation {
    
    public static void main(String[] args) {
        
        int anzahlKugel = 7;
        int anzahlKugelGesamt = 49;
        
        LottoSpiel lotto = new LottoSpiel(anzahlKugel, anzahlKugelGesamt);
        lotto.ziehen();
        System.out.println( lotto );
        System.out.println("-------------------------------------------");
        
        LottoTipp tipp = new LottoTipp(anzahlKugel, anzahlKugelGesamt);
        tipp.abgeben();
        System.out.println( tipp );
        System.out.println("-------------------------------------------");
        
        System.out.println("Gewinn: " + lotto.vergleichen(tipp));
        System.out.println("-------------------------------------------");
        
        int gewinn  = 0;
        int verlust = 0;
        for (int i = 0; i < 100000; i++) {
            lotto.ziehen();
            tipp.abgeben();
            gewinn += lotto.vergleichen(tipp);
            verlust++;
        }
        System.out.println("Gewinn: " + (gewinn - verlust));
    }
}
