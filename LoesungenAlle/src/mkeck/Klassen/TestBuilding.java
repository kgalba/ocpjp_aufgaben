package mkeck.Klassen;

/**
 *
 * @author mkeck
 */
public class TestBuilding {
    
    public static void main(String[] args) {
        
        Building bldg = new Building("Hauptstr.", "45", 3, 10);
        System.out.println(bldg.getRoom(0, 2));
        System.out.println(bldg.getRoom(1, 7));
    }
}
