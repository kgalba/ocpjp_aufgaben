package mkeck.Klassen;

import java.util.Random;

/**
 *
 * @author mkeck
 */

class Rectangle {
    
    //--------------------------------------------------------------------------
    // Variables
    
    /** Height of the rectangle in m */
    private int height;
    
    /** Length of the rectangle in m */
    private int length;
    
    //--------------------------------------------------------------------------
    // Constructors
    
    /** Default constructor */
    Rectangle() {
        height = 1;
        length = 1;
    }
    
    /** Constructor sccepting height and length */
    Rectangle(int height, int length) {
        this.height = height;
        this.length = length;
    }
    
    //--------------------------------------------------------------------------
    // Methods
    
    /** Returns the area of the rectangle in m² */
    public int getArea() {
        return this.height * this.length;
    }
    
    //--------------------------------------------------------------------------
    // Setter and Getter
    
    /** Set the height of the rectangle in m */
    public void setHeight(int height) {
        this.height = height;
    }
    
    /** Get the height of the rectangle in m*/
    public int getHeight() {
        return this.height;
    }
    
    /** Set the length of the rectangle in m */
    public void setLength(int length) {
        this.length = length;
    }
    
    /** Get the length of the rectangle in m*/
    public int getLength() {
        return this.length;
    }
    
}
public class RandomRectangle {

    public static void main(String[] args) {
        
        Random random = new Random();
        
        for (int i = 0; i < 100; i++) {
            int length = random.nextInt(100) + 1;
            int height = random.nextInt(100) + 1;
            
            Rectangle r = new Rectangle(height, length);
            
            System.out.print("Height = " + r.getHeight() + " m, ");
            System.out.print("Length = " + r.getLength() + " m, ");
            System.out.println("Area = " + r.getArea() + " m²");
            System.out.println("---------------------------------------------");
            
        }
        
    }
}