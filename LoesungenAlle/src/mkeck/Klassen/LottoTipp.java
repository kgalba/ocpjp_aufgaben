package mkeck.Klassen;

import java.util.Random;

/**
 *
 * @author mkeck
 */
public class LottoTipp extends LottoSpiel{  
    
    // Constructors
    LottoTipp(int anzahlKugel, int anzahlKugelGesamt) {
        super(anzahlKugel, anzahlKugelGesamt);        
    }
    
    // Methods    
    public void abgeben() {
        super.ziehen();
    }
}
