package mkeck.threads.weather;

/**
 *
 * @author mkeck
 */
public class TempWarning extends Thread {

    @Override
    public void run() {

        while (true) {
            try {
                warning();
                sleep(2000);
            } catch (InterruptedException ex) {
                System.out.println("Interrupted exception caught");
            }
        }
    }

    private synchronized void warning() {
        if (TempMeasurement.getCurrentTemp() > 50
                || TempMeasurement.getCurrentTemp() < -20) {
            System.out.println("WARNING: "
                    + TempMeasurement.getCurrentTemp() + "°C");
        }
    }
}
