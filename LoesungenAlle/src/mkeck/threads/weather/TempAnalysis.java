package mkeck.threads.weather;

/**
 *
 * @author mkeck
 */
public class TempAnalysis extends Thread {
    
    private int[] lastThreeTemps = new int[3];
    
    @Override
    public void run() {
        while (true) {
            try {
                analyzeTemp();
                sleep(1000);
            } catch (InterruptedException ex) {
                System.out.println("Interrupted exception caught");
            }
        }
    }
    
    private synchronized void analyzeTemp() {
        lastThreeTemps[0] = lastThreeTemps[1];
        lastThreeTemps[1] = lastThreeTemps[2];
        lastThreeTemps[2] = TempMeasurement.getCurrentTemp();
        
        if (lastThreeTemps[2] > lastThreeTemps[1] &&
                lastThreeTemps[1] > lastThreeTemps[0]) {
            System.out.println("Increasing temperatures: " + lastThreeTemps[0] +
                    ", " + lastThreeTemps[1] + ", " + lastThreeTemps[2]);
        } else if (lastThreeTemps[2] < lastThreeTemps[1] &&
                lastThreeTemps[1] < lastThreeTemps[0]) {
            System.out.println("Decreasing temperatures: " + lastThreeTemps[0] +
                    ", " + lastThreeTemps[1] + ", " + lastThreeTemps[2]);
        }
    }
}
