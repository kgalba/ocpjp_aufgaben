package mkeck.threads.weather;

import java.util.Random;

/**
 *
 * @author mkeck
 */
public class TempMeasurement extends Thread {

    private static int currentTemp;
    
    private Random randomTemp = new Random();

    @Override
    public void run() {
        while (true) {
            try {
                measureTemp();
                sleep(100);
            } catch (InterruptedException ex) {
                System.out.println("Interrupted exception caught");
            }
        }
    }

    private synchronized void measureTemp() {
            currentTemp = currentTemp - 2  + randomTemp.nextInt(5);
    }

    public synchronized static int getCurrentTemp() {
        return currentTemp;
    }


}
