package mkeck.threads.weather;

/**
 *
 * @author mkeck
 */
public class WeatherStation {
    
    public static void main(String[] args) throws InterruptedException {
        
        TempMeasurement tempMeasurement = new TempMeasurement();
        TempAnalysis tempAnalysis = new TempAnalysis();
        TempWarning tempWarning = new TempWarning();        
        
        tempMeasurement.start();
        tempAnalysis.start();
        tempWarning.start();
    }
}
