package mkeck.Iterable;

import java.util.*;
/**
 *
 * @author mkeck
 */
public class PersonList 
    implements Iterable<Person>, Iterator<Person> {
    
//    Variables
    private int counter  = 0; // how many elements are being used
    private int iterator = 0; // current element number
    Person[] list;
    
//    Constructors
    public PersonList(int length) {
        list = new Person[length];
    }
    
//    Methods
    public void add(Person p) {
        list[counter] = p;
        counter++;
    }
    
//    Methods from Itarable
    @Override
    public Iterator<Person> iterator() {
        return this;
    }
    
//    Methods from Iterator
    @Override
    public boolean hasNext() {
        if (list[iterator] != null)
            return true;
        
        iterator = 0;
        return false;
    }
    
    @Override
    public Person next() {
        return list[iterator++];
    }
    
    @Override
    public void remove() {
        throw new UnsupportedOperationException();
    }
}
