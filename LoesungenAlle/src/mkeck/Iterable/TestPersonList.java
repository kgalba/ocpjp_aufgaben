package mkeck.Iterable;

/**
 *
 * @author mkeck
 */
public class TestPersonList {
    
    public static void main(String[] args) {
        
        PersonList list = new PersonList(10);
	list.add(new Person("Peter", "Braun"));
	list.add(new Person("Michael", "Roth"));
        list.add(new Person("Alexander", "Patrin"));
        list.add(new Person("Oleg", "Benin"));
        list.add(new Person("Kay", "Galba"));
        list.add(new Person("Andreas", "Hüwel"));
        list.add(new Person("Carsten", "Petschel"));
        list.add(new Person("Marius", "Keck"));

	for(Person p : list) {
		System.out.println(p);
	}
        
        System.out.println("---------------------------------");
        for(Person p : list) {
		System.out.println(p);
	}

    }
}
