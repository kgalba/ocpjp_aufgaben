package mkeck.Iterable;

public class Person {
	
//  Variables
    private String lastName;
    private String firstName;
	
//  Constructors
    Person() {
    	this("Max", "Mustermann");
    }
	
    Person(String firstName, String lastName) {
    	this.lastName  = lastName;
    	this.firstName = firstName;
    }
	
//  Setter and Getter
    public String getName() {
	return lastName;
    }

    public void setName(String lastName) {
	this.lastName = lastName;
    }

    @Override
    public String toString() {
    	return "Person: " + firstName + " " + lastName;
    }
}
