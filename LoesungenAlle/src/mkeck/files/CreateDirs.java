package files;

import java.io.File;
/**
 *
 * @author mkeck
 */
public class CreateDirs {
    
    public static void createDirs(String s) {
        File dir = new File(s);
        dir.mkdirs();
    }
    
    public static void removeDir(String s) {
        File dir = new File(s);
        dir.delete();
    }
    
    public static void main(String[] args) {
        
        createDirs("a\\b\\c\\d");
        removeDir("a\\b\\c\\d");
        removeDir("a\\b\\c");
    }
}
