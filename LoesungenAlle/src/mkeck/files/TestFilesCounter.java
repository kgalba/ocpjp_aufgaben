package files;

/**
 *
 * @author mkeck
 */
public class TestFilesCounter {
    
    public static void main(String[] args) {
        
        FilesCounter fc = new FilesCounter("C:\\Windows");
        int number = fc.count("txt");
        System.out.println("Number of text files: " + number);
        number = fc.countDeep("txt");
        System.out.println("Number of text files (deep count): " + number);
    }
}
