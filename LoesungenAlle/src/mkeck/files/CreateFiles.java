package files;

import java.io.File;
import java.io.IOException;
import java.util.regex.*;

/**
 *
 * @author mkeck
 */
public class CreateFiles {
    
    static void createFiles(File root, String prefix, String extension,
                            int count) {
        
        try {
            if (count < 1)
                throw new IllegalArgumentException("count has to be greater"
                                                 + " than 0");
            for (int i = 1; i <= count; i++) {
                File file = new File(root, prefix +
                                     String.format("%03d.%s", i, extension));
                file.createNewFile();
            }
        } catch(IOException e) {
            e.printStackTrace();
        } catch(IllegalArgumentException e) {
            e.printStackTrace();
        }
        
    }
    
    static void deleteFiles(File root, String prefix, String extension) {
        Pattern p;
        Matcher m;
        
        for (File file : root.listFiles()) {

            p = Pattern.compile("^" + prefix + "[0-9]*[" + extension + "]$");
            m = p.matcher(file.getName());
            
            if (m.find()) {
                p = Pattern.compile(extension + "$");
                m = p.matcher(file.getName());
                if (m.find()) {
                    System.out.println(file.getName());
                    file.delete();
                }
            }
        }
    }
    
    public static void main(String[] args) {
        
        File root = new File(".");
        createFiles(root, "file", "txt", 10);
        
        deleteFiles(root, "file", "txt");
    }
    
}
