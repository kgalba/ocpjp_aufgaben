package files;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

/**
 *
 * @author mkeck
 */
public class CreateFiles2 {
    
    static void createFiles(File root, String prefix, String extension,
                            int count) {
        
        try {
            if (count < 1)
                throw new IllegalArgumentException("count has to be greater"
                                                 + " than 0");
            for (int i = 1; i <= count; i++) {
                File file = new File(root, prefix +
                                     String.format("%03d.%s", i, extension));
                file.createNewFile();
            }
        } catch(IOException e) {
            e.printStackTrace();
        } catch(IllegalArgumentException e) {
            e.printStackTrace();
        }
        
    }
    
    static void deleteFiles(File root, String prefix, String extension) {
        FileFilter filter = new FileFilter() {
            @Override
            public boolean accept(File filename) {
                if (filename.isDirectory())
                    return false;
                if (filename.getName().startsWith(prefix) && 
                    filename.getName().endsWith(extension))
                    return true;
                return false;
            }
        };
        
        for (File file : root.listFiles()) {
            if(filter.accept(file))
                file.delete();
        }
    }
    
    public static void main(String[] args) {
        
        File root = new File(".");
        createFiles(root, "file", "txt", 10);
        
        deleteFiles(root, "file", "txt");
    }
    
}
