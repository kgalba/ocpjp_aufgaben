package files;

import java.io.File;

/**
 *
 * @author mkeck
 */
public class FilesCounter {

//    VARIABLES
    private File dir;

//    CONSTRUCTORS
    public FilesCounter(String dir) {
        this.dir = new File(dir);
    }

//    METHODS
    public int count(String type) {
        int counter = 0;
        for (File file : dir.listFiles()) {
            if (file.isFile() && file.getName().endsWith(type)) {
                System.out.println(file);
                counter++;
            }
        }
        return counter;
    }

    public int countDeep(String type) {
        int counter = 0;
        for (File file : dir.listFiles()) {
            if (file.isFile() && file.getName().endsWith(type)) {
//                System.out.println(file);
                counter++;
            } else if (file.isDirectory()) {
                counter = counter + this.countDeep(type, file);
            }
        }
        return counter;
    }

    public int countDeep(String type, File dir) {
        int counter = 0;
        if (dir.listFiles() != null) {
            for (File file : dir.listFiles()) {
                if (file.isFile() && file.getName().endsWith(type)) {
//                    System.out.println(file);
                    counter++;
                } else if (file.isDirectory()) {
                    counter = counter + this.countDeep(type, file);
                }
            }
        }
        return counter;
    }

}
