package files;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;

/**
 *
 * @author mkeck
 */

class SpaceComparator implements Comparator<File> {
    @Override
    public int compare(File f1, File f2) {
        return (int)(f1.getFreeSpace() - f2.getFreeSpace());
    }
}

public class Laufwerke {
    
//    METHODS
    public static void print() {
        System.out.printf("| %4s | %16s | %16s | %16s |\n",
                          "LW ","Frei(MB)", "Belegt(MB)", "Gesamt(MB)");
        
        double div = 1024. * 1024.;
        File[] roots = File.listRoots();
        Arrays.sort(roots,new SpaceComparator());
        for (File root : roots) {
            System.out.printf("| %4s | %16.1f | %16.1f | %16.1f |\n",
                              root, root.getFreeSpace() / div,
                              (root.getTotalSpace()-root.getFreeSpace()) / div,
                              root.getTotalSpace() / div);
        }
        
    }
}
