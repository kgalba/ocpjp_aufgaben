package files;

import java.io.*;

/**
 *
 * @author mkeck
 */
public class Writer1 {
    
    public static void main(String[] args) {
        try {        
            File file = new File("fileWriter1.txt");
            System.out.println(file.exists());
        
            boolean newFile = false;
            newFile = file.createNewFile();
            System.out.println(newFile);
            System.out.println(file.exists());
        }
        catch(IOException e) { }
    }
}
