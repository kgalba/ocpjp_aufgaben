package files;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 *
 * @author mkeck
 */
public class CopyTextFile {

    public static void copyTextFile(String fromFile, String toFile) {

//        VARIABLES
        File in = new File(fromFile);
        File out = new File(toFile);

//        Check if the input file exists
        try {
            if (!in.isFile()) {
                throw new FileNotFoundException();
            }
        } catch (FileNotFoundException e) {
            System.out.println("Exception thrown: Source file does not exist");
            e.printStackTrace();
        }

//        Check if the output file already exists
        try {
            if (out.isFile()) {
                throw new IOException();
            }
        } catch (IOException e) {
            System.out.println("Exception thrown: Target file already exists");
            e.printStackTrace();
        }

//         Create the output file and copy text from source to target file
        try {
            FileReader fr = new FileReader(in);
            BufferedReader br = new BufferedReader(fr);

            PrintWriter pw = new PrintWriter(out);

            String s;
            while ((s = br.readLine()) != null) {
                System.out.println(s);
                pw.println(s);
            }
            pw.flush();
            pw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
