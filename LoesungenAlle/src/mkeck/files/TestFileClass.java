package files;

import java.io.File;

/**
 *
 * @author mkeck
 */
public class TestFileClass {
    
    public static void printSubdirs(File dir) {
        if (dir.isDirectory()) {
            for (File subdir : dir.listFiles()) {
                if (subdir.isDirectory())
                    System.out.println(subdir.getName());
            }
        }
    }
    
    public static void printFiles(File dir) {
        if (dir.isDirectory()) {
            for (File file : dir.listFiles()) {
                if (file.isFile())
                    System.out.println(file.getName());
            }
        }
    }
    
    public static void main(String[] args) {
        
        File dir = new File(".");
        printSubdirs(dir);
        System.out.println("---------");
        printFiles(dir);
    }  
}
