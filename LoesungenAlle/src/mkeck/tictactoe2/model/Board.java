package mkeck.tictactoe2.model;

/**
 *
 * @author mkeck
 */
public class Board {
    
//    OBJECTS
    private static Element[][] elements = new Element[3][3];
    
//    CONSTRUCTOR
    Board() {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                elements[row][col] = new Element(row, col);
            }
        }
    }
    
//    GETTER
    public static Element getElement(int row, int col) {
        return elements[row][col];
    }
}
