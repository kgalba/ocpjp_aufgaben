package mkeck.tictactoe2.model;

/**
 *
 * @author mkeck
 */
public class AI extends Player {

//    METHODS
    @Override
    public void setElement(Element e) {
        for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3 ; col++) {
                if (Board.getElement(row, col).isFree()) {
                    Board.getElement(row, col).setContent('O');
                    return;
                }
            } 
        }
    }
    
}
