package mkeck.tictactoe2.model;

import mkeck.tictactoe2.model.Board;

/**
 *
 * @author mkeck
 */
public abstract class Player {
    
//    METHODS
    public boolean playerWon(char c) {
//        horizontal
        for (int i = 0; i < 2; i++) {
            if (Board.getElement(i, 0).getContent() == c && 
                Board.getElement(i, 1).getContent() == c &&
                Board.getElement(i, 2).getContent() == c) 
                return true;
        }
//        vertical
        for (int j = 0; j < 2; j++) {
            if (Board.getElement(0, j).getContent() == c && 
                Board.getElement(1, j).getContent() == c &&
                Board.getElement(2, j).getContent() == c) 
                return true;
        }
            
//        diagonal
        if (Board.getElement(0, 0).getContent() == c && 
            Board.getElement(1, 1).getContent() == c &&
            Board.getElement(2, 2).getContent() == c) 
            return true;
            
        if (Board.getElement(2, 0).getContent() == c && 
            Board.getElement(1, 1).getContent() == c &&
            Board.getElement(0, 2).getContent() == c) 
            return true;    
        
        return false;
    }
    
//    ABSTRACT METHODS
    abstract public void setElement(Element e);
    
}
