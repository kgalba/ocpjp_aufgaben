package mkeck.tictactoe2.model;

import mkeck.tictactoe2.model.Element;
import mkeck.tictactoe2.model.Player;

/**
 *
 * @author mkeck
 */
public class Human extends Player {

//    METHODS
    @Override
    public void setElement(Element e) {
        e.setContent('X');
    }

}
