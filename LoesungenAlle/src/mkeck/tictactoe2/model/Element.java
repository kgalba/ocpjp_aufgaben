package mkeck.tictactoe2.model;

/**
 *
 * @author mkeck
 */
public class Element {
    
//    INSTANCE VARIABLES
    private int  row;
    private int  col;
    private char content;
    
//    CONSTRUCTOR
    public Element() {
        this(0, 0);
    }
    
    public Element(int row, int col) {
        this.row = row;
        this.col = col;
    }
    
//    METHODS
    public boolean isFree() {
        if (content == '\u0000')
            return true;
        return false;
    }
    
//    GETTER
    public int getRow() {
        return row;
    }

    public int getCol() {
        return col;
    }

    public char getContent() {
        return content;
    }
    
//    SETTER

    public void setContent(char content) {
        this.content = content;
    }
    

}
