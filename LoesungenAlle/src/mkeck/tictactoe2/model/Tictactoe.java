package mkeck.tictactoe2.model;

import mkeck.tictactoe2.model.Board;
import mkeck.tictactoe2.model.AI;
import mkeck.tictactoe2.model.Human;
import mkeck.tictactoe2.view.Input;
import mkeck.tictactoe2.view.Output;

/**
 *
 * @author mkeck
 */
public class Tictactoe {
    
//    STATIC VARIABLES
    private int count;
    private static Board board = new Board();
    
//    INSTANCE VARIABLES
    private final Player player = new Human();
    private final Player ai     = new AI();
    private Element nextElement = new Element();
    
//    CONSTRUCTOR
    public Tictactoe() {
        Output.showBoard();
    }
    
//    METHODS
    public void readNextElement() {
        while(true) {
            nextElement = Input.readNextElement();
            
            if (nextElement.isFree())
                break;
            else
                Output.showMessage("Element is not free!"); 
        }
    }
    
    public void playerMove() {
        player.setElement(nextElement);
        count++;
    }
    
    /** check if the human player won the game */
    public boolean hasPlayerWon() {
        if (player.playerWon('X')) {
            Output.showMessage("YOU WON!!!");
            return true;
        }
        return false;
    }
    
    /** AI makes a move */
    public void aiMove() {
        ai.setElement(null);
        count++;
    }
    
    /** check if the AI won the game */
    public boolean hasAIWon() {
        if (player.playerWon('O')) {
            Output.showMessage("YOU LOST!!!");
            return true;
        }
        return false;
    }
    
    /** check if the game ended with a draw */
    public boolean isDraw() {
        if (count == 9) {
            Output.showMessage("DRAW");
            return true;
        }
        return false;
    }
}
