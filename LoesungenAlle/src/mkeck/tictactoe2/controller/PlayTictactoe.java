package mkeck.tictactoe2.controller;

import mkeck.tictactoe2.model.Tictactoe;
import mkeck.tictactoe2.view.Output;

/**
 *
 * @author mkeck
 */
public class PlayTictactoe {
    
    public static void main(String[] args) {

//        OBJECTS
        Tictactoe ttt = new Tictactoe();
        
        
//        START GAME
        while (true) {
            
            ttt.readNextElement();
            ttt.playerMove();
            Output.showBoard();
            
            if (ttt.hasPlayerWon())
                break;
            
            if (ttt.isDraw())
                break;
            
            ttt.aiMove();
            Output.showBoard();
            
            if (ttt.hasAIWon())
                break;
        }
    }
}
