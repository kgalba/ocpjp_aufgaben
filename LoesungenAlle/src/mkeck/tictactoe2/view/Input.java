package mkeck.tictactoe2.view;

import java.util.Scanner;
import mkeck.tictactoe2.model.Board;
import mkeck.tictactoe2.model.Element;

/**
 *
 * @author mkeck
 */
public class Input {
    
//    STATIC OBJECTS
    private static Scanner scanner = new Scanner(System.in);
    private static int row;
    private static int col;
    
//    METHODS
    public static Element readNextElement() {
//        read in the element
        System.out.println("Make your next move");
        System.out.print("ROW: ");
        row = scanner.nextInt();
        System.out.print("COLUMN:");
        col = scanner.nextInt();
        System.out.println("");
        
//        return the element
        return Board.getElement(row, col);            
    }
}
