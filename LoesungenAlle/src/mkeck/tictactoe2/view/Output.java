package mkeck.tictactoe2.view;

import mkeck.tictactoe2.model.Board;

/**
 *
 * @author mkeck
 */
public class Output {

//    METHODS
    /** shows the board */
    public static void showBoard() {
        System.out.printf("     0   1   2\n");
        System.out.printf("    ----------- \n");
        System.out.printf("0  | " + Board.getElement(0, 0).getContent() + 
                            " | " + Board.getElement(0, 1).getContent() +
                            " | " + Board.getElement(0, 2).getContent() +
                          " |\n");
        System.out.printf("    ----------- \n");
        System.out.printf("1  | " + Board.getElement(1, 0).getContent() + 
                            " | " + Board.getElement(1, 1).getContent() +
                            " | " + Board.getElement(1, 2).getContent() +
                          " |\n");
        System.out.printf("    ----------- \n");
        System.out.printf("2  | " + Board.getElement(2, 0).getContent() +
                            " | " + Board.getElement(2, 1).getContent() +
                            " | " + Board.getElement(2, 2).getContent() +
                          " |\n");
        System.out.printf("    ----------- \n");
    }
    
    
    public static void showMessage(String s) {
        System.out.println(s);
    }
}
