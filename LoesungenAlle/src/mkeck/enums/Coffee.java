package mkeck.enums;

/**
 *
 * @author mkeck
 */
public enum Coffee {
    
    COFFEE(1.00), CAPPUCCINO(1.20), ESPRESSO(1.13);
    
//    CONSTRUCTOR
    private Coffee(double price) {
        this.price = price;
    }
    
//    VARIABLES
    private double price;
    
//    METHODS
    public double getPrice() {
        return price;
    }
    
}