package mkeck.enums;

/**
 *
 * @author mkeck
 */
public enum DogBreed {
    DACHSHUND(0.5),
    COLLIE(1.0) {
        @Override
        public String toString() {        
            return name() + ", max. size " + getMaxSize() + " m"; 
        }
    },
    MASTIFF(1.5);

//    CONSTRUCTOR
    private DogBreed(double maxSize) {
        this.maxSize = maxSize;
    }
    
//    VARIABLES
    private double maxSize;
    
//    METHODS
    public double getMaxSize() {
        return maxSize;
    }
    
    @Override
    public String toString() {
        String name = name();
        
        name = name.charAt(0) + name.substring(1).toLowerCase();
        
        return name + ", max. size " + getMaxSize() + " m"; 
    }
}
