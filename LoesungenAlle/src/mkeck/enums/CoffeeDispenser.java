package mkeck.enums;

import java.util.Locale;
import java.util.Scanner;

/**
 *
 * @author mkeck
 */
public class CoffeeDispenser {
    
    public static void main(String[] args) {
        
//        VARIABLES
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);
        Coffee coffee;
        
        int choice;  
        double price = 0.0;
        int paid  = 0; // in cents
        
        
//        START INTERACTION WITH THE USER
        System.out.println("---------");
        System.out.println(" WELCOME");
        System.out.println("---------");
        
//        output of possible choices
        for(Coffee c: Coffee.values())
            System.out.printf((c.ordinal()) + ": " + c + "\t" +
                              c.getPrice() + " € \n");
        
//        choose the kind of coffee
        System.out.println("\nPlease choose your beverage:");
        choice = scanner.nextInt();
        coffee = Coffee.values()[choice];
        price = coffee.getPrice();
        
        System.out.print("\nYou chose " + coffee + "\n");
        System.out.printf("Please insert " + price + " €\n");
        
        while ((double)paid / 100 < price) {
            double inserted = scanner.nextDouble();
            int oldPaid  = paid;
            
            // check if coin is valid
            for(Coin c : Coin.values()) {
                if (inserted == c.getValue()) {
                    paid = paid + (int)(inserted * 100);
                    break;
                }
            }
            
//            information output
            if (paid == oldPaid)
                System.out.println("INVALID COIN");
            else
                System.out.println("CREDIT: " + (double)paid / 100 + " €");
        }
        
//        return money
        
        
//        prepare the coffee
        System.out.printf("\nPreparing: " + coffee + "\n");
        System.out.println("Please take your coffee from the dispenser");
        
    }
}
