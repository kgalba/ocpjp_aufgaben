package mkeck.enums;

/**
 *
 * @author mkeck
 */
public enum Coin {
    
    ONECENT(0.01), TWOCENT(0.02), FIVECENT(0.05), TENCENT(0.10),
    TWENTYCENT(0.20), FIFTYCENT(0.50), ONEEURO(1.00), TWOEURO(2.00);
    
//    CONSTRUCTOR
    private Coin(double value) {
        this.value = value;
    }
    
//    VARIABLES
    private double value;
    
//    METHODS
    public double getValue() {
        return value;
    }
    
}
