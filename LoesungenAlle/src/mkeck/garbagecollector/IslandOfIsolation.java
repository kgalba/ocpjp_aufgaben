package mkeck.garbagecollector;

/**
 *
 * @author mkeck
 */

class Foo {
    Bar bar;
    
    @Override
    public void finalize() {
        System.out.println("Collect Foo");
    }
}

class Bar {
    Foo foo;
    
    Bar getBar() {
         return this;
    }
    
    @Override
    public void finalize() {
        System.out.println("Collect Bar");
    }
}

public class IslandOfIsolation {

    public static void main(String[] args) {
        
        Runtime rt = Runtime.getRuntime();
        System.out.println("Total JVM memory: " + rt.totalMemory());
        
        rt.gc();
        System.out.println("No objects " + rt.freeMemory());
   
        Foo foo = new Foo();
        Bar bar = new Bar();
        
        rt.gc();
        System.out.println("2 new Objects " + rt.freeMemory());
        
        foo.bar = bar.getBar();
        bar.foo = foo;

        //Zeile A: ? Objekte für GC
        rt.gc();
        System.out.println("A: " + rt.freeMemory());
        
        foo = null;
        
        //Zeile B: ? Objekte für GC
        rt.gc();
        System.out.println("B: " + rt.freeMemory());
        
        bar = null;
        
        //Zeile C: ? Objekte für GC
        rt.gc();
        System.out.println("C: " + rt.freeMemory());
        
        System.out.println("End of main");
    }
    
}
