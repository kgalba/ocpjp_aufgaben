package mkeck.Comparable;

import java.util.Comparator;

/**
 *
 * @author mkeck
 */
public class YearComparator implements Comparator<Person> {
    
//    Methods
//    Methods of Comparator
    @Override
    public int compare(Person p1, Person p2) {
//        from low to high
        return p1.getYearOfBirth() - p2.getYearOfBirth();
    }
}
