package mkeck.Comparable;

import java.util.Arrays;

public class TestPerson {

    public static void main(String[] args) {
		
	Person[] people = new Person[4];
		
	people[0] = new Person("Marius", "Keck", 1986);
	people[1] = new Person("Albert", "Einstein", 1879);
	people[2] = new Person("Isaac", "Newton", 1642);
        people[3] = new Person("Stephen", "Hawking", 1942);
		
        System.out.println("- UNSORTED -");
	for (int i = 0; i < people.length; i++) {
            System.out.println(people[i].toString());
	}
		
	System.out.println("-------------------------------------------");
        System.out.println("- SORT BY NAME -");
	Arrays.sort(people);
	for (int i = 0; i < people.length; i++) {
            System.out.println(people[i].toString());
	}
		
	System.out.println("-------------------------------------------");
        System.out.println("- SORT BY YEAR OF BIRTH -");
        Arrays.sort(people, new YearComparator());
        for (int i = 0; i < people.length; i++) {
            System.out.println(people[i].toString());
	}

    }
}

