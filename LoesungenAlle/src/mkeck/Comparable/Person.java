package mkeck.Comparable;

public class Person implements Comparable<Person> {
	
//  Variables
    private String lastName;
    private String firstName;
    private int yearOfBirth;
	
//  Constructors
    Person() {
    	this("Max", "Mustermann", 1960);
    }
	
    Person(String firstName, String lastName, int yearOfBirth) {
    	this.lastName    = lastName;
    	this.firstName   = firstName;
	this.yearOfBirth = yearOfBirth;
    }
	
//  Setter and Getter
    public String getLastName() {
	return lastName;
    }

    public void setLastName(String lastNname) {
	this.lastName = lastName;
    }
    
    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }
	
//  Methods
    @Override
    public int compareTo(Person p) {
    	return this.lastName.compareTo(p.getLastName());
    }

    @Override
    public String toString() {
    	return lastName + ", " + firstName + " (" + 
            this.yearOfBirth + ")";
    }
}
