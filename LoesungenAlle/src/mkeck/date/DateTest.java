package mkeck.date;

import java.util.Date;
import java.util.Locale;
import java.text.DateFormat;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

/**
 *
 * @author mkeck
 */
public class DateTest {
    
    public static void addDay(Date d) {
        d.setTime(d.getTime() + 86400000L);
    }
    
    public static void main(String[] args) {
        
//        DATE CONSTRUCTORS
        Date date1 = new Date();
        Date date2 = new Date(100000000000L);
        
//        SOME METHODS OF DATE
        System.out.println("DATE");
        System.out.println("date1 = " + date1.getTime());
        System.out.println("date2 = " + date2.getTime());
        
        System.out.println("date1 before date2? " + date1.before(date2));
        
        System.out.println("date1: " + date1.toString());
        addDay(date1);
        System.out.println("date1 + 1 day = " + date1.toString());
        System.out.println("date2: " + date2.toString());
        
//        DATEFORMAT
        System.out.println("-------------------");
        System.out.println("DATEFORMAT");
        System.out.println("Time of date1: " +
                           DateFormat.getTimeInstance().format(date1));
        System.out.println("Date of date1: " +
                           DateFormat.getDateInstance().format(date1));
        System.out.println("Date and time of date1: " +
                           DateFormat.getDateTimeInstance().format(date1));
        
        System.out.println("-------------------");
        System.out.println("DATEFORMAT SHORT");
        System.out.println("Time of date1: " +
            DateFormat.getTimeInstance(DateFormat.SHORT).format(date1));
        System.out.println("Date of date1: " +
            DateFormat.getDateInstance(DateFormat.SHORT).format(date1));
        System.out.println("Date and time of date1: " +
            DateFormat.getDateTimeInstance(DateFormat.SHORT,
                                           DateFormat.SHORT).format(date1));
        
        System.out.println("-------------------");
        System.out.println("DATEFORMAT LONG");
        System.out.println("Time of date1: " +
            DateFormat.getTimeInstance(DateFormat.LONG).format(date1));
        System.out.println("Date of date1: " +
            DateFormat.getDateInstance(DateFormat.LONG).format(date1));
        System.out.println("Date and time of date1: " +
            DateFormat.getDateTimeInstance(DateFormat.LONG,
                                           DateFormat.LONG).format(date1));
        
        System.out.println("-------------------");
        System.out.println("DATEFORMAT SHORT US");
        System.out.println("Time of date1: " +
            DateFormat.getTimeInstance(DateFormat.SHORT,
                                       Locale.US).format(date1));
        System.out.println("Date of date1: " +
            DateFormat.getDateInstance(DateFormat.SHORT,
                                       Locale.US).format(date1));
        System.out.println("Date and time of date1: " +
            DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT,
                                           Locale.US).format(date1));
        
        System.out.println("-------------------");
        System.out.println("DATEFORMAT LONG US");
        System.out.println("Time of date1: " +
            DateFormat.getTimeInstance(DateFormat.LONG,
                                       Locale.US).format(date1));
        System.out.println("Date of date1: " +
            DateFormat.getDateInstance(DateFormat.LONG,
                                       Locale.US).format(date1));
        System.out.println("Date and time of date1: " +
            DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG,
                                           Locale.US).format(date1));
        
        ZonedDateTime now = ZonedDateTime.now( ZoneOffset.UTC );
        System.out.println(now);
        
    } // end of main
}
