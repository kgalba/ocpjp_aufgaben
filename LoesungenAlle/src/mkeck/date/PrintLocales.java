package mkeck.date;

import java.util.Locale;

/**
 *
 * @author mkeck
 */
public class PrintLocales {
    
    public static void printTable(Locale[] locales) {
        System.out.format("|%16s|%24s|%16s|\n","Nr.", "Land", "Sprache");
        for (int i = 0; i < locales.length; i++) {
            System.out.format(Locale.US, "|%16d|%24s|%16s|\n", i,
                              locales[i].getDisplayCountry(Locale.US),
                              locales[i].getDisplayLanguage(Locale.US));
        }
        
    }
    
    public static void main(String[] args) {
        
        Locale[] locales = new Locale[4];
        locales[0] = Locale.UK;
        locales[1] = Locale.US;
        locales[2] = Locale.GERMANY;
        locales[3] = Locale.FRANCE;
        
        printTable(locales);
        
        System.out.println("-------------------");
        printTable(Locale.getAvailableLocales());
    }
}
