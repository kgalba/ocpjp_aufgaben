package interfaces;

/**
 * 
 * Testing Interfaces
 * tgottschau@gssup.de
 * @author kgalba
 */
public abstract interface Interable {
    public void test(String s);
}

abstract class interImplAbs implements Interable{}

class interImplAbs1 implements Interable {

    public  void test(String a) { }
    public  void test(int a) { }
    
}

class TestA {

    public TestA(String s) {
        System.out.println("A");
    }
   
}

class TestAA extends TestA {
    public TestAA(String s) {
        super(s);
        System.out.println("AA");
    }

}

