package scjp;

import java.util.Arrays;

/**
 *
 * @author mkeck
 */
public class InstanceCheck {
    
    public static void main(String[] args) {
        
        int i = 5;
        String s = "test";
        
        System.out.println(Integer.class.isInstance(i));
        System.out.println(Double.class.isInstance(i));
        System.out.println(s instanceof String);
        System.out.println(s instanceof Object);
    }
}
