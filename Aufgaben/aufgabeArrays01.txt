Aufgabe "Arrays 01"

Bitte definieren Sie eine Methode 'createArray', die ein int-Array erzeugt 
und mit Zufallswerten belegt. Die neue Methode soll aus der main-Methode 
folgendermassen aufgerufen werden koennen:
    
    int[] arr = createArray(2, 15, 30);

In diesem Beispiel wird ein Array der Laenge 30 erzeugt und mit den 
Zufallswerten aus dem Bereich [2 .. 15] belegt.

Bitte geben Sie das erzeugte Array aus. Dafür definieren Sie eine weitere 
Methode 'printArray', an die Sie das Array übergeben.
