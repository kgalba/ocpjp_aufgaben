﻿Aufgabe "For-Schleife"

Tipp: Fuer die Ausgabe ohne Zeilenumbruch kann man den Aufruf System.out.print(x) verwenden.
Tipp: Fuer einen Zeilenumbruch kann man den Aufruf System.out.println() verwenden.

1 - Folgende Zahlenreihe mit der for-Schleife bitte in einer Zeile ausgeben: 
    0 1 2 3 4 5 6 7 8 9
2 - Folgende Zahlenreihe mit der for-Schleife bitte in einer Zeile ausgeben: 
    0 2 3 4 5 6 7 8 9
3 - Folgende Zahlenreihe mit der for-Schleife bitte in einer Zeile ausgeben: 
    -4 -2 0 2 4 ... 50
4 - Bitte die englischen Kleinbuchstaben von a bis z in einer Zeile ausgeben: 
    a b c ... z
5 - Bitte die englischen Grossbuchstaben rueckwaerts in einer Zeile ausgeben: 
    Z Y X ... A
6 - Bitte die Zahlen aus dem Bereich [1 ... 100] ausgeben, die durch 5 ohne Rest dividierbar sind

Optional:
7 - Folgende Zahlenreihe bitte in einer Zeile ausgeben: 
    3 1 2 3 2 1 2 1 1
Tipp: Schleifen können geschachtelt werden