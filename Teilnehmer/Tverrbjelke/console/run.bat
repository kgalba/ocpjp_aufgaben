::Script to run a compiled java app
@set PATH=%PATH%;"G:\Programs\Tools\Java\jdk1.8.0_45\bin"
set PROJECTSOURCE=D:\Users\tverrbjelke\Documents\Coding\NetBeansProjects\ocpjp_aufgaben\LoesungenAlle\src
set LAUNCHPATH=.
set BUILDPATH=%LAUNCHPATH%.class
::set CP=%PROJECTSOURCE%
set CP=%BUILDPATH%;%PROJECTSOURCE%
set CODE=tverrbjelke.tictactoe.TicTacToeApp
java -classpath %CP% %CODE%
