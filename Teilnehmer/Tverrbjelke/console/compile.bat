::Script to compile a java app
@set PATH=%PATH%;"G:\Programs\Tools\Java\jdk1.8.0_45\bin"
set PROJECTSOURCE=D:\Users\tverrbjelke\Documents\Coding\NetBeansProjects\ocpjp_aufgaben\LoesungenAlle\src
set LAUNCHPATH=.
set CP=%PROJECTSOURCE%
set SOURCE=%CP%\tverrbjelke\tictactoe\TicTacToeApp.java
set BUILD=%LAUNCHPATH%\class
javac -classpath %CP% %SOURCE% -d %BUILD%
