/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itearablepersonlist;

import java.util.Iterator;

/**
 *
 * @author tverrbjelke
 */
public class PersonenList implements Iterable<Person> {

    private int maxPersonen = 10;
    /** holds a list of Person instances, must not be itearable by itself */
    private Person[] personen;
    private int numPersonen;

    public PersonenList() {
        this.personen = new Person[maxPersonen];
        this.numPersonen = 0; 
    }

    public int getMaxPersonen() {
        return maxPersonen;
    }

    public int getNumPersonen() {
        return numPersonen;
    }

    public static void main(String[] args) {
	PersonenList list = new PersonenList();
	list.add(new Person("Peter", "Braun"));
	list.add(new Person("Michael", "Roth"));

	for(Person p : list) {
		System.out.println(p);
	}
    }

    public Person atIndex(int index){
        if ((index < 0) || (index >= this.numPersonen)) {
             throw new IndexOutOfBoundsException("invalid index" + index);
        }
        return this.personen[index];
    }
    
    public void add(Person p){
        if (this.numPersonen >= this.maxPersonen){
            String msg = "maximal number of persons reached";
            throw new IndexOutOfBoundsException(msg);
        }
        else {
            this.personen[this.numPersonen] = p; // ggf deep copy?
            numPersonen++;
        }
    }
    
    @Override
    public Iterator<Person> iterator() {
        return new PersonenIterator(this);
    }

}
