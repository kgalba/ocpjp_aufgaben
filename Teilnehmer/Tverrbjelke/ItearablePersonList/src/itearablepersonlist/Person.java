/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itearablepersonlist;

/**
 *
 * @author tverrbjelke
 */
public class Person {

    private String firstName, lastNme;

    public Person(String FirstName, String LastNme) {
        this.firstName = FirstName;
        this.lastNme = LastNme;
    }
    
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String FirstName) {
        this.firstName = FirstName;
    }

    public String getLastNme() {
        return lastNme;
    }

    public void setLastNme(String LastNme) {
        this.lastNme = LastNme;
    }
    
    @Override public String toString(){
    return "Person: " + getFirstName() + " " + getLastNme();
    }

    
}
