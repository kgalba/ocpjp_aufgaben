/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itearablepersonlist;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 *
 * @author tverrbjelke
 */
public class PersonenIterator implements Iterator<Person> {
    private PersonenList iteratee;
    private int currentPosition;
            
    public PersonenIterator(PersonenList iteratee) {
        this.iteratee = iteratee;
        this.currentPosition = 0;
    }

    @Override
    public boolean hasNext() {
        return this.currentPosition < this.iteratee.getNumPersonen();
    }

    @Override
    public Person next() {
        if ( hasNext() ){
            int curTmp = this.currentPosition;
            this.currentPosition++;
            return this.iteratee.atIndex(curTmp);
        }
        else{
            throw new NoSuchElementException();
        }
    }
    
    
}
