/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150609.polymorphism;

/**
 * A shape is a geometrical object, which has an area size.
 *
 * @author Carsten Petschel &lt;carsten-petschel@t-online.de&gt;
 */
public interface Shape {

    /**
     * Returns the area size of this shape.
     *
     * @return the area size.
     */
    double getAreaSize();
}
