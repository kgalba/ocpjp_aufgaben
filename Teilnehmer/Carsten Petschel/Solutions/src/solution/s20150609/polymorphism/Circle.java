/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150609.polymorphism;

import java.util.Locale;

/**
 * A circle, with radius and area size.
 *
 * @author Carsten Petschel &lt;carsten-petschel@t-online.de&gt;
 */
public class Circle implements Shape {

    private double radius;

    /**
     * Create a Kreis object.
     */
    public Circle() {
        super();
    }

    /**
     * Create a Kreis object with the specified radius.
     *
     * @param radius
     */
    public Circle(double radius) {
        this();
        if (radius <= 0) {
            String message = "The radius should be a positive value.";
            throw new IllegalArgumentException(message);
        }
        this.radius = radius;
    }

    /**
     * Return the radius of this Circle object.
     *
     * @return the radius
     */
    public double getRadius() {
        return radius;
    }

    /**
     * Set the radius of this Circle object.
     *
     * @param radius the radius
     */
    public void setRadius(double radius) {
        if (radius <= 0) {
            String message = "The radius should be a positive value.";
            throw new IllegalArgumentException(message);
        }
        this.radius = radius;
    }

    /**
     * Returns the area size of this Circle object.
     *
     * @return the area size
     */
    @Override
    public double getAreaSize() {
        return Math.PI * radius * radius;
    }

    /**
     * Returns a String representation of this Circle object.
     *
     * @return a String representation
     */
    @Override
    public String toString() {
        String lineSeparator = System.lineSeparator();
        String stringRadius = String.format(Locale.ENGLISH, "%,.2f", radius);
        String stringAreaSize = String.format(Locale.ENGLISH, "%,.2f", getAreaSize());
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Circle : ").append(lineSeparator);
        stringBuilder.append("\tRadius : ").append(stringRadius).append(lineSeparator);
        stringBuilder.append("\tArea   : ").append(stringAreaSize);
        return stringBuilder.toString();
    }

}
