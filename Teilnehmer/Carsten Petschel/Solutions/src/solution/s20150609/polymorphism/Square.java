/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150609.polymorphism;

import java.util.Locale;

/**
 * A square, with height and width, and an area size.
 *
 * @author Carsten Petschel &lt;carsten-petschel@t-online.de&gt;
 */
public class Square implements Shape {

    private double width;
    private double height;

    /**
     * Create a Rechteck object.
     */
    public Square() {
        super();
    }

    /**
     * Create a Rechteck object with the specified width and height.
     *
     * @param width the width
     * @param height the height
     */
    public Square(double width, double height) {
        this();
        if (width <= 0) {
            String message = "The width should be a positive value.";
            throw new IllegalArgumentException(message);
        }
        if (height <= 0) {
            String message = "The height should be a positive value.";
            throw new IllegalArgumentException(message);
        }
        this.width = width;
        this.height = height;
    }

    /**
     * Returns the width of this Square object.
     *
     * @return the width
     */
    public double getWidth() {
        return width;
    }

    /**
     * Set the width of this Square object with the specified value.
     *
     * @param width the width
     */
    public void setWidth(double width) {
        if (width <= 0) {
            String message = "The width should be a positive value.";
            throw new IllegalArgumentException(message);
        }
        this.width = width;
    }

    /**
     * Returns the height of this Square object.
     *
     * @return the height
     */
    public double getHeight() {
        return height;
    }

    /**
     * Set the height of this Square object with the specified value.
     *
     * @param height the height
     */
    public void setHeight(double height) {
        if (height <= 0) {
            String message = "The height should be a positive value.";
            throw new IllegalArgumentException(message);
        }
        this.height = height;
    }

    /**
     * Returns the area size of this Square object.
     *
     * @return the area size
     */
    @Override
    public double getAreaSize() {
        return width * height;
    }

    /**
     * Returns a String representation of the Square object.
     *
     * @return a String representation
     */
    @Override
    public String toString() {
        String lineSeparator = System.lineSeparator();
        StringBuilder stringBuilder = new StringBuilder();
        String stringWidht = String.format(Locale.ENGLISH, "%,.2f", width);
        String stringHeight = String.format(Locale.ENGLISH, "%,.2f", height);
        String stringAreaSize = String.format(Locale.ENGLISH, "%,.2f", getAreaSize());
        stringBuilder.append("Square :").append(lineSeparator);
        stringBuilder.append("\tWidht  : ").append(stringWidht).append(lineSeparator);
        stringBuilder.append("\tHeight : ").append(stringHeight).append(lineSeparator);
        stringBuilder.append("\tArea   : ").append(stringAreaSize);
        return stringBuilder.toString();
    }

}
