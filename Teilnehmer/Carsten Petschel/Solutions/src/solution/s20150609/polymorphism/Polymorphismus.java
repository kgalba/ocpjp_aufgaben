/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150609.polymorphism;

import java.util.Locale;
import java.util.Random;

/**
 * Task: Aufgabe Polymorphismus - Geometrie.doc
 *
 * @author Carsten Petschel &lt;carsten-petschel@t-online.de&gt;
 */
public class Polymorphismus {

    /**
     * Create a random double value, which is bigger or equal to 1, and lower
     * than the specified maximum value.
     *
     * @param maximum the maximum value
     * @return a random value
     */
    private static double createRandomDoubleFrom1toMaximum(double maximum) {
        if (maximum <= 0) {
            String message = "The argument value should be positive.";
            throw new IllegalArgumentException(message);
        }
        return Math.random() * (maximum - 1) + 1;
    }

    /**
     * Print a couple of Square objects with a specified maximum width and
 height.
     *
     * @param number the number of objects to be printed
     * @param maximumWidth the maximum width
     * @param maximumHeight the maximum height
     */
    private static void printSquares(int number, double maximumWidth, double maximumHeight) {
        if (number <= 0 || maximumHeight <= 0 || maximumWidth <= 0) {
            String message = "Argument values should be positive.";
            throw new IllegalArgumentException(message);
        }
        for (int i = 0; i < number; i++) {
            double breite = createRandomDoubleFrom1toMaximum(maximumWidth);
            double höhe = createRandomDoubleFrom1toMaximum(maximumHeight);
            Square rechteck = new Square(breite, höhe);
            System.out.println(rechteck);
        }
    }

    /**
     * Print a number of Circle objects with a specified maximum radius.
     *
     * @param number the number of objects to be printed
     * @param maximumRadius the maximum radius
     */
    private static void printCircles(int number, double maximumRadius) {
        if (number <= 0 || maximumRadius <= 0) {
            String message = "Argument values should be positive.";
            throw new IllegalArgumentException(message);
        }
        for (int i = 0; i < number; i++) {
            double radius = createRandomDoubleFrom1toMaximum(maximumRadius);
            Circle kreis = new Circle(radius);
            System.out.println(kreis);
        }
    }

    /**
     * Create a couple of random shapes.
     *
     * @param number the number of shapes to be created
     * @return an array of shapes
     */
    private static Shape[] createRandomShapes(int number) {
        if (number <= 0) {
            String message = "Argument value should be positive.";
            throw new IllegalArgumentException(message);
        }
        Random random = new Random();
        double maximum = 20;
        Shape[] shapes = new Shape[number];
        for (int i = 0; i < number; i++) {
            if (random.nextBoolean()) {
                double breite = createRandomDoubleFrom1toMaximum(maximum);
                double höhe = createRandomDoubleFrom1toMaximum(maximum);
                shapes[i] = new Square(breite, höhe);
            } else {
                double radius = createRandomDoubleFrom1toMaximum(maximum);
                shapes[i] = new Circle(radius);
            }
        }
        return shapes;
    }

    /**
     * Print the combined area size of a couple of shapes.
     *
     * @param shapes the shapes
     */
    private static void printCombinedAreaSize(Shape[] shapes) {
        if (shapes == null) {
            String message = "Argument value should be not null.";
            throw new IllegalArgumentException(message);
        }
        for (Shape shape : shapes) {
            if (shape == null) {
                String message = "The array should not contain null values.";
                throw new IllegalArgumentException(message);
            }
        }
        double area = 0;
        for (Shape shape : shapes) {
            area += shape.getAreaSize();
        }
        String stringArea = String.format(Locale.ENGLISH, "%,.2f", area);
        System.out.println("Area size of 100 random shapes : " + stringArea);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        printSquares(100, 20, 20);
        System.out.println("---");
        printCircles(1, 20);
        System.out.println("---");
        printCombinedAreaSize(createRandomShapes(100));
    }

}
