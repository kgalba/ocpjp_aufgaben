/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150522.operands;

/**
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class Operatoren {

    private static String lineSeparator = System.lineSeparator();

    private static void printArithmeticOperators() {
        int one = 1;
        double two = 2.0;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Arithmetic Operators: ").append(lineSeparator);
        stringBuilder.append(one).append(" + ").append(two).append(" = ").append(one + two).append(lineSeparator);
        stringBuilder.append(one).append(" - ").append(two).append(" = ").append(one - two).append(lineSeparator);
        stringBuilder.append(one).append(" * ").append(two).append(" = ").append(one * two).append(lineSeparator);
        stringBuilder.append(one).append(" / ").append(two).append(" = ").append(one / two).append(lineSeparator);
        stringBuilder.append(one).append(" % ").append(two).append(" = ").append(one % two).append(lineSeparator);
        stringBuilder.append("++").append(one).append(" = ").append(++one).append(lineSeparator);
        --one;
        stringBuilder.append("--").append(one).append(" = ").append(--one).append(lineSeparator);
        ++one;
        stringBuilder.append(one).append("++").append(" = ").append(one++).append(lineSeparator);
        stringBuilder.append(" = ").append(one).append(lineSeparator);
        --one;
        System.out.println(stringBuilder);
    }

    private static void printEqualityOperators() {
        int one = 1;
        double two = 2.0;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Equality Operators:").append(lineSeparator);
        stringBuilder.append(one).append(" == ").append(two).append(" = ").append(one == two).append(lineSeparator);
        stringBuilder.append(one).append(" != ").append(two).append(" = ").append(one != two).append(lineSeparator);
        stringBuilder.append(one).append(" <= ").append(two).append(" = ").append(one <= two).append(lineSeparator);
        stringBuilder.append(one).append(" >= ").append(two).append(" = ").append(one >= two).append(lineSeparator);
        System.out.println(stringBuilder);
    }

    private static void printLogicalOperators() {
        boolean one = false;
        boolean two = true;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(one).append(" && ").append(two).append(" = ").append(one && two).append(lineSeparator);
        stringBuilder.append(one).append(" || ").append(two).append(" = ").append(one || two).append(lineSeparator);
        stringBuilder.append(one).append(" | ").append(two).append(" = ").append(one | two).append(lineSeparator);
        stringBuilder.append(one).append(" & ").append(two).append(" = ").append(one & two).append(lineSeparator);
        stringBuilder.append(one).append(" ^ ").append(two).append(" = ").append(one ^ two).append(lineSeparator);
        stringBuilder.append("!").append(one).append(" = ").append(!one).append(lineSeparator);
        System.out.println(stringBuilder);
    }

    private static void printCasting() {
        int a = 0b1_1111_1111;
        byte b = (byte) 0b1_1111_1111;
        System.out.println(a + " " + b);
    }

    private static void printLogicTableXOR() {
        boolean[] a = {false, true};
        boolean[] b = {false, true};
        System.out.printf(" %5s | %5b | %5b |%n", "^", b[0], b[1]);
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print("-");
            }
            System.out.print("+");
        }
        System.out.println();
        for (boolean b1 : a) {
            System.out.printf(" %5b |", b1);
            for (boolean b2 : b) {
                System.out.printf(" %5b |", b1 ^ b2);
            }
            System.out.println();
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 7; j++) {
                System.out.print("-");
            }
            System.out.print("+");
        }
        System.out.println();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        printArithmeticOperators();
//        printEqualityOperators();
//        printLogicalOperators();
        printLogicTableXOR();
    }

}
