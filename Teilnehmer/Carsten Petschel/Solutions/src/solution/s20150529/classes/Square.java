/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150529.classes;

import java.util.Random;

/**
 * A square has a height and a width, and an area.
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class Square {

    private Random random;
    private Integer width;
    private Integer height;

    /**
     * Generates a Square with a width and height of 0.
     */
    public Square() {
        super();
        random = new Random();
        width = 0;
        width = 0;
    }

    /**
     * Generates a Square with a random width and height between 1 and 10
     * inclusively, if randomSize is specified as true.
     *
     * @param randomSize flag to indicate a random generation
     */
    public Square(final Boolean randomSize) {
        this();
        if (randomSize) {
            width = random.nextInt(10) + 1;
            height = random.nextInt(10) + 1;
        }
    }

    /**
     * Generates a Square with the specified widht and height, which must have
     * positive values between 1 and 10 inclusively.
     *
     * @param width the square width
     * @param height the square height
     */
    public Square(final Integer width, final Integer height) {
        this();
        if (width < 0 || width > 10 || height < 0 || height > 10) {
            String message = "Width and height need to be positive integer values between 1 and 10 inclusively.";
            throw new IllegalArgumentException(message);
        }
        this.width = width;
        this.height = height;
    }

    /**
     * Returns the area of this square.
     *
     * @return the area value
     */
    public Integer getArea() {
        return width * height;
    }

    /**
     * Returns the width of the square.
     *
     * @return the width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * Sets the width of the square.
     *
     * @param width the width
     */
    public void setWidth(final Integer width) {
        this.width = width;
    }

    /**
     * Returns the height of the square.
     *
     * @return the height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * Sets the height of the square.
     *
     * @param height the height
     */
    public void setHeight(final Integer height) {
        this.height = height;
    }

}
