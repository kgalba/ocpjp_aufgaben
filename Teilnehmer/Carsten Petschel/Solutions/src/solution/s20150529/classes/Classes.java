/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150529.classes;

import java.util.Random;

/**
 * Contains a class example with different constructors, some functionality in
 * methods and an array.
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class Classes {

    /**
     * Print the area of a Square, with and option to choose filled style.
     *
     * @param square the square to be printed
     * @param filled set to true, if filled style is preferred
     */
    public static void printSquareArea(final Square square, final Boolean filled) {
        int width = square.getWidth();
        int height = square.getHeight();
        for (int h = 0; h < height; h++) {
            for (int w = 0; w < width; w++) {
                if (filled || h == 0 || h == (height - 1) || w == 0 || w == (width - 1)) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println("");
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int size = 100;
        Square[] squares = new Square[size];
        for (int i = 0; i < size; i++) {
            squares[i] = new Square(Boolean.TRUE);
        }
        Random random = new Random();
        for (Square s : squares) {
            printSquareArea(s, random.nextBoolean());
            int width = s.getWidth();
            int height = s.getHeight();
            int area = s.getArea();
            System.out.println(width + " * " + height + " = " + area);
            System.out.println("-------");
        }
    }

}
