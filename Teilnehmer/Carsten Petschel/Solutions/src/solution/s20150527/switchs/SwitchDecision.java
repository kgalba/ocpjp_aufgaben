/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150527.switchs;

/**
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class SwitchDecision {

    public static void analyseCharacter(Character c) {
        if (c < 'a' || c > 'w') {
            String message = "This method accepts small characters from the english alphabet only.";
            throw new IllegalArgumentException(message);
        }
        switch (c) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
                System.out.println("das ist ein Vokal");
                break;
            case 'b':
            case 'c':
            case 'd':
            case 'f':
            case 'g':
            case 'h':
            case 'j':
            case 'k':
            case 'l':
            case 'm':
            case 'n':
            case 'p':
            case 'q':
            case 'r':
            case 's':
            case 't':
            case 'v':
            case 'w':
                System.out.println("das ist ein Konsonant");
                break;
            default:
                System.err.println("Should not happen.");
                break;
        }
    }

    public static void analyseLightSignalColor(LightSignalColor lightSignalColor) {
        switch (lightSignalColor) {
            case RED:
                System.out.println("Rot. Bitte warten");
                break;
            case YELLOW:
                System.out.println("Gelb. Geht get es los");
                break;
            case GREEN:
                System.out.println("Gruen. Weg frei");
                break;
            default:
                System.out.println("Fehler! Diese Farbe gibt es nicht.");
                break;
        }
    }

    /**
     * 28.05.2015
     */
    private static void testCharacters() {
        System.out.println("test characters:");
        System.out.println("=======");
        System.out.println("");
        for (char c = 'a'; c <= 'w'; c++) {
            System.out.print(c + ": ");
            SwitchDecision.analyseCharacter(c);
        }
        System.out.println("");
    }

    /**
     * 28.05.2015
     */
    private static void testLightSignalColor() {
        System.out.println("test light signal color");
        System.out.println("=======");
        System.out.println("");
        System.out.println("Switch mit Enum:");
        System.out.println("-------");
        LightSignalColor[] lightSignalColors = {LightSignalColor.GREEN, LightSignalColor.YELLOW, LightSignalColor.RED};
        for (LightSignalColor l : lightSignalColors) {
            SwitchDecision.analyseLightSignalColor(l);
        }
        System.out.println("");
        System.out.println("Alternative mit if / else if / else:");
        System.out.println("-------");
        for (int i = 0; i <= 3; i++) {
            switch (i) {
                case 0:
                    System.out.println("Rot. Bitte warten");
                    break;
                case 1:
                    System.out.println("Gelb. Geht get es los");
                    break;
                case 2:
                    System.out.println("Gruen. Weg frei");
                    break;
                default:
                    System.out.println("Fehler! Diese Farbe gibt es nicht.");
                    break;
            }
        }
        System.out.println("");
    }

    public static void main(String[] args) {
        testCharacters();
        testLightSignalColor();
    }
}
