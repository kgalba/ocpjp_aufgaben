/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150526.fors;

/**
 * 27.05.2015
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class ForLoop {

    public static void print0To9() {
        for (int i = 0; i < 10; i++) {
            System.out.print(i + " ");
        }
        System.out.println("");
    }

    public static void print0To9Without1() {
        for (int i = 0; i < 10; i++) {
            if (i != 1) {
                System.out.print(i + " ");
            }
        }
        System.out.println("");
    }

    public static void printMinus4To50WithStepsOf2() {
        for (int i = -2; i <= 50; i += 2) {
            System.out.print(i + " ");
        }
        System.out.println("");
    }

    public static void printSmallAToSmallZ() {
        for (char c = 'a'; c <= 'z'; c++) {
            System.out.print(c + " ");
        }
        System.out.println("");
    }

    public static void printBigZDownToBigA() {
        for (char c = 'Z'; c >= 'A'; c--) {
            System.out.print(c + " ");
        }
        System.out.println("");
    }

    public static void printNumbersFrom1to100DividableBy5() {
        for (int i = 0; i <= 100; i++) {
            if (i % 5 == 0) {
                System.out.print(i + " ");
            }
        }
        System.out.println("");
    }

    public static void printNumberPattern() {
        for (int i = 3; i >= 1; i--) {
            System.out.print(i + " ");
            for (int j = 1; j <= i; j++) {
                System.out.print(j + " ");
            }
        }
        System.out.println("");
    }

    /**
     * 27.05.2015
     */
    private static void testForLoop() {
        ForLoop.print0To9();
        System.out.println("---");
        ForLoop.print0To9Without1();
        System.out.println("---");
        ForLoop.printMinus4To50WithStepsOf2();
        System.out.println("---");
        ForLoop.printSmallAToSmallZ();
        System.out.println("---");
        ForLoop.printBigZDownToBigA();
        System.out.println("---");
        ForLoop.printNumbersFrom1to100DividableBy5();
        System.out.println("---");
        ForLoop.printNumberPattern();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        testForLoop();
    }
}
