/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150528.methods;

import java.math.BigInteger;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

/**
 * 28.05.2015
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class TestMethods {

    /**
     * Definieren Sie eine statische Methode "printFromTo" mit zwei
     * int-Parameter "from" und "to". Die Methode gibt aller int-Werte von
     * "from" bis "to" auf der Konsole in einer Zeile aus. Testen Sie Ihre
     * Lösung.
     */
    private static void printFromTo(final Integer from, final Integer to) {
        if (from <= to) {
            for (int i = from; i <= to; i++) {
                System.out.print(i + " ");
            }
        } else {
            for (int i = from; i >= to; i--) {
                System.out.print(i + " ");
            }
        }
        System.out.println("");
    }

    /**
     * Definieren Sie eine statische Methode "sum", die zwei int-Parameter
     * erhält, die Summe berechnet und das Ergebnis zurück liefert. Testen Sie
     * Ihre Lösung. Wenn die Aufgabe gelöst ist, überlegen Sie ob Ihre Methode
     * immer die korrekten Ergebnisse liefert.
     */
    private static Integer sum(final Integer a, final Integer b) {
        return a + b;
    }

    private static Integer sumFromTo(final Integer from, final Integer to) {
        int sum = 0;
        if (from <= to) {
            for (int i = from; i <= to; i++) {
                sum += i;
            }
        } else {
            for (int i = from; i >= to; i--) {
                sum += i;
            }
        }
        return sum;
    }

    /**
     * Definieren Sie eine statische Methode "zeichneRechteck", die zwei
     * int-Parameter "breite" und "hoehe" hat. Die Methode zeichnet ein
     * gefülltes Rechteck auf der Konsole.
     *
     * @param breite
     * @param höhe
     */
    private static void zeichneRechteck(final Integer breite, final Integer höhe) {
        if (breite <= 0 || höhe <= 0) {
            String message = "This method accepts positive non-zero integer values only.";
            throw new IllegalArgumentException(message);
        }
        for (int h = 0; h < höhe; h++) {
            for (int b = 0; b < breite; b++) {
                System.out.print("*");
            }
            System.out.println("");
        }
    }

    /**
     * Definieren Sie eine statische Methode "zeichneRechteck", die zwei
     * int-Parameter "breite" und "hoehe" und einen boolean-Parameter "fuellen"
     * hat. Die Methode zeichnet ein Rechteck auf der Konsole. Wenn der
     * Parameter "fuellen" true ist, wird ein gefülltes Rechteck gezeichnet,
     * sonst ein leeres.
     *
     * @param breite
     * @param höhe
     * @param füllen
     */
    private static void zeichneRechteck(final Integer breite, final Integer höhe, final boolean füllen) {
        if (breite <= 0 || höhe <= 0) {
            String message = "This method accepts positive non-zero integer values only.";
            throw new IllegalArgumentException(message);
        }
        for (int h = 0; h < höhe; h++) {
            for (int b = 0; b < breite; b++) {
                if (füllen || h == 0 || h == (höhe - 1) || b == 0 || b == (breite - 1)) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println("");
        }
    }

    /**
     * Definieren Sie eine statische Methode "printRandom", die die N
     * int-Zufallszahlen aus einem Zahlenbereich [VON...BIS] generiert und auf
     * der Konsole ausgibt. Die Anzahl der Zahlen und die Unter- und Obergrenze
     * des Zahlenbereiches sollen beim Aufruf der Methode als Argumente
     * übergeben werden.
     *
     * @param n
     * @param von
     * @param bis
     */
    private static void printRandom(final Integer n, final Integer von, final Integer bis) {
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            int r;
            if (von <= bis) {
                r = random.nextInt(bis - von + 1) + von;
            } else {
                r = random.nextInt(von - bis + 1) + bis;
            }
            System.out.print(r + " ");
        }
        System.out.println("");
    }

    /**
     * Definieren Sie eine statische Methode "getZeitspanneInGanzenTagen", an
     * die man als Argumente zwei Datumsangaben (zu Tag 1 und Tag 2) übergibt.
     * Die Methode liefert dann die Anzahl der ganzen Tagen zwischen den zwei
     * Daten.
     *
     * @param von
     * @param bis
     * @return
     */
    private static Long getZeitspanneInGanzenTagen(final Date von, final Date bis) {
        long v = von.getTime();
        long b = bis.getTime();
        return Math.round(1.0 * (v <= b ? b - v : v - b) / 1000 / 60 / 60 / 24);
    }

    /**
     * Definieren Sie eine statische Methode "getFakultaet", die die Fakultät zu
     * einem int-Wert berechnet.
     *
     * @param value
     * @return
     */
    private static BigInteger getFakultaet(final Integer value) {
        if (value < 0) {
            String message = "This method accepts positive integer values only.";
            throw new IllegalArgumentException(message);
        }
        BigInteger f = new BigInteger("1");
        for (int i = 1; i <= value; i++) {
            f = f.multiply(new BigInteger("" + i));
        }
        return f;
    }

    /**
     * Definieren Sie eine statische Methode "getFakultaetRekursiv", die die
     * Fakultät zu einem int-Wert rekursiv berechnet.
     *
     * @param value
     * @return
     */
    private static BigInteger getFakultaetRekursiv(final Integer value) {
        if (value < 0) {
            String message = "This method accepts positive integer values only.";
            throw new IllegalArgumentException(message);
        }
        BigInteger f = new BigInteger("" + value);
        if (value > 1) {
            f = f.multiply(getFakultaetRekursiv(value - 1));
        } else if (value == 0) {
            f = new BigInteger("1");
        }
        return f;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        printFromTo(4, -7);
        System.out.println(sum(-7, 7));
        System.out.println(sumFromTo(5, -4));
        zeichneRechteck(3, 2);
        zeichneRechteck(6, 3, false);
        printRandom(10, -7, 7);
        System.out.println(getZeitspanneInGanzenTagen(new GregorianCalendar(2015, 5, 22).getTime(), new GregorianCalendar(2015, 5, 28).getTime()));
        System.out.println(getFakultaet(1000));
        System.out.println(getFakultaetRekursiv(0));
    }

}
