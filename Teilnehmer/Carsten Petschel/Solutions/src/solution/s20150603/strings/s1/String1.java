/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150603.strings.s1;

/**
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class String1 {

    /**
     * Tests, if a name ends with ".txt".
     *
     * @param name the name to be tested
     * @return true, if name ends with ".txt", otherwise false
     */
    public static boolean isTextFile(String name) {
        return name.endsWith(".txt");
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String message = "Java ist toll";
        System.out.print(message + " : ");
        char[] characters = message.toCharArray();
        for (int i = characters.length - 1; i >= 0; i--) {
            System.out.print(characters[i]);
        }
        System.out.println();
        System.out.println("---");
        String name = "test.txt";
        System.out.println(name + " : " + isTextFile(name));
    }
}
