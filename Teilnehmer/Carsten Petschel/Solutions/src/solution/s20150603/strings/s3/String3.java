/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150603.strings.s3;

/**
 * Task: aufgabe_StringApi.txt
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class String3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String string1 = "Test";
        System.out.println("string1 : " + string1);
        System.out.println("---");
        String string2 = string1.concat(string1);
        System.out.println("string1.concat(string1) : " + string2);
        System.out.println("---");
        System.out.println("string1.charAt(2) : " + string1.charAt(2));
        System.out.println("---");
        System.out.println("string1.length() : " + string1.length());
        System.out.println("---");
        System.out.println("string1.isEmpty() : " + string1.isEmpty());
        System.out.println("---");
        System.out.println("string1.toUpperCase() : " + string1.toUpperCase());
        System.out.println("---");
        System.out.println("string1.toLowerCase() : " + string1.toLowerCase());
        System.out.println("---");
        System.out.println("string2 : " + string2);
        System.out.println("string2.endsWith(string1) : " + string2.endsWith(string1));
        System.out.println("---");
        System.out.println("string2.startsWith(string1) : " + string2.startsWith(string1));
        System.out.println("---");
        System.out.println("string2.startsWith(string1, 2) : " + string2.startsWith(string1, 2));
        System.out.println("---");
        System.out.println("string2.equals(string1) : " + string2.equals(string1));
        System.out.println("string2.equals(string2) : " + string2.equals(string2));
        String string3 = "Test";
        System.out.println("string3 : " + string3);
        System.out.println("string3 == string1 : " + (string2 == string2));
        System.out.println("---");
        String string4 = "TeST";
        System.out.println("string4 : " + string4);
        System.out.println("string1.equalsIgnoreCase(string4) : " + string1.equalsIgnoreCase(string4));
        System.out.println("---");
        System.out.println("string1.indexOf('s') : " + string1.indexOf('s'));
        System.out.println("string1.indexOf('s', 3) : " + string1.indexOf('s', 3));
        System.out.println("string1.indexOf(\"est\") : " + string1.indexOf("est"));
        System.out.println("string2.indexOf(\"est\", 2) : " + string2.indexOf("est", 2));
        System.out.println("---");
        System.out.println("string1.replace('e', 'a') : " + string1.replace('e', 'a'));
        System.out.println("---");
        System.out.println("string1.subString(1) : " + string1.substring(1));
        System.out.println("string1.subString(1, 3) : " + string1.substring(1, 3));
        System.out.println("---");
        System.out.println("String.valueOf(true) : " + String.valueOf(true));
        System.out.println("String.valueOf('A') : " + String.valueOf('A'));
        System.out.println("String.valueOf(new char[] { 'a', 's' }) : " + String.valueOf(new char[] { 'a', 's' }));
        System.out.println("String.valueOf(1.2) : " + String.valueOf(1.2));
        System.out.println("String.valueOf(1.2f) : " + String.valueOf(1.2f));
        System.out.println("String.valueOf(1) : " + String.valueOf(1));
        System.out.println("String.valueOf(1L) : " + String.valueOf(1L));
        System.out.println("String.valueOf(new Object()) : " + String.valueOf(new Object()));
    }
    
}
