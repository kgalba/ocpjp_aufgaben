/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150603.strings.s2;

import java.util.Random;

/**
 * Task: aufgabe_String_Namen.txt
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class String2 {

    /**
     * Generates a name out of specified syllables and a syllable count.
     *
     * @param syllables an array of syllables
     * @param syllableCount the syllable count of the wanted name
     * @return the name
     */
    public static String generateName(String[] syllables, int syllableCount) {
        StringBuilder name = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < syllableCount; i++) {
            String syllable = syllables[random.nextInt(syllables.length)];
            name.append(syllable);
        }
        return name.toString();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String[] syllables = {
            "car", "sten", "pet", "schel", "ja", "va", "stan", "dard", "and", "en", "ter", "pri", "se", "edi", "tion", "pro", "gram", "mer"
        };
        Random random = new Random();
        for (int i = 0; i < 50; i++) {
            String name = generateName(syllables, random.nextInt(5)+ 2);
            char[] characters = name.toCharArray();
            characters[0] = Character.toUpperCase(characters[0]);
            name = new String(characters);
            System.out.println(name);
        }
    }

}
