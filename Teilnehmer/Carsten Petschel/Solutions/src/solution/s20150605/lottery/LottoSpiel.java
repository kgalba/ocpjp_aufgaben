/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150605.lottery;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Random;

/**
 * Task: aufgabeLotto.txt
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class LottoSpiel {

    private int anzahlKugel;
    private int anzahlKugelGesamt;
    private Random random;
    private int[] drawing;

    /**
     * Constructs a LottoSpiel with 0 balls.
     */
    public LottoSpiel() {
        super();
        anzahlKugel = 0;
        anzahlKugelGesamt = 0;
        random = new Random();
        drawing = new int[0];
    }

    /**
     * Constructs a LottoSpiel with a specified number of balls and drawing
     * size.
     *
     * @param anzahlKugel the number of balls
     * @param anzahlKugelGesamt the drawing size
     */
    public LottoSpiel(final int anzahlKugel, final int anzahlKugelGesamt) {
        this();
        if (anzahlKugel < 0 || anzahlKugelGesamt < 0) {
            String message = "Integer values for anzahlKugel and anzahlKugelGesamt should be positive.";
            throw new IllegalArgumentException(message);
        }
        this.anzahlKugel = anzahlKugel;
        this.anzahlKugelGesamt = anzahlKugelGesamt;
        drawing = new int[anzahlKugel];
    }

    /**
     * Silumates a drawing.
     */
    public void ziehen() {
        for (int i = 0; i < anzahlKugel; i++) {
            drawing[i] = 0;
        }
        for (int i = 0; i < anzahlKugel; i++) {
            int number = -1;
            do {
                number = random.nextInt(anzahlKugelGesamt) + 1;
            } while (Arrays.binarySearch(drawing, number) >= 0);
            drawing[i] = number;
        }
        Arrays.sort(drawing);
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Spiel ").append(anzahlKugel).append(" aus ").append(anzahlKugelGesamt).append(". ").append(Arrays.toString(drawing));
        return stringBuilder.toString();
    }

    /**
     * Returns the number of balls.
     *
     * @return the number of balls
     */
    public int getAnzahlKugel() {
        return anzahlKugel;
    }

    /**
     * Returns the drawing size.
     *
     * @return the drawing size
     */
    public int getAnzahlKugelGesamt() {
        return anzahlKugelGesamt;
    }

    /**
     * Returns the drawing result.
     *
     * @return the drawing result
     */
    public int[] getDrawing() {
        return drawing;
    }

    /**
     * Check, if a lottery tip has won something, and print the result.
     *
     * @param lottoTipp the lottery tip
     */
    public void vergleichen(LottoTipp lottoTipp) {
        int hits = 0;
        int[] tip = lottoTipp.getTip();
        for (int t : tip) {
            if (Arrays.binarySearch(drawing, t) >= 0) {
                ++hits;
            }
        }
        System.out.print(hits);
        System.out.print(" richtige: ");
        BigDecimal profit = new BigDecimal("0");
        if (hits > 0) {
            profit = profit.add(new BigDecimal(10).pow(hits - 1));
        }
        System.out.print(profit);
        System.out.println(" Euro");
    }

}
