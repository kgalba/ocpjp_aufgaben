/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150605.lottery;

import java.util.Arrays;

/**
 * Task: aufgabeLotto.txt
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class LottoTipp {

    private LottoSpiel lottoSpiel;

    /**
     * Constructs a LottoTipp.
     */
    public LottoTipp() {
        super();
        lottoSpiel = new LottoSpiel();
    }

    /**
     * Constructs a LottoTipp with a specified number of balls and drawing size.
     *
     * @param anzahlKugel the number of balls
     * @param anzahlKugelGesamt the drawing size
     */
    public LottoTipp(final int anzahlKugel, final int anzahlKugelGesamt) {
        this();
        if (anzahlKugel < 0 || anzahlKugelGesamt < 0) {
            String message = "Integer values for anzahlKugel and anzahlKugelGesamt should be positive.";
            throw new IllegalArgumentException(message);
        }
        lottoSpiel = new LottoSpiel(anzahlKugel, anzahlKugelGesamt);
    }

    /**
     * Simulates a drawing.
     */
    public void abgeben() {
        lottoSpiel.ziehen();
    }

    @Override
    public String toString() {
        int[] drawing = lottoSpiel.getDrawing();
        int draws = lottoSpiel.getAnzahlKugel();
        int balls = lottoSpiel.getAnzahlKugelGesamt();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Tipp ").append(draws).append(" aus ").append(balls).append(". ").append(Arrays.toString(drawing));
        return stringBuilder.toString();
    }

    public int[] getTip() {
        return lottoSpiel.getDrawing();
    }
}
