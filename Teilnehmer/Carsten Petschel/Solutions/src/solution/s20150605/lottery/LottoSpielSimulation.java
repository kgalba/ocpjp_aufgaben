/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150605.lottery;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Arrays;
import java.util.Locale;

/**
 * Task: aufgabeLotto.txt
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class LottoSpielSimulation {

    /**
     * Simulate a couple of games.
     *
     * @param costsPerRound costs for each round
     * @param rounds number of rounds
     * @param drawingSize the drawing size
     * @param balls the number of balls in the game
     * @return the profit after a couple of games
     */
    public static BigDecimal simulateForProfit(BigDecimal costsPerRound, Integer rounds, Integer drawingSize, Integer balls) {
        if (rounds < 0 || drawingSize < 0 || balls < 0) {
            String message = "Integer values for rounds, drawingSize and balls should be positive.";
            throw new IllegalArgumentException(message);
        }
        BigDecimal profit = new BigDecimal("0");
        LottoTipp lottoTipp = new LottoTipp(drawingSize, balls);
        lottoTipp.abgeben();
        int[] tip = lottoTipp.getTip();
        for (int i = 0; i < rounds; i++) {
            profit = profit.subtract(costsPerRound);
            LottoSpiel lottoSpiel = new LottoSpiel(drawingSize, balls);
            lottoSpiel.ziehen();
            int[] drawing = lottoSpiel.getDrawing();
            int hits = 0;
            for (int t : tip) {
                if (Arrays.binarySearch(drawing, t) >= 0) {
                    ++hits;
                }
            }
            if (hits > 0) {
                profit = profit.add(new BigDecimal(10).pow(hits - 1));
            }
        }
        return profit;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int anzahlKugel = 7;
        int anzahlKugelGesamt = 49;
        LottoSpiel lotto = new LottoSpiel(anzahlKugel, anzahlKugelGesamt);
        lotto.ziehen();
        System.out.println(lotto);
        System.out.println("---");
        LottoTipp tipp = new LottoTipp(anzahlKugel, anzahlKugelGesamt);
        tipp.abgeben();
        System.out.println(tipp);
        System.out.println("---");
        lotto.vergleichen(tipp);
        System.out.println("--- bonus ---");
        BigDecimal costPerTip = new BigDecimal("1");
        System.out.println("Kosten pro Tipp : " + costPerTip);
        int rounds = 100_000;
        NumberFormat numberFormat = NumberFormat.getInstance(Locale.GERMAN);
        System.out.println("Anzahl Spiele : " + numberFormat.format(rounds));
        BigDecimal profit = simulateForProfit(costPerTip, rounds, anzahlKugel, anzahlKugelGesamt);
        System.out.println("Gesamtgewinn : " + numberFormat.format(profit));
    }

}
