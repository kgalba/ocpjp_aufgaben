/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150610.comparables;

/**
 * This class represents a person, with forename, surname and year of birth.
 *
 * @author Carsten Petschel &lt;carsten-petschel@t-online.de&gt
 */
public class Person implements Comparable<Person> {

    private String forename;
    private String surname;
    private int yearOfBirth;

    /**
     * Creates a Person with no name, and the year of birth set to 0.
     */
    public Person() {
        super();
    }

    /**
     * Create a Person with the specified name information, and year of birth.
     *
     * @param forename the forename
     * @param surname the surname
     * @param yearOfBirth the year of birth
     */
    public Person(String forename, String surname, int yearOfBirth) {
        this();
        this.forename = forename;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
    }

    /**
     * Returns the forename.
     *
     * @return the forename
     */
    public String getForename() {
        return forename;
    }

    /**
     * Set the forename to the specified value.
     *
     * @param forename the forename
     */
    public void setForename(String forename) {
        this.forename = forename;
    }

    /**
     * Return the surname.
     *
     * @return the surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Set the surname to the specified value.
     *
     * @param surname the surname
     */
    public void setSurname(String surname) {
        this.surname = surname;
    }

    /**
     * Return the year of birth.
     *
     * @return the year of birth
     */
    public int getYearOfBirth() {
        return yearOfBirth;
    }

    /**
     * Set the year of birth to the specified value.
     *
     * @param yearOfBirth the year of birth
     */
    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    /**
     * Compares this Person object to another Person object.
     *
     * @param o the object to be compared with
     * @return
     */
    @Override
    public int compareTo(Person o) {
        if (yearOfBirth == o.yearOfBirth) {
            int compareSurnameResult = compareSurnameAscending(o);
            if (compareSurnameResult == 0) {
                return compareForenameAscending(o);
            } else {
                return compareSurnameResult;
            }
        } else {
            return yearOfBirth - o.yearOfBirth;
        }
    }

    /**
     * Compare the surname of this Person object with the surname of the
     * specified Person object in ascending order.
     *
     * @param o the Person object to be compared with
     * @return the compare result
     */
    public int compareSurnameAscending(Person o) {
        if (surname == null && o.surname == null) {
            return 0;
        } else if (surname == null) {
            return -1;
        } else if (o.surname == null) {
            return 1;
        } else {
            return surname.compareTo(o.surname);
        }
    }

    /**
     * Compare the forename of this Person object with the forename of the
     * specified Person object in ascending order.
     *
     * @param o the Person object to be compared with
     * @return the compare result
     */
    public int compareForenameAscending(Person o) {
        if (forename == null && o.forename == null) {
            return 0;
        } else if (forename == null) {
            return -1;
        } else if (o.forename == null) {
            return 1;
        } else {
            return forename.compareTo(o.forename);
        }
    }

    /**
     * Returns a String representation of this Person object.
     *
     * @return a String representation
     */
    @Override
    public String toString() {
        return "Person{" + "forename=" + forename + ", surname=" + surname + ", yearOfBirth=" + yearOfBirth + '}';
    }

}
