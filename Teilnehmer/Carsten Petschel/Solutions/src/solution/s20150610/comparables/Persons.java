/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150610.comparables;

import java.util.Arrays;
import java.util.Comparator;

/**
 * Personen haben: Vornamen, Nachnamen, Geburtsjahre. Ein Array mit 3-4 Personen
 * erzeugen. Mit java.util.Arrays.sort sortieren.
 *
 * @author &lt;carsten-petschel@t-online.de&gt;
 */
public class Persons {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Person[] persons = {new Person(), new Person("Anne", "Bäcker", 1967), new Person("Claus", "Dill", 1951), new Person("Claus", "Dill", 1952), new Person("Carsten", "Petschel", 1981), new Person(null, "Germ", 0), new Person("Edwin", "Fang", 1991)};
        Arrays.sort(persons);
        String stringSortedArray = Arrays.toString(persons);
        System.out.println("year, surname and forename in ascending order :");
        System.out.println(stringSortedArray);
        System.out.println("---");
        ComparatorSurnameForenameYearAscending comparatorSurnameForenameYearAscending = new ComparatorSurnameForenameYearAscending();
        Arrays.sort(persons, comparatorSurnameForenameYearAscending);
        String stringSurnameForenameYearAscending = Arrays.toString(persons);
        System.out.println("surname, forename and surname in ascending order :");
        System.out.println(stringSurnameForenameYearAscending);
        System.out.println("---");
        Comparator<Person> comparatorForenameYearSurnameDescending = (Person o1, Person o2) -> {
            int forenameComparisonResult = -1 * o1.compareForenameAscending(o2);
            if (forenameComparisonResult == 0) {
                int yearComparisonResult = -1 * (o1.getYearOfBirth() - o2.getYearOfBirth());
                if (yearComparisonResult == 0) {
                    return -1 * o1.compareSurnameAscending(o2);
                } else {
                    return yearComparisonResult;
                }
            } else {
                return forenameComparisonResult;
            }
        };
        Arrays.sort(persons, comparatorForenameYearSurnameDescending);
        String stringForenameYearSurnameDescending = Arrays.toString(persons);
        System.out.println("forename, year and surname in descending order :");
        System.out.println(stringForenameYearSurnameDescending);
    }

}
