/*
 * Copyright (C) 2015 Carsten Petschel &lt;carsten-petschel@t-online.de&gt;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150610.comparables;

import java.util.Comparator;

/**
 *
 * @author Carsten Petschel &lt;carsten-petschel@t-online.de&gt;
 */
public class ComparatorSurnameForenameYearAscending implements Comparator<Person> {

    @Override
    public int compare(Person o1, Person o2) {
        int surnameComparisonResult = o1.compareSurnameAscending(o2);
        if (surnameComparisonResult == 0) {
            int forenameComparisonResult = o1.compareForenameAscending(o2);
            if (forenameComparisonResult == 0) {
                return o1.getYearOfBirth() - o2.getYearOfBirth();
            } else {
                return forenameComparisonResult;
            }
        } else {
            return surnameComparisonResult;
        }
    }
}
