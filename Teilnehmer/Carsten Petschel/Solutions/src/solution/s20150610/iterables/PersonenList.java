/*
 * Copyright (C) 2015 Carsten Petschel &lt;carsten-petschel@t-online.de&gt;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150610.iterables;

import java.util.Iterator;

/**
 * Ein PersonenList Objekt kann mindestens 10 Personen beinhalten.
 *
 * @author Carsten Petschel &lt;carsten-petschel@t-online.de&gt;
 */
public class PersonenList implements Iterable<Person> {

    private Person[] personen;
    private int maxLength;
    private int length;

    /**
     * Erzeugt ein leeres PersonenList Objekt, welches mit mindestens zehn
     * Person Objekt befüllt werden kann.
     */
    public PersonenList() {
        super();
        this.maxLength = 10;
        this.personen = new Person[this.maxLength];
    }

    /**
     * Fügt ein Person Objekt diesem PersonenList Objekt hinzu.
     *
     * @param person das Person Objekt
     */
    public void add(Person person) {
        if (person == null) {
            String message = "The method argument should not be null.";
            throw new IllegalArgumentException(message);
        }
        if (maxLength == 0) {
            maxLength = 10;
        }
        if (personen == null) {
            personen = new Person[maxLength];
        }
        personen[length] = person;
        ++length;
        if (length == maxLength) {
            Person[] personenAlt = personen;
            maxLength *= 2;
            personen = new Person[maxLength];
            System.arraycopy(personenAlt, 0, personen, 0, length);
        }
    }

    /**
     * Liefert einen Iterator über die in PersonenList referenzierten Person Objekte.
     * @return 
     */
    @Override
    public Iterator<Person> iterator() {
        return new Iterator<Person>() {

            int index = 0;

            @Override
            public boolean hasNext() {
                return index < length;
            }

            @Override
            public Person next() {
                return personen[index++];
            }
        };
    }

}
