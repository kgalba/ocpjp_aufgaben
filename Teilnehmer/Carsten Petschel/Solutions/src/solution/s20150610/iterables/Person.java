/*
 * Copyright (C) 2015 Carsten Petschel &lt;carsten-petschel@t-online.de&gt;
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150610.iterables;

/**
 * Eine Person, mit einem Namen und einem Vornamen.
 *
 * @author Carsten Petschel &lt;carsten-petschel@t-online.de&gt;
 */
public class Person {

    private String vorname;
    private String nachname;

    /**
     * Erzeugt eine Person ohne Vorname oder Nachname.
     */
    public Person() {
        super();
    }

    /**
     * Erzeugt eine Person mit einem spezifischen Vornamen und Nachnamen.
     *
     * @param vorname der Vorname
     * @param nachname der Nachname
     */
    public Person(String vorname, String nachname) {
        this();
        this.vorname = vorname;
        this.nachname = nachname;
    }

    /**
     * Liefert den Vornamen der Person.
     *
     * @return der Vorname
     */
    public String getVorname() {
        return vorname;
    }

    /**
     * Setzt den Vornamen der Person.
     *
     * @param vorname der Vorname
     */
    public void setVorname(String vorname) {
        this.vorname = vorname;
    }

    /**
     * Liefert den Nachnamen der Person.
     *
     * @return der Nachname
     */
    public String getNachname() {
        return nachname;
    }

    /**
     * Setzt den Nachnamen der Person.
     *
     * @param nachname der Nachname
     */
    public void setNachname(String nachname) {
        this.nachname = nachname;
    }

    /**
     * Liefert eine String Repräsentation dieses Person Objekts.
     * @return eine String Repräsentation
     */
    @Override
    public String toString() {
        return "Person : " + vorname + " " + nachname;
    }

    
}
