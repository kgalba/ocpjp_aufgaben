Aufgabe "Arrays 2D / 03"

Definieren Sie bitte in einer neuen Klasse die Methode 'createBorderedArray', die
ein neues 2D-char-Array erzeugt, das die Dimensionen 'dim' X 'dim' hat. 
Das Array ist mit Leerzeichen gefuellt, nur der Rand besteht aus dem Zeichen 'ch'.
Wird ein Array z.B. durch den Aufruf createBorderedArray(5, 'X') erzeugt und 
dann ausgegeben, sieht die Ausgabe etwa so aus: 
    XXXXX
    X   X
    X   X
    X   X
    XXXXX

Testen Sie die Methoden in der main-Methode der Klasse.
