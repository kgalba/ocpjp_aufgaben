/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150601.arrays.a3;

/**
 * Task: aufgabeArrays03_2D.txt
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class Arrays3 {

    /**
     * Create a two-dimensional array of characters, where the values are
     * initialized with ' ' (space) except for the border values, where a
     * special character should be used.
     *
     * @param size the size to be used for the two-dimensional array
     * @param borderCharacter the special character used for the border values
     * @return a two-dimensional array of characters with intended size and
     * border values
     */
    public static char[][] createBorderedArray(int size, char borderCharacter) {
        char[][] values = new char[size][];
        for (int h = 0; h < size; h++) {
            values[h] = new char[size];
            for (int w = 0; w < size; w++) {
                if (h == 0 || h == size - 1 || w == 0 || w == size - 1) {
                    values[h][w] = borderCharacter;
                } else {
                    values[h][w] = ' ';
                }
            }
        }
        return values;
    }

    /**
     * Prints an array of character values.
     *
     * @param values the values to be printed
     */
    public static void printArray(char[][] values) {
        if (values == null) {
            return;
        }
        for (char[] row : values) {
            if (row == null) {
                continue;
            }
            for (char c : row) {
                System.out.print(c);
            }
            System.out.println();
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        char[][] values = createBorderedArray(5, 'X');
        printArray(values);
    }

}
