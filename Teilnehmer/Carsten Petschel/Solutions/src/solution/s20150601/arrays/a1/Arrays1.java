/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150601.arrays.a1;

import java.util.Arrays;
import java.util.Random;

/**
 * Task: aufgabeArrays01.txt
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class Arrays1 {

    /**
     * Creates an array of a bunch of values.
     *
     * @param values the values to be put in an array
     * @return an array containing the values
     */
    public static int[] createArray(int... values) {
        return values;
    }

    /**
     * Prints the content of an integer array.
     *
     * @param values the values to be printed
     */
    public static void printArray(int[] values) {
        String content = Arrays.toString(values);
        System.out.println(content);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] arr = createArray(2, 15, 30);
        printArray(arr);
        System.out.println("---");
        Random random = new Random();
        int[] values = new int[30];
        for (int i = 0; i < values.length; i++) {
            values[i] = random.nextInt(14) + 2;
        }
        printArray(values);
    }

}
