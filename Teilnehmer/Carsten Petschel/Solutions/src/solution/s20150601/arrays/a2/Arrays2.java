/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150601.arrays.a2;

/**
 * Task: aufgabeArrays02_2D.txt
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class Arrays2 {

    /**
     * Create an array with the specified dimension, and initialize every array
     * value to 0.
     *
     * @param height the array height
     * @param width the array width
     * @return a zero filled array with the specified dimension
     */
    public static int[][] createArray(int height, int width) {
        if (height < 0 || width < 0) {
            String message = "The parameters should be positive.";
            throw new IllegalArgumentException(message);
        }
        int[][] values = new int[height][];
        for (int h = 0; h < height; h++) {
            values[h] = new int[width];
            for (int w = 0; w < width; w++) {
                values[h][w] = 0;
            }
        }
        return values;
    }

    /**
     * Print a two-dimensional array of integer values.
     *
     * @param values the values to be printed
     */
    public static void printArray(int[][] values) {
        if (values == null) {
            return;
        }
        for (int[] row : values) {
            if (row == null) {
                continue;
            }
            for (int column = 0; column < row.length; column++) {
                System.out.print(row[column]);
                if (column < row.length - 1) {
                    System.out.print(", ");
                }
            }
            System.out.println();
        }
    }

    /**
     * Fill a two-dimensional array with an intended value.
     *
     * @param values the array to be filled
     * @param fillValue the intended fill value
     */
    public static void fillArray(int[][] values, int fillValue) {
        if (values == null) {
            return;
        }
        for (int[] row : values) {
            if (row == null) {
                continue;
            }
            for (int column = 0; column < row.length; column++) {
                row[column] = fillValue;
            }
        }
    }

    /**
     * Create a two-dimensional array with the intended height, width and fill
     * value.
     *
     * @param height the array height
     * @param width the array width
     * @param fillValue the fill value
     * @return a two-dimensional array with the intended height, width and fill
     * value
     */
    public static int[][] createArray(int height, int width, int fillValue) {
        if (height < 0 || width < 0) {
            String message = "The values for height and width should be positive.";
            throw new IllegalArgumentException(message);
        }
        int[][] values = createArray(height, width);
        fillArray(values, fillValue);
        return values;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[][] values1 = createArray(4, 5);
        printArray(values1);
        System.out.println("---");
        values1[1][1] = 5;
        printArray(values1);
        System.out.println("---");
        fillArray(values1, 2);
        printArray(values1);
        System.out.println("---");
        int[][] values2 = createArray(4, 5, 2);
        printArray(values2);
    }

}
