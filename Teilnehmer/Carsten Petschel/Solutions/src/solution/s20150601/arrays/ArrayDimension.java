/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150601.arrays;

import java.util.Random;

/**
 * How many values are stored in an array with 7 dimensions?
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class ArrayDimension {

    /**
     * Calculate the accumulated length of all values in an array with 7
     * dimensions.
     *
     * @param values the array with 7 dimensions
     * @return the accumulated length of all values in the array
     */
    public static Long calculateArrayLength(Object[][][][][][][] values) {
        if (values == null) {
            return null;
        }
        long size = 0;
        for (Object[][][][][][] o1 : values) {
            if (o1 == null) {
                continue;
            }
            for (Object[][][][][] o2 : o1) {
                if (o2 == null) {
                    continue;
                }
                for (Object[][][][] o3 : o2) {
                    if (o3 == null) {
                        continue;
                    }
                    for (Object[][][] o4 : o3) {
                        if (o4 == null) {
                            continue;
                        }
                        for (Object[][] o5 : o4) {
                            if (o5 == null) {
                                continue;
                            }
                            for (Object[] o6 : o5) {
                                if (o6 == null) {
                                    continue;
                                }
                                size += o6.length;
                            }
                        }
                    }
                }
            }
        }
        return size;
    }

    /**
     * Creates a random sized object array with seven dimensions.
     *
     * @param maxSize the maximum length a single array can have
     * @return a random sized object array
     */
    public static Object[][][][][][][] createRandomObjectArray(int maxSize)
    {
        if (maxSize < 0) {
            String message = "The method parameter should be positive.";
            throw new IllegalArgumentException(message);
        }
        Random random = new Random();
        int size = random.nextInt(maxSize) + 1;
        Object[][][][][][][] values = new Object[size][][][][][][];
        for (int i1 = 0; i1 < values.length; i1++) {
            size = random.nextInt(maxSize) + 1;
            values[i1] = new Object[size][][][][][];
            for (int i2 = 0; i2 < values[i1].length; i2++) {
                size = random.nextInt(maxSize) + 1;
                values[i1][i2] = new Object[size][][][][];
                for (int i3 = 0; i3 < values[i1][i2].length; i3++) {
                    size = random.nextInt(maxSize) + 1;
                    values[i1][i2][i3] = new Object[size][][][];
                    for (int i4 = 0; i4 < values[i1][i2][i3].length; i4++) {
                        size = random.nextInt(maxSize) + 1;
                        values[i1][i2][i3][i4] = new Object[size][][];
                        for (int i5 = 0; i5 < values[i1][i2][i3][i4].length; i5++) {
                            size = random.nextInt(maxSize) + 1;
                            values[i1][i2][i3][i4][i5] = new Object[size][];
                            for (int i6 = 0; i6 < values[i1][i2][i3][i4][i5].length; i6++) {
                                size = random.nextInt(maxSize) + 1;
                                values[i1][i2][i3][i4][i5][i6] = new Object[size];
                                for (int i7 = 0; i7 < values[i1][i2][i3][i4][i5][i6].length; i7++) {
                                    values[i1][i2][i3][i4][i5][i6][i7] = new Object();
                                }
                            }
                        }
                    }
                }
            }
        }
        return values;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Object[][][][][][][] values = createRandomObjectArray(7);
        Long length = calculateArrayLength(values);
        System.out.println(length);
    }

}
