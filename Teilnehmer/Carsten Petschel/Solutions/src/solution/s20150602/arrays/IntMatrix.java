/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150602.arrays;

import java.util.Random;

/**
 * Defines a member variable, which is an int matrix.
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class IntMatrix {

    int[][] values;

    /**
     * Creates an IntMatrix object width a height and width of 0.
     */
    public IntMatrix() {
        super();
        values = new int[0][];
    }

    /**
     * Creates an IntMatrix object with the specified height and width.
     *
     * @param height the height of the value matrix
     * @param width the width oft the value matrix
     */
    public IntMatrix(int height, int width) {
        this();
        if (height < 0 || width < 0) {
            String message = "Height and width should not be negative.";
            throw new IllegalArgumentException(message);
        }
        values = new int[height][];
        for (int i = 0; i < values.length; i++) {
            values[i] = new int[width];
        }
    }

    /**
     * Creates an IntMatrix object with the specified height, width and a
     * default value set.
     *
     * @param height the height of the value matrix
     * @param width the width of the value matrix
     * @param defaultValue the default value of the matrix
     */
    public IntMatrix(int height, int width, int defaultValue) {
        this(height, width);
        for (int[] row : values) {
            for (int j = 0; j < row.length; j++) {
                row[j] = defaultValue;
            }
        }
    }

    /**
     * Prints the array of values.
     */
    public void printArray() {
        System.out.println(toString());
    }

    /**
     * Returns a string representation of the value matrix.
     *
     * @return a string
     */
    @Override
    public String toString() {
        int valueWidth = 1;
        for (int i = 0; i < values.length; i++) {
            if (values[i] == null) {
                continue;
            }
            for (int j = 0; j < values[i].length; j++) {
                int value = values[i][j];
                int currentValueWidth = 0;
                while (value > 0) {
                    ++currentValueWidth;
                    value /= 10;
                }
                if (currentValueWidth > valueWidth) {
                    valueWidth = currentValueWidth;
                }
            }
        }
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < values.length; i++) {
            if (values[i] == null) {
                continue;
            }
            for (int j = 0; j < values[i].length; j++) {
                String value = String.format("%" + valueWidth + "d", values[i][j]);
                stringBuilder.append(value);
                if (j < values[i].length - 1) {
                    stringBuilder.append(", ");
                }
            }
            if (i < values.length - 1) {
                stringBuilder.append(System.lineSeparator());
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Returns the value of the matrix at the specified position.
     *
     * @param row the row index, starting with 1
     * @param column the column index, starting with 1
     * @return the matrix value at the specified position
     * @throws IndexOutOfBoundsException exception is thrown, if the specified
     * row or column lies outside of the matrix bounds
     */
    public int get(int row, int column) throws IndexOutOfBoundsException {
        if (row < 1 || column < 1) {
            String message = "Values for row and column should not be lower than 1.";
            throw new IllegalArgumentException(message);
        }
        if (row - 1 > values.length || (values[0] != null && column - 1 > values[0].length)) {
            String message = "The specified row and column parameter lies outside of matrix bounds.";
            throw new IndexOutOfBoundsException(message);
        }
        return values[row - 1][column - 1];
    }

    /**
     * Returns an IntMatrix object with the specified height and width, randomly
     * filled with values from 0 to a maximum default value.
     *
     * @param height the height of the value matrix
     * @param width the width of the value matrix
     * @param maxDefaultValue the maximum default value
     * @return the newly created IntMatrix object
     */
    public static IntMatrix getRandomMatrix(int height, int width, int maxDefaultValue) {
        if (height < 0 || width < 0 || maxDefaultValue < 0) {
            String message = "All method paramters should be positive.";
            throw new IllegalArgumentException(message);
        }
        Random random = new Random();
        IntMatrix intMatrix = new IntMatrix(height, width);
        for (int[] row : intMatrix.values) {
            for (int i = 0; i < row.length; i++) {
                row[i] = random.nextInt(maxDefaultValue + 1);
            }
        }
        return intMatrix;
    }

    /**
     * Returns a hash code for this IntMatrix instance.
     *
     * @return the hash code
     */
    @Override
    public int hashCode() {
        int hash = 0;
        for (int i = 0; i < values.length; i++) {
            for (int j = 0; j < values[i].length; j++) {
                hash += values[i][j] * i * j;
            }
        }
        return hash;
    }

    /**
     * Test if another object is equal to this IntMatrix object.
     *
     * @param obj the object to be tested of equality
     * @return true, if the other object is equal to this IntMatrix object,
     * otherwise false
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final IntMatrix other = (IntMatrix) obj;
        if (values.length != other.values.length) {
            return false;
        }
        for (int i = 0; i < values.length; i++) {
            if (values[i].length != other.values[i].length) {
                return false;
            }
            for (int j = 0; j < values[i].length; j++) {
                if (values[i][j] != other.values[i][j]) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Transposes the value matrix of this IntMatrix object.
     *
     * It is necessary, that the value matrix has a square form (no arrays with
     * different sizes in the second dimension).
     */
    public void transpose() {
        if (!hasSquareForm(this)) {
            String message = "The value matrix needs to be in a square form.";
            throw new UnsupportedOperationException(message);
        }
        int valuesHeight = values.length;
        int valuesWidth = values[0].length;
        int[][] transposedValues = new int[valuesWidth][];
        for (int i = 0; i < transposedValues.length; i++) {
            transposedValues[i] = new int[valuesHeight];
            for (int j = 0; j < transposedValues[i].length; j++) {
                transposedValues[i][j] = values[j][i];
            }
        }
        values = transposedValues;
    }

    /**
     * Tests, if an IntMatrix objects value matrix has a square form. All arrays
     * of the second dimensions needs to be equal in length.
     *
     * @param intMatrix the IntMatrix object to be tested
     * @return true, if value matrix has square form, otherwise false
     */
    private boolean hasSquareForm(IntMatrix intMatrix) {
        if (intMatrix == null) {
            String message = "An IntMatrix object is required.";
            throw new IllegalArgumentException(message);
        }
        int valuesWidth = intMatrix.values[0].length;
        for (int[] row : intMatrix.values) {
            if (valuesWidth != row.length) {
                return false;
            }
        }
        return true;
    }

    /**
     * Adds the specified IntMatrix to the current instance value matrix.
     *
     * It is necessary, that the specfied IntMatrix value matrix has the same
     * height and width dimensions, as the current instance value matrix.
     *
     * @param intMatrix the IntMatrix object whose value matrix should be added
     */
    public void add(IntMatrix intMatrix) {
        boolean sameSize = true;
        if (intMatrix.values.length == values.length) {
            for (int i = 0; i < values.length; i++) {
                if (intMatrix.values[i].length != values[i].length) {
                    sameSize = false;
                }
            }
        } else {
            sameSize = false;
        }
        if (!sameSize) {
            String message = "The specfied IntMatrix value matrix should have the same height and width dimensions, as the current instance value matrix.";
            throw new IllegalArgumentException(message);
        }
        for (int i = 0; i < values.length; i++) {
            for (int j = 0; j < values[i].length; j++) {
                values[i][j] += intMatrix.values[i][j];
            }
        }
    }

    /**
     * Multiplies the specified IntMatrix with the current instance.
     *
     * It is necessary, that both IntMatrix objects have square form, and that
     * the width of the current instance value matrix equals the height of the
     * specified IntMatrix object value matrix.
     *
     * @param intMatrix the IntMatrix object to be multiplied with the current
     * instance
     */
    public void multiply(IntMatrix intMatrix) {
        if (!hasSquareForm(this) || !hasSquareForm(intMatrix)) {
            String message = "Both value matrices need to be in square form.";
            throw new IllegalArgumentException(message);
        }
        if (values[0].length != intMatrix.values.length) {
            String message = "It's necessary that the width of the current instance value matrix equals the height of the specified IntMatrix object value matrix.";
            throw new IllegalArgumentException(message);
        }
        int height = values.length;
        int width = intMatrix.values[0].length;
        int[][] multipliedValues = new int[height][];
        for (int i = 0; i < multipliedValues.length; i++) {
            multipliedValues[i] = new int[width];
        }
        for (int i = 0; i < multipliedValues.length; i++) {
            for (int j = 0; j < multipliedValues[0].length; j++) {
                for (int k = 0; k < intMatrix.values.length; k++) {
                    multipliedValues[i][j] += values[i][k] * intMatrix.values[k][j];
                }
            }
        }
        values = multipliedValues;
    }
}
