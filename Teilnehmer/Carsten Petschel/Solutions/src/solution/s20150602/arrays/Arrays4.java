/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package solution.s20150602.arrays;

import java.util.Random;

/**
 * Task: aufgabeArrays04_IntMatrix.txt
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class Arrays4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        IntMatrix m1 = new IntMatrix(2, 3);
        IntMatrix m2 = new IntMatrix(5, 3, 100);
        System.out.println("m2 :");
        m2.printArray();
        System.out.println("---");
        System.out.println("m1 :");
        System.out.println(m1);
        System.out.println("---");
        int i = m1.get(1, 2);
        System.out.println("m1[1][2] : " + i);
        System.out.println("---");
        IntMatrix m3 = IntMatrix.getRandomMatrix(2, 2, 2);
        System.out.println("m3 :");
        m3.printArray();
        System.out.println("---");
        System.out.println("m1.equals(m2) : " + m1.equals(m2));
        System.out.println("--- bonus ---");
        m3.transpose();
        System.out.println("m3.transpose() : ");
        System.out.println(m3);
        System.out.println("m4 :");
        IntMatrix m4 = IntMatrix.getRandomMatrix(2, 2, 2);
        System.out.println(m4);
        System.out.println("m3.add(m4) :");
        m3.add(m4);
        System.out.println(m3);
        System.out.println("m3.multiply(m4) :");
        m3.multiply(m4);
        System.out.println(m3);
        System.out.println("--- bonus ---");
        System.out.print("IntMatrix.getRandomMatrix(10_000_000, 10_000_000, 10_000_000) : ");
        boolean success = true;
        try {
            IntMatrix.getRandomMatrix(10_000_000, 10_000_000, 10_000_000);
        } catch (OutOfMemoryError error) {
            System.out.println(error);
            success = false;
        } finally {
            if (success) {
                System.out.println("Success!");
            }
        }
        System.out.println("--- bonus ---");
        int maxSize = 5;
        Random random = new Random();
        int[][][] values = new int[random.nextInt(maxSize) + 2][][];
        for (int j = 0; j < values.length; j++) {
            values[j] = new int[random.nextInt(maxSize) + 2][];
            int length = random.nextInt(maxSize) + 2;
            for (int k = 0; k < values[j].length; k++) {
                values[j][k] = new int[length];
            }
        }
        System.out.println("[");
        for (int a = 0; a < values.length; a++) {
            System.out.println("[");
            for (int h = 0; h < values[a].length; h++) {
                for (int w = 0; w < values[a][h].length; w++) {
                    System.out.print(values[a][h][w]);
                    if (w < values[a][h].length - 1) {
                        System.out.print(", ");
                    }
                }
                System.out.println();
            }
            System.out.print("]");
            if (a < values.length - 1) {
                System.out.println(",");
            } else {
                System.out.println();
            }
        }
        System.out.println("]");
    }

}
