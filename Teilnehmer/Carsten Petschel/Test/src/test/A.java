/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package test;

/**
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class A {
  
    int a = 9;
    
    static {
        int[] a = new int[2];
        a[4] = 5;
    }
    
    public A() {
        super();
    }
    
    private final void test() {
        System.out.println("A.test()");
    }
    
    public void print() {
        System.out.println("A");
    }
    
}
