/*
 * Copyright (C) 2015 Carsten Petschel <carsten-petschel@t-online.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package test;

import java.util.Random;

/**
 *
 * @author Carsten Petschel <carsten-petschel@t-online.de>
 */
public class Test {

    public void overload(int a, int b) {
        System.out.println("overload(int a, int b)");
    }

    public void overload(int a, double b) {
        System.out.println("overload(int a, double b)");
    }

    public void overload(double a, double b) {
        System.out.println("overload(double a, double b)");
    }

    public static void testMethodOverloading() {
        Test test = new Test();
        test.overload(1, 2);
        test.overload(1, 2f);
        test.overload(1f, 2f);
        test.overload(1f, 1);
    }

    public static void testConstructors() {
        A a = new B();
        a.print();
    }

    public static void testString() {
        String text = "Text";
        int i = 1337;
        text += " is here.";
        text += String.valueOf(i);
        System.out.println(text);
    }

    public static void testArray() {
        int[][] integers = new int[3][];
        integers[0] = new int[2];
//        integers[1] = 5; // won't compile
    }

    public static void testWrapping() {
        Integer i = 1;
        Integer j = i;
        System.out.println(i == j);
        j += 1;
        System.out.println(i == j);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
//        testMethodOverloading();
//        testConstructors();
//        testString();
        testWrapping();
    }

}
