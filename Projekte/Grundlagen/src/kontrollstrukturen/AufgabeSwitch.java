package kontrollstrukturen;

/**
 * @author apatrin
 */
public class AufgabeSwitch {

    public static void main(String[] args) {

        char ch = 'b';

        switch (ch) {
            case 'a':
            case 'e':
            case 'o':
            case 'u':
            case 'i':
                System.out.println(ch + " ist ein Vokal");
                break;
            default:
                System.out.println(ch + " ist ein Konsonant");
        }

    }

}
