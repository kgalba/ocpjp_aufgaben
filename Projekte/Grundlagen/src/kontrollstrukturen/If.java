package kontrollstrukturen;

/**
 * @author apatrin
 */
public class If {

    public static void main(String[] args) {
        
        boolean var = true;
        
        if( var )
            System.out.println("var ist true");
        
        
        if( var | !var && 3*5==22 ) {
            System.out.println("Heute ist Sonntag");
        }
        
        int x = 3;
//        if( x ) {} //Compilerfehler!
        
        System.out.println("------------------------------");
        if( x==1 ) {
            System.out.println("Mo");
        } else if(x==2) {
            System.out.println("Di");
        }

        System.out.println("------------------------------");
        if( x==1 ) {
            System.out.println("Mo");
        } else if(x==2) {
            System.out.println("Di");
        } else if(x==3) {
            System.out.println("Mi");
        } //...

          
        System.out.println("------------------------------");
        if( x==1 ) {
            System.out.println("Mo");
        } else {
            System.out.println("Etwas andere als Mo");
        }
        
        
        System.out.println("-------------");
        System.out.println("---- exam ---");
        System.out.println("-------------");
        
        x = 22;
//        if( x = 23 ) {} //Compilerfehler!
        
        boolean b = false;
        if( b = true ) {    //Achtung! Zuweisung!!!
            System.out.println("b ist true");
        }
        
        
        
        if(!b)
            System.out.println("Zeile A. Bedingt");
            System.out.println("Zeile B. Unbedingt");
        
            
            
//        if(b) {
//            
//        } else if {  //Compilerfehler!
//            
//        }
        
    } //end of main
    
}
