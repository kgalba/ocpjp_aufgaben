package kontrollstrukturen;

/**
 * @author apatrin
 */
public class Switch {

    public static void main(String[] args) {

        //byte, short, int, char (auch die entsprechenden Wrapper-Klassen)
        //enum - Klassen
        //ab Java 7: String
        int var = 3;

        switch (var) {
            case 1:
        }

        //-----------------------
        final int red = 1;

        switch (var) {
            case red:
        }

        //----------------------
        final int maerz;
        maerz = 3;

        switch (var) {
//            case maerz: //Compilerfehler
        }

        //---------------------
        //---------------------
        //---------------------
        //1. Passenden Zweig finden
        //2. Alle ab dem gefundenen Zweig bis zum 
        //   Ende der switch (oder bis zum break) ausführen
        var = 900;
        switch (var) {
            case 3:
                System.out.println("Drei!");
            case 1:
                System.out.println("die Einz");
            default: //Nicht zu empfehlen. Lieber am Ende
                System.out.println("Alles andere mit default");
            case -20:
                System.out.println("Ich will nicht negativ sein");
                break;
            case 2:
                System.out.println("2 ist toll");
        }

    } //end of main

}
