package kontrollstrukturen;

//import java.util.PriorityQueue;

/**
 * @author apatrin
 */
public class WhileDoWhile {
    
    public static void main(String[] args) {
        
//        PriorityQueue pQueue = new PriorityQueue();
//        pQueue.add("a");
//        pQueue.add("b");
//        pQueue.add("c");
//        
//        while( !pQueue.isEmpty() ) {
//            System.out.println(pQueue.poll());
//        }

        for (int i = 0; i < 10; i++) {
            System.out.print(i + " ");
        }
        System.out.println();
        
        
        int i=0;
        while(i < 10) {
            System.out.print(i++ + " ");
        }
        System.out.println();
        
        
        i = 0;
        do {
            System.out.print(i + " ");
        } while(++i<10);
        
        System.out.println();
        
    }

}
