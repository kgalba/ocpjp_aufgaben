package kontrollstrukturen;

/**
 * @author apatrin
 */
public class Labels {

    public static void main(String[] args) {
        
        tom:
        for(;;){
            break tom;
        }
        
        //---------------------------------------
        tom:
        for (int i = 0; i < 10; i++) {
            if(i==3)
                break tom;
            
            System.out.print(i + " ");
        }
        System.out.println();
        
        //---------------------------------------
        for (int i = 0; i < 10; i++) {
            if(i==3)
                break;
            System.out.print(i + " ");            
        }
        System.out.println();

        //---------------------------------------
        for (int i = 0; i < 10; i++) {
            if( i==3 )
                continue;
            
            System.out.print(i + " ");
        }
        System.out.println("");
        
        //---------------------------------------
        jerry:
        for (int i = 0; i < 10; i++) {
            if( i==3 )
                continue jerry;
            
            System.out.print(i + " ");
        }
        System.out.println("");
        
        
        //--------------------------------------
        System.out.println("*****************************");
        
        tom:
        for (int i = 0; i < 10; i++) {

            int x = 0;

            jerry:
            while(x++<3) {
                if(x%2==0) {
//                    break tom;
                    break;
                }
                
            }
            System.out.print(i + " ");
        }
        System.out.println();
        
    }
    
}
