package kontrollstrukturen;

/**
 * @author apatrin
 */
public class SchleifenSchachteln {

    public static void main(String[] args) {
        
        int x = 0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) { 
                x++;
                System.out.print(x + " ");
            }
        }
        System.out.println();
        
    } //end of main
    
}
