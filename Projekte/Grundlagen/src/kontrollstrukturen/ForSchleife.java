package kontrollstrukturen;

/**
 * @author apatrin
 */
public class ForSchleife {

    public static void main(String[] args) {
        
//        for(;;) 
//            System.out.println("endlos");

//        boolean var = true;
//        for(;var;) 
//            System.out.println("endlos");
        
        //1. Init-Bereich auswerten
        //2. Ausführungskriterium auswerten 
        //  - bei false Schleife beenden
        //  - bei true Schleifendurchlauf ausführen
        
        //3. Inkrement-Bereich auswerten
        //4. Ausführungskriterium auswerten 
        //  - bei false Schleife beenden
        //  - bei true Schleifendurchlauf ausführen
        
        for (int i = 0; i > 100; i++) {
            System.out.println("Durchlauf " + i);
        }
        
        for(int i=1, j=2, k=33 ; i<100 ; System.out.println("hallo"), i++ ){
            System.out.println("Durchlauf " + i);
        }
        
        System.out.println("end of main");
    }
    
}
