package methoden;

/**
 * @author apatrin
 */
public class Ueberladen {

    //Signatur: Name + Liste der Parametertypen
    static void print(int x) { 
        System.out.println("x = " + x); 
    }
    
    static int print(int x, int y) { 
        System.out.println(x + " " + y); 
        return -1;
    }
    
    public static void main(String[] args) {
        
        print(12);
        print(12, 13);
        
        
    }
    
}
