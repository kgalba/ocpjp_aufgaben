package methoden;

/**
 * @author apatrin
 */
public class StackAndStatic {
    
    static int VAR = 33;
    static final int CONST = 330;

    public static void main(String[] args) {
        
        System.out.println(VAR);
        System.out.println(CONST);
        
        System.out.println(StackAndStatic.VAR);
        System.out.println(StackAndStatic.CONST);
     
        {
            int blockVar = 3;
        }
        //System.out.println(blockVar); //Compilerfehler!
        
    }
    
}
