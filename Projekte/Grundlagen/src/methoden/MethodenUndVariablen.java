package methoden;

/**
 * @author apatrin
 */
public class MethodenUndVariablen {

    static void modify(int x) {
        x = 22;
    }
    
    public static void main(String[] args) {
        
        int x = 3;
        
        modify(x);
        
        System.out.println("x = " + x);// 22, 22, ?, 22
        
    }
}
