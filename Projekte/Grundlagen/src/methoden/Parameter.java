package methoden;

/**
 * @author apatrin
 */
public class Parameter {
    
    static int add(int a, int b) {
        int erg = a + b;
        
        return erg;
    }

    public static void main(String[] args) {
        
        int x = 1; //initialisiert vor dem ersten Lesen
        int y = 2;
        
        int z = x + y;
        
        z = add(x, 2); //int a = x; int b = 2;
        
        System.out.println("z = " + z);
        
        //Achtung!
        byte b1 = 3; 
//        byte b2 = add(1, 2); // byte <- int  : Compilerfehler
        byte b2 = (byte) add(1, 2);
        
    }
    
}
