package methoden;

/**
 * @author apatrin
 */
public class MethodeDefinieren {

   
    public static void main(String[] args) {
    
        sayHello();

        MethodeDefinieren.sayHello();
        
        sayHello();
    }

    static void sayHello() {
        System.out.println("***************");
        System.out.println("***** hallo ***");
        System.out.println("***************");
    }
}
