package operatoren;

/**
 * @author apatrin
 */
public class Arithmetische {

    public static void main(String[] args) {
        
        int x = 1;
        int y = 2;
        int z = x + y;
        
        //--------------------------------------
        // Das Ergebnis einer arithmetischen 
        // Operation ist MINDESTENS vom Typ 'int'
        //--------------------------------------
        
        byte b1 = 1 + 2;  //Ausnahme: implizites Casting
//        byte b2 = b1 + 1;         //Compilerfehler
        byte b2 = (byte) (b1 + 1);  
        
        //---------------------------------------
        //------------- Modulo ------------------
        //---------------------------------------
        System.out.println( 4 % 2 );
        System.out.println( 4 % 3 );
        System.out.println( 3 % 4 ); //3
        
    }
    
}
