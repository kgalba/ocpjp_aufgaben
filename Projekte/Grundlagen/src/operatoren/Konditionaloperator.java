package operatoren;

/**
 * @author apatrin
 */
public class Konditionaloperator {
    
    public static void main(String[] args) {
        
        int x = 3;
        
        int y;
        if(x > 0) {
            y = 1;
        } else {
            y = -1;
        }
        System.out.println("y = " + y);
        
        int z = x > 0 ? 1 : -1;
        System.out.println("z = " + z);

        //---------------------
        x = 5;
        double var = x==3 ? 22.0 : 1;
        
        
    }

}
