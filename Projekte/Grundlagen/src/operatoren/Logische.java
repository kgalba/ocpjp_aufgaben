package operatoren;

/**
 * @author apatrin
 */
public class Logische {

    public static void main(String[] args) {
        
        //nur für boolean-Operanden
        
        // OR:      |           || (Kurzschluß)
        // AND:     &           && (Kurzschluß)
        // XOR:     ^
        // NOT:     !
        
        int x = 1;
        x++; // x = x + 1;
        System.out.println("x = " + x); //2
        
        boolean erg = x == 2 || x++ < 100; // true ODER egal was -> true
        System.out.println("erg = " + erg);
        System.out.println("x = " + x);
        
        //x = 2
        erg = x == 2 | x++ < 100; //true ODER true -> true
        System.out.println("erg = " + erg);
        System.out.println("x = " + x); // x = 3
    
        printLogicTable_XOR();
    } //end of main
    
    static void printLogicTable_XOR() {
        System.out.println("----------------------------------");
        System.out.println("----------------------------------");
        System.out.println( "true ^ true = " + (true ^ true) );
        System.out.println( "true ^ false = " + (true ^ false) );
        System.out.println( "false ^ true = " + (false ^ true) );
        System.out.println( "false ^ false = " + (false ^ false) );
    }
    
}
