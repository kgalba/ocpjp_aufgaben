package operatoren;

/**
 * @author apatrin
 */
public class ZuweisungUndOperation {
    
    public static void main(String[] args) {
        
        //exam: += -= *= /= 
        
        int x = 2;
        x += 33; //x = x + 33;
        
        //----------------------------
        // Inkrement / Dekrement
        //----------------------------
        
        x = 0;
        x++;   //x = x + 1;
        System.out.println("x = " + x); //1
        
        x = 0;
        ++x;
        System.out.println("x = " + x); //1
        
        System.out.println("-------------------------");
        x = 0;
        int y = x++;
        System.out.println("x = " + x); //1
        System.out.println("y = " + y); //0
        
        
        System.out.println("-------------------------");
        x = 0;
        y = ++x;
        System.out.println("x = " + x); //1
        System.out.println("y = " + y); //1
        
        
        System.out.println("--------------------------");
        
        x = 0;
        y = ++x + x++;
        System.out.println("x = " + x);
        System.out.println("y = " + y);
        
        System.out.println("-----------------------------");
        
        x = 0;
        x = x++;
        System.out.println("x = " + x); //0
     
        
        x = 0;
        x = ++x;
        System.out.println("x = " + x); //1
        
    }

}
