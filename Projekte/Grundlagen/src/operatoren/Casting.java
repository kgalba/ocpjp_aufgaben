package operatoren;

/**
 * @author apatrin
 */
public class Casting {

    public static void main(String[] args) {
        
        int x = 1;  //000...00000001
        
        byte b = (byte)x;
        
        x = 128;
        
        b = (byte)x;
        
        System.out.println("b = " + b); //binär: 10000000
        
        //10000000
        System.out.println( "x (binär) = " + Integer.toBinaryString(x) );
        
        float f1 = 12.1f;
        x = (int)f1;
        
        System.out.println("x = " + x);
        
    }
    
}
