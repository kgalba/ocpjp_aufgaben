package datentypen;

/**
 * @author apatrin
 */
public class Identifier {

    public static void main(String[] args) {
        
        //Buchstaben
        int var = 12;
        
        //Währungssysmbole
        int $ = 12; //nicht empfehlenswert
        int € = 12; //nicht empfehlenswert
        
        //Unterstrich
        int _ = 12;  //nicht empfehlenswert
        System.out.println("_ = " + _);
        
        int ___ = 13;  //nicht empfehlenswert
        int ____ = 14; //nicht empfehlenswert
        
        int _var = 44;
        
        //Ziffern, aber nicht vorne
//        int 12x;
        int x12;
        
    } //
    
}
