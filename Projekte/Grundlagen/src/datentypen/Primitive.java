package datentypen;

/**
 * @author apatrin
 */
public class Primitive {

    //Die Version 2.0 der main-Methode :)
    public static void main(String[] args) {
        
        byte    b;  //8 bit
        short   s;  //16 bit
        int     i;  //32 bit
        long    lo; //64 bit
        
        float   f;  //32 bit
        double  db; //64 bit        
                
        //------------------------
        char    ch; //16 bit (nichtnegative ganze Zahl)
        
        //
        boolean bool = true; // false
    }
    
}
