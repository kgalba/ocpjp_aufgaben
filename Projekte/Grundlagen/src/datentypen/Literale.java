package datentypen;

public class Literale {
    
    public static void main(String[] args) {
        
        //***************
        //int-Literale
        
        byte b  = 12;
        short s = 12;
        
        int x   = 12;
        x = +12;
        x = -12;
        
        x = 012; //octal
        x = 0x12; //hexadecimal
        
        //nicht in der Prüfung (ab java 7)
        x = 0b01010101; //binär
        
        //***************
        //long-Literale
//        long lo = 12345678901234; //int-Literal: Compilerfehler
        long lo = 12345678901234L;  //long-Literal
        lo = 12l;   //kleines l sieht aus wie eine 1
        
        //****************
        //nicht in der Prüfung:
        lo = 1_123_456; 
        
        //***************
        //char-Literale:
        char ch = 'a';  //char-Literal
        ch = 97;        //int-Literal
        
        System.out.println("ch = " + ch);
        
        ch = '\ucafe'; //char-Literal. ein Zeichen hexadecimal

        System.out.println("ch = " + ch);
        
        //******************
        //double-Literale
        
        double d = 12.0;
        d = 12.;
        d = 0.12;
        d = .12;
        
        System.out.println("d = " + d);
        
        d = 2e3; //double-Literal, wissenschaftlich
                 //2e3 = 2 mal 10 hoch
        d = 2E3; //2E3 = 2 mal 10 hoch
        System.out.println("d = " + d);
        
        d = 2e-3;
        System.out.println("d = " + d); //0.002
        
        //******************
        //float Literale
        byte b2 = 12;   //int-Literal ausgewertet und gekastet
        
        //float f = 12.0; //double-Literal: Compilerfehler!
        
        float f = 12.0F;    //float-Literal
        f = 12.0f;          //float-Literal
        
        f = (float)12.0;    //explizites Casting
        
        //f = 1e2;      // double-Literal: Compilerfehler
        
        
        //*********************
        //boolean-Literale
        
        boolean bool1 = true; //false;
        
        
    } //end of main

}
