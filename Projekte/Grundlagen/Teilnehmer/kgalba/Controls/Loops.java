/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controls;

/**
 *
 * @author kgalba
 */
public class Loops {

    static void whileLoop() {
        int x = 0;
        while (x < 3) {
                        x++;
            if (x < 3) {
                System.out.println("X:" + x);
            } else {
                System.out.println("Bye: mit x = " + x);
            }
        }
    }

        static void forLoop() {
            for (int i = 0; i < 10; i++) {
                System.out.println("i:" + i);
            }
    }
    
    
    public static void main(String[] args) {

        whileLoop();
        int i = (2<1) ? 3 : 5;
        System.out.println("i:" + ++i);
        
        //x++ : x = x +1;
        //++x : x = 1+x;
        i=0;
        boolean b = true | false && false;
        if (i==1) {
            System.out.println("val b:" + b);
        } else {
            System.out.println("val b:" + b);

           
        }
        
        forLoop();

    }

}
