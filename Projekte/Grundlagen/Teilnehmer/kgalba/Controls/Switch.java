/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controls;

/**
 *
 * @author kgalba
 */
public class Switch {

    char[] vocals = {'a', 'e', 'i', 'o', 'u'};

    public static void main(String[] args) {

        System.out.println("***********switch1************");

        switch1('a');
        switch1('b');
        switch1('Q');

        System.out.println("***********switch2************");

        switch2("green");
        switch2("yellow");
        switch2("red");
        switch2("Other");
        System.out.println("***********switch3************");

        switch3("green");
        switch3("yellow");
        switch3("red");
        switch3("Other");
    }

    private static void switch1(char ch) {

        switch (Character.toLowerCase(ch)) {
            case 'a':
            case 'e':
            case 'i':
            case 'o':
            case 'u':
            case 'y':
                System.out.println(ch + " ist ein Vokal");
                break;

            default:
                System.out.println(ch + " ist ein Konsonat");

        }
    }

    private static void switch2(String farbe) {

        char firstChar = farbe.charAt(0);
        switch (firstChar) {
            case 'r':
                System.out.println(farbe + " Rot. Bitte warten");
                break;
            case 'y':
                System.out.println(farbe + " Gelb. Gleich get es los");
                break;
            case 'g':
                System.out.println(farbe + " Gruen. Weg frei");
                break;
            default:
                System.out.println(farbe + " Fehler! Diese Farbe gibt es nicht.");

        }
    }

    /**
     * switch mit string in java 7
     * @param farbe 
     */
    private static void switch3(String farbe) {

        switch (farbe) {
            case "red":
                System.out.println(farbe + " Rot. Bitte warten");
                break;
            case "yellow":
                System.out.println(farbe + " Gelb. Gleich get es los");
                break;
            case "green":
                System.out.println(farbe + " Gruen. Weg frei");
                break;
            default:
                System.out.println(farbe + " Fehler! Diese Farbe gibt es nicht.");

        }
        char x = farbe=="red"? 'b': 'g';
        
    }
}
