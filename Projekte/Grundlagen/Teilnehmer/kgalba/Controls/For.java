/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controls;

/**
 *
 * @author kgalba
 */
public class For {

    public static void main(String[] args) {

        for1();

        for2();

        for3();

        for4();

        for5();

        for6();
        
        for7();
    }

    static void for1() {
        System.out.println("");
        System.out.println(" "
                + Thread.currentThread().getStackTrace()[1].getMethodName()
                + " ");
        for (int i = 0; i < 10; i++) {
            System.out.print(" " + i);
        }
        System.out.println("");

    }

    static void for2() {
        System.out.println("");

        System.out.println(" "
                + Thread.currentThread().getStackTrace()[1].getMethodName()
                + " ");

        for (int i = 0; i < 10; i++) {
            if (i == 1) {
                continue;
            }
            System.out.print(" " + i);
        }
        System.out.println("");

    }

    static void for3() {
        System.out.println("");

        System.out.println(" "
                + Thread.currentThread().getStackTrace()[1].getMethodName()
                + " ");

        for (int i = -4; i <= 50; i += 2) {
            System.out.print(" " + i);
        }
        System.out.println("");

    }

    static void for4() {
        System.out.println("");

        System.out.println(" "
                + Thread.currentThread().getStackTrace()[1].getMethodName()
                + " ");

        for (int i = 97; i < 123; i++) {
            System.out.print(" " + (char) i);
        }
        System.out.println("");

    }

    static void for5() {
        System.out.println("");

        System.out.println(" "
                + Thread.currentThread().getStackTrace()[1].getMethodName()
                + " ");

        for (int i = 90; i > 64; i--) {
            System.out.print(" " + (char) i);
        }
        System.out.println("");
    }

    static void for6() {
        System.out.println("");

        System.out.println(" "
                + Thread.currentThread().getStackTrace()[1].getMethodName()
                + " ");

        for (int i = 1; i <= 1000; i++) {

            if ((i % 5) != 0) {
                continue;
            }
            System.out.print(" " + i);
        }
        System.out.println("");
    }

    static void for7() {
        System.out.println("");

        System.out.println(" "
                + Thread.currentThread().getStackTrace()[1].getMethodName()
                + " ");

        for (int i = 3; i > 0; i--) {

            System.out.print(" " + i);
            
            for (int j = 1; j <= i; j++) {
                System.out.print(" " + j);
            }
        }
        System.out.println("");
    }
}
