package klassen;



class Kreis {
    static double getFlaeche(double r) {
        return r*r*Math.PI;
    }

    static double getFlaeche(Kreis k) {
//        return k.radius * k.radius * Math.PI;
        return getFlaeche(k.radius);
    }
    
    //-----------------------
    int radius;
    
    double getFlaeche() {
//        Kreis k = this;
//        return k.radius * k.radius * Math.PI;
        return radius * radius * Math.PI;
    }
    
}
    
//------------------------------------
public class MethodenUndAttribute {

    public static void main(String[] args) {
        
        Kreis k1 = new Kreis();
        double f1 = Kreis.getFlaeche(k1.radius);

        Kreis k2 = new Kreis();
        k2.radius = 3;
        double f2 = Kreis.getFlaeche(k2);
        
        k2.getFlaeche();
        
        
    } 
    
}
