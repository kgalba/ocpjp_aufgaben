package klassen;


class Auto {
    int baujahr; // = 0;
    
    //default: Auto() { super(); this.baujahr = 0; }
    Auto() {
        //super();
        //this.baujahr = 0;
    }
    
    Auto(int baujahr) {
        //super();
        this.baujahr = baujahr;
    }
    
    void fahren() {
        System.out.println("ein Auto fährt. Baujahr: " + baujahr);
    }
}


public class Constuctors {
    
    public static void main(String[] args) {
        
        Auto a1 = new Auto();
        
        a1.baujahr = 2000; //Eigenschaften mit Feldern (Instanz-Variablen, Attributen)
        a1.fahren(); //Verhalten mit einer Instanz-Methode
        
        
        Auto a2 = new Auto(2000);
        a2.fahren();
        
        
    }
    
}
