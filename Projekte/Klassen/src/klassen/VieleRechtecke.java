package klassen;

import java.util.Random;


class Rechteck {
    
    static void print(Rechteck r) {
        System.out.println("Rechteck. " + r.breite + " X " + r.hoehe);
    }
    
    //---------
    int breite;
    int hoehe;
    
    Rechteck() {
        System.out.println("Konstruktor");
    }
    
    void print() {
        //System.out.println("Rechteck. " + this.breite + " X " + this.hoehe);
        System.out.println("Rechteck. " + breite + " X " + hoehe);
    }
    
    int getFlaeche() {
        return breite * this.hoehe;
    }
}

//------------------------------------------
public class VieleRechtecke {
    
    public static void main(String[] args) {
        
        Rechteck var = new Rechteck();
        
        //System.out.println("beite: " + var.breite);
        
        var.print();
        
        var.breite = 3;
        var.hoehe = 4;
        
        Rechteck.print(var);
        
        System.out.println("--------------------------");

        Random random = new Random();
        
        for (int i = 0; i < 100; i++) {
            Rechteck r = new Rechteck();
            
            //Math.random();
            int b = random.nextInt(100) + 1; //[1 ... 100]
            int h = random.nextInt(100) + 1; //[1 ... 100]
            
            r.breite = b;
            r.hoehe = h;
            
            //System.out.println("Rechteck. " + r.breite + " X " + r.hoehe);
            r.print();
            System.out.println("-- Fläche: " + r.getFlaeche());
            
        } //end of for
        
    }

}
