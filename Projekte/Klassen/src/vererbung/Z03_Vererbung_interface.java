package vererbung;

public class Z03_Vererbung_interface {

    static abstract interface Figur {
        abstract double getFlaeche();
    }
    
    static class Kreis implements Figur {
        int radius;

        @Override
        public double getFlaeche() {
            return Math.PI * radius * radius;
        }
    }
    
    static class Rechteck implements Figur {
        int breite, hoehe;
        
    }
    
    public static void main(String[] args) {
        //Aufgabe:
        //Kreis und Rechteck haben nur Unterschiede in der Realisierung.
        //- Nicht alles nötige kann man in der Basisklasse sinnvoll realisieren
        //- Objekte vom Typ 'Basisklasse' sind nicht sinnvoll
        
        

        Figur[] arr = {
            new Kreis(),
            new Rechteck(),
        };
        
        //Universell:
        for(Figur f : arr) {
            f.getFlaeche();  //Polymorphismus
        }        
    }
    
}
