package vererbung;

class Base {
    private String name;
    
    //Base() { super(); } //default deaktiviert
    Base(String name) {
        setName(name); //Polymorphie
    }
    
    public void setName(String name) {
        if(name==null) {
            name = "";
        }
        
        this.name = name;
    }

    @Override
    public String toString() {
        return name.toUpperCase();
    }
}

//--------------------------
class Derived extends Base {

    //Derived() { super(); } //default deaktiviert
    
    public Derived() {
        super("Tom");
    }

    @Override
    public void setName(String name) {
        System.out.println("neue setName");
    }
}

public class Z06_Constructors {

    public static void main(String[] args) {
        
        Base d1 = new Derived();
        System.out.println(d1);
        
    }
}
