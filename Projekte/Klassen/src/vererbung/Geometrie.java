package vererbung;

class Figur {
    int x, y;
    
    void move(int deltaX, int deltaY) {
        x += deltaX;
        y += deltaY;
    }
    
    int getFlaeche() { return -1; }
}

class Kreis extends Figur { // Kreis AS-A Figur
    int radius;
    
    //Überschreiben
    double getFlaeche() {
        return Math.PI * radius * radius;
    }
}

class Rechteck extends Figur { //Richteck IS-A Figur
    int breite, hoehe;
    
    int getFlaeche() {
        return breite * hoehe;
    }
}

public class Geometrie {

    public static void main(String[] args) {
        //1. Kreis hat Radius
        //2. Rechteck hat Breite und Höhe
        //3. Alle Figuren haben x,y Koordinaten
        
        Kreis k = new Kreis();
        System.out.println("k.x = " + k.x);
        
        k.move(3, 4);
        
        k.radius = 22;
     
        Figur f = k;
        
        double flaeche1 = ((Kreis)f).getFlaeche();
        System.out.println("flaeche1 = " + flaeche1);
        
    }
    
}
