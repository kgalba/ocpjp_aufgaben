package vererbung;

public class Z02_Vererbung_abstrakt {
    
    static abstract class Figur {
        int x, y;
        
        void method() {  }
        abstract double getFlaeche();
    }
    
    static class Kreis extends Figur {
        int radius;

        @Override
        double getFlaeche() {
            return Math.PI * radius * radius;
        }
    }
    
    static class Rechteck extends Figur {
        int breite, hoehe;
        
        //...
    }

    public static void main(String[] args) {
        //Aufgabe:
        //Kreis und Rechteck zu haben, die gemeinsame Implementierungen haben,
        //aber auch Unterschiede.
        //- Nicht alles nötige kann man in der Basisklasse sinnvoll realisieren
        //- Objekte vom Typ 'Basisklasse' sind nicht sinnvoll
        
        //new Figur(); //Compilerfehler
        
        
        Figur[] arr = {
            new Kreis(),
            new Rechteck(),
        };
        
        //Universell:
        for(Figur f : arr) {
            f.getFlaeche();  //Polymorphismus
        }
    }
    
}
