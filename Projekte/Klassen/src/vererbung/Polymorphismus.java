package vererbung;

import java.util.Random;

class Tier {
    void laufen() {
        System.out.println("ein Tier läuft...");
    }
}
class Katze extends Tier {
    void laufen() { //Überschreiben
        System.out.println("eine Katzen läuft...");
    }
}
class Hund extends Tier {
}

public class Polymorphismus {

    public static void main(String[] args) {
        
        Tier tier;
        
        if( new Random().nextBoolean() ) {
            tier = new Hund();
        } else {
            tier = new Katze();
        }
        
        //Universell:        
        tier.laufen(); //Polymorphie (late binding)
        
        tier = new Katze();
        tier.laufen(); //Polymorphie (late binding)
        
        //Voraussetzungen für Polymorphismus:
        //1. Vererbungshierarchie
        //2. Eine überschriebene Methode
        //3. Aufruf der Methode mit einer Basistypvariable
    }
    
}
