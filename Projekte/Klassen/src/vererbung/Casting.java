package vererbung;

class Person {
    String name;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

class Dozent extends Person {
    Dozent() { 
        super("Jerry"); 
    }
}

public class Casting {
    
    public static void main(String[] args) {
        
        Object obj = "Hallo Vererbung"; // Object <= IS-A <= String
        
        String s = obj.toString();
        
        //obj.getName(); //Kontrolle mit dem Typ der Referenz
        
        obj = new Person("Tom"); // Object <= IS-A <= String
        
        //String name = obj.getName(); //Kontrolle mit dem Typ der Referenz

        if( obj instanceof Person ) {
            //Person p = obj; // Person <= IS-A <= Object - FALSCH!
            Person p = (Person) obj;
            String name = p.getName();
            System.out.println("name = " + name);
        }
        
        //---------------------------------------
        obj = "Bin keine Person und habe keinen Namen";
        Person p = (Person)obj; //ClassCastException
        p.getName(); //Kontrolle mit dem Typ der Referenz
        
        //Casting zur Laufzeit (vor der Zuweisung in der Zeile 38):
//        if( !(obj instanceof Person ) ) {
//            throw new ClassCastException();
//        }
        
        //--------------------------------------------
        Object obj2 = (Object) p;
//        if( !(p instanceof Object ) ) {
//            throw new ClassCastException();
//        }
        
        Person p3 = new Dozent(); //Dozent IS-A Person
        p3 = (Person)p3;
        
        Dozent d1 = new Dozent();
        p3 = (Person)d1;
//        if( !(d1 instanceof Person ) ) {
//            throw new ClassCastException();
//        }
        
        String str = null;
        Dozent dozent = new Dozent();
        
        //str = (String)dozent; //Compilerfehler! Kein Casting zw. Geschwistertypen
        
        str = (String)(Object)dozent;
        
    } 

}
