package vererbung;

public class Z01_Vererbung {

    static class Figur {
        int x, y;
    }
    
    static class Kreis extends Figur {
        int radius;
    }
    
    static class Rechteck extends Figur {
        int breite, hoehe;
    }
    
    public static void main(String[] args) {
        
        //Aufgabe:
        //Kreis und Rechteck zu haben, die gemeinsame Implementierungen haben,
        //aber auch Unterschiede
        
        Figur f = new Kreis();
        System.out.println( f.x );
        
        f = new Rechteck();
        System.out.println( f.x );
    }
    
}
