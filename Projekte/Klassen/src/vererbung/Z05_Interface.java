package vererbung;

/* abstract */ interface BaseInterface {
    
    /* public static final */ int constValue = 22; //implizit: public static final
    
    /* public abstract */ void method(); //implizit: public abstract
    
    /* public */ static void staticMethod() {  //implizit: public
        System.out.println("static definition"); 
    }
    
    //Ab Java 8 :)
    default void defaultMethod() { 
        System.out.println("default definition"); 
        method(); 
    }
}

class SubClass implements BaseInterface {
    public void method() {}
}

public class Z05_Interface {

    public static void main(String[] args) {
        
        System.out.println(BaseInterface.constValue);
        
    }
    
}
