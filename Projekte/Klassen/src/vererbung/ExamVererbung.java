package vererbung;

class Fahrzeug {
    int baujahr = 2000;
}

class PKW extends Fahrzeug {
    int baujahr = 1990;

    @Override
    public String toString() {
        return "this.baujahr: " + baujahr + ", super.baujahr: " + super.baujahr;
    }
}

public class ExamVererbung {
    
    public static void main(String[] args) {
        
        PKW pkw1 = new PKW();
        System.out.println(pkw1);
        
        System.out.println("pkw1.baujahr: " + pkw1.baujahr);
        
        Fahrzeug f1 = pkw1;
        System.out.println("f1.baujahr: " + f1.baujahr); //early binding
        
    }

}
