package vererbung;

class Dienst {
    void ausfuehren() {    
    }
}

class Defragmentierung extends Dienst {
    
    //Regeln:
    //1. Sichtbarkeit nicht verschärfen
    //2. Rückgabetyp nicht ändern (s. Kovarianz)
    //3. Keine zusätzlichen checked Exceptions in der throws-Klausel
    public void ausfuehren() {
        
    }
}

public class Z04_Ueberschreiben {

    public static void main(String[] args) {
        
        
        
    }
    
}
