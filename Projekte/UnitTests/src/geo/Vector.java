package geo;

public class Vector {
    
    private int x, y;

    public Vector(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    public double getLength() {
        return Math.sqrt(x*x + y*y);
    }
    
}
