package lotto;

import java.util.Random;

public class Zahlen {
    
    /**
     * Erzeugt ein int-Array mit zufälligen einzigartigen Werten aus dem 
     * Bereich [min, max]
     * @param len
     * @param min
     * @param max (inklusiv)
     * @return 
     */
    static int[] createRandomUniqueArray(int len, int min, int max) {
        //Fehlerbehandlung?
        
        int[] arr = new int[len];
        
        Random r = new Random();
        
        outer:
        for (int i = 0; i < len; i++) {
            
            int value = r.nextInt(max-min+1) + min;
            
            for(int j=0; j<i; j++ ) {
                if( arr[j] == value ) {
                    i--;
                    continue outer;
                }
            }
                    
            arr[i] = value;
        }
        
        return arr;
    }

}
