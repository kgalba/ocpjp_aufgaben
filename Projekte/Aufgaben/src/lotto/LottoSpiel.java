package lotto;

import java.util.Arrays;

public class LottoSpiel {

    int anzahlKugel, anzahlKugelGesamt;
    int[] zahlen;

    public LottoSpiel(int anzahlKugel, int anzahlKugelGesamt) {
        this.anzahlKugel = anzahlKugel;
        this.anzahlKugelGesamt = anzahlKugelGesamt;
    }

    void ziehen() {
        zahlen = Zahlen.createRandomUniqueArray(anzahlKugel, 1, anzahlKugelGesamt);
        Arrays.sort(zahlen);
    }
    
    public String toString() {
        return "Spiel " + anzahlKugel + " aus " + anzahlKugelGesamt 
                + ". " + Arrays.toString(zahlen);
    }
    
//0 richtige: 0 Euro      10 hoch -1 -> 0.1
//1 richtige: 1 Euro      10 hoch 0
//2 richtige: 10 Euro     10 hoch 1
//3 richtige: 100 Euro    10 hoch 2
//4 richtige: 1000 Euro   10 hoch 3
    int vergleichen(LottoTipp tipp) {
        
        int anzahlRichtige = 0;
        
        for (int i = 0; i < zahlen.length; i++) {
            int v1 = zahlen[i];
            
            for (int j = 0; j < tipp.zahlen.length; j++) {
                int v2 = tipp.zahlen[j];
                if(v1 == v2) {
                    anzahlRichtige++;
                    break;
                }
            }
        }
        
        return (int)Math.pow(10, anzahlRichtige-1);
    }
    
}
