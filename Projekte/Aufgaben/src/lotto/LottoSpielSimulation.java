package lotto;

public class LottoSpielSimulation {

    public static void main(String[] args) {

        int anzahlKugel = 7;
        int anzahlKugelGesamt = 49;

        LottoSpiel lotto = new LottoSpiel(anzahlKugel, anzahlKugelGesamt);

        lotto.ziehen();

        System.out.println(lotto); // Spiel 7 aus 49. [3, 7, 11, 28, 35, 40, 48]

        LottoTipp tipp = new LottoTipp(anzahlKugel, anzahlKugelGesamt);

        tipp.abgeben();

        System.out.println(tipp); 
        //Object obj = tipp;
        //Augeben: tipp.toString();

        int gewinn = lotto.vergleichen(tipp);
        
        System.out.println("gewinn: " + gewinn);
    } //end of main

}
