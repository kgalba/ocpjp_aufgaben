package lotto;

import java.util.Arrays;

public class LottoTipp {

    int anzahlKugel, anzahlKugelGesamt;
    int[] zahlen;
    
    //Alt + Ins

    public LottoTipp(int anzahlKugel, int anzahlKugelGesamt) {
        this.anzahlKugel = anzahlKugel;
        this.anzahlKugelGesamt = anzahlKugelGesamt;
    }

    void abgeben() {
        zahlen = Zahlen.createRandomUniqueArray(anzahlKugel, 1, anzahlKugelGesamt);
        Arrays.sort(zahlen);
    }

    public String toString() {
        return "Tipp " + anzahlKugel + " aus " + anzahlKugelGesamt + ". " +
                Arrays.toString(zahlen);
    }
    
    
}
