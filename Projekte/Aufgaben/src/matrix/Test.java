/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package matrix;

/**
 *
 * @author apatrin
 */
public class Test {

    public static void main(String[] args) {
        //-----------------------------------------------------
        //Erstellt eine IntMatrix mit 2 Zeilen und 3 Spalten:

        IntMatrix m1 = new IntMatrix(2, 3);

        System.out.println(m1);  // m1.toString()

    //-----------------------------------------------------
        //Erstellt eine IntMatrix mit 5 Zeilen und 3 Spalten. 
        //Alle Elemente der IntMatrix sind mit dem Wert 100 initialisiert
        IntMatrix m2 = new IntMatrix(5, 3, 100);

        System.out.println(m2);

    //-----------------------------------------------------
        //Liefert den Wert an der Stelle (1, 2) der IntMatrix (Zeile 1, Spalte 2)
        int i = m1.get(1, 2);

    //-----------------------------------------------------
        //Erstellt eine IntMatrix mit 4 Zeilen und 6 Spalten. Alle Elemente werden zufällig 
        //initialisiert. Der erlaubte Wertebereich für die Elemente: 0 bis 200.
        IntMatrix m3 = IntMatrix.getRandomMatrix(4, 6, 200);

    //-----------------------------------------------------
        //Liefert true, falls die Matrizen die gleichen Werte an den entsprechenden 
        //Stellen gespeichert haben. Die Matrizen mit ungleichen Dimensionen sind ungleich.
        IntMatrix m4 = new IntMatrix(2, 3);

        System.out.println( "m1 == m4 ?" + (m1 == m4) );

        m1.equals(m4);
        
        
    //-----------------------------------------------------
        //Optional. Überlegen Sie sich weitere Operationen, die mit Matrizen möglich sind.
        //Z.B.: Matrix transponieren, Matrizen addieren, Matrizen multiplizieren u.s.w.
    //-----------------------------------------------------
        //Optional. Versuchen Sie eine Matrix zu erstellen, die zu dem Heap Overflow führt. 
        //http://stackoverflow.com/questions/37335/how-to-deal-with-java-lang-outofmemoryerror-java-heap-space-error-64mb-heap
    //-----------------------------------------------------
        //Erzeugen Sie ein Array aus Matrizen mit unterschiedlichen Dimensionen
    }

}
