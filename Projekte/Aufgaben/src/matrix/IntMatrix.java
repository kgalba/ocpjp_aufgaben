package matrix;

import java.util.Arrays;
import java.util.Random;

public class IntMatrix {

    /**
     * liefert eine IntMatrix mit den Zufallswerten aus dem Bereich [0...max]
     * @param rows
     * @param cols
     * @param max Obergenze für die Zufallswerte (inklusive)
     * @return 
     */
    static IntMatrix getRandomMatrix(int rows, int cols, int max) {
        IntMatrix m = new IntMatrix(rows, cols);
        
        Random r = new Random();

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                int wert = r.nextInt(max+1);
                m.data[i][j] = wert;
            }
        }
        
        return m;
    }
    
    //-------------------------------------------
    
    //public IntMatrix() { super(); }
    int[][] data;
    
    IntMatrix(int rows, int cols) {
        //super();
        if(rows <= 0 || cols <= 0) {
            //??? Wie die Ausnahmesituation behandeln?
        }
        
        data = new int[rows][cols];
    }

    IntMatrix(int rows, int cols, int initValue) {
        this(rows, cols); //kein super(); mehr
        
        for (int i = 0; i < rows; i++) {
            //Arrays.fill(data[i], initValue);
            
            for (int j = 0; j < cols; j++) {
                data[i][j] = initValue;
            }
        }
    }

    int get(int row, int col) {
        return data[row][col];
    }
    
    public String toString() {
        
        String s = "";
        
        for (int i = 0; i < data.length; i++) {
            for (int j = 0; j < data[i].length; j++) {
                
                int wert = data[i][j];
                
                s = s + wert + " ";
            }
            
            s += '\n';
        }
        
        return s;
    }
    
    int getRows() {
        return data.length;
    }
    
    int getCols() {
        return data[0].length;
    }
    
    boolean equals(IntMatrix m2) {
        if(m2 == null) {
            return false;
        }
        
        if( getRows() != m2.getRows() || getCols() != m2.getCols() ) {
            return false;
        }
        
        return Arrays.deepEquals(data, m2.data);
        
//        for (int i = 0; i < getRows(); i++) {
//            for (int j = 0; j < getCols(); j++) {
//                if( get(i, j) != m2.get(i, j) ) {
//                    return false;
//                }
//            }
//        }
//        
//        return true;
    }
}
