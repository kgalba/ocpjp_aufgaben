package klasse_StringBuilder_StringBuffer;

public class UseStringBuilder_StringBuffer_Exam {
    
    public static void main(String[] args) {
        
        String s = "a";
        s+="b"; //1. neues Objekt
        s+='c'; //1. neues Objekt
        
        StringBuilder sb = new StringBuilder("a");
        sb.append("b");
        sb.append('c');
        
        StringBuffer sbuff = new StringBuffer("a");
        sbuff.append("b");
        sbuff.append('c');
        
        //Exam:
        sb.append("d").append('e').append(22).insert(0, "ABC").replace(2, 5, "xy");
        
        //abc
        //abcd
        //abcde
        //abcde22
        //ABCabcde22
            //0123456789
            //ABCabcde22
        //ABxycde22
        System.out.println(sb); //ABxycde22
        
        //Exam:
        //substring liefert einen String zurück! Also Compilerfehler:
        //sb.append("d").append("efghih").substring(1).append("!");
        
    }
    
}
