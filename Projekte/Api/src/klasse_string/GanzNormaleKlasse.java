package klasse_string;

public class GanzNormaleKlasse {
    
    public static void main(String[] args) {
        
        String s1 = new String("Java");
        
        char ch = s1.charAt(0);
        
        //Strings sind immutable
        s1.toUpperCase();
        System.out.println(s1); //Java
        
        s1 = s1.toUpperCase();
        System.out.println(s1); //JAVA
    }
    
}
