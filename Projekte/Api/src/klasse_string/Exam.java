package klasse_string;

public class Exam {
    
    static String method() {
        String s = "Hallo";
        
        s += " Welt"; //1. String (Konkatenation)
        
        s = s.toUpperCase(); //1. String (toUpperCase)
        
        return s.toString(); //return this;
    }
    
    
    public static void main(String[] args) {
        //Wie viele String-Objekte werden 
        //beim Aufruf der Methode method erstellt:
        method(); //2 Objekte
        
        method(); //2 Objekte
    }
    
}
