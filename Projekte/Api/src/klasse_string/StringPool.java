package klasse_string;

class Auto {}

public class StringPool {

   
    public static void main(String[] args) {
        
        String s1 = "Mo";
        String s2 = "Mo";
        
        System.out.println(s1 == s2);
        
        String s3 = new String("Mo");
        String s4 = new String("Mo");
        
        System.out.println(s3 == s4);
    }
    
}
