/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package klasse_string;

/**
 *
 * @author apatrin
 */
public class BesondereKlasse {
    
    public static void main(String[] args) {
        
        //1. Literale
        String s1 = "Java";
        
        //2. Konkatenationsoperator
        s1 = s1 + " ist toll!";
        
        //3. (seit Java 7) switch mit Strings
        switch(s1) {
            case "Java ist toll!":
                System.out.println("Treffer!");
                break;
        }
        
        
        
    } //end of main
    
}
