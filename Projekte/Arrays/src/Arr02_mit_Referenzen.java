class Person {
    String name; // = null;
}
        
public class Arr02_mit_Referenzen {
 
    public static void main(String[] args) {
        
        int[] a1 = new int[22];
        
        String[] a2 = new String[22];
        
        for (int i = 0; i < a2.length; i++) {
            System.out.println(a2[i]);
        }
        
    }
    
}
