public class Arr01 {
    
    public static void main(String[] args) {
   
        //1. ----------------------------------
        int[] a = new int[2];
        
        for( int i = 0; i<a.length; i++ ) {
            System.out.print(a[i] + " ");
        }
        System.out.println();
        
        //2. ----------------------------------
        int[] a2 = { 1, 2, 3, 1, 2, 3 }; //Nur beim direkten Initialisieren einer neuen Variable
        System.out.println(a2);
        System.out.println( java.util.Arrays.toString(a2) );
        
        int[] a2a;
        //a2a = { 2, 2, 2 }; //Compilerfehler
        
        //3. ----------------------------------
        int[] a3 = new int[] { 9, 8, 7 }; //bitte keine Dimensionen
        System.out.println( java.util.Arrays.toString(a3) );

        //test( { true, false } ); //Compilerfehler
        test( new boolean[] {true, false} );
        
    } //end of main
    
    static void test(boolean[] a) {}
    
    static double[] getReferenceArray() {
        //return { 5., 6., 7 };     //Compilerfehler
        return new double[] { 5., 6., 7 };
    }
    
}
