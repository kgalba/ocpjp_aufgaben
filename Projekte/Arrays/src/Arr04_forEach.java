
public class Arr04_forEach {

    public static void main(String[] args) {

        String[] arr = new String[50];
        
        String[][] arr2D = {
            { "a", "b", "c" },
            { "Mo", "Di", "Mi" },
            args,
            args,
            arr
        };
        
        for (int i = 0; i < arr2D.length; i++) {
            String[] subArr = arr2D[i];
            
            for(int j=0; j<subArr.length; j++) {
                String element = subArr[j];
                System.out.print(element + " ");
            }
            System.out.println();
        }
        
        System.out.println("---------------------------------------");
        
        for(String var : arr) {
            System.out.println(var);
        }

        System.out.println("---------------------------------------");
        for(String[] subArr : arr2D) {
            for(String element : subArr ) {
                System.out.print(element + " ");
                //element = "Hallo";
            }
            System.out.println();
        }
        
    } //

}
